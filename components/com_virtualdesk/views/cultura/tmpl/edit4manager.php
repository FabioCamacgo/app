<?php

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteCulturaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura.php');
JLoader::register('VirtualDeskSiteCulturaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura_files.php');

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailEditAccess('cultura');
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('cultura', 'edit4managers');
$vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

$app    = JFactory::getApplication();
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

$localScripts .= $addscript_ini . 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.all.min.js' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/summernote/summernote.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');

//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css');
//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.min.css');
//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/js/summernote/summernote.min.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/cultura/tmpl/cultura-comum.css');

$labelseparator    = ' : ';
$params            = JComponentHelper::getParams('com_virtualdesk');
$getInputCultura_Id = JFactory::getApplication()->input->getInt('cultura_id');


$this->data = array();
$this->data = VirtualDeskSiteCulturaHelper::getCulturaView4ManagerDetail($getInputCultura_Id);
if( empty($this->data) ) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_NORESULTS'), 'error' );
    return false;
}

$vdcleanstate = $app->input->getInt('vdcleanstate');
if($vdcleanstate==1) {
    VirtualDeskSiteCulturaHelper::cleanAllTmpUserState();
}
else {
    if (!is_array($this->data)) {
        $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.edit4manager.cultura.data', array());
        foreach ($temp as $k => $v) {
            $this->data->{$k} = $v;
        }
    }
}

if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';

$UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
$obParam    = new VirtualDeskSiteParamsHelper();

$concelhoCultura      = $obParam->getParamsByTag('concelhoCultura');
$espera              = $obParam->getParamsByTag('espera');
$copyrightAPP = $obParam->getParamsByTag('copyrightAPP');

$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>


    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_LISTA' ); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_EDIT' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=cultura'); ?>"
                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>">
                    <i class="fa fa-ban"></i>
                    <?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>

        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>

            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <form id="edit-cultura"
                  action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=cultura.update4manager'); ?>" method="post"
                  class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
                  role="form">



                <div class="form-body">

                    <legend><h3 class=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_INFO_PROMOTOR'); ?></h3></legend>

                    <div class="form-group BlocoNIFPromotor" >
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FISCALID_LABEL'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="nipc" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_FISCALID_LABEL'); ?>"
                                   name="nif_evento" id="nif_evento" maxlength="9" value="<?php echo $this->data->nif_evento; ?>"
                                   data-vd_url_get="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=cultura.checkPromotorNif4ManagerByAjax'); ?>"
                            />
                        </div>
                    </div>

                    <div class=" BlocoPromotorStatic" style="display: none;">
                        <div class="form-group ">
                            <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_NOME_LABEL' ); ?></label>
                            <div class="col-md-9"> <div class="value NomePromotorValStatic"></div> </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_EMAIL' ); ?></label>
                            <div class="col-md-9"> <div class="value EmailPromotorValStatic"></div> </div>
                        </div>
                    </div>

                    <div class=" BlocoPromotorInput" style="display: none;">
                        <div class="form-group ">
                            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOME_LABEL'); ?><span class="required">*</span></label>
                            <div class="col-md-9">
                                <input type="text" required class="form-control" name="promotor_nome" id="promotor_nome" maxlength="200" value="<?php echo $this->data->promotor_nome; ?>" />
                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_EMAIL'); ?><span class="required">*</span></label>
                            <div class="col-md-9">
                                <input type="email" required class="form-control" name="promotor_email" id="promotor_email" maxlength="100" value="<?php echo $this->data->promotor_email; ?>" />
                            </div>
                        </div>
                    </div>

                    <!-- Coorganizador -->
                    <div class="form-group" id="coOrganizador">
                        <label class="col-md-3 control-label"><?php echo JText::sprintf('COM_VIRTUALDESK_CULTURA_COORGANIZACAO', $copyrightAPP); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="radio" name="radioval" id="sim" value="sim" <?php if ($this->data->coOrganizacao == 1) {echo 'checked="checked"'; } ?>> <?php echo 'Sim'; ?>
                            <input type="radio" name="radioval" id="nao" value="nao" <?php if ($this->data->coOrganizacao == 2) { echo 'checked="checked"'; } ?>> <?php echo 'Não'; ?>
                        </div>

                        <input type="hidden" required id="coOrganizacao" name="coOrganizacao" value="<?php echo $this->data->coOrganizacao; ?>">
                    </div>


                    <?php
                        if($this->data->coOrganizacao == 1){
                            ?>
                            <script>
                                document.getElementById('sim').onclick = function() {
                                    document.getElementById("coOrganizacao").value = 1;
                                };

                                document.getElementById('nao').onclick = function() {
                                    document.getElementById("coOrganizacao").value = 2;
                                };

                            </script>
                        <?php
                        } else{
                        ?>
                            <script>
                                document.getElementById('sim').onclick = function() {
                                    document.getElementById("coOrganizacao").value = 1;
                                };

                                document.getElementById('nao').onclick = function() {
                                    document.getElementById("coOrganizacao").value = 2;
                                };

                            </script>

                            <?php
                        }
                    ?>



                    <legend><h3 class=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_INFO_EVENTO'); ?></h3></legend>

                    <div class="form-group" id="eventName">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOMEEVENTO_LABEL'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" required class="form-control" name="nome_evento" id="nome_evento" maxlength="250" value="<?php echo $this->data->nome_evento; ?>"/>
                        </div>
                    </div>

                    <div class="form-group" id="desc">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DESCRICAO_LABEL'); ?></label>
                        <div class="col-md-9">
                            <textarea  class="form-control wysihtml5" rows="12" name="desc_evento" id="desc_evento" maxlength="4000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->desc_evento); ?></textarea>
                        </div>
                    </div>

                    <div class="form-group" id="cat">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_CATEGORIA'); ?><span class="required">*</span></label>
                        <?php $categorias = VirtualDeskSiteCulturaHelper::getCategoria()?>
                        <div class="col-md-9">
                            <select name="cat_evento" required value="" id="cat_evento" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                <?php foreach($categorias as $rowStatus) : ?>
                                    <option value="<?php echo $rowStatus['id']; ?>"
                                        <?php if((int)$this->data->id_categoria == (int)$rowStatus['id']) echo('selected'); ?>
                                    ><?php echo $rowStatus['categoria']; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="freg">
                        <label class="col-md-3 control-label" for="field_freguesia"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FREGUESIAS_LABEL') ?><span class="required">*</span></label>
                        <?php $CulturaFreguesias = VirtualDeskSiteCulturaHelper::getCulturaFreguesia($concelhoCultura);  ?>
                        <div class="col-md-9">
                            <select name="freguesia_evento" value="" id="freguesia_evento" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                <?php foreach($CulturaFreguesias as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                    <?php if((int)$this->data->id_freguesia == (int)$rowWSL['id']) echo('selected'); ?>
                                ><?php echo $rowWSL['name']; ?>
                                    <?php endforeach;?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="local">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LOCALEVENTO_LABEL'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" required class="form-control" name="local_evento" id="local_evento" maxlength="250" value="<?php echo $this->data->local_evento; ?>"/>
                        </div>
                    </div>

                    <div class="form-group" id="beginDate">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DATAINICIO_LABEL'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="date" required class="form-control" name="data_inicio" id="data_inicio" value="<?php echo $this->data->data_inicio; ?>"/>
                        </div>
                    </div>

                    <div class="form-group" id="beginHour">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_HORAINICIO_LABEL'); ?></label>
                        <div class="col-md-9">
                            <input type="time" class="form-control" name="hora_inicio" id="hora_inicio"  value="<?php echo $this->data->hora_inicio; ?>"/>
                        </div>
                    </div>

                    <div class="form-group" id="endDate">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DATAFIM_LABEL'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="date" required class="form-control" name="data_fim" id="data_fim" value="<?php echo $this->data->data_fim; ?>"/>
                        </div>
                    </div>

                    <div class="form-group" id="endHour">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_HORAFIM_LABEL'); ?></label>
                        <div class="col-md-9">
                            <input type="time" class="form-control" name="hora_fim" id="hora_fim"  value="<?php echo $this->data->hora_fim; ?>"/>
                        </div>
                    </div>

                    <div class="form-group" id="web">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_WEBSITEEVENTO_LABEL'); ?></label>
                        <div class="col-md-9">
                            <input type="text"  class="form-control" name="website" id="website" maxlength="250" value="<?php echo $this->data->website; ?>"/>
                        </div>
                    </div>

                    <div class="form-group" id="face">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FACEBOOKEVENTO_LABEL'); ?></label>
                        <div class="col-md-9">
                            <input type="text"  class="form-control" name="facebook" id="facebook" maxlength="250" value="<?php echo $this->data->facebook; ?>"/>
                        </div>
                    </div>

                    <div class="form-group" id="insta">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_INSTAGRAMEVENTO_LABEL'); ?></label>
                        <div class="col-md-9">
                            <input type="text"  class="form-control" name="instagram" id="instagram" maxlength="250" value="<?php echo $this->data->instagram; ?>"/>
                        </div>
                    </div>

                    <div class="form-group" id="mapa">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_MAPA'); ?></label>
                        <div class="col-md-9">
                            <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>
                            <input type="hidden" class="form-control" name="coordenadas" id="coordenadas"
                                   value="<?php echo htmlentities($this->data->latitude . ',' . $this->data->longitude , ENT_QUOTES, 'UTF-8'); ?>"/>
                        </div>
                    </div>

                    <div class="form-group" id="uploadField">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_IMAGENS'); ?></label>
                        <div class="col-md-9">
                            <div class="file-loading">
                                <input type="file" name="fileupload[]" id="fileupload" multiple>
                            </div>
                            <div id="errorBlock" class="help-block"></div>
                        </div>
                        <input type="hidden" name="vdFileUpChanged" value="0">
                    </div>

                    <div class="form-group" id="layout">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LAYOUT'); ?><span class="required">*</span></label>
                        <?php $tipolayouts = VirtualDeskSiteCulturaHelper::getLayout()?>
                        <div class="col-md-9">
                            <select name="layout_evento" required value="" id="layout_evento" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                <?php foreach($tipolayouts as $rowTL) : ?>
                                <option value="<?php echo $rowTL['id']; ?>"
                                    <?php if((int)$this->data->id_layout_evento == (int)$rowTL['id']) echo('selected'); ?>
                                ><?php echo $rowTL['layout']; ?>
                                    <?php endforeach;?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group" id="obs">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION4_LABEL'); ?></label>
                        <div class="col-md-9">
                            <textarea  class="form-control wysihtml5" rows="6" maxlength="500" name="observacoes" id="observacoes" ><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->observacoes);  ?></textarea>
                        </div>
                    </div>

                </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span></button>
                                <a class="btn default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=cultura'); ?>"
                                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>
                                </a>
                            </div>
                        </div>
                    </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('cultura_id',$setencrypt_forminputhidden); ?>"        value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->cultura_id) ,$setencrypt_forminputhidden); ?>"/>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('cultura.update4manager',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>

                <?php echo JHtml::_('form.token'); ?>

            </form>

        </div>


    </div>

<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/cultura/tmpl/edit4manager.js.php');
    echo ('</script>');
?>