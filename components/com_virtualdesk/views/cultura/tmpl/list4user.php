<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteCulturaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura.php');
JLoader::register('VirtualDeskSiteCulturaStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura_stats.php');

JLoader::register('VirtualDeskSiteStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_stats.php');
JLoader::register('VirtualDeskSiteContactUsReportsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactus_reports.php');


/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('cultura', 'list4users');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}




// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/counterup/jquery.waypoints.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/counterup/jquery.counterup.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/amcharts.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/serial.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/pie.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/light.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/dark.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');

// Cultura - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/cultura/tmpl/cultura-comum.css');

$userSessionID      = VirtualDeskSiteUserHelper::getUserSessionId();

/*  ESTATISTICAS */
$objCulturaStats = new VirtualDeskSiteCulturaStatsHelper();
$objCulturaStats->setAllStats4User();

$culturaNumPedidosOnline       = $objCulturaStats->TotalPedidosOnline4User;
$culturaNumPedidosBalcao       = $objCulturaStats->TotalPedidosBalcao4User;
$culturaNumResolvidos          = $objCulturaStats->TotalResolvidos4User;
$culturaNumNaoResolvidos       = $objCulturaStats->TotalNaoResolvidos4User;
$dataCulturaTotaisPorCategoria = $objCulturaStats->TotaisPorCategoria4User;
$dataCulturaTotPorAnoMes       = $objCulturaStats->TotaisPorAnoMes4User;
$dataCulturaTotaisPorEstado    = $objCulturaStats->TotalPedidosPorEstado4User;

?>

<div class="portlet light bordered cultura">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-calendar font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>

        <div class="actions">

            <div class="btn-group">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=addnew4user&vdcleanstate=1'); ?>" >
                    <button class="btn btn-circle btn-outline blue btn-sm" type="button"><i class="fa fa-plus"></i> <?php echo JText::_('COM_VIRTUALDESK_CULTURA_ADDNEW'); ?></button>
                </a>
            </div>

            <!-- Botões DataTables -->
            <div class="btn-group">
                <a class="btn green btn-outline btn-circle" href="javascript:;"
                   data-toggle="dropdown">
                    <i class="fa fa-share"></i>
                    <span class="hidden-xs"><?php echo JText::_('COM_VIRTUALDESK_2PRINTANDEXPORT'); ?></span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right" id="tabela_lista_cultura_tools">
                    <li>
                        <a href="javascript:;" data-action="0" class="tool-action">
                            <i class="fa fa-print"></i> <?php echo JText::_('COM_VIRTUALDESK_2PRINT'); ?>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="1" class="tool-action">
                            <i class="icon-paper-clip"></i> <?php echo JText::_('COM_VIRTUALDESK_2COPY'); ?>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="2" class="tool-action">
                            <i class="fa fa-file-pdf-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2PDF'); ?>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="3" class="tool-action">
                            <i class="fa fa-file-excel-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2EXCEL'); ?>
                        </a>
                    </li>


                </ul>
            </div>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"
               data-original-title="" title=""></a>

        </div>
    </div>


    <div class="portlet-body ">


        <div class="tabbable-line nav-justified ">
            <ul class="nav nav-tabs  ">

                <li class="">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=addnew4user&vdcleanstate=1'); ?>" >
                        <h4>
                            <i class="fa fa-plus"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_CULTURA_ADDNEW'); ?>
                        </h4>
                    </a>
                </li>

                <li class="active">
                    <a href="#tab_Cultura_EventosSubmetidos" data-toggle="tab">
                        <h4>
                            <i class="fa fa-tasks"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_CULTURA_TAB_EVENTOSSUBMETIDOS'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="#tab_Cultura_Estatistica" data-toggle="tab">
                        <h4>
                            <i class="fa fa-bar-chart"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_ESTATISTICA'); ?>
                        </h4>
                    </a>
                </li>

            </ul>


            <div class="tab-content">


                <div class="tab-pane active " id="tab_Cultura_EventosSubmetidos">

                    <div class="portlet">

                        <div class="portlet-body ">

                            <table class="table table-striped table-bordered table-hover order-column"
                                   id="tabela_lista_cultura">
                                <thead>
                                <tr>
                                    <th style="min-width: 100px;"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LISTA_DATA'); ?> </th>
                                    <th><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LISTA_EVENTO'); ?> </th>
                                    <th><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LISTA_CATEGORIA'); ?></th>
                                    <th><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LISTA_ESTADO'); ?></th>
                                    <th><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LISTA_REFERENCIA'); ?> </th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>


                        </div>
                    </div>

                </div>

                <div class="tab-pane " id="tab_Cultura_Estatistica">

                    <div class="row widget-row">

                        <div class="col-md-4">
                            <!-- BEGIN NUM PEDIDOS EFETUADOS -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMPEDIDOS_EFETUADOS'); ?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-blue icon-users"></i>

                                    <div class="widget-thumb-body vdBlocoContador">
                                        <span class="widget-thumb-subtitle"><?php echo JText::_('COM_VIRTUALDESK_STATS_ONLINE'); ?></span>
                                        <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $culturaNumPedidosOnline; ?>"><?php echo $culturaNumPedidosOnline; ?></span>
                                    </div>
                                    <div class="widget-thumb-body vdBlocoContador">
                                        <span class="widget-thumb-subtitle"><?php echo JText::_('COM_VIRTUALDESK_STATS_BALCAO'); ?></span>
                                        <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $culturaNumPedidosBalcao; ?>"><?php echo $culturaNumPedidosBalcao; ?></span>
                                    </div>

                                </div>
                            </div>
                            <!-- END NUM PEDIDOS EFETUADOS -->
                        </div>

                        <div class="col-md-4">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_RESOLVIDOS'); ?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-purple icon-speedometer"></i>
                                    <div class="widget-thumb-body">
                                        <span class="widget-thumb-subtitle"></span>
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $culturaNumResolvidos; ?>"><?php echo $culturaNumResolvidos; ?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>

                        <div class="col-md-4">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_PENDENTES'); ?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-red icon-paper-clip"></i>
                                    <div class="widget-thumb-body">
                                        <span class="widget-thumb-subtitle"></span>
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $culturaNumNaoResolvidos; ?>"><?php echo $culturaNumNaoResolvidos; ?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                    </div>

                    <div class="row">

                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart font-blue"></i>
                                        <span class="caption-subject bold uppercase font-blue"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMREQUESTS_PORMES'); ?></span>
                                        <span class="caption-helper"></span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chartCulturaTotPorAnoMes" class="chart" style="height: 250px;"></div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pie-chart font-green-haze"></i>
                                        <span class="caption-subject bold uppercase font-green-haze"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMREQUESTS_PORESTADO'); ?></span>
                                        <span class="caption-helper"></span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chartCulturaTotaisPorEstado" class="chart" style="height: 250px;"></div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pie-chart "></i>
                                        <span class="caption-subject bold uppercase "><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMREQUESTS_PORCATEGORIA'); ?></span>
                                        <span class="caption-helper"></span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chartCulturaTotaisPorCategoria" class="chart" style="height: 500px;"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="tab-pane " id="tab_Cultura_NovoEvento">
                </div>


            </div>

        </div>




    </div>

</div>

<?php

echo $localScripts;


echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/cultura/tmpl/list4user.js.php');
echo ('</script>');

?>