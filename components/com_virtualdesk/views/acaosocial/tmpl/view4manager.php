<?php

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');
    JLoader::register('VirtualDeskSiteAcaosocialHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_acaosocial.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkDetailReadAccess('acaosocial');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('acaosocial', 'view4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts = '';
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-ui/jquery-ui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/ui-modals.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');

    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputPedido_Id = JFactory::getApplication()->input->getInt('pedido_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAcaosocialHelper::getDadosFormView4ManagerDetail($getInputPedido_Id);

    $dadosFormRel = VirtualDeskSiteAcaosocialHelper::getDadosFormRel4manager($getInputPedido_Id);

    foreach($dadosFormRel as $dados):
        $referencia = $dados['ref'];
        $estadoId = $dados['estado_id'];
        $estadoName = $dados['estado'];
    endforeach;

    $estadoCSS = VirtualDeskSiteFormmainHelper::getEstadoCSS($estadoId);
    $EstadoList2Change = VirtualDeskSiteFormmainHelper::getEstadoList2Change ($fileLangSufix, $estadoId, 'acaosocial');

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ACAOSOCIAL_EMPTYLIST'), 'error' );
        return false;
    }

    $formId = VirtualDeskSiteFormmainHelper::getFormIdByIdrel($getInputPedido_Id, 'acaosocial');

    if($formId == 1){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/acaosocial/tmpl/viewreqapoiosocial4manager.php');
    } else if($formId == 2){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/acaosocial/tmpl/viewbolsaestudoensinosuperior4manager.php');
    } else if($formId == 3){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/acaosocial/tmpl/viewcandapoiosocial4manager.php');
    } else if($formId == 4){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/acaosocial/tmpl/viewmarcacaoatendimentosocial4manager.php');
    }

    echo $localScripts;

?>