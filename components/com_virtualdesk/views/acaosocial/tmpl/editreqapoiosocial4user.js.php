<?php
    defined('_JEXEC') or die;
?>

var ReqApoioSocialEdit = function() {

    var handleReqApoioSocial = function() {

        jQuery('#edit-reqapoiosocial').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#edit-reqapoiosocial').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#edit-reqapoiosocial').validate().form()) {
                    jQuery('#edit-reqapoiosocial').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleReqApoioSocial();
        }
    };

}();

jQuery(document).ready(function() {

    ReqApoioSocialEdit.init();

    var qualidadeRep = document.getElementById("qualidadeRep").value;
    var tipoPedido = document.getElementById("tipopedido").value;

    if(qualidadeRep != 'repLegal' && qualidadeRep != 'gestNeg' && qualidadeRep != 'mandatario' && qualidadeRep != ''){
        document.getElementById("fieldOutraQualRep").style.display = "block";
    }

    if(tipoPedido != 'educacao' && tipoPedido != 'habitacao' && tipoPedido != 'saude' && tipoPedido != 'deficiencia' && tipoPedido != 'subsistencia' && tipoPedido != 'agricultura' && tipoPedido != ''){
        document.getElementById("fieldOutroTipoPedido").style.display = "block";
    }

    document.getElementById('cc').onclick = function() {
        document.getElementById("fieldDocId").value = 1;
    };

    document.getElementById('passaporte').onclick = function() {
        document.getElementById("fieldDocId").value = 2;
    };

    document.getElementById('ccRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 1;
    };

    document.getElementById('passaporteRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 2;
    };

    document.getElementById('repLegalRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 1;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('gestNegRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 2;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('mandatRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 3;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('outraRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 4;
        document.getElementById("hideFieldOutraQualRep").style.display = "block";
    };


    document.getElementById('educ').onclick = function() {
        document.getElementById("fieldtipopedido").value = 1;
        document.getElementById("hideFieldOutroTipoPedido").style.display = "none";
    };

    document.getElementById('habi').onclick = function() {
        document.getElementById("fieldtipopedido").value = 2;
        document.getElementById("hideFieldOutroTipoPedido").style.display = "none";
    };

    document.getElementById('saud').onclick = function() {
        document.getElementById("fieldtipopedido").value = 3;
        document.getElementById("hideFieldOutroTipoPedido").style.display = "none";
    };

    document.getElementById('defi').onclick = function() {
        document.getElementById("fieldtipopedido").value = 4;
        document.getElementById("hideFieldOutroTipoPedido").style.display = "none";
    };

    document.getElementById('subs').onclick = function() {
        document.getElementById("fieldtipopedido").value = 5;
        document.getElementById("hideFieldOutroTipoPedido").style.display = "none";
    };

    document.getElementById('agric').onclick = function() {
        document.getElementById("fieldtipopedido").value = 6;
        document.getElementById("hideFieldOutroTipoPedido").style.display = "none";
    };

    document.getElementById('outra').onclick = function() {
        document.getElementById("fieldtipopedido").value = 7;
        document.getElementById("hideFieldOutroTipoPedido").style.display = "block";
    };

});