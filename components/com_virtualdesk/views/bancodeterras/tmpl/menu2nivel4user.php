<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteBancoDeTerrasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_bancodeterras.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('bancodeterras', 'list4users');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');


?>
    <style>
        .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
        .fileinput-preview img {display:block; float: none; margin: 0 auto;}
        /* BEGIN CARD */
        .card-icon {
            width: 100%;
            text-align: center;
            overflow: hidden; }

        .card-icon i {
            font-size: 50px;
            border: 1px solid #ecf0f4;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
            padding: 47px 30px;
            margin: 30px 0 30px 0; }

        .card-title {
            text-align: center; }

        .card-title span {
            font-size: 18px;
            font-weight: 600;
            color: #373d43; }

        .card-desc {
            text-align: center;
            margin-top: 20px;
            margin-bottom: 30px; }

        .card-desc span {
            font-size: 14px;
            font-weight: 400;
            color: #808a94; }

        /* END CARD */


        /***
    Pricing Table 2
    ***/
        .pricing-content-2 {
            background-color: #fff; }
        .pricing-content-2 .no-padding {
            padding: 0; }
        .pricing-content-2 .text-left {
            text-align: left; }
        .pricing-content-2 .text-right {
            text-align: right; }
        .pricing-content-2.pricing-bg-dark {
            background-color: #2F353B; }
        .pricing-content-2 .pricing-title {
            border-color: #444; }
        .pricing-content-2 .pricing-title > h1 {
            color: #fff; }
        .pricing-content-2 .pricing-table-container {
            padding-top: 40px;
            padding-bottom: 40px; }
        .pricing-content-2 .pricing-table-container .padding-fix {
            padding-left: 15px;
            padding-right: 15px; }
        .pricing-content-2 .pricing-table-container .price-column-container {
            background-color: #fff;
            margin: 30px 0;
            padding: 60px 0;
            text-align: center;
            border-bottom: 4px solid #ccc; }
        .pricing-content-2 .pricing-table-container .price-column-container.border-right {
            border-right: 1px solid #ccc; }
        .pricing-content-2 .pricing-table-container .price-column-container.border-left {
            border-left: 1px solid #ccc; }
        .pricing-content-2 .pricing-table-container .price-column-container.border-top {
            border-top: 1px solid #ccc; }
        .pricing-content-2 .pricing-table-container .price-column-container.featured-price {
            margin: 0;
            padding: 0;
            border: none;
        }
        .pricing-content-2 .pricing-table-container .price-column-container.featured-price > .price-feature-label {
            position: absolute;
            top: 0;
            left: 50%;
            display: inline-block;
            width: 110px;
            margin: 0 0 0 -60px;
            padding: 7px 15px;
            color: #fff;
            font-weight: 300; }
        .pricing-content-2 .pricing-table-container .price-column-container > .price-table-head > h2 {
            letter-spacing: 1px;
            font-weight: 600;
            font-size: 18px;
            color: #ACB5C3; }
        .pricing-content-2 .pricing-table-container .price-column-container > .price-table-head > h2.opt-pricing-5 {
            padding: 7px 15px;
            display: inline;
            margin: 0 auto 20px auto;
            font-size: 16px; }
        .pricing-content-2 .pricing-table-container .price-column-container > .price-table-pricing > h3 {
            font-size: 60px;
            position: relative;
            font-weight: 600; }
        .pricing-content-2 .pricing-table-container .price-column-container > .price-table-pricing > h3 > .price-sign {
            font-size: 24px;
            margin-left: -15px;
            vertical-align: top;
            top: 15px; }
        .pricing-content-2 .pricing-table-container .price-column-container > .price-table-pricing > p {
            margin-top: 0; }
        .pricing-content-2 .pricing-table-container .price-column-container > .price-table-content {
            color: #333;
            font-weight: 300;
            font-size: 16px; }
        .pricing-content-2 .pricing-table-container .price-column-container > .price-table-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
            border-bottom: 1px solid;
            border-color: #eee; }
        .pricing-content-2 .pricing-table-container .price-column-container > .price-table-content .row:first-child {
            border-top: 1px solid;
            border-color: #eee; }
        .pricing-content-2 .pricing-table-container .price-column-container > .price-table-footer {
            padding: 40px 0 0 0; }
        .pricing-content-2 .pricing-table-container .price-column-container > .price-table-footer > .featured-price {
            font-size: 20px;
            font-weight: 300;
            border-bottom: 3px solid #3FABA4; }

        .pricing-content-2 [class^=icon-] {font-size: 20px;font-weight: bold}

        @media (max-width: 991px) {
            .pricing-content-2 .price-column-container {
                border-left: 1px solid;
                border-right: 1px solid;
                border-color: #ccc; } }









    </style>


    <div class="portlet light bordered">


        <div class="portlet-title">
            <div class="caption">
                <i class="icon-briefcase font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?> U </span>
            </div>

            <div class="actions">

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

            </div>

        </div>

        <div class="portlet-body ">

            <div class="row">
                <div class="col-lg-4">
                    <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                        <div class="kt-portlet__body">
                            <div class="kt-callout__body">
                                <div class="kt-callout__content">
                                    <h3 class="kt-callout__title">Oferta de Terras</h3>
                                    <p class="kt-callout__desc">
                                        Oferta de Terras
                                    </p>
                                </div>
                                <div class="kt-callout__action">
                                    <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success">Aceder</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                        <div class="kt-portlet__body">
                            <div class="kt-callout__body">
                                <div class="kt-callout__content">
                                    <h3 class="kt-callout__title">Procura de Terras</h3>
                                    <p class="kt-callout__desc">
                                        Procura de Terras
                                    </p>
                                </div>
                                <div class="kt-callout__action">
                                    <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                        <div class="kt-portlet__body">
                            <div class="kt-callout__body">
                                <div class="kt-callout__content">
                                    <h3 class="kt-callout__title">PRODERAM</h3>
                                    <p class="kt-callout__desc">
                                        PRODERAM
                                    </p>
                                </div>
                                <div class="kt-callout__action">
                                    <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-lg-4">
                    <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                        <div class="kt-portlet__body">
                            <div class="kt-callout__body">
                                <div class="kt-callout__content">
                                    <h3 class="kt-callout__title">Transporte Municipal</h3>
                                    <p class="kt-callout__desc">
                                        Transporte Municipal
                                    </p>
                                </div>
                                <div class="kt-callout__action">
                                    <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">

                </div>
                <div class="col-lg-4">

                </div>
            </div>

        </div>


    </div>



<?php

echo $localScripts;



?>