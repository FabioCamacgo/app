<?php
defined('_JEXEC') or die;
?>

var InscricaoVeraoNew = function() {

    var handleInscricaoVeraoNew = function() {

        jQuery('#new-inscricaoverao').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#new-inscricaoverao').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#new-inscricaoverao').validate().form()) {
                    jQuery('#new-inscricaoverao').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleInscricaoVeraoNew();
        }
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

jQuery(document).ready(function() {

    InscricaoVeraoNew.init();

    ComponentsSelect2.init();

    document.getElementById('cc').onclick = function() {
        document.getElementById("fieldDocId").value = 1;
    };

    document.getElementById('passaporte').onclick = function() {
        document.getElementById("fieldDocId").value = 2;
    };

    document.getElementById('ccRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 1;
    };

    document.getElementById('passaporteRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 2;
    };

    document.getElementById('repLegalRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 1;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('gestNegRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 2;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('mandatRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 3;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('outraRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 4;
        document.getElementById("hideFieldOutraQualRep").style.display = "block";
    };

    document.getElementById('julho').onclick = function() {
        document.getElementById("fieldTempoParticipacao").value = 1;
        document.getElementById("datasTempoParticipacao").style.display = "none";
    };

    document.getElementById('agosto').onclick = function() {
        document.getElementById("fieldTempoParticipacao").value = 2;
        document.getElementById("datasTempoParticipacao").style.display = "none";
    };

    document.getElementById('outroTempoParticipacao').onclick = function() {
        document.getElementById("fieldTempoParticipacao").value = 3;
        document.getElementById("datasTempoParticipacao").style.display = "block";
    };

    document.getElementById('diasCompletos').onclick = function() {
        document.getElementById("fieldTipoInscricao").value = 1;
    };

    document.getElementById('manhas').onclick = function() {
        document.getElementById("fieldTipoInscricao").value = 2;
    };

    document.getElementById('simTransporte').onclick = function() {
        document.getElementById("fieldTransporte").value = 1;
        document.getElementById("hideFieldParagem").style.display = "block";
    };

    document.getElementById('naoTransporte').onclick = function() {
        document.getElementById("fieldTransporte").value = 2;
        document.getElementById("hideFieldParagem").style.display = "none";
    };

    document.getElementById('simResidente').onclick = function() {
        document.getElementById("fieldResidente").value = 1;
    };

    document.getElementById('naoResidente').onclick = function() {
        document.getElementById("fieldResidente").value = 2;
    };

    document.getElementById('simEscalao').onclick = function() {
        document.getElementById("fieldEscalao").value = 1;
        document.getElementById("hideFieldQualEscalao").style.display = "block";
    };

    document.getElementById('naoEscalao').onclick = function() {
        document.getElementById("fieldEscalao").value = 2;
        document.getElementById("hideFieldQualEscalao").style.display = "none";
    };

    document.getElementById('simProbSaude').onclick = function() {
        document.getElementById("fieldProbSaude").value = 1;
        document.getElementById("hideFieldQualProbSaude").style.display = "block";
    };

    document.getElementById('naoProbSaude').onclick = function() {
        document.getElementById("fieldProbSaude").value = 2;
        document.getElementById("hideFieldQualProbSaude").style.display = "none";
    };

    document.getElementById('simLesao').onclick = function() {
        document.getElementById("fieldLesao").value = 1;
        document.getElementById("hideFieldQualLesao").style.display = "block";
    };

    document.getElementById('naoLesao').onclick = function() {
        document.getElementById("fieldLesao").value = 2;
        document.getElementById("hideFieldQualLesao").style.display = "none";
    };

    document.getElementById('simMobilidade').onclick = function() {
        document.getElementById("fieldMobilidade").value = 1;
        document.getElementById("hideFieldQualMobilidade").style.display = "block";
    };

    document.getElementById('naoMobilidade').onclick = function() {
        document.getElementById("fieldMobilidade").value = 2;
        document.getElementById("hideFieldQualMobilidade").style.display = "none";
    };

    document.getElementById('simProbResp').onclick = function() {
        document.getElementById("fieldProbResp").value = 1;
    };

    document.getElementById('naoProbResp').onclick = function() {
        document.getElementById("fieldProbResp").value = 2;
    };

    document.getElementById('simProbCard').onclick = function() {
        document.getElementById("fieldProbCard").value = 1;
    };

    document.getElementById('naoProbCard').onclick = function() {
        document.getElementById("fieldProbCard").value = 2;
    };

    document.getElementById('simAlergia').onclick = function() {
        document.getElementById("fieldAlergia").value = 1;
        document.getElementById("alergiaDescription").style.display = "block";
    };

    document.getElementById('naoAlergia').onclick = function() {
        document.getElementById("fieldAlergia").value = 2;
        document.getElementById("alergiaDescription").style.display = "none";
    };

    document.getElementById('cloro').onclick = function() {
        document.getElementById("fieldTipoAlergia").value = 1;
        document.getElementById("fieldOutraAlergia").style.display = "none";
    };

    document.getElementById('gluten').onclick = function() {
        document.getElementById("fieldTipoAlergia").value = 2;
        document.getElementById("hideFieldOutraAlergia").style.display = "none";
    };

    document.getElementById('outraAlergia').onclick = function() {
        document.getElementById("fieldTipoAlergia").value = 3;
        document.getElementById("hideFieldOutraAlergia").style.display = "block";
    };
});
