<?php
defined('_JEXEC') or die;
?>

var InstalacoesEquipamentosNew = function() {

    var handleInstalacoesEquipamentosNew = function() {

        jQuery('#new-instalacoesequipamentos').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#new-instalacoesequipamentos').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#new-instalacoesequipamentos').validate().form()) {
                    jQuery('#new-instalacoesequipamentos').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleInstalacoesEquipamentosNew();
        }
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


jQuery(document).ready(function() {

    InstalacoesEquipamentosNew.init();

    ComponentsSelect2.init();

    document.getElementById('cc').onclick = function() {
        document.getElementById("fieldDocId").value = 1;
    };

    document.getElementById('passaporte').onclick = function() {
        document.getElementById("fieldDocId").value = 2;
    };

    document.getElementById('ccRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 1;
    };

    document.getElementById('passaporteRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 2;
    };

    document.getElementById('repLegalRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 1;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('gestNegRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 2;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('mandatRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 3;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('outraRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 4;
        document.getElementById("hideFieldOutraQualRep").style.display = "block";
    };

    document.getElementById('campo').onclick = function() {
        if ( this.checked ) {
            document.getElementById("fieldCampoValue").value = 1;
        } else {
            document.getElementById("fieldCampoValue").value = 0;
        }
    };

    document.getElementById('balneario').onclick = function() {
        if ( this.checked ) {
            document.getElementById("fieldBalnearioValue").value = 1;
        } else {
            document.getElementById("fieldBalnearioValue").value = 0;
        }
    };

    document.getElementById('outroCampoMunicipal').onclick = function() {
        if ( this.checked ) {
            document.getElementById("fieldOutroCampoMunicipalValue").value = 1;
            document.getElementById("hideFieldOutroCampoMunicipal").style.display = "block";
        } else {
            document.getElementById("fieldOutroCampoMunicipalValue").value = 0;
            document.getElementById("hideFieldOutroCampoMunicipal").style.display = "none";
        }
    };

    document.getElementById('campoFutebol').onclick = function() {
        if ( this.checked ) {
            document.getElementById("fieldCampoFutebolValue").value = 1;
        } else {
            document.getElementById("fieldCampoFutebolValue").value = 0;
        }
    };

    document.getElementById('balnearioFutebol').onclick = function() {
        if ( this.checked ) {
            document.getElementById("fieldBalnearioFutebolValue").value = 1;
        } else {
            document.getElementById("fieldBalnearioFutebolValue").value = 0;
        }
    };

    document.getElementById('outroCampoFutebol').onclick = function() {
        if ( this.checked ) {
            document.getElementById("fieldOutroCampoFutebolValue").value = 1;
            document.getElementById("hideFieldOutroCampoFutebol").style.display = "block";
        } else {
            document.getElementById("fieldOutroCampoFutebolValue").value = 0;
            document.getElementById("hideFieldOutroCampoFutebol").style.display = "none";
        }
    };

    document.getElementById('campoTenis').onclick = function() {
        if ( this.checked ) {
            document.getElementById("fieldCampoTenisValue").value = 1;
        } else {
            document.getElementById("fieldCampoTenisValue").value = 0;
        }
    };

    document.getElementById('balnearioTenis').onclick = function() {
        if ( this.checked ) {
            document.getElementById("fieldBalnearioTenisValue").value = 1;
        } else {
            document.getElementById("fieldBalnearioTenisValue").value = 0;
        }
    };

    document.getElementById('outroCampoTenis').onclick = function() {
        if ( this.checked ) {
            document.getElementById("fieldOutroCampoTenisValue").value = 1;
            document.getElementById("hideFieldOutroCampoTenis").style.display = "block";
        } else {
            document.getElementById("fieldOutroCampoTenisValue").value = 0;
            document.getElementById("hideFieldOutroCampoTenis").style.display = "none";
        }
    };

    document.getElementById('diario').onclick = function() {
        document.getElementById("fieldPeriocidade").value = 1;
        document.getElementById("hideFieldOutraPeriocidade").style.display = "none";
    };

    document.getElementById('semanal').onclick = function() {
        document.getElementById("fieldPeriocidade").value = 2;
        document.getElementById("hideFieldOutraPeriocidade").style.display = "none";
    };

    document.getElementById('mensal').onclick = function() {
        document.getElementById("fieldPeriocidade").value = 3;
        document.getElementById("hideFieldOutraPeriocidade").style.display = "none";
    };

    document.getElementById('anual').onclick = function() {
        document.getElementById("fieldPeriocidade").value = 4;
        document.getElementById("hideFieldOutraPeriocidade").style.display = "none";
    };

    document.getElementById('outraPeriocidade').onclick = function() {
        document.getElementById("fieldPeriocidade").value = 5;
        document.getElementById("hideFieldOutraPeriocidade").style.display = "block";
    };

});
