<?php
    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteDesportoJuventudeHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_desportojuventude.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('desportojuventude');
    if($vbHasAccess===false ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    $tag_form='OiWixXYu';
    $formId = VirtualDeskSiteFormmainHelper::getFormId($tag_form);
    $modulo_id=VirtualDeskSiteFormmainHelper::getModuleId('desportojuventude');

    $fieldsForm = VirtualDeskSiteFormmainHelper::getFieldsData($tag_form, $modulo_id);

    $newFieldsExists = array();
    foreach ($fieldsForm as $keyF) {
        $newFieldsExists[] = $keyF['tag'];
    }


    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

    //Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteDesportoJuventudeHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnewinstalacoesequipamentos4user.desportojuventude.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';

    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
    $UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
    $nome =  $UserProfileData->name;
    $email    = $UserProfileData->email;
    $email2   = $UserProfileData->email;
    $fiscalid = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
    $telefone = $UserProfileData->phone1;
    $morada = $UserProfileData->address;
    $codPostal = $UserProfileData->postalcode;


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=desportojuventude'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=desportojuventude&layout=menu3nivelinstalacoes'); ?>"><span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TAB_INSTALACOESEQUIPAMENTOS'); ?></span></a>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag($tag_form); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=desportojuventude&layout=menu3nivelinstalacoes'); ?>"
                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>">
                    <i class="fa fa-ban"></i>
                    <?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <div class="tabbable-line nav-justified ">

                <div class="tab-content">

                    <form id="new-instalacoesequipamentos" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=desportojuventude.createinstalacoesequipamentos4user'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_REQUERENTE');?></h3></legend>
                            <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_REQUIRED');?></p>


                            <?php if(in_array('fieldName',$newFieldsExists)) :?>
                                <div class="form-group fieldName">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NOME'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldName" id="fieldName" maxlength="500" value="<?php echo $nome; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldMorada',$newFieldsExists)) :?>
                                <div class="form-group fieldMorada">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_MORADA'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldMorada" id="fieldMorada" maxlength="500" value="<?php echo $morada; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldPorta',$newFieldsExists)) :?>
                                <div class="form-group fieldPorta">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NUM_PORTA'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldPorta" id="fieldPorta" maxlength="5" value="<?php echo $numPorta; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldLote',$newFieldsExists)) :?>
                                <div class="form-group fieldLote">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_LOTE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldLote" id="fieldLote" maxlength="5" value="<?php echo $lote; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldLocalidade',$newFieldsExists)) :?>
                                <div class="form-group fieldLocalidade">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_LOCALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldLocalidade" id="fieldLocalidade" maxlength="100" value="<?php echo $localidade; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldCodPostal',$newFieldsExists)) :?>
                                <div class="form-group fieldCodPostal">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CODPOSTAL'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldCodPostal" id="fieldCodPostal" maxlength="30" value="<?php echo $codPostal; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldfreguesia',$newFieldsExists)) :?>
                                <div class="form-group fieldfreguesia">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_FREGUESIA'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldfreguesia" id="fieldfreguesia" maxlength="100" value="<?php echo $freguesia; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldNif',$newFieldsExists)) :?>
                                <div class="form-group fieldNif">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NIF'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="number" required readonly class="form-control" name="fieldNif" id="fieldNif" maxlength="9" value="<?php echo $fiscalid; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldDocId',$newFieldsExists)) :?>
                                <div class="form-group fieldDocId">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TIPODOCIDENTIFICACAO'); ?></label>
                                    <div class="col-md-12">
                                        <input type="radio" name="radioval" id="cc" value="cc" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocId'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CC'); ?>
                                        <input type="radio" name="radioval" id="passaporte" value="passaporte" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocId'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_PASSAPORTE'); ?>
                                    </div>

                                    <input type="hidden" id="fieldDocId" name="fieldDocId" value="<?php echo $fieldDocId; ?>">
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldNumId',$newFieldsExists)) :?>
                                <div class="form-group fieldNumId">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NUMERO'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldNumId" id="fieldNumId" maxlength="30" value="<?php echo $numID; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldValId',$newFieldsExists)) :?>
                                <div class="form-group fieldValId">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_VALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="date" class="form-control" name="fieldValId" id="fieldValId" value="<?php echo $validade; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldCCP',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldCCP">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CCP'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="ccp" id="ccp" maxlength="100" value="<?php echo $ccp; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldTelemovel',$newFieldsExists)) :?>
                                <div class="form-group fieldTelemovel">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TELEMOVEL'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="number" required class="form-control" name="fieldTelemovel" id="fieldTelemovel" maxlength="9" value="<?php echo $telemovel; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldTelefone',$newFieldsExists)) :?>
                                <div class="form-group fieldTelefone">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TELEFONE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldTelefone" id="fieldTelefone" maxlength="9" value="<?php echo $telefone; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldFax',$newFieldsExists)) :?>
                                <div class="form-group fieldFax">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_FAX'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldFax" id="fieldFax" maxlength="9" value="<?php echo $fax; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldEmail',$newFieldsExists)) :?>
                                <div class="form-group fieldEmail">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_EMAIL'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldEmail" id="fieldEmail" maxlength="200" value="<?php echo $email; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldQualidade',$newFieldsExists)) :?>
                                <div class="form-group fieldQualidade">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldQualidade" id="fieldQualidade" maxlength="200" value="<?php echo $qualidade; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div>

                        <?php if(in_array('fieldNomeRep',$newFieldsExists) || in_array('fieldMoradaRep',$newFieldsExists) || in_array('fieldPortaRep',$newFieldsExists) || in_array('fieldLoteRep',$newFieldsExists) || in_array('fieldLocalRep',$newFieldsExists) || in_array('fieldCodPostRep',$newFieldsExists) || in_array('fieldfreguesiaRep',$newFieldsExists) || in_array('fieldNifRep',$newFieldsExists) || in_array('fieldDocIdRep',$newFieldsExists) || in_array('fieldNumIdRep',$newFieldsExists) || in_array('fieldValIdRep',$newFieldsExists) || in_array('fieldCCPRep',$newFieldsExists) || in_array('fieldTelemovelRep',$newFieldsExists) || in_array('fieldTelefoneRep',$newFieldsExists) || in_array('fieldFaxRep',$newFieldsExists) || in_array('fieldEmailRep',$newFieldsExists) || in_array('fieldQualidadeRep',$newFieldsExists) || in_array('fieldOutraQualRep',$newFieldsExists)) :?>

                            <div class="bloco">

                                <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_REPRESENTANTE');?></h3></legend>


                                <?php if(in_array('fieldNomeRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldNomeRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NOME'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldNomeRep" id="fieldNomeRep" maxlength="500" value="<?php echo $nomeRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldMoradaRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldMoradaRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_MORADA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldMoradaRep" id="fieldMoradaRep" maxlength="500" value="<?php echo $moradaRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldPortaRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldPortaRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NUM_PORTA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldPortaRep" id="fieldPortaRep" maxlength="5" value="<?php echo $numPortaRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldLoteRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldLoteRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_LOTE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldLoteRep" id="fieldLoteRep" maxlength="5" value="<?php echo $loteRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldLocalRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldLocalRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_LOCALIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldLocalRep" id="fieldLocalRep" maxlength="100" value="<?php echo $localidadeRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldCodPostRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldCodPostRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CODPOSTAL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldCodPostRep" id="fieldCodPostRep" maxlength="30" value="<?php echo $codPostalRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldfreguesiaRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldfreguesiaRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGUA_FREGUESIA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldfreguesiaRep" id="fieldfreguesiaRep" maxlength="100" value="<?php echo $fieldfreguesiaRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNifRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldNifRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NIF'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldNifRep" id="fieldNifRep" maxlength="9" value="<?php echo $fiscalidRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldDocIdRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldDocIdRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TIPODOCIDENTIFICACAO'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalRep" id="ccRep" value="ccRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocIdRep'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CC'); ?>
                                            <input type="radio" name="radiovalRep" id="passaporteRep" value="passaporteRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocIdRep'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_PASSAPORTE'); ?>
                                        </div>

                                        <input type="hidden" id="fieldDocIdRep" name="fieldDocIdRep" value="<?php echo $fieldDocIdRep; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNumIdRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldNumIdRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NUMERO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldNumIdRep" id="fieldNumIdRep" maxlength="30" value="<?php echo $numIDRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldValIdRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldValIdRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_VALIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="date" class="form-control" name="fieldValIdRep" id="fieldValIdRep" value="<?php echo $validadeRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldCCPRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldCCPRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CCP'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldCCPRep" id="fieldCCPRep" maxlength="100" value="<?php echo $ccpRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTelemovelRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldTelemovelRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TELEMOVEL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldTelemovelRep" id="fieldTelemovelRep" maxlength="9" value="<?php echo $telemovelRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTelefoneRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldTelefoneRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TELEFONE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldTelefoneRep" id="fieldTelefoneRep" maxlength="9" value="<?php echo $telefoneRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldFaxRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldFaxRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_FAX'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldFaxRep" id="fieldFaxRep" maxlength="9" value="<?php echo $faxRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldEmailRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldEmailRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_EMAIL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldEmailRep" id="fieldEmailRep" maxlength="200" value="<?php echo $emailRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldQualidadeRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldQualidadeRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUALIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="radio" name="radiovalQuaRep" id="repLegalRep" value="repLegalRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_REPLEGAL'); ?>
                                            <input type="radio" name="radiovalQuaRep" id="gestNegRep" value="gestNegRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_GESTNEG'); ?>
                                            <input type="radio" name="radiovalQuaRep" id="mandatRep" value="mandatRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_MANDATARIO'); ?>
                                            <input type="radio" name="radiovalQuaRep" id="outraRep" value="outraRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 4) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_OUTRA'); ?>
                                        </div>

                                        <input type="hidden" id="fieldQualidadeRep" name="fieldQualidadeRep" value="<?php echo $qualidadeRep; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldOutraQualRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldOutraQualRep" id="hideFieldOutraQualRep" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_OUTRAQUALIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldOutraQualRep" id="fieldOutraQualRep" maxlength="200" value="<?php echo $outraQualRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            </div>

                        <?php endif; ?>


                        <?php if(in_array('fieldAtividadeRealizar',$newFieldsExists) || in_array('fieldCampoMunicipal',$newFieldsExists) || in_array('fieldCampoFutebol',$newFieldsExists) || in_array('fieldCampoTenis',$newFieldsExists) || in_array('fieldOutroCampoTenis',$newFieldsExists) || in_array('fieldOutrasInstalacoes',$newFieldsExists) || in_array('fieldDataInicio',$newFieldsExists) || in_array('fieldDataTermo',$newFieldsExists) || in_array('fieldHoraInicio',$newFieldsExists) || in_array('fieldHoraTermo',$newFieldsExists) || in_array('fieldPeriocidade',$newFieldsExists) || in_array('fieldOutraPeriocidade',$newFieldsExists)) :?>

                            <div class="bloco">

                                <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_PEDIDO');?></h3></legend>
                                <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_INTRO_CEDENCIAINSTALACOES');?></p>


                                <?php if(in_array('fieldAtividadeRealizar',$newFieldsExists)) :?>
                                    <div class="form-group fieldAtividadeRealizar">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_ATIVIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldAtividadeRealizar" id="fieldAtividadeRealizar" maxlength="200" value="<?php echo $atividade; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldCampoMunicipal',$newFieldsExists)) :?>
                                    <div class="form-group fieldCampoMunicipal">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CAMPOMUNICIPAL'); ?></label>
                                        <div class="name col-md-12">

                                            <?php if(in_array('fieldCampoValue',$newFieldsExists)) :?>
                                                <input type="checkbox" name="campo" id="campo" value="<?php echo $fieldCampoValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['fieldCampoValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CAMPO'); ?>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldBalnearioValue',$newFieldsExists)) :?>
                                                <input type="checkbox" name="balneario" id="balneario" value="<?php echo $fieldBalnearioValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['fieldBalnearioValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_BALNEARIO'); ?>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldOutroCampoMunicipalValue',$newFieldsExists)) :?>
                                                <input type="checkbox" name="outroCampoMunicipal" id="outroCampoMunicipal" value="<?php echo $fieldOutroCampoMunicipalValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['fieldOutroCampoMunicipalValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_OUTRO'); ?>
                                            <?php endif; ?>
                                        </div>

                                        <?php if(in_array('fieldCampoValue',$newFieldsExists)) :?>
                                            <input type="hidden" id="fieldCampoValue" name="fieldCampoValue" value="<?php echo $fieldCampoValue; ?>">
                                        <?php endif; ?>

                                        <?php if(in_array('fieldBalnearioValue',$newFieldsExists)) :?>
                                            <input type="hidden" id="fieldBalnearioValue" name="fieldBalnearioValue" value="<?php echo $fieldBalnearioValue; ?>">
                                        <?php endif; ?>

                                        <?php if(in_array('fieldOutroCampoMunicipalValue',$newFieldsExists)) :?>
                                            <input type="hidden" id="fieldOutroCampoMunicipalValue" name="fieldOutroCampoMunicipalValue" value="<?php echo $fieldOutroCampoMunicipalValue; ?>">
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldOutroCampoMunicipal',$newFieldsExists) && in_array('fieldOutroCampoMunicipalValue',$newFieldsExists)) :?>
                                    <div class="form-group fieldOutroCampoMunicipal" id="hideFieldOutroCampoMunicipal" style="display:none;">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUAL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldOutroCampoMunicipal" id="fieldOutroCampoMunicipal" maxlength="200" value="<?php echo $fieldOutroCampoMunicipal; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldCampoFutebol',$newFieldsExists)) :?>
                                    <div class="form-group fieldCampoFutebol">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CAMPOFUTEBOL'); ?></label>
                                        <div class="name col-md-12">
                                            <?php if(in_array('fieldCampoFutebolValue',$newFieldsExists)) :?>
                                                <input type="checkbox" name="campoFutebol" id="campoFutebol" value="<?php echo $fieldCampoFutebolValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['fieldCampoFutebolValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CAMPO'); ?>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldBalnearioFutebolValue',$newFieldsExists)) :?>
                                                <input type="checkbox" name="balnearioFutebol" id="balnearioFutebol" value="<?php echo $fieldBalnearioFutebolValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['fieldBalnearioFutebolValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_BALNEARIO'); ?>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldOutroCampoFutebolValue',$newFieldsExists)) :?>
                                                <input type="checkbox" name="outroCampoFutebol" id="outroCampoFutebol" value="<?php echo $fieldOutroCampoFutebolValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['fieldOutroCampoFutebolValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_OUTRO'); ?>
                                            <?php endif; ?>
                                        </div>

                                        <?php if(in_array('fieldCampoFutebolValue',$newFieldsExists)) :?>
                                            <input type="hidden" id="fieldCampoFutebolValue" name="fieldCampoFutebolValue" value="<?php echo $fieldCampoFutebolValue; ?>">
                                        <?php endif; ?>

                                        <?php if(in_array('fieldBalnearioFutebolValue',$newFieldsExists)) :?>
                                            <input type="hidden" id="fieldBalnearioFutebolValue" name="fieldBalnearioFutebolValue" value="<?php echo $fieldBalnearioFutebolValue; ?>">
                                        <?php endif; ?>

                                        <?php if(in_array('fieldOutroCampoFutebolValue',$newFieldsExists)) :?>
                                            <input type="hidden" id="fieldOutroCampoFutebolValue" name="fieldOutroCampoFutebolValue" value="<?php echo $fieldOutroCampoFutebolValue; ?>">
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldOutroCampoFutebol',$newFieldsExists) && in_array('fieldOutroCampoFutebolValue',$newFieldsExists)) :?>
                                    <div class="form-group fieldOutroCampoFutebol" id="hideFieldOutroCampoFutebol" style="display:none;">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUAL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldOutroCampoFutebol" id="fieldOutroCampoFutebol" maxlength="200" value="<?php echo $fieldOutroCampoFutebol; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldCampoTenis',$newFieldsExists)) :?>
                                    <div class="form-group fieldCampoTenis">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CAMPOTENIS'); ?></label>
                                        <div class="name col-md-12">
                                            <?php if(in_array('fieldCampoTenisValue',$newFieldsExists)) :?>
                                                <input type="checkbox" name="campoTenis" id="campoTenis" value="<?php echo $fieldCampoTenisValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['fieldCampoTenisValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CAMPO'); ?>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldBalnearioTenisValue',$newFieldsExists)) :?>
                                                <input type="checkbox" name="balnearioTenis" id="balnearioTenis" value="<?php echo $fieldBalnearioTenisValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['fieldBalnearioTenisValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_BALNEARIO'); ?>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldOutroCampoTenisValue',$newFieldsExists)) :?>
                                                <input type="checkbox" name="outroCampoTenis" id="outroCampoTenis" value="<?php echo $fieldOutroCampoTenisValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['fieldOutroCampoTenisValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_OUTRO'); ?>
                                            <?php endif; ?>
                                        </div>

                                        <?php if(in_array('fieldCampoTenisValue',$newFieldsExists)) :?>
                                            <input type="hidden" id="fieldCampoTenisValue" name="fieldCampoTenisValue" value="<?php echo $fieldCampoTenisValue; ?>">
                                        <?php endif; ?>

                                        <?php if(in_array('fieldBalnearioTenisValue',$newFieldsExists)) :?>
                                            <input type="hidden" id="fieldBalnearioTenisValue" name="fieldBalnearioTenisValue" value="<?php echo $fieldBalnearioTenisValue; ?>">
                                        <?php endif; ?>

                                        <?php if(in_array('fieldOutroCampoTenisValue',$newFieldsExists)) :?>
                                            <input type="hidden" id="fieldOutroCampoTenisValue" name="fieldOutroCampoTenisValue" value="<?php echo $fieldOutroCampoTenisValue; ?>">
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldOutroCampoTenis',$newFieldsExists)) :?>
                                    <div class="form-group fieldOutroCampoTenis" id="hideFieldOutroCampoTenis" style="display:none;">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUAL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldOutroCampoTenis" id="fieldOutroCampoTenis" maxlength="200" value="<?php echo $fieldOutroCampoTenis; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldOutrasInstalacoes',$newFieldsExists)) :?>
                                    <div class="form-group fieldOutrasInstalacoes">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_OUTRASINSTALACOES'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldOutrasInstalacoes" id="fieldOutrasInstalacoes" maxlength="200" value="<?php echo $outrasInstalacoes; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldDataInicio',$newFieldsExists)) :?>
                                    <div class="form-group fieldDataInicio">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_DATAINICIO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="date" class="form-control" name="fieldDataInicio" id="fieldDataInicio" value="<?php echo $dataInicio; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldDataTermo',$newFieldsExists)) :?>
                                    <div class="form-group fieldDataTermo">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_DATATERMO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="date" class="form-control" name="fieldDataTermo" id="fieldDataTermo" value="<?php echo $dataTermo; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldHoraInicio',$newFieldsExists)) :?>
                                    <div class="form-group fieldHoraInicio">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_HORAINICIO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="time" class="form-control" name="fieldHoraInicio" id="fieldHoraInicio" value="<?php echo $horaInicio; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldHoraTermo',$newFieldsExists)) :?>
                                    <div class="form-group fieldHoraTermo">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_HORATERMO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="time" class="form-control" name="fieldHoraTermo" id="fieldHoraTermo" value="<?php echo $horaTermo; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldPeriocidade',$newFieldsExists)) :?>
                                    <div class="form-group fieldPeriocidade">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_PERIOCIDADE'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalPeriocidade" id="diario" value="diario" <?php if (isset($_POST["submitForm"]) && $_POST['fieldPeriocidade'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_DIARIA'); ?>
                                            <input type="radio" name="radiovalPeriocidade" id="semanal" value="semanal" <?php if (isset($_POST["submitForm"]) && $_POST['fieldPeriocidade'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_SEMANAL'); ?>
                                            <input type="radio" name="radiovalPeriocidade" id="mensal" value="mensal" <?php if (isset($_POST["submitForm"]) && $_POST['fieldPeriocidade'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_MENSAL'); ?>
                                            <input type="radio" name="radiovalPeriocidade" id="anual" value="anual" <?php if (isset($_POST["submitForm"]) && $_POST['fieldPeriocidade'] == 4) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_ANUAL'); ?>
                                            <input type="radio" name="radiovalPeriocidade" id="outraPeriocidade" value="outraPeriocidade" <?php if (isset($_POST["submitForm"]) && $_POST['fieldPeriocidade'] == 5) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_OUTRA'); ?>
                                        </div>

                                        <input type="hidden" id="fieldPeriocidade" name="fieldPeriocidade" value="<?php echo $fieldPeriocidade; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldPeriocidade',$newFieldsExists) && in_array('fieldOutraPeriocidade',$newFieldsExists)) :?>
                                    <div class="form-group fieldOutraPeriocidade" id="hideFieldOutraPeriocidade" style="display:none;">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUAL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldOutraPeriocidade" id="fieldOutraPeriocidade" maxlength="200" value="<?php echo $qualPeriocidade; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            </div>

                        <?php endif; ?>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                    </button>
                                    <a class="btn default"
                                       href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=desportojuventude&layout=menu3nivelinstalacoes'); ?>"
                                       title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"     value="<?php echo $obVDCrypt->formInputValueEncrypt('desportojuventude.createinstalacoesequipamentos4user',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt('addnewinstalacoesequipamentos4user',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('formId',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($formId,$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('userID',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($UserJoomlaID,$setencrypt_forminputhidden); ?>"/>

                        <?php echo JHtml::_('form.token'); ?>

                    </form>

                </div>
            </div>
        </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/desportojuventude/tmpl/addnewinstalacoesequipamentos4user.js.php');
    echo ('</script>');
?>