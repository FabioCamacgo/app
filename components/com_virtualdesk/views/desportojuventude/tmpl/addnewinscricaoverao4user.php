<?php
    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteDesportoJuventudeHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_desportojuventude.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('desportojuventude');
    if($vbHasAccess===false ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    $tag_form='nGyS7IYl';
    $formId = VirtualDeskSiteFormmainHelper::getFormId($tag_form);
    $modulo_id=VirtualDeskSiteFormmainHelper::getModuleId('desportojuventude');

    $fieldsForm = VirtualDeskSiteFormmainHelper::getFieldsData($tag_form, $modulo_id);

    $newFieldsExists = array();
    foreach ($fieldsForm as $keyF) {
        $newFieldsExists[] = $keyF['tag'];
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

    //Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteDesportoJuventudeHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnewinscricaoverao4user.desportojuventude.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
    $UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
    $nome =  $UserProfileData->name;
    $email    = $UserProfileData->email;
    $email2   = $UserProfileData->email;
    $fiscalid = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
    $telefone = $UserProfileData->phone1;
    $morada = $UserProfileData->address;
    $codPostal = $UserProfileData->postalcode;


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=desportojuventude'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=desportojuventude&layout=menu3niveleventos'); ?>"><span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TAB_EVENTOSATIVIDADE'); ?></span></a>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag($tag_form); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=desportojuventude&layout=menu3niveleventos'); ?>"
                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>">
                    <i class="fa fa-ban"></i>
                    <?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <div class="tabbable-line nav-justified ">

                <div class="tab-content">

                    <form id="new-inscricaoverao" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=desportojuventude.createinscricaoverao4user'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_REQUERENTE');?></h3></legend>
                            <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_REQUIRED');?></p>

                            <?php if(in_array('fieldName',$newFieldsExists)) :?>
                                <div class="form-group fieldName">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NOME'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldName" id="fieldName" maxlength="500" value="<?php echo $nome; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldMorada',$newFieldsExists)) :?>
                                <div class="form-group fieldMorada">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_MORADA'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldMorada" id="fieldMorada" maxlength="500" value="<?php echo $morada; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldPorta',$newFieldsExists)) :?>
                                <div class="form-group fieldPorta">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NUM_PORTA'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldPorta" id="fieldPorta" maxlength="5" value="<?php echo $numPorta; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldLote',$newFieldsExists)) :?>
                                <div class="form-group fieldLote">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_LOTE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldLote" id="fieldLote" maxlength="5" value="<?php echo $lote; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldLocalidade',$newFieldsExists)) :?>
                                <div class="form-group fieldLocalidade">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_LOCALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldLocalidade" id="fieldLocalidade" maxlength="100" value="<?php echo $localidade; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldCodPostal',$newFieldsExists)) :?>
                                <div class="form-group fieldCodPostal">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CODPOSTAL'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldCodPostal" id="fieldCodPostal" maxlength="30" value="<?php echo $codPostal; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldfreguesia',$newFieldsExists)) :?>
                                <div class="form-group fieldfreguesia">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_FREGUESIA'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldfreguesia" id="fieldfreguesia" maxlength="100" value="<?php echo $freguesia; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldNif',$newFieldsExists)) :?>
                                <div class="form-group fieldNif">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NIF'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="number" required readonly class="form-control" name="fieldNif" id="fieldNif" maxlength="9" value="<?php echo $fiscalid; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldDocId',$newFieldsExists)) :?>
                                <div class="form-group fieldDocId">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TIPODOCIDENTIFICACAO'); ?></label>
                                    <div class="col-md-12">
                                        <input type="radio" name="radioval" id="cc" value="cc" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocId'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CC'); ?>
                                        <input type="radio" name="radioval" id="passaporte" value="passaporte" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocId'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_PASSAPORTE'); ?>
                                    </div>

                                    <input type="hidden" id="fieldDocId" name="fieldDocId" value="<?php echo $fieldDocId; ?>">
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldNumId',$newFieldsExists)) :?>
                                <div class="form-group fieldNumId">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NUMERO'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldNumId" id="fieldNumId" maxlength="30" value="<?php echo $numID; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldValId',$newFieldsExists)) :?>
                                <div class="form-group fieldValId">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_VALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="date" class="form-control" name="fieldValId" id="fieldValId" value="<?php echo $validade; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldCCP',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldCCP">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CCP'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="ccp" id="ccp" maxlength="100" value="<?php echo $ccp; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldTelemovel',$newFieldsExists)) :?>
                                <div class="form-group fieldTelemovel">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TELEMOVEL'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="number" required class="form-control" name="fieldTelemovel" id="fieldTelemovel" maxlength="9" value="<?php echo $telemovel; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldTelefone',$newFieldsExists)) :?>
                                <div class="form-group fieldTelefone">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TELEFONE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldTelefone" id="fieldTelefone" maxlength="9" value="<?php echo $telefone; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldFax',$newFieldsExists)) :?>
                                <div class="form-group fieldFax">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_FAX'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldFax" id="fieldFax" maxlength="9" value="<?php echo $fax; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldEmail',$newFieldsExists)) :?>
                                <div class="form-group fieldEmail">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_EMAIL'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldEmail" id="fieldEmail" maxlength="200" value="<?php echo $email; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldQualidade',$newFieldsExists)) :?>
                                <div class="form-group fieldQualidade">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldQualidade" id="fieldQualidade" maxlength="200" value="<?php echo $qualidade; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div>

                        <?php if(in_array('fieldNomeRep',$newFieldsExists) || in_array('fieldMoradaRep',$newFieldsExists) || in_array('fieldPortaRep',$newFieldsExists) || in_array('fieldLoteRep',$newFieldsExists) || in_array('fieldLocalRep',$newFieldsExists) || in_array('fieldCodPostRep',$newFieldsExists) || in_array('fieldfreguesiaRep',$newFieldsExists) || in_array('fieldNifRep',$newFieldsExists) || in_array('fieldDocIdRep',$newFieldsExists) || in_array('fieldNumIdRep',$newFieldsExists) || in_array('fieldValIdRep',$newFieldsExists) || in_array('fieldCCPRep',$newFieldsExists) || in_array('fieldTelemovelRep',$newFieldsExists) || in_array('fieldTelefoneRep',$newFieldsExists) || in_array('fieldFaxRep',$newFieldsExists) || in_array('fieldEmailRep',$newFieldsExists) || in_array('fieldQualidadeRep',$newFieldsExists) || in_array('fieldOutraQualRep',$newFieldsExists)) :?>

                            <div class="bloco">

                                <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_REPRESENTANTE');?></h3></legend>

                                <?php if(in_array('fieldNomeRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldNomeRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NOME'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldNomeRep" id="fieldNomeRep" maxlength="500" value="<?php echo $nomeRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldMoradaRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldMoradaRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_MORADA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldMoradaRep" id="fieldMoradaRep" maxlength="500" value="<?php echo $moradaRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldPortaRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldPortaRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NUM_PORTA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldPortaRep" id="fieldPortaRep" maxlength="5" value="<?php echo $numPortaRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldLoteRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldLoteRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_LOTE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldLoteRep" id="fieldLoteRep" maxlength="5" value="<?php echo $loteRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldLocalRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldLocalRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_LOCALIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldLocalRep" id="fieldLocalRep" maxlength="100" value="<?php echo $localidadeRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldCodPostRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldCodPostRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CODPOSTAL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldCodPostRep" id="fieldCodPostRep" maxlength="30" value="<?php echo $codPostalRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldfreguesiaRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldfreguesiaRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGUA_FREGUESIA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldfreguesiaRep" id="fieldfreguesiaRep" maxlength="100" value="<?php echo $fieldfreguesiaRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNifRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldNifRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NIF'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldNifRep" id="fieldNifRep" maxlength="9" value="<?php echo $fiscalidRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldDocIdRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldDocIdRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TIPODOCIDENTIFICACAO'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalRep" id="ccRep" value="ccRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocIdRep'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CC'); ?>
                                            <input type="radio" name="radiovalRep" id="passaporteRep" value="passaporteRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocIdRep'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_PASSAPORTE'); ?>
                                        </div>

                                        <input type="hidden" id="fieldDocIdRep" name="fieldDocIdRep" value="<?php echo $fieldDocIdRep; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNumIdRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldNumIdRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NUMERO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldNumIdRep" id="fieldNumIdRep" maxlength="30" value="<?php echo $numIDRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldValIdRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldValIdRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_VALIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="date" class="form-control" name="fieldValIdRep" id="fieldValIdRep" value="<?php echo $validadeRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldCCPRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldCCPRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CCP'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldCCPRep" id="fieldCCPRep" maxlength="100" value="<?php echo $ccpRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTelemovelRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldTelemovelRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TELEMOVEL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldTelemovelRep" id="fieldTelemovelRep" maxlength="9" value="<?php echo $telemovelRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTelefoneRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldTelefoneRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TELEFONE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldTelefoneRep" id="fieldTelefoneRep" maxlength="9" value="<?php echo $telefoneRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldFaxRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldFaxRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_FAX'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldFaxRep" id="fieldFaxRep" maxlength="9" value="<?php echo $faxRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldEmailRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldEmailRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_EMAIL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldEmailRep" id="fieldEmailRep" maxlength="200" value="<?php echo $emailRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldQualidadeRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldQualidadeRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUALIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="radio" name="radiovalQuaRep" id="repLegalRep" value="repLegalRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_REPLEGAL'); ?>
                                            <input type="radio" name="radiovalQuaRep" id="gestNegRep" value="gestNegRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_GESTNEG'); ?>
                                            <input type="radio" name="radiovalQuaRep" id="mandatRep" value="mandatRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_MANDATARIO'); ?>
                                            <input type="radio" name="radiovalQuaRep" id="outraRep" value="outraRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 4) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_OUTRA'); ?>
                                        </div>

                                        <input type="hidden" id="fieldQualidadeRep" name="fieldQualidadeRep" value="<?php echo $qualidadeRep; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldOutraQualRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldOutraQualRep" id="hideFieldOutraQualRep" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_OUTRAQUALIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldOutraQualRep" id="fieldOutraQualRep" maxlength="200" value="<?php echo $outraQualRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            </div>

                        <?php endif; ?>


                        <?php if(in_array('fieldTempoParticipacao',$newFieldsExists) || in_array('fieldInicioParticipacao',$newFieldsExists) || in_array('fieldFimParticipacao',$newFieldsExists) || in_array('fieldTipoInscricao',$newFieldsExists) || in_array('fieldTransporte',$newFieldsExists) || in_array('fieldParagem',$newFieldsExists) || in_array('fieldNomeCrianca',$newFieldsExists) || in_array('fieldNascimentoCrianca',$newFieldsExists) || in_array('fieldCCCrianca',$newFieldsExists) || in_array('fieldResidente',$newFieldsExists) || in_array('fieldEscalao',$newFieldsExists) || in_array('fieldProbSaude',$newFieldsExists) || in_array('fieldLesao',$newFieldsExists) || in_array('fieldMobilidade',$newFieldsExists) || in_array('fieldProbResp',$newFieldsExists) || in_array('fieldProbCard',$newFieldsExists) || in_array('fieldOutroProbSaude',$newFieldsExists) || in_array('fieldAlergia',$newFieldsExists) || in_array('fieldNomeEmergencia',$newFieldsExists) || in_array('fieldTelefoneEmergencia',$newFieldsExists)) :?>

                            <div class="bloco">

                                <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_PEDIDO');?></h3></legend>
                                <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_INTRO_OCUPACAOVERAO');?></p>

                                <?php if(in_array('fieldTempoParticipacao',$newFieldsExists)) :?>
                                    <div class="form-group fieldTempoParticipacao">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TEMPOPARTICIPACAO'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalTempoParticipacao" id="julho" value="julho" <?php if (isset($_POST["submitForm"]) && $_POST['fieldTempoParticipacao'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_JULHO'); ?>
                                            <input type="radio" name="radiovalTempoParticipacao" id="agosto" value="agosto" <?php if (isset($_POST["submitForm"]) && $_POST['fieldTempoParticipacao'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_AGOSTO'); ?>
                                            <input type="radio" name="radiovalTempoParticipacao" id="outroTempoParticipacao" value="outroTempoParticipacao" <?php if (isset($_POST["submitForm"]) && $_POST['fieldTempoParticipacao'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_OUTRA'); ?>
                                        </div>

                                        <input type="hidden" id="fieldTempoParticipacao" name="fieldTempoParticipacao" value="<?php echo $fieldTempoParticipacao; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldInicioParticipacao',$newFieldsExists) || in_array('fieldFimParticipacao',$newFieldsExists)) :?>
                                    <div id="datasTempoParticipacao" style="display:none";?>
                                        <?php if(in_array('fieldInicioParticipacao',$newFieldsExists)) :?>
                                            <div class="form-group fieldInicioParticipacao">
                                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_DE'); ?></label>
                                                <div class="value col-md-12">
                                                    <input type="date" class="form-control" name="fieldInicioParticipacao" id="fieldInicioParticipacao" value="<?php echo $inicioParticipacao; ?>"/>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldFimParticipacao',$newFieldsExists)) :?>
                                            <div class="form-group fieldFimParticipacao">
                                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_A'); ?></label>
                                                <div class="value col-md-12">
                                                    <input type="date" class="form-control" name="fieldFimParticipacao" id="fieldFimParticipacao" value="<?php echo $fimParticipacao; ?>"/>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTipoInscricao',$newFieldsExists)) :?>
                                    <div class="form-group fieldTipoInscricao">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_INSCRICAO'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalInscricao" id="diasCompletos" value="diasCompletos" <?php if (isset($_POST["submitForm"]) && $_POST['fieldTipoInscricao'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_DIASCOMPLETOS'); ?>
                                            <input type="radio" name="radiovalInscricao" id="manhas" value="manhas" <?php if (isset($_POST["submitForm"]) && $_POST['fieldTipoInscricao'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_MANHAS'); ?>
                                        </div>

                                        <input type="hidden" id="fieldTipoInscricao" name="fieldTipoInscricao" value="<?php echo $inscricao; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTransporte',$newFieldsExists)) :?>
                                    <div class="form-group fieldTransporte">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_TRANSPORTE'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalTransporte" id="simTransporte" value="simTransporte" <?php if (isset($_POST["submitForm"]) && $_POST['fieldTransporte'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_SIM'); ?>
                                            <input type="radio" name="radiovalTransporte" id="naoTransporte" value="naoTransporte" <?php if (isset($_POST["submitForm"]) && $_POST['fieldTransporte'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NAO'); ?>
                                        </div>

                                        <input type="hidden" id="fieldTransporte" name="fieldTransporte" value="<?php echo $fieldTransporte; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTransporte',$newFieldsExists) && in_array('fieldParagem',$newFieldsExists)) :?>
                                    <div class="form-group" id="hideFieldParagem" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_PARAGEM'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldParagem" id="fieldParagem" maxlength="200" value="<?php echo $fieldParagem; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNomeCrianca',$newFieldsExists) || in_array('fieldNascimentoCrianca',$newFieldsExists) || in_array('fieldCCCrianca',$newFieldsExists) || in_array('fieldResidente',$newFieldsExists) || in_array('fieldEscalao',$newFieldsExists) || in_array('fieldProbSaude',$newFieldsExists) || in_array('fieldLesao',$newFieldsExists) || in_array('fieldMobilidade',$newFieldsExists) || in_array('fieldProbResp',$newFieldsExists) || in_array('fieldProbCard',$newFieldsExists) || in_array('fieldOutroProbSaude',$newFieldsExists) || in_array('fieldAlergia',$newFieldsExists)) :?>
                                    <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_DADOSCRIANCA');?></p>
                                <?php endif; ?>


                                <?php if(in_array('fieldNomeCrianca',$newFieldsExists)) :?>
                                    <div class="form-group fieldNomeCrianca">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NOMEFIELD'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldNomeCrianca" id="fieldNomeCrianca" maxlength="200" value="<?php echo $nomeCrianca; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNascimentoCrianca',$newFieldsExists)) :?>
                                    <div class="form-group fieldNascimentoCrianca">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NASCIMENTO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="date" class="form-control" name="fieldNascimentoCrianca" id="fieldNascimentoCrianca" value="<?php echo $nascimentoCrianca; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldCCCrianca',$newFieldsExists)) :?>
                                    <div class="form-group fieldCCCrianca">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CC'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldCCCrianca" id="fieldCCCrianca" maxlength="9" value="<?php echo $ccCrianca; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldResidente',$newFieldsExists)) :?>
                                    <div class="form-group fieldResidente">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_RESIDENTECONCELHO'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalResidente" id="simResidente" value="simResidente" <?php if (isset($_POST["submitForm"]) && $_POST['fieldResidente'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_SIM'); ?>
                                            <input type="radio" name="radiovalResidente" id="naoResidente" value="naoResidente" <?php if (isset($_POST["submitForm"]) && $_POST['fieldResidente'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NAO'); ?>
                                        </div>

                                        <input type="hidden" id="fieldResidente" name="fieldResidente" value="<?php echo $fieldResidente; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldEscalao',$newFieldsExists)) :?>
                                    <div class="form-group fieldEscalao">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_ESCALAO'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalEscalao" id="simEscalao" value="simEscalao" <?php if (isset($_POST["submitForm"]) && $_POST['fieldEscalao'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_SIM'); ?>
                                            <input type="radio" name="radiovalEscalao" id="naoEscalao" value="naoEscalao" <?php if (isset($_POST["submitForm"]) && $_POST['fieldEscalao'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NAO'); ?>
                                        </div>

                                        <input type="hidden" id="fieldEscalao" name="fieldEscalao" value="<?php echo $fieldEscalao; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldEscalao',$newFieldsExists) && in_array('fieldQualEscalao',$newFieldsExists)) :?>
                                    <div class="form-group fieldQualEscalao" id="hideFieldQualEscalao" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUAL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldQualEscalao" id="fieldQualEscalao" maxlength="200" value="<?php echo $qualEscalao; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldProbSaude',$newFieldsExists)) :?>
                                    <div class="form-group fieldProbSaude">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_PROBLEMASAUDE'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalProbSaude" id="simProbSaude" value="simProbSaude" <?php if (isset($_POST["submitForm"]) && $_POST['fieldProbSaude'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_SIM'); ?>
                                            <input type="radio" name="radiovalProbSaude" id="naoProbSaude" value="naoProbSaude" <?php if (isset($_POST["submitForm"]) && $_POST['fieldProbSaude'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NAO'); ?>
                                        </div>

                                        <input type="hidden" id="fieldProbSaude" name="fieldProbSaude" value="<?php echo $fieldProbSaude; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldProbSaude',$newFieldsExists) && in_array('fieldQualProbSaude',$newFieldsExists)) :?>
                                    <div class="form-group fieldQualProbSaude" id="hideFieldQualProbSaude" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUAL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldQualProbSaude" id="fieldQualProbSaude" maxlength="200" value="<?php echo $qualProbSaude; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldLesao',$newFieldsExists)) :?>
                                    <div class="form-group fieldLesao">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_LESAO'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalLesao" id="simLesao" value="simLesao" <?php if (isset($_POST["submitForm"]) && $_POST['fieldLesao'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_SIM'); ?>
                                            <input type="radio" name="radiovalLesao" id="naoLesao" value="naoLesao" <?php if (isset($_POST["submitForm"]) && $_POST['fieldLesao'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NAO'); ?>
                                        </div>

                                        <input type="hidden" id="fieldLesao" name="fieldLesao" value="<?php echo $fieldLesao; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldQualLesao',$newFieldsExists) && in_array('fieldLesao',$newFieldsExists)) :?>
                                    <div class="form-group fieldQualLesao" id="hideFieldQualLesao" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUAL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldQualLesao" id="fieldQualLesao" maxlength="200" value="<?php echo $fieldQualLesao; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldMobilidade',$newFieldsExists)) :?>
                                    <div class="form-group fieldMobilidade">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_MOBILIDADE'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalMobilidade" id="simMobilidade" value="simMobilidade" <?php if (isset($_POST["submitForm"]) && $_POST['fieldMobilidade'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_SIM'); ?>
                                            <input type="radio" name="radiovalMobilidade" id="naoMobilidade" value="naoMobilidade" <?php if (isset($_POST["submitForm"]) && $_POST['fieldMobilidade'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NAO'); ?>
                                        </div>

                                        <input type="hidden" id="fieldMobilidade" name="fieldMobilidade" value="<?php echo $fieldMobilidade; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldQualMobilidade',$newFieldsExists) && in_array('fieldMobilidade',$newFieldsExists)) :?>
                                    <div class="form-group" id="hideFieldQualMobilidade" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUAL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldQualMobilidade" id="fieldQualMobilidade" maxlength="200" value="<?php echo $qualMobilidade; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldProbResp',$newFieldsExists)) :?>
                                    <div class="form-group fieldProbResp">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_PROBLEMASRESPIRATORIOS'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalProbResp" id="simProbResp" value="simProbResp" <?php if (isset($_POST["submitForm"]) && $_POST['fieldProbResp'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_SIM'); ?>
                                            <input type="radio" name="radiovalProbResp" id="naoProbResp" value="naoProbResp" <?php if (isset($_POST["submitForm"]) && $_POST['fieldProbResp'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NAO'); ?>
                                        </div>

                                        <input type="hidden" id="fieldProbResp" name="fieldProbResp" value="<?php echo $fieldProbResp; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldProbCard',$newFieldsExists)) :?>
                                    <div class="form-group fieldProbCard">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_PROBLEMASCARDIACOS'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalProbCard" id="simProbCard" value="simProbCard" <?php if (isset($_POST["submitForm"]) && $_POST['fieldProbCard'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_SIM'); ?>
                                            <input type="radio" name="radiovalProbCard" id="naoProbCard" value="naoProbCard" <?php if (isset($_POST["submitForm"]) && $_POST['fieldProbCard'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NAO'); ?>
                                        </div>

                                        <input type="hidden" id="fieldProbCard" name="fieldProbCard" value="<?php echo $fieldProbCard; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldOutroProbSaude',$newFieldsExists)) :?>
                                    <div class="form-group fieldOutroProbSaude">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_OBSPROBSAUDE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldOutroProbSaude" id="fieldOutroProbSaude" maxlength="200" value="<?php echo $obsProbSaude; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldAlergia',$newFieldsExists)) :?>
                                    <div class="form-group fieldAlergia">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_ALERGIA'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalAlergia" id="simAlergia" value="simAlergia" <?php if (isset($_POST["submitForm"]) && $_POST['fieldAlergia'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_SIM'); ?>
                                            <input type="radio" name="radiovalAlergia" id="naoAlergia" value="naoAlergia" <?php if (isset($_POST["submitForm"]) && $_POST['fieldAlergia'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NAO'); ?>
                                        </div>

                                        <input type="hidden" id="fieldAlergia" name="fieldAlergia" value="<?php echo $fieldAlergia; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if((in_array('fieldAlergia',$newFieldsExists) && in_array('fieldTipoAlergia',$newFieldsExists))) :?>

                                    <div id="alergiaDescription" style="display:none;">
                                        <?php if(in_array('fieldTipoAlergia',$newFieldsExists)) :?>
                                            <div class="form-group fieldTipoAlergia">
                                               <div class="col-md-12">
                                                    <input type="radio" name="radiovalTipoAlergia" id="cloro" value="cloro" <?php if (isset($_POST["submitForm"]) && $_POST['fieldTipoAlergia'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CLORO'); ?>
                                                    <input type="radio" name="radiovalTipoAlergia" id="gluten" value="gluten" <?php if (isset($_POST["submitForm"]) && $_POST['fieldTipoAlergia'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_GLUTEN'); ?>
                                                    <input type="radio" name="radiovalTipoAlergia" id="outraAlergia" value="outraAlergia" <?php if (isset($_POST["submitForm"]) && $_POST['fieldTipoAlergia'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_OUTRA'); ?>
                                                </div>

                                                <input type="hidden" id="fieldTipoAlergia" name="fieldTipoAlergia" value="<?php echo $fieldTipoAlergia; ?>">
                                            </div>
                                        <?php endif; ?>


                                        <?php if(in_array('fieldOutraAlergia',$newFieldsExists)) :?>
                                            <div class="form-group fieldOutraAlergia" id="hideFieldOutraAlergia" style="display:none;">
                                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_QUAL'); ?></label>
                                                <div class="value col-md-12">
                                                    <input type="text" class="form-control" name="fieldOutraAlergia" id="fieldOutraAlergia" maxlength="200" value="<?php echo $qualAlergia; ?>"/>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                    </div>

                                <?php endif; ?>


                                <?php if(in_array('fieldNomeEmergencia',$newFieldsExists) || in_array('fieldTelefoneEmergencia',$newFieldsExists)) :?>
                                    <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_EMERGENCIA');?></p>
                                <?php endif; ?>


                                <?php if(in_array('fieldNomeEmergencia',$newFieldsExists)) :?>
                                    <div class="form-group fieldNomeEmergencia">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_NOMEFIELD'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldNomeEmergencia" id="fieldNomeEmergencia" maxlength="200" value="<?php echo $nomeEmergencia; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTelefoneEmergencia',$newFieldsExists)) :?>
                                    <div class="form-group fieldTelefoneEmergencia">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DESPORTOJUVENTUDE_CONTACTOTELEFONICO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldTelefoneEmergencia" id="fieldTelefoneEmergencia" maxlength="9" value="<?php echo $telefoneEmergencia; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            </div>

                        <?php endif; ?>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                    </button>
                                    <a class="btn default"
                                       href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=desportojuventude&layout=menu3niveleventos'); ?>"
                                       title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"     value="<?php echo $obVDCrypt->formInputValueEncrypt('desportojuventude.createinscricaoverao4user',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt('addnewinscricaoverao4user',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('formId',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($formId,$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('userID',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($UserJoomlaID,$setencrypt_forminputhidden); ?>"/>

                        <?php echo JHtml::_('form.token'); ?>

                    </form>

                </div>
            </div>
        </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/desportojuventude/tmpl/addnewinscricaoverao4user.js.php');
    echo ('</script>');
?>