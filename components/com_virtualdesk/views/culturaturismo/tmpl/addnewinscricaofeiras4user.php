<?php
    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteCulturaTurismoHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_culturaturismo.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('culturaturismo');
    if($vbHasAccess===false ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    $tag_form='ZfkMUNsP';
    $formId = VirtualDeskSiteFormmainHelper::getFormId($tag_form);
    $modulo_id=VirtualDeskSiteFormmainHelper::getModuleId('culturaturismo');

    $fieldsForm = VirtualDeskSiteFormmainHelper::getFieldsData($tag_form, $modulo_id);

    $newFieldsExists = array();
    foreach ($fieldsForm as $keyF) {
        $newFieldsExists[] = $keyF['tag'];
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

    //Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteCulturaTurismoHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnewinscricaofeiras4user.culturaturismo.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';

    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
    $UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
    $nome =  $UserProfileData->name;
    $email    = $UserProfileData->email;
    $email2   = $UserProfileData->email;
    $fiscalid = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
    $telefone = $UserProfileData->phone1;
    $morada = $UserProfileData->address;
    $codPostal = $UserProfileData->postalcode;


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=culturaturismo'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=culturaturismo&layout=menu3nivelinscricoes'); ?>"><span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_TAB_INSCRICOES'); ?></span></a>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag($tag_form); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=culturaturismo&layout=menu3nivelinscricoes'); ?>"
                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>">
                    <i class="fa fa-ban"></i>
                    <?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <div class="tabbable-line nav-justified ">

                <div class="tab-content">

                    <form id="new-inscricaofeiras" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=culturaturismo.createinscricaofeiras4user'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_REQUERENTE');?></h3></legend>
                            <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_REQUIRED');?></p>

                            <?php if(in_array('fieldName',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldName">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_NOME'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldName" id="fieldName" maxlength="500" value="<?php echo $nome; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldMorada',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldMorada">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_MORADA'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldMorada" id="fieldMorada" maxlength="500" value="<?php echo $morada; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldPorta',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldPorta">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_NUM_PORTA'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldPorta" id="fieldPorta" maxlength="5" value="<?php echo $fieldPorta; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldLote',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldLote">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_LOTE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldLote" id="fieldLote" maxlength="5" value="<?php echo $fieldLote; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldLocalidade',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldLocalidade">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_LOCALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldLocalidade" id="fieldLocalidade" maxlength="100" value="<?php echo $fieldLocalidade; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldCodPostal',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldCodPostal">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_CODPOSTAL'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldCodPostal" id="fieldCodPostal" maxlength="30" value="<?php echo $codPostal; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldfreguesia',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldfreguesia">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_FREGUESIA'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldfreguesia" id="fieldfreguesia" maxlength="100" value="<?php echo $fieldfreguesia; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldNif',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldNif">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_NIF'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="number" required readonly class="form-control" name="fieldNif" id="fieldNif" maxlength="9" value="<?php echo $fiscalid; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldDocId',$newFieldsExists)) :?>
                                <div class="form-group fieldDocId">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_TIPODOCIDENTIFICACAO'); ?></label>
                                    <div class="col-md-12">
                                        <input type="radio" name="radioval" id="cc" value="cc" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocId'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_CC'); ?>
                                        <input type="radio" name="radioval" id="passaporte" value="passaporte" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocId'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_PASSAPORTE'); ?>
                                    </div>

                                    <input type="hidden" id="fieldDocId" name="fieldDocId" value="<?php echo $fieldDocId; ?>">
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldNumId',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldNumId">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_NUMERO'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldNumId" id="fieldNumId" maxlength="30" value="<?php echo $fieldNumId; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldValId',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldValId">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_VALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="date" class="form-control" name="fieldValId" id="fieldValId" value="<?php echo $fieldValId; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldCCP',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldCCP">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_CCP'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldCCP" id="fieldCCP" maxlength="100" value="<?php echo $fieldCCP; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldTelemovel',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldTelemovel">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_TELEMOVEL'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="number" required class="form-control" name="fieldTelemovel" id="fieldTelemovel" maxlength="9" value="<?php echo $fieldTelemovel; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldTelefone',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldTelefone">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_TELEFONE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldTelefone" id="fieldTelefone" maxlength="9" value="<?php echo $telefone; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldFax',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldFax">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_FAX'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldFax" id="fieldFax" maxlength="9" value="<?php echo $fieldFax; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldEmail',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldEmail">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_EMAIL'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldEmail" id="fieldEmail" maxlength="200" value="<?php echo $email; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldQualidade',$newFieldsExists)) :?>
                                <div class="form-group" id="fieldQualidade">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_QUALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldQualidade" id="fieldQualidade" maxlength="200" value="<?php echo $fieldQualidade; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div>

                        <?php if(in_array('fieldNomeRep',$newFieldsExists) || in_array('fieldMoradaRep',$newFieldsExists) || in_array('fieldPortaRep',$newFieldsExists) || in_array('fieldLoteRep',$newFieldsExists) || in_array('fieldLocalRep',$newFieldsExists) || in_array('fieldCodPostRep',$newFieldsExists) || in_array('fieldfreguesiaRep',$newFieldsExists) || in_array('fieldNifRep',$newFieldsExists) || in_array('fieldDocIdRep',$newFieldsExists) || in_array('fieldNumIdRep',$newFieldsExists) || in_array('fieldValIdRep',$newFieldsExists) || in_array('fieldCCPRep',$newFieldsExists) || in_array('fieldTelemovelRep',$newFieldsExists) || in_array('fieldTelefoneRep',$newFieldsExists) || in_array('fieldFaxRep',$newFieldsExists) || in_array('fieldEmailRep',$newFieldsExists) || in_array('fieldQualidadeRep',$newFieldsExists) || in_array('fieldOutraQualRep',$newFieldsExists)) :?>

                            <div class="bloco">

                                <legend><h3><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_REPRESENTANTE');?></h3></legend>

                                <?php if(in_array('fieldNomeRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldNomeRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_NOME'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldNomeRep" id="fieldNomeRep" maxlength="500" value="<?php echo $fieldNomeRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldMoradaRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldMoradaRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_MORADA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldMoradaRep" id="fieldMoradaRep" maxlength="500" value="<?php echo $fieldMoradaRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldPortaRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldPortaRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_NUM_PORTA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldPortaRep" id="fieldPortaRep" maxlength="5" value="<?php echo $fieldPortaRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldLoteRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldLoteRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_LOTE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldLoteRep" id="fieldLoteRep" maxlength="5" value="<?php echo $fieldLoteRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldLocalRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldLocalRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_LOCALIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldLocalRep" id="fieldLocalRep" maxlength="100" value="<?php echo $fieldLocalRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldCodPostRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldCodPostRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_CODPOSTAL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldCodPostRep" id="fieldCodPostRep" maxlength="30" value="<?php echo $fieldCodPostRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldfreguesiaRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldfreguesiaRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGUA_FREGUESIA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldfreguesiaRep" id="fieldfreguesiaRep" maxlength="100" value="<?php echo $fieldfreguesiaRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNifRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldNifRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_NIF'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldNifRep" id="fieldNifRep" maxlength="9" value="<?php echo $fieldNifRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldDocIdRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldDocIdRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_TIPODOCIDENTIFICACAO'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalRep" id="ccRep" value="ccRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocIdRep'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_CC'); ?>
                                            <input type="radio" name="radiovalRep" id="passaporteRep" value="passaporteRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocIdRep'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_PASSAPORTE'); ?>
                                        </div>

                                        <input type="hidden" id="fieldDocIdRep" name="fieldDocIdRep" value="<?php echo $fieldDocIdRep; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNumIdRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldNumIdRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_NUMERO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldNumIdRep" id="fieldNumIdRep" maxlength="30" value="<?php echo $fieldNumIdRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldValIdRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldValIdRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_VALIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="date" class="form-control" name="fieldValIdRep" id="fieldValIdRep" value="<?php echo $fieldValIdRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldCCPRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldCCPRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_CCP'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldCCPRep" id="fieldCCPRep" maxlength="100" value="<?php echo $fieldCCPRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTelemovelRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldTelemovelRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_TELEMOVEL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldTelemovelRep" id="fieldTelemovelRep" maxlength="9" value="<?php echo $fieldTelemovelRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTelefoneRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldTelefoneRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_TELEFONE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldTelefoneRep" id="fieldTelefoneRep" maxlength="9" value="<?php echo $fieldTelefoneRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldFaxRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldFaxRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_FAX'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldFaxRep" id="fieldFaxRep" maxlength="9" value="<?php echo $fieldFaxRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldEmailRep',$newFieldsExists)) :?>
                                    <div class="form-group" id="fieldEmailRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_EMAIL'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldEmailRep" id="fieldEmailRep" maxlength="200" value="<?php echo $fieldEmailRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldQualidadeRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldQualidadeRep">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_QUALIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="radio" name="radiovalQuaRep" id="repLegalRep" value="repLegalRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_REPLEGAL'); ?>
                                            <input type="radio" name="radiovalQuaRep" id="gestNegRep" value="gestNegRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_GESTNEG'); ?>
                                            <input type="radio" name="radiovalQuaRep" id="mandatRep" value="mandatRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_MANDATARIO'); ?>
                                            <input type="radio" name="radiovalQuaRep" id="outraRep" value="outraRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 4) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_OUTRA'); ?>
                                        </div>

                                        <input type="hidden" id="fieldQualidadeRep" name="fieldQualidadeRep" value="<?php echo $fieldQualidadeRep; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldOutraQualRep',$newFieldsExists)) :?>
                                    <div class="form-group fieldOutraQualRep" id="hideFieldOutraQualRep" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_OUTRAQUALIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldOutraQualRep" id="fieldOutraQualRep" maxlength="200" value="<?php echo $fieldOutraQualRep; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            </div>

                        <?php endif; ?>

                        <?php if(in_array('fieldEvento',$newFieldsExists) || in_array('fieldDataParticipacao',$newFieldsExists) || in_array('fieldHoraParticipacao',$newFieldsExists) || in_array('fieldAtividade',$newFieldsExists)) :?>

                            <div class="bloco">

                                <legend><h3><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_PEDIDO');?></h3></legend>
                                <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_INTRO_INSCRICAOFEIRAS');?></p>


                                <?php if(in_array('fieldEvento',$newFieldsExists)) :?>
                                    <div class="form-group fieldEvento">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_EVENTO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldEvento" id="fieldEvento" maxlength="200" value="<?php echo $evento; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldDataParticipacao',$newFieldsExists)) :?>
                                    <div class="form-group fieldDataParticipacao">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_DATAPARTICIPACAO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="date" class="form-control" name="fieldDataParticipacao" id="fieldDataParticipacao" value="<?php echo $dataParticipacao; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldHoraParticipacao',$newFieldsExists)) :?>
                                    <div class="form-group fieldHoraParticipacao">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_HORAPARTICIPACAO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="time" class="form-control" name="fieldHoraParticipacao" id="fieldHoraParticipacao" value="<?php echo $horaParticipacao; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldAtividade',$newFieldsExists)) :?>
                                    <div class="form-group fieldAtividade">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_CULTURATURISMO_ATIVIDADE'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldAtividade" id="fieldAtividade" maxlength="200" value="<?php echo $atividade; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            </div>

                        <?php endif; ?>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                    </button>
                                    <a class="btn default"
                                       href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=culturaturismo&layout=menu3nivelinscricoes'); ?>"
                                       title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"     value="<?php echo $obVDCrypt->formInputValueEncrypt('culturaturismo.createinscricaofeiras4user',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt('addnewinscricaofeiras4user',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('formId',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($formId,$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('userID',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($UserJoomlaID,$setencrypt_forminputhidden); ?>"/>

                        <?php echo JHtml::_('form.token'); ?>

                    </form>

                </div>
            </div>
        </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/culturaturismo/tmpl/addnewinscricaofeiras4user.js.php');
    echo ('</script>');
?>