<?php
// No direct access
defined('_JEXEC') or die;
JLoader::registerPrefix('VirtualDesk', JPATH_SITE . '/components/com_virtualdesk/');

/**
 * Class VirtualDeskRouter
 *
 * @since  3.3
 */
class VirtualDeskRouter extends JComponentRouterBase
{


    /**
    * Carrega itens de menu associados ao component
     *
    * @return array
    *
    * @since version
    */
    public static function &getItems()
    {
        static $items;
        // Get the menu items for this component.
        if (!isset($items)) {
            $app	= JFactory::getApplication();
            $menu	= $app->getMenu();
            $com	= JComponentHelper::getComponent('com_virtualdesk');
            $items	= $menu->getItems('component_id', $com->id);
            // If no items found, set to empty array.
            if (!$items) {
                $items = array();
            }
        }
        return $items;
    }


    /**
     *
     * @param   array  $query  An array of URL arguments
     *
     * @return	mixed		Integer menu id on success, null on failure.
     * @since	1.6
     * @static
     */
    public static function getMenuItemForViewLayout($query)
    {
        $items = self::getItems();
        $itemid	= null;

        if(empty($query['view'])) $query['view'] = '';
        $queryRouteView = $query['view'];

        if(empty($query['layout'])) $query['layout'] = '';
        $queryRouteLayout = $query['layout'];


        // a partir dos itens e e de acordo com a VIEW e/ou LAYOUT decide se existe um item de menu associado
        foreach ($items as $item) {

            if(empty($item->query['view'])) $item->query['view'] = '';
            $itemQueryView = $item->query['view'];

            if(empty($item->query['layout'])) $item->query['layout'] = '';
            $itemQueryLayout = $item->query['layout'];

            // View PROFILE
            if ($itemQueryView === 'profile' && $queryRouteView==='profile') {
                // Layout : por defeito se estiver vazio vai para o default, que no menu item também é vazio
                if($queryRouteLayout==='default') $queryRouteLayout = '';
                if ($itemQueryLayout === $queryRouteLayout ) {
                    $itemid = $item->id;
                    break;
                }


            //


            }
        }

        return $itemid;
    }




    /**
     * Build method for URLs
     * This method is meant to transform the query parameters into a more human
     * readable form. It is only executed when SEF mode is switched on.
     *
     * Eete método será invocado ao fazer na página/layout/controller:
     *   JRoute::_('index.php?option=com_virtualdesk&view=xxxx
     * OU
     *  $application = JFactory::getApplication();
     *  $router = $application->getRouter();
     *  $componentUri = $router->build('index.php?option=com_virtualdesk&view=xxxxxx');
     *
     * Sequência / Prioridade (view -> layout -> Itemid
     *  1º Verifica se estiver definida a view (e layout associado) tenta encontrar o itemid do menu
     *  2º Só se não vier definida a "view" é que mantém o Itemid forçado no url enviado
     *
     *
     * @param   array  &$query  An array of URL arguments
     *
     * @return  array  The URL arguments to use to assemble the subsequent URL.
     *
     * @since   3.3
     */
    public function build(&$query)
    {
        $segments = array();
        $view     = null;

        // Tenta encontrar o item de menu associado à view+layout atual
        // Só em algum caso especifico é que poderá ser encessário "forçar" o itemid
        $itemid      = self::getMenuItemForViewLayout($query);
        $queryItemid = null;
        if( !empty($query['Itemid']) ) $queryItemid = $query['Itemid'];

        // Se não estiver explicito na query o itemid do menu tenta encontrar.
        if( (!empty($itemid)) && ((int)$itemid <> $queryItemid) ) {
            // Foi encontrado o item de menu associado e é diferente do atual, logo não precisa de enviar a view+layout que ficarão "mais" no url
            unset($query['view']);
            unset($query['layout']);
            unset($query['task']);
            $query['Itemid'] = $itemid;
        }

        if (isset($query['task']))
        {
            $taskParts  = explode('.', $query['task']);
            $segments[] = implode('/', $taskParts);
            $view       = $taskParts[0];
            unset($query['task']);
        }

        if (isset($query['view']))
        {
            $segments[] = $query['view'];
            $view = $query['view'];

            unset($query['view']);
        }

        if (isset($query['id']))
        {
            if ($view !== null)
            {
                $segments[] = $query['id'];
            }
            else
            {
                $segments[] = $query['id'];
            }

            unset($query['id']);
        }


        return $segments;
    }

    /**
     * Parse method for URLs
     * This method is meant to transform the human readable URL back into
     * query parameters. It is only executed when SEF mode is switched on.
     *
     * @param   array  &$segments  The segments of the URL to parse.
     *
     * @return  array  The URL attributes to be used by the application.
     *
     * @since   3.3
     */
    public function parse(&$segments)
    {
        $vars = array();

        // View is always the first element of the array
        $vars['view'] = array_shift($segments);
//        $model        = Teste1HelpersTeste1::getModel($vars['view']);

        while (!empty($segments))
        {
            $segment = array_pop($segments);

            // If it's the ID, let's put on the request
            if (is_numeric($segment))
            {
                $vars['id'] = $segment;
            }
            else
            {
                $vars['task'] = $vars['view'] . '.' . $segment;
            }
        }

        return $vars;
    }
}