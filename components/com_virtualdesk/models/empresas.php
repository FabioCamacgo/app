<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_virtualdesk
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
    JLoader::register('VirtualDeskSiteEmpresasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_empresas.php');


    use Joomla\Registry\Registry;

    /**
     * Empresas model class for Users.
     *
     * @since  1.6
     */
    class VirtualDeskModelEmpresas extends JModelForm
    {
        /**
         * @var		object	The user empresas data.
         * @since   1.6
         */
        protected $data;
        protected $userSessionID;
        protected $userEmpresasData;
        public    $forcedLogout;

        //protected $ObjEmpresasFields;


        public function __construct($config = array())
        {
            $config = array_merge(
                array(
                    'events_map' => array('validate' => 'user')
                ), $config
            );

            $this->forcedLogout = false;

            parent::__construct($config);

        }

        /*  ------- REMOVE ... ??? */

        public function checkin($userId = null)
        {

            return true;
        }

        public function checkout($userId = null)
        {

            return true;
        }

        public function getData()
        {
            return false;
        }

        public function getDataNew()
        {
            return false;

        }

        public function getForm($data = array(), $loadData = true)
        {
            // optamos por apenas carregar os dados, o form é feito na template
            return false;
        }

        /* --------------------- */


        public function setUserIdData()
        {
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
            if ($userSessionID <= 0) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
            }
            $this->userSessionID   = $userSessionID;
            return true;
        }


        public function setUserIdAndData4User($data)
        {

            return true;
        }


        public function setUserIdAndData4Manager($data)
        {
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
            if ($userSessionID <= 0) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
            }

            // Inicializa o array que compara depois se houve alteração de valores
            $loadedData = VirtualDeskSiteEmpresasHelper::getEmpresasDetail4Manager($data['empresa_id']);
            if (empty($loadedData)) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }

            $this->userSessionID = $userSessionID;
            $this->userEmpresasData = $loadedData;
            return true;
        }


        protected function loadFormData()
        {
            // optamos por apenas carregar os dados, o form é feito na template
            return false;
        }


        protected function populateState()
        {
            // Get the application object.
            //$params = JFactory::getApplication()->getParams('com_virtualdesk');
            $params = JFactory::getApplication()->getParams();
            $userId = (int) JFactory::getUser()->get('id');
            // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
            // Set the user id.
            $this->setState('user.id', $userId);
            // Load the parameters.
            $this->setState('params', $params);
        }


        public function getPostedFormData()
        {
            $app         = JFactory::getApplication();
            $data        = array();
            return($data);
        }


        public function getPostedFormData4User()
        {   $app         = JFactory::getApplication();
            $data        = array();



            return($data);

        }


        public function getPostedFormData4Manager()
        {   $app         = JFactory::getApplication();
            $data        = array();

            $empresa_id  = $app->input->getInt('empresa_id');
            if (!empty($empresa_id)) $data['empresa_id'] = $empresa_id;



            $nomeEmpresa            = $app->input->get('nomeEmpresa',null,'STRING');
            $designacaoComercial    = $app->input->get('designacaoComercial',null,'STRING');
            $apresentacao           = $app->input->get('apresentacao',null,'RAW');
            $categoria              = $app->input->get('categoria',null,'STRING');
            $subcategoria           = $app->input->get('subcategoria',null,'STRING');
            $nregisto               = $app->input->get('nregisto',null,'STRING');
            $capacidade             = $app->input->get('capacidade',null,'STRING');
            $unidades               = $app->input->get('unidades',null,'STRING');
            $tipologia              = $app->input->get('tipologia',null,'STRING');
            $nQuartos               = $app->input->get('nQuartos',null,'STRING');
            $nUtentes               = $app->input->get('nUtentes',null,'STRING');
            $nCamas                 = $app->input->get('nCamas',null,'STRING');
            $morada                 = $app->input->get('morada',null,'STRING');
            $coordenadas            = $app->input->get('coordenadas',null,'STRING');
            $codPostal4             = $app->input->get('codPostal4',null,'STRING');
            $codPostal3             = $app->input->get('codPostal3',null,'STRING');
            $codPostalText          = $app->input->get('codPostalText',null,'STRING');
            $freguesia              = $app->input->get('freguesia',null,'STRING');
            $fiscalid               = $app->input->get('fiscalid',null,'STRING');
            $telefone               = $app->input->get('telefone',null,'STRING');
            $email                  = $app->input->get('email',null,'STRING');
            $confEmail              = $app->input->get('confEmail',null,'STRING');
            $website                = $app->input->get('website',null,'STRING');
            $facebook               = $app->input->get('facebook',null,'STRING');
            $instagram              = $app->input->get('instagram',null,'STRING');
            $nomeResponsavel        = $app->input->get('nomeResponsavel',null,'STRING');
            $cargoResponsavel       = $app->input->get('cargoResponsavel',null,'STRING');
            $telefoneResponsavel    = $app->input->get('telefoneResponsavel',null,'STRING');
            $emailResponsavel       = $app->input->get('emailResponsavel',null,'STRING');
            $confEmailResponsavel   = $app->input->get('confEmailResponsavel',null,'STRING');
            $estado                 = $app->input->get('estado',null,'STRING');

            // Verifica se foi alterados algum ficheiro
            $vdFileUpChangedFoto    = $app->input->get('vdFileUpChangedFoto',null,'STRING');
            $vdFileUpChangedLogo    = $app->input->get('vdFileUpChangedLogo',null,'STRING');

            // Campos EXTRA
            // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
            // se os campos estiverem hidden não devem passar
            $arCamposGet = array ();
            $arCamposGet['nomeEmpresa']             = $nomeEmpresa;
            $arCamposGet['designacaoComercial']     = $designacaoComercial;
            $arCamposGet['apresentacao']            = $apresentacao;
            $arCamposGet['categoria']               = $categoria;
            $arCamposGet['subcategoria']            = $subcategoria;
            $arCamposGet['nregisto']                = $nregisto;
            $arCamposGet['capacidade']              = $capacidade;
            $arCamposGet['unidades']                = $unidades;
            $arCamposGet['tipologia']               = $tipologia;
            $arCamposGet['nQuartos']                = $nQuartos;
            $arCamposGet['nUtentes']                = $nUtentes;
            $arCamposGet['nCamas']                  = $nCamas;
            $arCamposGet['morada']                  = $morada;
            $arCamposGet['coordenadas']             = $coordenadas;
            $arCamposGet['codPostal4']              = $codPostal4;
            $arCamposGet['codPostal3']              = $codPostal3;
            $arCamposGet['codPostalText']           = $codPostalText;
            $arCamposGet['freguesia']               = $freguesia;
            $arCamposGet['fiscalid']                = $fiscalid;
            $arCamposGet['telefone']                = $telefone;
            $arCamposGet['email']                   = $email;
            $arCamposGet['confEmail']               = $confEmail;
            $arCamposGet['website']                 = $website;
            $arCamposGet['facebook']                = $facebook;
            $arCamposGet['instagram']               = $instagram;
            $arCamposGet['nomeResponsavel']         = $nomeResponsavel;
            $arCamposGet['cargoResponsavel']        = $cargoResponsavel;
            $arCamposGet['telefoneResponsavel']     = $telefoneResponsavel;
            $arCamposGet['emailResponsavel']        = $emailResponsavel;
            $arCamposGet['confEmailResponsavel']    = $confEmailResponsavel;
            $arCamposGet['estado']                  = $estado;

            $arCamposGet['vdFileUpChangedFoto']     = $vdFileUpChangedFoto;
            $arCamposGet['vdFileUpChangedLogo']     = $vdFileUpChangedLogo;


            //$data = $this->ObjAlertaFields->checkCamposEmpty($arCamposGet, $data);

            $data = $arCamposGet;

            if (!empty($empresa_id)) $data['empresa_id'] = $empresa_id;

            return($data);

        }


        public function getOnlyChangedPostedFormData4User($data)
         {

             return($data);
         }


        public function getOnlyChangedPostedFormData4Manager($data)
        {
            if ( (string)$data['nomeEmpresa']           === (string) $this->userEmpresasData->nomeEmpresa )             unset($data['nomeEmpresa']);
            if ( (string)$data['designacaoComercial']   === (string) $this->userEmpresasData->designacaoComercial )     unset($data['designacaoComercial']);
            if ( (string)$data['apresentacao']          === (string) $this->userEmpresasData->apresentacao )            unset($data['apresentacao']);
            if ( (int)$data['categoria']                === (int) $this->userEmpresasData->categoria )                  unset($data['categoria']);
            if ( (int)$data['subcategoria']             === (int) $this->userEmpresasData->subcategoria )               unset($data['subcategoria']);
            if ( (string)$data['nregisto']              === (string) $this->userEmpresasData->nregisto )                unset($data['nregisto']);
            if ( (string)$data['capacidade']            === (string) $this->userEmpresasData->capacidade )              unset($data['capacidade']);
            if ( (string)$data['unidades']              === (string) $this->userEmpresasData->unidades )                unset($data['unidades']);
            if ( (int)$data['tipologia']                === (int) $this->userEmpresasData->tipologia )                  unset($data['tipologia']);
            if ( (string)$data['nQuartos']              === (string) $this->userEmpresasData->nQuartos )                unset($data['nQuartos']);
            if ( (string)$data['nUtentes']              === (string) $this->userEmpresasData->nUtentes )                unset($data['nUtentes']);
            if ( (string)$data['nCamas']                === (string) $this->userEmpresasData->nCamas )                  unset($data['nCamas']);
            if ( (string)$data['morada']                === (string) $this->userEmpresasData->morada )                  unset($data['morada']);
            if ( (string)$data['coordenadas']           === (string) $this->userEmpresasData->coordenadas )             unset($data['coordenadas']);
            if ( (string)$data['codPostal4']            === (string) $this->userEmpresasData->codPostal4 )              unset($data['codPostal4']);
            if ( (string)$data['codPostal3']            === (string) $this->userEmpresasData->codPostal3 )              unset($data['codPostal3']);
            if ( (string)$data['codPostalText']         === (string) $this->userEmpresasData->codPostalText )           unset($data['codPostalText']);
            if ( (int)$data['freguesia']                === (int) $this->userEmpresasData->freguesia )                  unset($data['freguesia']);
            if ( (string)$data['fiscalid']              === (string) $this->userEmpresasData->fiscalid )                unset($data['fiscalid']);
            if ( (string)$data['telefone']              === (string) $this->userEmpresasData->telefone )                unset($data['telefone']);
            if ( (string)$data['email']                 === (string) $this->userEmpresasData->email )                   unset($data['email']);
            if ( (string)$data['website']               === (string) $this->userEmpresasData->website )                 unset($data['website']);
            if ( (string)$data['facebook']              === (string) $this->userEmpresasData->facebook )                unset($data['facebook']);
            if ( (string)$data['instagram']             === (string) $this->userEmpresasData->instagram )               unset($data['instagram']);
            if ( (string)$data['nomeResponsavel']       === (string) $this->userEmpresasData->nomeResponsavel )         unset($data['nomeResponsavel']);
            if ( (string)$data['cargoResponsavel']      === (string) $this->userEmpresasData->cargoResponsavel )        unset($data['cargoResponsavel']);
            if ( (string)$data['telefoneResponsavel']   === (string) $this->userEmpresasData->telefoneResponsavel )     unset($data['telefoneResponsavel']);
            if ( (string)$data['emailResponsavel']      === (string) $this->userEmpresasData->emailResponsavel )        unset($data['emailResponsavel']);
            if ( (int)$data['estado']                   === (int) $this->userEmpresasData->estado )                     unset($data['estado']);

            if ( (string)$data['vdFileUpChangedCapa']   == '0' )   unset($data['vdFileUpChangedCapa']);
            if ( (string)$data['vdFileUpChangedLogo']   == '0' )   unset($data['vdFileUpChangedLogo']);

            // Se no final só tivermos definido o id, então não devemos atualizar nada
            if( (sizeof($data)==1) && (array_key_exists('empresa_id',$data)) ) unset($data['empresa_id']);

            return($data);
        }


        public function validateBeforeSave4User($data)
        {

            return(true);

        }


        public function validateBeforeSave4Manager($data)
        {
            $msg = '';
            if (empty($data['nomeEmpresa']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_EMPRESASGOV_ERRO_NOMEEMPRESA');
            if (empty($data['designacaoComercial']))  $msg .= "<br>" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_EMPRESASGOV_ERRO_DESIGNACAO');

            if($msg != "" ) {
                JFactory::getApplication()->enqueueMessage($msg, 'error' );
                return false;
            }

            return(true);

        }


        public function save4User($data)
        {


            return true;
        }


        public function save4Manager($data)
        {
            $app = JFactory::getApplication();
            // Check for request forgeries.
            JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

            // check session +  joomla user
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
            if ($userSessionID <= 0) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }

            // check se tem VD user associado and loads VD profile table and fields
            $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
            if ($userVDTable === false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }


            // executa faz o update (bind + save ) no VD user e no joomla user
            $resUpdate = VirtualDeskSiteEmpresasHelper::update4Manager($userSessionID, $userVDTable->id, $data);
            if ($resUpdate===false)
            {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                return false;
            }

            // Flush the data from the session.
            VirtualDeskSiteEmpresasHelper::cleanAllTmpUserState();
            return true;
        }


        public function create4User($data)
        {

            return true;
        }


        public function create4Manager($data)
        {
            $app = JFactory::getApplication();
            // Check for request forgeries.
            JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

            // check session +  joomla user
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
            if ($userSessionID <= 0) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }

            // check se tem VD user associado and loads VD profile table and fields
            $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
            if ($userVDTable === false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }


            // executa faz o update (bind + save ) no VD user e no joomla user
            $resUpdate = VirtualDeskSiteEmpresasHelper::create4Manager($userSessionID, $userVDTable->id, $data);
            if ($resUpdate===false)
            {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                return false;
            }

            // Flush the data from the session.
            VirtualDeskSiteEmpresasHelper::cleanAllTmpUserState();
            return true;
        }


        public function delete4User($data)
        {

            return true;
        }


        public function delete4Manager($data)
        {

            return true;
        }


    }

?>