<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');

use Joomla\Registry\Registry;

/**
 * Agenda model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelAgenda extends JModelForm
{
	/**
	 * @var		object	The user agenda data.
	 * @since   1.6
	 */
	protected $data;
    protected $userSessionID;
    protected $userAgendaData;
    public    $forcedLogout;

    //protected $ObjAgendaFields;


	public function __construct($config = array())
	{
		$config = array_merge(
			array(
				'events_map' => array('validate' => 'user')
			), $config
		);

        $this->forcedLogout = false;

		parent::__construct($config);

	}

	/*  ------- REMOVE ... ??? */

	public function checkin($userId = null)
	{

		return true;
	}

	public function checkout($userId = null)
	{

		return true;
	}

	public function getData()
	{
        return false;
	}

    public function getDataNew()
    {
        return false;

    }

	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	/* --------------------- */


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    public function setUserIdAndData4User($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteAgendaHelper::getAgendaView4UserDetail($data['agenda_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userAgendaData = $loadedData;

        return true;
    }


    public function setUserIdAndData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteAgendaHelper::getAgendaView4ManagerDetail($data['agenda_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userAgendaData = $loadedData;
        return true;
    }


	protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}


	protected function populateState()
	{
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
	}


    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }


    /*Gestão de eventos*/
    public function getPostedFormData4User()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $agenda_id  = $app->input->getInt('agenda_id');
        if (!empty($agenda_id)) $data['agenda_id'] = $agenda_id;

        $nif_evento        = $app->input->get('nif_evento',null,'STRING');
        $email_evento      = $app->input->get('email_evento',null,'STRING');
        $nome_evento       = $app->input->get('nome_evento',null,'STRING');
        $descricao_evento  = $app->input->get('descricao_evento',null,'RAW');
        $concelho       = $app->input->get('concelho_evento',null,'STRING');
        $freguesia      = $app->input->get('freguesia_evento',null,'STRING');
        $data_inicio    = $app->input->get('data_inicio',null,'STRING');
        $hora_inicio    = $app->input->get('hora_inicio',null,'STRING');
        $data_fim       = $app->input->get('data_fim',null,'STRING');
        $hora_fim       = $app->input->get('hora_fim',null,'STRING');
        $categoria      = $app->input->get('categoria',null,'STRING');
        $subcategoria   = $app->input->get('subcategoria',null,'STRING');
        $coordenadas    = $app->input->get('coordenadas',null,'STRING');
        $local_evento   = $app->input->get('local_evento',null,'STRING');
        $preco_evento   = $app->input->get('preco_evento',null,'STRING');
        $facebook       = $app->input->get('facebook',null,'STRING');
        $instagram      = $app->input->get('instagram',null,'STRING');
        $youtube        = $app->input->get('youtube',null,'STRING');
        $vimeo          = $app->input->get('vimeo',null,'STRING');
        $layout_evento  = $app->input->get('layout_evento',null,'STRING');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChangedCapa      = $app->input->get('vdFileUpChangedCapa',null,'STRING');
        $vdFileUpChangedCartaz    = $app->input->get('vdFileUpChangedCartaz',null,'STRING');
        $vdFileUpChangedGaleria   = $app->input->get('vdFileUpChangedGaleria',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['nif_evento']       = $nif_evento;
        $arCamposGet['email_evento']     = $email_evento;
        $arCamposGet['nome_evento']      = $nome_evento;
        $arCamposGet['descricao_evento'] = $descricao_evento;
        $arCamposGet['concelho']         = $concelho;
        $arCamposGet['freguesia']        = $freguesia;
        $arCamposGet['data_inicio']      = $data_inicio;
        $arCamposGet['hora_inicio']      = $hora_inicio;
        $arCamposGet['data_fim']         = $data_fim;
        $arCamposGet['hora_fim']         = $hora_fim;
        $arCamposGet['categoria']        = $categoria;
        $arCamposGet['subcategoria']     = $subcategoria;
        $arCamposGet['coordenadas']      = $coordenadas;
        $arCamposGet['local_evento']     = $local_evento;
        $arCamposGet['preco_evento']     = $preco_evento;
        $arCamposGet['facebook']         = $facebook;
        $arCamposGet['instagram']        = $instagram;
        $arCamposGet['youtube']          = $youtube;
        $arCamposGet['vimeo']            = $vimeo;
        $arCamposGet['layout_evento']    = $layout_evento;

        $arCamposGet['vdFileUpChangedCapa']    = $vdFileUpChangedCapa;
        $arCamposGet['vdFileUpChangedCartaz']  = $vdFileUpChangedCartaz;
        $arCamposGet['vdFileUpChangedGaleria'] = $vdFileUpChangedGaleria;

        //$data = $this->ObjAgendaFields->checkCamposEmpty($arCamposGet, $data);

        $data = $arCamposGet;

        if (!empty($agenda_id)) $data['agenda_id'] = $agenda_id;


        return($data);

    }

    public function getPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $agenda_id  = $app->input->getInt('agenda_id');
        if (!empty($agenda_id)) $data['agenda_id'] = $agenda_id;

        $nif_evento        = $app->input->get('nif_evento',null,'STRING');
        $nomepromotor      = $app->input->get('nomepromotor',null,'STRING');
        $email_evento      = $app->input->get('email_evento',null,'STRING');
        $nome_evento       = $app->input->get('nome_evento',null,'STRING');
        $categoria      = $app->input->get('categoria',null,'STRING');
        $subcategoria   = $app->input->get('subcategoria',null,'STRING');
        $TagsInput     = $app->input->get('TagsInput',null,'RAW');
        $descricao_evento  = $app->input->get('descricao_evento',null,'RAW');
        $data_inicio    = $app->input->get('data_inicio',null,'STRING');
        $hora_inicio    = $app->input->get('hora_inicio',null,'STRING');
        $data_fim       = $app->input->get('data_fim',null,'STRING');
        $hora_fim       = $app->input->get('hora_fim',null,'STRING');
        $concelho       = $app->input->get('concelho',null,'STRING');
        $freguesia_evento      = $app->input->get('freguesia_evento',null,'STRING');
        $local_evento   = $app->input->get('local_evento',null,'STRING');
        $coordenadas    = $app->input->get('coordenadas',null,'STRING');
        $preco   = $app->input->get('preco',null,'STRING');
        $observacoes_precario  = $app->input->get('observacoes_precario',null,'RAW');
        $facebook       = $app->input->get('facebook',null,'STRING');
        $instagram      = $app->input->get('instagram',null,'STRING');
        $youtube        = $app->input->get('youtube',null,'STRING');
        $vimeo          = $app->input->get('vimeo',null,'STRING');
        $VideoInput     = $app->input->get('VideoInput',null,'RAW');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChangedCapa      = $app->input->get('vdFileUpChangedCapa',null,'STRING');
        $vdFileUpChangedCartaz    = $app->input->get('vdFileUpChangedCartaz',null,'STRING');
        $vdFileUpChangedGaleria   = $app->input->get('vdFileUpChangedGaleria',null,'STRING');
        $vdFileUpChangedPrecario   = $app->input->get('vdFileUpChangedPrecario',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['nif_evento']       = $nif_evento;
        $arCamposGet['nomepromotor']       = $nomepromotor;
        $arCamposGet['email_evento']     = $email_evento;
        $arCamposGet['nome_evento']      = $nome_evento;
        $arCamposGet['categoria']        = $categoria;
        $arCamposGet['subcategoria']     = $subcategoria;
        $arCamposGet['TagsInput']        = $TagsInput;
        $arCamposGet['descricao_evento'] = $descricao_evento;
        $arCamposGet['data_inicio']      = $data_inicio;
        $arCamposGet['hora_inicio']      = $hora_inicio;
        $arCamposGet['data_fim']         = $data_fim;
        $arCamposGet['hora_fim']         = $hora_fim;
        $arCamposGet['concelho']         = $concelho;
        $arCamposGet['freguesia_evento'] = $freguesia_evento;
        $arCamposGet['local_evento']     = $local_evento;
        $arCamposGet['coordenadas']      = $coordenadas;
        $arCamposGet['preco']            = $preco;
        $arCamposGet['observacoes_precario'] = $observacoes_precario;
        $arCamposGet['facebook']         = $facebook;
        $arCamposGet['instagram']        = $instagram;
        $arCamposGet['youtube']          = $youtube;
        $arCamposGet['vimeo']            = $vimeo;
        $arCamposGet['VideoInput']       = $VideoInput;

        $arCamposGet['vdFileUpChangedCapa']    = $vdFileUpChangedCapa;
        $arCamposGet['vdFileUpChangedCartaz']  = $vdFileUpChangedCartaz;
        $arCamposGet['vdFileUpChangedGaleria'] = $vdFileUpChangedGaleria;
        $arCamposGet['vdFileUpChangedPrecario'] = $vdFileUpChangedPrecario;

        //$data = $this->ObjAgendaFields->checkCamposEmpty($arCamposGet, $data);

        $data = $arCamposGet;

        if (!empty($agenda_id)) $data['agenda_id'] = $agenda_id;


        return($data);


    }

    public function getOnlyChangedPostedFormData4User($data)
     {
         if ( (string)$data['nif_evento']   === (string) $this->userAgendaData->nif_evento )    unset($data['nif_evento']);
         if ( (string)$data['email_evento'] === (string) $this->userAgendaData->email_evento )  unset($data['email_evento']);
         if ( (string)$data['nome_evento']  === (string) $this->userAgendaData->nome_evento )   unset($data['nome_evento']);
         if ( (string)$data['descricao_evento'] === (string) $this->userAgendaData->descricao_evento )   unset($data['descricao_evento']);
         if ( (int)$data['concelho']    === (int) $this->userAgendaData->id_concelho ) unset($data['concelho']);
         if ( (int)$data['freguesia']    === (int) $this->userAgendaData->id_freguesia ) unset($data['freguesia']);

         if ( (int)$data['data_inicio']    === (int) $this->userAgendaData->data_inicio ) unset($data['data_inicio']);
         if ( (int)$data['hora_inicio']    === (int) $this->userAgendaData->hora_inicio ) unset($data['hora_inicio']);
         if ( (int)$data['data_fim']    === (int) $this->userAgendaData->data_fim ) unset($data['data_fim']);
         if ( (int)$data['hora_fim']    === (int) $this->userAgendaData->hora_fim ) unset($data['hora_fim']);

         if ( (int)$data['categoria']     === (int) $this->userAgendaData->id_categoria )  unset($data['categoria']);
         if ( (int)$data['subcategoria']  === (int) $this->userAgendaData->id_subcategoria )   unset($data['subcategoria']);

         if ( (string)$data['coordenadas']=== ((string)$this->userAgendaData->latitude.','.(string)$this->userAgendaData->longitude) )   unset($data['coordenadas']);

         if ( (int)$data['local_evento']  === (int) $this->userAgendaData->local_evento )   unset($data['local_evento']);
         if ( (int)$data['preco_evento']  === (int) $this->userAgendaData->preco_evento )   unset($data['preco_evento']);
         if ( (int)$data['facebook']      === (int) $this->userAgendaData->facebook )   unset($data['facebook']);
         if ( (int)$data['instagram']     === (int) $this->userAgendaData->instagram )   unset($data['instagram']);
         if ( (int)$data['youtube']       === (int) $this->userAgendaData->youtube )   unset($data['youtube']);
         if ( (int)$data['vimeo']         === (int) $this->userAgendaData->vimeo )   unset($data['vimeo']);
         if ( (int)$data['layout_evento'] === (int) $this->userAgendaData->id_layout_evento )   unset($data['layout_evento']);

         if ( (string)$data['vdFileUpChangedCapa']     == '0' )   unset($data['vdFileUpChangedCapa']);
         if ( (string)$data['vdFileUpChangedCartaz']   == '0' )   unset($data['vdFileUpChangedCartaz']);
         if ( (string)$data['vdFileUpChangedGaleria']  == '0' )   unset($data['vdFileUpChangedGaleria']);
         $vdFileUpChangedPrecario   = $app->input->get('vdFileUpChangedPrecario',null,'STRING');

         // Se no final só tivermos definido o id, então não devemos atualizar nada
         if( (sizeof($data)==1) && (array_key_exists('agenda_id',$data)) ) unset($data['agenda_id']);

         return($data);
     }

    public function getOnlyChangedPostedFormData4Manager($data)
    {
        if ( (string)$data['nif_evento']            === (string) $this->userAgendaData->nif_evento )            unset($data['nif_evento']);
        if ( (string)$data['nome_evento']           === (string) $this->userAgendaData->nome_evento )           unset($data['nome_evento']);
        if ( (string)$data['descricao_evento']      === (string) $this->userAgendaData->descricao_evento )      unset($data['descricao_evento']);
        if ( (int)$data['concelho']                 === (int) $this->userAgendaData->concelho )                 unset($data['concelho']);
        if ( (int)$data['freguesia_evento']         === (int) $this->userAgendaData->freguesia_evento )         unset($data['freguesia_evento']);

        if ( (string)$data['data_inicio']           === (int) $this->userAgendaData->data_inicio )              unset($data['data_inicio']);
        if ( (string)$data['hora_inicio']           === (int) $this->userAgendaData->hora_inicio )              unset($data['hora_inicio']);
        if ( (string)$data['data_fim']              === (int) $this->userAgendaData->data_fim )                 unset($data['data_fim']);
        if ( (string)$data['hora_fim']              === (int) $this->userAgendaData->hora_fim )                 unset($data['hora_fim']);

        if ( (int)$data['categoria']                === (int) $this->userAgendaData->id_categoria )             unset($data['categoria']);
        if ( (int)$data['subcategoria']             === (int) $this->userAgendaData->id_subcategoria )          unset($data['subcategoria']);
        if ( (string)$data['TagsInput']             === (string) $this->userAgendaData->TagsInput )             unset($data['TagsInput']);

        if ( (string)$data['coordenadas']           === ((string)$this->userAgendaData->latitude.','.(string)$this->userAgendaData->longitude) )   unset($data['coordenadas']);

        if ( (string)$data['local_evento']          === (int) $this->userAgendaData->local_evento )             unset($data['local_evento']);
        if ( (int)$data['preco']                    === (int) $this->userAgendaData->preco_evento )             unset($data['preco']);
        if ( (string)$data['observacoes_precario']  === (string) $this->userAgendaData->observacoes_precario )  unset($data['observacoes_precario']);

        if ( (string)$data['facebook']              === (int) $this->userAgendaData->facebook )                 unset($data['facebook']);
        if ( (string)$data['instagram']             === (int) $this->userAgendaData->instagram )                unset($data['instagram']);
        if ( (string)$data['youtube']               === (int) $this->userAgendaData->youtube )                  unset($data['youtube']);
        if ( (string)$data['vimeo']                 === (int) $this->userAgendaData->vimeo )                    unset($data['vimeo']);
        if ( (string)$data['VideoInput']            === (string) $this->userAgendaData->VideoInput )            unset($data['VideoInput']);

        if ( (string)$data['vdFileUpChangedCapa']     == '0' )   unset($data['vdFileUpChangedCapa']);
        if ( (string)$data['vdFileUpChangedCartaz']   == '0' )   unset($data['vdFileUpChangedCartaz']);
        if ( (string)$data['vdFileUpChangedGaleria']  == '0' )   unset($data['vdFileUpChangedGaleria']);
        if ( (string)$data['vdFileUpChangedPrecario']  == '0' )   unset($data['vdFileUpChangedPrecario']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('agenda_id',$data)) ) unset($data['agenda_id']);

        return($data);
    }

    public function validateBeforeSave4User($data)
    {
        $msg = '';
        if (empty($data['nome_evento']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['descricao_evento']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function validateBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['nome_evento']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }

	public function save4User($data)
	{
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::update4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;

        return true;
	}

    public function save4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::update4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;

    }

    public function create4User($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::create4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;
    }

    public function create4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::create4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;
    }

    public function delete4User($data)
    {

        return true;
    }

    public function delete4Manager($data)
    {

        return true;
    }



    /*Gestao de categorias*/
    public function validateCatBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['cat_PT']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_CATPT');
        if (empty($data['cat_EN']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_CATEN');
        if (empty($data['cat_FR']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_CATFR');
        if (empty($data['cat_DE']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_CATDE');
        if (empty($data['Ref']))        $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_CATREF');
        if (empty($data['estado']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_CATESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }

    public function getCatPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $agenda_id  = $app->input->getInt('agenda_id');
        if (!empty($agenda_id)) $data['agenda_id'] = $agenda_id;

        $cat_PT     = $app->input->get('cat_PT',null,'STRING');
        $cat_EN     = $app->input->get('cat_EN',null,'STRING');
        $cat_FR     = $app->input->get('cat_FR',null,'STRING');
        $cat_DE     = $app->input->get('cat_DE',null,'STRING');
        $Ref        = $app->input->get('Ref',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['cat_PT']      = $cat_PT;
        $arCamposGet['cat_EN']      = $cat_EN;
        $arCamposGet['cat_FR']      = $cat_FR;
        $arCamposGet['cat_DE']      = $cat_DE;
        $arCamposGet['Ref']         = $Ref;
        $arCamposGet['estado']      = $estado;

        $data = $arCamposGet;

        if (!empty($agenda_id)) $data['agenda_id'] = $agenda_id;


        return($data);


    }

    public function getOnlyCatChangedPostedFormData4Manager($data)
    {
        if ( (string)$data['cat_PT']        === (string) $this->userAgendaData->cat_PT )    unset($data['cat_PT']);
        if ( (string)$data['cat_EN']        === (string) $this->userAgendaData->cat_EN )   unset($data['cat_EN']);
        if ( (string)$data['cat_FR']        === (string) $this->userAgendaData->cat_FR )   unset($data['cat_FR']);
        if ( (string)$data['cat_DE']        === (string) $this->userAgendaData->cat_DE ) unset($data['cat_DE']);
        if ( (string)$data['Ref']           === (string) $this->userAgendaData->Ref ) unset($data['Ref']);
        if ( (int)$data['estado']           === (int) $this->userAgendaData->idestado ) unset($data['estado']);


        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('agenda_id',$data)) ) unset($data['agenda_id']);

        return($data);
    }

    public function savecat4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::updatecat4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;

    }

    public function createcat4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::createcat4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;
    }



    /*Gestao de subcategorias*/
    public function getSubCatPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $agenda_id  = $app->input->getInt('agenda_id');
        if (!empty($agenda_id)) $data['agenda_id'] = $agenda_id;

        $subcat_PT     = $app->input->get('subcat_PT',null,'STRING');
        $subcat_EN     = $app->input->get('subcat_EN',null,'STRING');
        $subcat_FR     = $app->input->get('subcat_FR',null,'STRING');
        $subcat_DE     = $app->input->get('subcat_DE',null,'STRING');
        $categoria     = $app->input->get('categoria',null,'STRING');
        $estado        = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['subcat_PT']      = $subcat_PT;
        $arCamposGet['subcat_EN']      = $subcat_EN;
        $arCamposGet['subcat_FR']      = $subcat_FR;
        $arCamposGet['subcat_DE']      = $subcat_DE;
        $arCamposGet['categoria']      = $categoria;
        $arCamposGet['estado']         = $estado;

        $data = $arCamposGet;

        if (!empty($agenda_id)) $data['agenda_id'] = $agenda_id;


        return($data);


    }

    public function validateSubCatBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['subcat_PT']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_SUBCATPT');
        if (empty($data['subcat_EN']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_SUBCATEN');
        if (empty($data['subcat_FR']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_SUBCATFR');
        if (empty($data['subcat_DE']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_SUBCATDE');
        if (empty($data['categoria']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_SUBCATCAT');
        if (empty($data['estado']))        $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_SUBCATESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }

    public function getOnlySubCatChangedPostedFormData4Manager($data)
    {
        if ( (string)$data['subcat_PT']        === (string) $this->userAgendaData->subcat_PT )    unset($data['subcat_PT']);
        if ( (string)$data['subcat_EN']        === (string) $this->userAgendaData->subcat_EN )   unset($data['subcat_EN']);
        if ( (string)$data['subcat_FR']        === (string) $this->userAgendaData->subcat_FR )   unset($data['subcat_FR']);
        if ( (string)$data['subcat_DE']        === (string) $this->userAgendaData->subcat_DE )    unset($data['subcat_DE']);
        if ( (int)$data['categoria']           === (int) $this->userAgendaData->categoria ) unset($data['categoria']);
        if ( (int)$data['estado']              === (int) $this->userAgendaData->idestado ) unset($data['estado']);


        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('agenda_id',$data)) ) unset($data['agenda_id']);

        return($data);
    }

    public function savesubcat4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::updatesubcat4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;

    }

    public function createsubcat4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::createsubcat4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;
    }



    /*Gestao Estou com vontade*/
    public function getEstouComVontadePostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $agenda_id  = $app->input->getInt('agenda_id');
        if (!empty($agenda_id)) $data['agenda_id'] = $agenda_id;

        $nome_PT        = $app->input->get('nome_PT',null,'STRING');
        $nome_EN        = $app->input->get('nome_EN',null,'STRING');
        $nome_FR        = $app->input->get('nome_FR',null,'STRING');
        $nome_DE        = $app->input->get('nome_DE',null,'STRING');
        $tipoSel        = $app->input->get('tipoSel',null,'STRING');
        $catAssociadas  = $app->input->get('catAssociadas',null,'ARRAY');
        $subcatAssociadas  = $app->input->get('subcatAssociadas',null,'ARRAY');
        $estado         = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['nome_PT']                 = $nome_PT;
        $arCamposGet['nome_EN']                 = $nome_EN;
        $arCamposGet['nome_FR']                 = $nome_FR;
        $arCamposGet['nome_DE']                 = $nome_DE;
        $arCamposGet['tipoSel']                 = $tipoSel;
        $arCamposGet['catAssociadas']           = $catAssociadas;
        $arCamposGet['subcatAssociadas']        = $subcatAssociadas;
        $arCamposGet['estado']                  = $estado;

        $data = $arCamposGet;

        if (!empty($agenda_id)) $data['agenda_id'] = $agenda_id;


        return($data);


    }

    public function validateEstouComVontadeBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['nome_PT']))        $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_ESTOUCOMVONTADE_NOMEPT');
        if (empty($data['nome_EN']))        $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_ESTOUCOMVONTADE_NOMEEN');
        if (empty($data['nome_FR']))        $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_ESTOUCOMVONTADE_NOMEFR');
        if (empty($data['nome_DE']))        $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_ESTOUCOMVONTADE_NOMEDE');

        if($data['tipoSel'] == '1'){
            if (empty($data['catAssociadas']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_ESTOUCOMVONTADE_CATASSOC');
        } else if($data['tipoSel'] == '2'){
            if (empty($data['subcatAssociadas']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_ESTOUCOMVONTADE_SUBCATASSOC');
        } else if($data['tipoSel'] == '3'){

        } else {
            $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_ESTOUCOMVONTADE_TIPOSELECAO');
        }


        if (empty($data['estado']))         $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ERRO_ESTOUCOMVONTADE_ESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }

    public function createEstouComVontade4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::createEstouComVontade4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;
    }

    public function setUserIdAndEstouComVontadeData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteAgendaHelper::getAgendaViewEstouComVontade4ManagerDetail($data['agenda_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userAgendaData = $loadedData;
        return true;
    }

    public function getOnlyChangedPostedEstouComVontadeFormData4Manager($data)
    {
        if ( (string)$data['nome_PT']           === (string) $this->userAgendaData->nome_PT )           unset($data['nome_PT']);
        if ( (string)$data['nome_EN']           === (string) $this->userAgendaData->nome_EN )           unset($data['nome_EN']);
        if ( (string)$data['nome_FR']           === (string) $this->userAgendaData->nome_FR )           unset($data['nome_FR']);
        if ( (string)$data['nome_DE']           === (string) $this->userAgendaData->nome_DE )           unset($data['nome_DE']);
        if ( (int)$data['tipoSel']              === (int) $this->userAgendaData->tipoSel )              unset($data['tipoSel']);
        if ( (string)$data['catAssociadas']     === (string) $this->userAgendaData->catAssociadas )     unset($data['catAssociadas']);
        if ( (string)$data['subcatAssociadas']  === (string) $this->userAgendaData->subcatAssociadas )  unset($data['subcatAssociadas']);
        if ( (int)$data['estado']               === (int) $this->userAgendaData->estado )               unset($data['estado']);


        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('agenda_id',$data)) ) unset($data['agenda_id']);

        return($data);
    }

    public function saveEstouComVontade4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::updateEstouComVontade4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;
    }




    /*Gestao de empresas*/
    public function getPostedFormEmpresasData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $empresa_id  = $app->input->getInt('empresa_id');
        if (!empty($empresa_id)) $data['empresa_id'] = $empresa_id;


        $referencia                 = $app->input->get('referencia',null,'STRING');
        $nomeEmpresa                = $app->input->get('nomeEmpresa',null,'STRING');
        $designacaoComercial        = $app->input->get('designacaoComercial',null,'STRING');
        $nipcEmpresa                = $app->input->get('nipcEmpresa',null,'STRING');
        $emailContacto              = $app->input->get('emailContacto',null,'STRING');
        $descricao                  = $app->input->get('descricao',null,'RAW');
        $categoria                  = $app->input->get('categoria',null,'STRING');
        $subcategoria               = $app->input->get('subcategoria',null,'ARRAY');
        $checkboxHide1              = $app->input->get('checkboxHide1',null,'STRING');
        $checkboxHide2              = $app->input->get('checkboxHide2',null,'STRING');
        $checkboxHide3              = $app->input->get('checkboxHide3',null,'STRING');
        $checkboxHide4              = $app->input->get('checkboxHide4',null,'STRING');
        $checkboxHide5              = $app->input->get('checkboxHide5',null,'STRING');
        $checkboxHide6              = $app->input->get('checkboxHide6',null,'STRING');
        $checkboxHide7              = $app->input->get('checkboxHide7',null,'STRING');
        $checkboxHide8              = $app->input->get('checkboxHide8',null,'STRING');
        $checkboxHide9              = $app->input->get('checkboxHide9',null,'STRING');
        $checkboxHide10             = $app->input->get('checkboxHide10',null,'STRING');
        $outroTipoEvento            = $app->input->get('outroTipoEvento',null,'STRING');
        $concelho                   = $app->input->get('concelho',null,'STRING');
        $freguesia                  = $app->input->get('freguesia',null,'STRING');
        $morada                     = $app->input->get('morada',null,'STRING');
        $coordenadas                = $app->input->get('coordenadas',null,'STRING');
        $telef                      = $app->input->get('telef',null,'STRING');
        $website                    = $app->input->get('website',null,'STRING');
        $emailComercial             = $app->input->get('emailComercial',null,'STRING');
        $emailAdministrativo        = $app->input->get('emailAdministrativo',null,'STRING');
        $facebook                   = $app->input->get('facebook',null,'STRING');
        $instagram                  = $app->input->get('instagram',null,'STRING');
        $youtube                    = $app->input->get('youtube',null,'STRING');
        $twitter                    = $app->input->get('twitter',null,'STRING');


        // Verifica se foi alterados algum ficheiro
        $vdFileUpChangedCapa        = $app->input->get('vdFileUpChangedCapa',null,'STRING');
        $vdFileUpChangedLogo        = $app->input->get('vdFileUpChangedLogo',null,'STRING');
        $vdFileUpChangedGaleria     = $app->input->get('vdFileUpChangedGaleria',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['referencia']              = $referencia;
        $arCamposGet['nomeEmpresa']             = $nomeEmpresa;
        $arCamposGet['designacaoComercial']     = $designacaoComercial;
        $arCamposGet['nipcEmpresa']             = $nipcEmpresa;
        $arCamposGet['emailContacto']           = $emailContacto;
        $arCamposGet['descricao']               = $descricao;
        $arCamposGet['categoria']               = $categoria;
        $arCamposGet['subcategoria']            = $subcategoria;
        $arCamposGet['checkboxHide1']           = $checkboxHide1;
        $arCamposGet['checkboxHide2']           = $checkboxHide2;
        $arCamposGet['checkboxHide3']           = $checkboxHide3;
        $arCamposGet['checkboxHide4']           = $checkboxHide4;
        $arCamposGet['checkboxHide5']           = $checkboxHide5;
        $arCamposGet['checkboxHide6']           = $checkboxHide6;
        $arCamposGet['checkboxHide7']           = $checkboxHide7;
        $arCamposGet['checkboxHide8']           = $checkboxHide8;
        $arCamposGet['checkboxHide9']           = $checkboxHide9;
        $arCamposGet['checkboxHide10']          = $checkboxHide10;
        $arCamposGet['outroTipoEvento']         = $outroTipoEvento;
        $arCamposGet['concelho']                = $concelho;
        $arCamposGet['freguesia']               = $freguesia;
        $arCamposGet['morada']                  = $morada;
        $arCamposGet['coordenadas']             = $coordenadas;
        $arCamposGet['telef']                   = $telef;
        $arCamposGet['website']                 = $website;
        $arCamposGet['emailComercial']          = $emailComercial;
        $arCamposGet['emailAdministrativo']     = $emailAdministrativo;
        $arCamposGet['facebook']                = $facebook;
        $arCamposGet['instagram']               = $instagram;
        $arCamposGet['youtube']                 = $youtube;
        $arCamposGet['twitter']                 = $twitter;

        $arCamposGet['vdFileUpChangedCapa']     = $vdFileUpChangedCapa;
        $arCamposGet['vdFileUpChangedLogo']     = $vdFileUpChangedLogo;
        $arCamposGet['vdFileUpChangedGaleria']  = $vdFileUpChangedGaleria;


        $data = $arCamposGet;

        if (!empty($empresa_id)) $data['empresa_id'] = $empresa_id;

        return($data);

    }

    public function setUserIdAndEmpresasData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteAgendaHelper::getEmpresaDetail4Manager($data['empresa_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userAgendaData = $loadedData;
        return true;
    }

    public function validateBeforeSaveEmpresas4Manager($data)
    {
        $msg = '';
        if (empty($data['nomeEmpresa']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_ERRO_NOMEEMPRESA');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }

    public function getOnlyChangedPostedEmpresasFormData4Manager($data)
    {
        if ( (string)$data['referencia']                === (string) $this->userAgendaData->referencia )                unset($data['referencia']);
        if ( (string)$data['nomeEmpresa']               === (string) $this->userAgendaData->nomeEmpresa )               unset($data['nomeEmpresa']);
        if ( (string)$data['designacaoComercial']       === (string) $this->userAgendaData->designacaoComercial )       unset($data['designacaoComercial']);
        if ( (string)$data['nipcEmpresa']               === (string) $this->userAgendaData->nipcEmpresa )               unset($data['nipcEmpresa']);
        if ( (string)$data['emailContacto']             === (string) $this->userAgendaData->emailContacto )             unset($data['emailContacto']);
        if ( (int)$data['checkboxHide1']                === (int) $this->userAgendaData->checkboxHide1 )                unset($data['checkboxHide1']);
        if ( (int)$data['checkboxHide2']                === (int) $this->userAgendaData->checkboxHide2 )                unset($data['checkboxHide2']);
        if ( (int)$data['checkboxHide3']                === (int) $this->userAgendaData->checkboxHide3 )                unset($data['checkboxHide3']);
        if ( (int)$data['checkboxHide4']                === (int) $this->userAgendaData->checkboxHide4 )                unset($data['checkboxHide4']);
        if ( (int)$data['checkboxHide5']                === (int) $this->userAgendaData->checkboxHide5 )                unset($data['checkboxHide5']);
        if ( (int)$data['checkboxHide6']                === (int) $this->userAgendaData->checkboxHide6 )                unset($data['checkboxHide6']);
        if ( (int)$data['checkboxHide7']                === (int) $this->userAgendaData->checkboxHide7 )                unset($data['checkboxHide7']);
        if ( (int)$data['checkboxHide8']                === (int) $this->userAgendaData->checkboxHide8 )                unset($data['checkboxHide8']);
        if ( (int)$data['checkboxHide9']                === (int) $this->userAgendaData->checkboxHide9 )                unset($data['checkboxHide9']);
        if ( (int)$data['checkboxHide10']               === (int) $this->userAgendaData->checkboxHide10 )               unset($data['checkboxHide10']);
        if ( (string)$data['outroTipoEvento']           === (string) $this->userAgendaData->outroTipoEvento )           unset($data['outroTipoEvento']);
        if ( (string)$data['descricao']                 === (string) $this->userAgendaData->descricao )                 unset($data['descricao']);
        if ( (int)$data['categoria']                    === (int) $this->userAgendaData->categoria )                    unset($data['categoria']);
        if ( (string)$data['subcategoria']              === (string) $this->userAgendaData->subcategoria )              unset($data['subcategoria']);
        if ( (int)$data['concelho']                     === (int) $this->userAgendaData->concelho )                     unset($data['concelho']);
        if ( (int)$data['freguesia']                    === (int) $this->userAgendaData->freguesia )                    unset($data['freguesia']);
        if ( (string)$data['morada']                    === (string) $this->userAgendaData->morada )                    unset($data['morada']);
        if ( (string)$data['coordenadas']               === ((string)$this->userAgendaData->latitude.','.(string)$this->userAgendaData->longitude) )   unset($data['coordenadas']);
        if ( (string)$data['telef']                     === (string) $this->userAgendaData->telef )                     unset($data['telef']);
        if ( (string)$data['website']                   === (string) $this->userAgendaData->website )                   unset($data['website']);
        if ( (string)$data['emailComercial']            === (string) $this->userAgendaData->emailComercial )            unset($data['emailComercial']);
        if ( (string)$data['emailAdministrativo']       === (string) $this->userAgendaData->emailAdministrativo )       unset($data['emailAdministrativo']);
        if ( (string)$data['facebook']                  === (string) $this->userAgendaData->facebook )                  unset($data['facebook']);
        if ( (string)$data['instagram']                 === (string) $this->userAgendaData->instagram )                 unset($data['instagram']);
        if ( (string)$data['youtube']                   === (string) $this->userAgendaData->youtube )                   unset($data['youtube']);
        if ( (string)$data['twitter']                   === (string) $this->userAgendaData->twitter )                   unset($data['twitter']);

        if ( (string)$data['vdFileUpChangedCapa']     == '0' )   unset($data['vdFileUpChangedCapa']);
        if ( (string)$data['vdFileUpChangedLogo']   == '0' )   unset($data['vdFileUpChangedLogo']);
        if ( (string)$data['vdFileUpChangedGaleria']  == '0' )   unset($data['vdFileUpChangedGaleria']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('empresa_id',$data)) ) unset($data['empresa_id']);

        return($data);
    }

    public function createEmpresas4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::createEmpresas4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;
    }

    public function saveEmpresas4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::updateEmpresas4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;
    }


    /*Patrocinadores*/

    public function getPostedFormDataPatrocinador4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $patrocinador_id  = $app->input->getInt('patrocinador_id');
        if (!empty($patrocinador_id)) $data['patrocinador_id'] = $patrocinador_id;

        $nifpatrocinador    = $app->input->get('nifpatrocinador',null,'STRING');
        $nomepatrocinador   = $app->input->get('nomepatrocinador',null,'STRING');
        $urlpatrocinador    = $app->input->get('urlpatrocinador',null,'STRING');
        $estado             = $app->input->get('estado',null,'STRING');
        $oldNif             = $app->input->get('oldNif',null,'STRING');


        // Verifica se foi alterados algum ficheiro
        $vdFileUpChangedLogo      = $app->input->get('vdFileUpChangedLogo',null,'STRING');

        $arCamposGet = array ();
        $arCamposGet['nifpatrocinador']     = $nifpatrocinador;
        $arCamposGet['nomepatrocinador']    = $nomepatrocinador;
        $arCamposGet['urlpatrocinador']     = $urlpatrocinador;
        $arCamposGet['estado']              = $estado;
        $arCamposGet['oldNif']              = $oldNif;

        $arCamposGet['vdFileUpChangedLogo']    = $vdFileUpChangedLogo;


        $data = $arCamposGet;

        if (!empty($patrocinador_id)) $data['patrocinador_id'] = $patrocinador_id;


        return($data);

    }

    public function validatePatrocinadorBeforeSave4Manager($data){
        $msg = '';
        if (empty($data['nifpatrocinador']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NIF');
        if (empty($data['nomepatrocinador']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');

        $checkNIF = VirtualDeskSiteVAgendaHelper::CheckNifPatrocinador($data['nifpatrocinador']);

        if((int)$checkNIF > 0){
            $msg .= "\n" . JText::_('COM_VIRTUALDESK_VAGENDA_PATROCINADORES_ERROR_NIFEXISTE');
        }

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }

    public function validatePatrocinadorBeforeUpdate4Manager($data)
    {
        $msg = '';
        if (empty($data['nifpatrocinador']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NIF');
        if (empty($data['nomepatrocinador']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');

        if($data['nifpatrocinador'] != $data['oldNif']){
            $checkNIF = VirtualDeskSiteVAgendaHelper::CheckNifPatrocinador($data['nifpatrocinador']);

            if((int)$checkNIF > 0){
                $msg .= "\n" . JText::_('COM_VIRTUALDESK_VAGENDA_PATROCINADORES_ERROR_NIFEXISTE');
            }
        }

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }

    public function createPatrocinador4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::createPatrocinador4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;
    }

    public function setUserIdAndPatrocinadorData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteAgendaHelper::getPatrocinadorDetail4Manager($data['patrocinador_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userAgendaData = $loadedData;
        return true;
    }

    public function getOnlyChangedPostedPatrocinadorFormData4Manager($data)
    {
        if ( (string)$data['referencia']        === (string) $this->userAgendaData->referencia )        unset($data['referencia']);
        if ( (string)$data['nifpatrocinador']   === (string) $this->userAgendaData->nifpatrocinador )   unset($data['nifpatrocinador']);
        if ( (string)$data['nomepatrocinador']  === (string) $this->userAgendaData->nomepatrocinador )  unset($data['nomepatrocinador']);
        if ( (string)$data['urlpatrocinador']   === (string) $this->userAgendaData->urlpatrocinador )   unset($data['urlpatrocinador']);
        if ( (int)$data['estado']               === (string) $this->userAgendaData->estado )            unset($data['estado']);

        if ( (string)$data['vdFileUpChangedLogo']     == '0' )   unset($data['vdFileUpChangedLogo']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('patrocinador_id',$data)) ) unset($data['patrocinador_id']);

        return($data);
    }

    public function savePatrocinador4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::updatePatrocinador4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;
    }


    /*Dias Internacionais*/

    public function getPostedDiasInternacionaisFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $diainternacional_id  = $app->input->getInt('diainternacional_id');
        if (!empty($diainternacional_id)) $data['diainternacional_id'] = $diainternacional_id;

        $nome_evento        = $app->input->get('nome_evento',null,'STRING');
        $descricao_evento   = $app->input->get('descricao_evento',null,'RAW');
        $categoria          = $app->input->get('categoria',null,'STRING');
        $data_evento        = $app->input->get('data_evento',null,'STRING');
        $patrocinador       = $app->input->get('patrocinador',null,'STRING');
        $estado             = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        $arCamposGet = array ();
        $arCamposGet['nome_evento']         = $nome_evento;
        $arCamposGet['descricao_evento']    = $descricao_evento;
        $arCamposGet['categoria']           = $categoria;
        $arCamposGet['data_evento']         = $data_evento;
        $arCamposGet['patrocinador']        = $patrocinador;
        $arCamposGet['estado']              = $estado;

        $data = $arCamposGet;

        if (!empty($diainternacional_id)) $data['diainternacional_id'] = $diainternacional_id;

        return($data);

    }

    public function validateDiasInternacionaisBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['nome_evento']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }

    public function createDiasInternacionais4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::createDiasInternacionais4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;
    }

    public function setUserIdAndDiasInternacionaisData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteAgendaHelper::getDiasInternacionaisDetail4Manager($data['diainternacional_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userAgendaData = $loadedData;
        return true;
    }

    public function getOnlyChangedPostedDiasInternacionaisFormData4Manager($data)
    {
        if ( (string)$data['referencia']        === (string) $this->userAgendaData->referencia )        unset($data['referencia']);
        if ( (string)$data['nome_evento']       === (string) $this->userAgendaData->nome_evento )       unset($data['nome_evento']);
        if ( (string)$data['descricao_evento']  === (string) $this->userAgendaData->descricao_evento )  unset($data['descricao_evento']);
        if ( (string)$data['categoria']         === (string) $this->userAgendaData->categoria )         unset($data['categoria']);
        if ( (string)$data['data_evento']       === (string) $this->userAgendaData->data_evento )       unset($data['data_evento']);
        if ( (string)$data['patrocinador']      === (string) $this->userAgendaData->patrocinador )      unset($data['patrocinador']);
        if ( (int)$data['estado']               === (string) $this->userAgendaData->estado )            unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('diainternacional_id',$data)) ) unset($data['diainternacional_id']);

        return($data);
    }

    public function saveDiasInternacionais4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAgendaHelper::updateDiasInternacionais4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
        return true;
    }

}
