<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
/**
 * HelloWorld Model
 *
 * @since  0.0.1
 */
class VirtualDeskModelVirtualDesk extends JModelItem
{
	/**
	 * @var array messages
	 */
	protected $messages;
 
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   1.6
	 */
	public function getTable($type = 'VirtualDesk', $prefix = 'VirtualDeskTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);

	}
 
	/**
	 * Get the message
	 *
	 * @param   integer  $id  Greeting Id
	 *
	 * @return  string        Fetched String from Table for relevant Id
	 */
	public function getMsg($id = 1)
	{
		if (!is_array($this->messages))
		{
			$this->messages = array();
		}
 
		if (!isset($this->messages[$id]))
		{
			// Request the selected id
			$jinput = JFactory::getApplication()->input;
			$id     = $jinput->get('id', 1, 'INT');
 
			// Get a TableHelloWorld instance
			$table = $this->getTable();
 
			// Load the message
			$table->load($id);
 
			// Assign the message
			$this->messages[$id] = $table->greeting;
		}
 
		return $this->messages[$id];
	}

    public function getForm($data = array(), $loadData = true)
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }

    /* --------------------- */


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }



    protected function loadFormData()
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }


    protected function populateState()
    {
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
    }


    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }

}