<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteSuporteHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_suporte.php');


use Joomla\Registry\Registry;

/**
 * Suporte model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelSuporte extends JModelForm
{
    /**
     * @var		object	The user suporte data.
     * @since   1.6
     */
    protected $data;
    protected $userSessionID;
    protected $userSuporteData;
    public    $forcedLogout;

    //protected $ObjSuporteFields;


    public function __construct($config = array())
    {
        $config = array_merge(
            array(
                'events_map' => array('validate' => 'user')
            ), $config
        );

        $this->forcedLogout = false;

        parent::__construct($config);

    }

    /*  ------- REMOVE ... ??? */

    public function checkin($userId = null)
    {

        return true;
    }

    public function checkout($userId = null)
    {

        return true;
    }

    public function getData()
    {
        return false;
    }

    public function getDataNew()
    {
        return false;

    }

    public function getForm($data = array(), $loadData = true)
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }

    /* --------------------- */


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    public function setUserIdAndData4User($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteSuporteHelper::getSuporteView4UserDetail($data['suporte_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userSuporteData = $loadedData;

        return true;
    }


    public function setUserIdAndData4Admin($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteSuporteHelper::getSuporteView4AdminDetail($data['suporte_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userSuporteData = $loadedData;
        return true;
    }


    public function setUserIdAndData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteSuporteHelper::getSuporteView4ManagerDetail($data['suporte_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userSuporteData = $loadedData;
        return true;
    }


    protected function loadFormData()
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }


    protected function populateState()
    {
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
    }


    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }


    public function getPostedFormData4User()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $suporte_id  = $app->input->getInt('suporte_id');
        if (!empty($suporte_id)) $data['suporte_id'] = $suporte_id;

        $assunto         = $app->input->get('assunto',null,'STRING');
        $descricao       = $app->input->get('descricao',null,'RAW');
        $departamento    = $app->input->get('departamento',null,'STRING');
        $observacoes     = $app->input->get('observacoes',null,'RAW');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChanged      = $app->input->get('vdFileUpChanged',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();

        $arCamposGet['assunto']         = $assunto;
        $arCamposGet['descricao']       = $descricao;
        $arCamposGet['departamento']    = $departamento;
        $arCamposGet['observacoes']     = $observacoes;

        $arCamposGet['vdFileUpChanged'] = $vdFileUpChanged;

        //$data = $this->ObjSuporteFields->checkCamposEmpty($arCamposGet, $data);
        $data = $arCamposGet;
        if (!empty($suporte_id)) $data['suporte_id'] = $suporte_id;
        return($data);
    }


    public function getPostedFormData4Admin()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $suporte_id  = $app->input->getInt('suporte_id');
        if (!empty($suporte_id)) $data['suporte_id'] = $suporte_id;

        $utilizador      = $app->input->get('utilizador',null,'STRING');
        $assunto         = $app->input->get('assunto',null,'STRING');
        $descricao       = $app->input->get('descricao',null,'RAW');
        $departamento    = $app->input->get('departamento',null,'STRING');
        $observacoes     = $app->input->get('observacoes',null,'RAW');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChanged      = $app->input->get('vdFileUpChanged',null,'STRING');

        /// Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();

        $arCamposGet['utilizador']      = $utilizador;
        $arCamposGet['assunto']         = $assunto;
        $arCamposGet['descricao']       = $descricao;
        $arCamposGet['departamento']    = $departamento;
        $arCamposGet['observacoes']     = $observacoes;

        $arCamposGet['vdFileUpChanged'] = $vdFileUpChanged;

        //$data = $this->ObjSuporteFields->checkCamposEmpty($arCamposGet, $data);
        $data = $arCamposGet;
        if (!empty($suporte_id)) $data['suporte_id'] = $suporte_id;
        return($data);
    }


    public function getPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $suporte_id  = $app->input->getInt('suporte_id');
        if (!empty($suporte_id)) $data['suporte_id'] = $suporte_id;

        //$utilizador      = $app->input->get('utilizador',null,'STRING');
        $assunto         = $app->input->get('assunto',null,'STRING');
        $descricao       = $app->input->get('descricao',null,'RAW');
        $departamento    = $app->input->get('departamento',null,'STRING');
        $observacoes     = $app->input->get('observacoes',null,'RAW');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChanged      = $app->input->get('vdFileUpChanged',null,'STRING');

        /// Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();

        //$arCamposGet['utilizador']      = $utilizador;
        $arCamposGet['assunto']         = $assunto;
        $arCamposGet['descricao']       = $descricao;
        $arCamposGet['departamento']    = $departamento;
        $arCamposGet['observacoes']     = $observacoes;

        $arCamposGet['vdFileUpChanged'] = $vdFileUpChanged;

        //$data = $this->ObjSuporteFields->checkCamposEmpty($arCamposGet, $data);
        $data = $arCamposGet;
        if (!empty($suporte_id)) $data['suporte_id'] = $suporte_id;
        return($data);
    }


    public function getOnlyChangedPostedFormData4User($data)
    {
        if ( (string)$data['assunto']        === (string) $this->userSuporteData->assunto )      unset($data['assunto']);
        if ( (string)$data['descricao']      === (string) $this->userSuporteData->descricao )     unset($data['descricao']);
        if ( (int)$data['departamento']      === (int) $this->userSuporteData->id_departamento )   unset($data['departamento']);
        if ( (string)$data['observacoes']    === (string) $this->userSuporteData->observacoes )   unset($data['observacoes']);

        if ( (string)$data['vdFileUpChanged']     == '0' )   unset($data['vdFileUpChanged']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('suporte_id',$data)) ) unset($data['suporte_id']);

        return($data);
    }


    public function getOnlyChangedPostedFormData4Admin($data)
    {
        if ( (int)$data['utilizador']         === (int) $this->userSuporteData->iduser )     unset($data['utilizador']);
        if ( (string)$data['assunto']         === (string) $this->userSuporteData->assunto )      unset($data['assunto']);
        if ( (string)$data['descricao']       === (string) $this->userSuporteData->descricao )     unset($data['descricao']);
        if ( (int)$data['departamento']       === (int) $this->userSuporteData->id_departamento )   unset($data['departamento']);
        if ( (string)$data['observacoes']     === (string) $this->userSuporteData->observacoes )   unset($data['observacoes']);

        if ( (string)$data['vdFileUpChanged'] == '0' )   unset($data['vdFileUpChanged']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('suporte_id',$data)) ) unset($data['suporte_id']);

        return($data);
    }


    public function getOnlyChangedPostedFormData4Manager($data)
    {
        //if ( (int)$data['utilizador']         === (int) $this->userSuporteData->iduser )     unset($data['utilizador']);
        if ( (string)$data['assunto']         === (string) $this->userSuporteData->assunto )      unset($data['assunto']);
        if ( (string)$data['descricao']       === (string) $this->userSuporteData->descricao )     unset($data['descricao']);
        if ( (int)$data['departamento']       === (int) $this->userSuporteData->id_departamento )   unset($data['departamento']);
        if ( (string)$data['observacoes']     === (string) $this->userSuporteData->observacoes )   unset($data['observacoes']);

        if ( (string)$data['vdFileUpChanged'] == '0' )   unset($data['vdFileUpChanged']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('suporte_id',$data)) ) unset($data['suporte_id']);

        return($data);
    }


    public function validateBeforeSave4User($data)
    {

        $msg = '';

        // Se o estado não for Inicia , o user não deve poder editar
        $IdEstadoInicial = (int) VirtualDeskSiteSuporteHelper::getEstadoIdInicio();
        if ((int)$data['idestado']<>$IdEstadoInicial && (int)$data['idestado']>0) $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_CANNOTEDITONSTATE');

        if (empty($data['assunto']))       $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_FIELDREQUIRED_ASSUNTO') ;
        if (empty($data['descricao']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_FIELDREQUIRED_DESCRICAO') ;
        //if ((int)$data['departamento']<=0) $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_FIELDREQUIRED_DEPARTAMENTO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }


    public function validateBeforeSave4Admin($data)
    {
        $msg = '';
        if ((int)$data['utilizador']<=0) $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_FIELDREQUIRED_UTILIZADOR');
        if (empty($data['assunto']))       $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_FIELDREQUIRED_ASSUNTO') ;
        if (empty($data['descricao']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_FIELDREQUIRED_DESCRICAO') ;
       // if ((int)$data['departamento']<=0) $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_FIELDREQUIRED_DEPARTAMENTO');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function validateBeforeSave4Manager($data)
    {
        $msg = '';

        // Se o estado não for Inicia , o user não deve poder editar
        $IdEstadoInicial = (int) VirtualDeskSiteSuporteHelper::getEstadoIdInicio();
        if ((int)$data['idestado']<>$IdEstadoInicial && (int)$data['idestado']) $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_CANNOTEDITONSTATE');

        //if ((int)$data['utilizador']<=0) $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_FIELDREQUIRED_UTILIZADOR');
        if (empty($data['assunto']))       $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_FIELDREQUIRED_ASSUNTO') ;
        if (empty($data['descricao']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_FIELDREQUIRED_DESCRICAO') ;
        //if ((int)$data['departamento']<=0) $msg .= "\n" . JText::_('COM_VIRTUALDESK_SUPORTE_FIELDREQUIRED_DEPARTAMENTO');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function save4User($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteSuporteHelper::update4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteSuporteHelper::cleanAllTmpUserState();
        return true;

        return true;
    }


    public function save4Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteSuporteHelper::update4Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteSuporteHelper::cleanAllTmpUserState();
        return true;

    }


    public function save4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteSuporteHelper::update4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteSuporteHelper::cleanAllTmpUserState();
        return true;

    }


    public function create4User($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteSuporteHelper::create4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteSuporteHelper::cleanAllTmpUserState();
        return true;

    }


    public function create4Admin($data)
    {

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteSuporteHelper::create4Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteSuporteHelper::cleanAllTmpUserState();
        return true;
    }


    public function create4Manager($data)
    {

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteSuporteHelper::create4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteSuporteHelper::cleanAllTmpUserState();
        return true;
    }


    public function delete4User($data)
    {

        return true;
    }


    public function delete4Manager($data)
    {

        return true;
    }


}
