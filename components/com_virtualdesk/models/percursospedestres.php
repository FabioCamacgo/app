<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSitePercursosPedestresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/virtualdesksite_percursospedestres.php');


use Joomla\Registry\Registry;

/**
 * Percursos Pedestres model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelPercursosPedestres extends JModelForm
{
    /**
     * @var		object	The user Percursos Pedestres data.
     * @since   1.6
     */
    protected $data;
    protected $userSessionID;
    protected $userPercursosPedestresData;
    public    $forcedLogout;

    //protected $ObjPercursosPedestresFields;


    public function __construct($config = array())
    {
        $config = array_merge(
            array(
                'events_map' => array('validate' => 'user')
            ), $config
        );

        $this->forcedLogout = false;

        parent::__construct($config);

    }


    public function checkin($userId = null)
    {

        return true;
    }


    public function checkout($userId = null)
    {

        return true;
    }


    public function getData()
    {
        return false;
    }


    public function getDataNew()
    {
        return false;

    }


    public function getForm($data = array(), $loadData = true)
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    public function setUserIdAndData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSitePercursosPedestresHelper::getPercursosDetail4Manager($data['percursospedestres_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userPercursosPedestresData = $loadedData;
        return true;
    }


    protected function loadFormData()
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }


    protected function populateState()
    {
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
    }


    public function getPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $percursospedestres_id  = $app->input->getInt('percursospedestres_id');
        if (!empty($percursospedestres_id)) $data['percursospedestres_id'] = $percursospedestres_id;

        $nomepercurso               = $app->input->get('nomepercurso',null,'STRING');
        $descricao                  = $app->input->get('descricao',null,'RAW');
        $descricaoEN                = $app->input->get('descricaoEN',null,'RAW');
        $descricaoFR                = $app->input->get('descricaoFR',null,'RAW');
        $descricaoDE                = $app->input->get('descricaoDE',null,'RAW');
        $tipologia                  = $app->input->get('tipologia',null,'STRING');
        $tipoPaisagem               = $app->input->get('tipoPaisagem',null,'STRING');
        $percLaurissilvaValue       = $app->input->get('percLaurissilvaValue',null,'STRING');
        $costLaurissilvaValue       = $app->input->get('costLaurissilvaValue',null,'STRING');
        $costSolValue               = $app->input->get('costSolValue',null,'STRING');
        $mmgValue                   = $app->input->get('mmgValue',null,'STRING');
        $sentido                    = $app->input->get('sentido',null,'STRING');
        $terreno                    = $app->input->get('terreno',null,'STRING');
        $vertigens                  = $app->input->get('vertigens',null,'STRING');
        $dificuldade                = $app->input->get('dificuldade',null,'STRING');
        $distancia                  = $app->input->get('distancia',null,'STRING');
        $horas                      = $app->input->get('horas',null,'STRING');
        $minutos                    = $app->input->get('minutos',null,'STRING');
        $altitudemax                = $app->input->get('altitudemax',null,'STRING');
        $altitudemin                = $app->input->get('altitudemin',null,'STRING');
        $subidaAcumulado            = $app->input->get('subidaAcumulado',null,'STRING');
        $descidaAcumulado           = $app->input->get('descidaAcumulado',null,'STRING');
        $distrito                   = $app->input->get('distrito',null,'STRING');
        $concelho                   = $app->input->get('concelho',null,'STRING');
        $freguesia                  = $app->input->get('freguesia',null,'STRING');
        $zonamento                  = $app->input->get('zonamento',null,'STRING');
        $freguesiaInicio            = $app->input->get('freguesiaInicio',null,'STRING');
        $freguesiaFim               = $app->input->get('freguesiaFim',null,'STRING');
        $sitioInicio                = $app->input->get('sitioInicio',null,'STRING');
        $sitioFim                   = $app->input->get('sitioFim',null,'STRING');
        $localidades                = $app->input->get('localidades',null,'RAW');
        $nomecredito                = $app->input->get('nomecredito',null,'STRING');
        $linkcredito                = $app->input->get('linkcredito',null,'STRING');
        $DestaqueInput              = $app->input->get('DestaqueInput',null,'RAW');
        $HastagsInput               = $app->input->get('HastagsInput',null,'RAW');
        $patrocinador               = $app->input->get('patrocinador',null,'STRING');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChangedCapa      = $app->input->get('vdFileUpChangedCapa',null,'STRING');
        $vdFileUpChangedGaleria    = $app->input->get('vdFileUpChangedGaleria',null,'STRING');
        $vdFileUpChangedGPX   = $app->input->get('vdFileUpChangedGPX',null,'STRING');
        $vdFileUpChangedKML   = $app->input->get('vdFileUpChangedKML',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['nomepercurso']            = $nomepercurso;
        $arCamposGet['descricao']               = $descricao;
        $arCamposGet['descricaoEN']             = $descricaoEN;
        $arCamposGet['descricaoFR']             = $descricaoFR;
        $arCamposGet['descricaoDE']             = $descricaoDE;
        $arCamposGet['tipologia']               = $tipologia;
        $arCamposGet['tipoPaisagem']            = $tipoPaisagem;
        $arCamposGet['percLaurissilvaValue']    = $percLaurissilvaValue;
        $arCamposGet['costLaurissilvaValue']    = $costLaurissilvaValue;
        $arCamposGet['costSolValue']            = $costSolValue;
        $arCamposGet['mmgValue']                = $mmgValue;
        $arCamposGet['sentido']                 = $sentido;
        $arCamposGet['terreno']                 = $terreno;
        $arCamposGet['vertigens']               = $vertigens;
        $arCamposGet['dificuldade']             = $dificuldade;
        $arCamposGet['distancia']               = $distancia;
        $arCamposGet['horas']                   = $horas;
        $arCamposGet['minutos']                 = $minutos;
        $arCamposGet['altitudemax']             = $altitudemax;
        $arCamposGet['altitudemin']             = $altitudemin;
        $arCamposGet['subidaAcumulado']         = $subidaAcumulado;
        $arCamposGet['descidaAcumulado']        = $descidaAcumulado;
        $arCamposGet['distrito']                = $distrito;
        $arCamposGet['concelho']                = $concelho;
        $arCamposGet['freguesia']               = $freguesia;
        $arCamposGet['zonamento']               = $zonamento;
        $arCamposGet['freguesiaInicio']         = $freguesiaInicio;
        $arCamposGet['freguesiaFim']            = $freguesiaFim;
        $arCamposGet['sitioInicio']             = $sitioInicio;
        $arCamposGet['sitioFim']                = $sitioFim;
        $arCamposGet['localidades']             = $localidades;
        $arCamposGet['nomecredito']             = $nomecredito;
        $arCamposGet['linkcredito']             = $linkcredito;
        $arCamposGet['DestaqueInput']           = $DestaqueInput;
        $arCamposGet['HastagsInput']            = $HastagsInput;
        $arCamposGet['patrocinador']            = $patrocinador;

        $arCamposGet['vdFileUpChangedCapa']     = $vdFileUpChangedCapa;
        $arCamposGet['vdFileUpChangedGaleria']  = $vdFileUpChangedGaleria;
        $arCamposGet['vdFileUpChangedGPX']      = $vdFileUpChangedGPX;
        $arCamposGet['vdFileUpChangedKML']      = $vdFileUpChangedKML;

        $data = $arCamposGet;

        if (!empty($percursospedestres_id)) $data['percursospedestres_id'] = $percursospedestres_id;

        return($data);


    }


    public function validateBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['nomepercurso']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function create4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::create4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;
    }


    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }


    public function getOnlyChangedPostedFormData4Manager($data)
    {
        if ( (string)$data['nomepercurso']      === (string) $this->userPercursosPedestresData->nomepercurso )    unset($data['nomepercurso']);
        if ( (string)$data['descricao']         === (string) $this->userPercursosPedestresData->descricao )    unset($data['descricao']);
        if ( (string)$data['descricaoEN']       === (string) $this->userPercursosPedestresData->descricaoEN )    unset($data['descricaoEN']);
        if ( (string)$data['descricaoFR']       === (string) $this->userPercursosPedestresData->descricaoFR )    unset($data['descricaoFR']);
        if ( (string)$data['descricaoDE']       === (string) $this->userPercursosPedestresData->descricaoDE )    unset($data['descricaoDE']);
        if ( (int)$data['tipologia']            === (int) $this->userPercursosPedestresData->tipologia )    unset($data['tipologia']);
        if ( (int)$data['tipoPaisagem']         === (int) $this->userPercursosPedestresData->tipoPaisagem )    unset($data['tipoPaisagem']);
        if ( (int)$data['percLaurissilvaValue'] === (int) $this->userPercursosPedestresData->percLaurissilvaValue )    unset($data['percLaurissilvaValue']);
        if ( (int)$data['costLaurissilvaValue'] === (int) $this->userPercursosPedestresData->costLaurissilvaValue )    unset($data['costLaurissilvaValue']);
        if ( (int)$data['costSolValue']         === (int) $this->userPercursosPedestresData->costSolValue )    unset($data['costSolValue']);
        if ( (int)$data['mmgValue']             === (int) $this->userPercursosPedestresData->mmgValue )    unset($data['mmgValue']);
        if ( (int)$data['sentido']              === (int) $this->userPercursosPedestresData->sentido )    unset($data['sentido']);
        if ( (int)$data['terreno']              === (int) $this->userPercursosPedestresData->terreno )    unset($data['terreno']);
        if ( (int)$data['vertigens']            === (int) $this->userPercursosPedestresData->vertigens )    unset($data['vertigens']);
        if ( (int)$data['dificuldade']          === (int) $this->userPercursosPedestresData->dificuldade )   unset($data['dificuldade']);
        if ( (string)$data['distancia']         === (string) $this->userPercursosPedestresData->distancia )   (string)$data['distancia'] = $data['distancia'];
        if ( (string)$data['horas']             === (string) $this->userPercursosPedestresData->horas ) unset($data['horas']);
        if ( (string)$data['minutos']           === (string) $this->userPercursosPedestresData->minutos ) unset($data['minutos']);
        if ( (string)$data['altitudemax']       === (string) $this->userPercursosPedestresData->altitudemax ) unset($data['altitudemax']);
        if ( (string)$data['altitudemin']       === (string) $this->userPercursosPedestresData->altitudemin ) unset($data['altitudemin']);
        if ( (string)$data['subidaAcumulado']   === (string) $this->userPercursosPedestresData->subidaAcumulado ) unset($data['subidaAcumulado']);
        if ( (string)$data['descidaAcumulado']  === (string) $this->userPercursosPedestresData->descidaAcumulado ) unset($data['descidaAcumulado']);
        if ( (int)$data['distrito']             === (int) $this->userPercursosPedestresData->distrito )  {
            unset($data['distrito']);
        } else {
            if ( (int)$data['concelho']             === (int) $this->userPercursosPedestresData->concelho ){
                $data['concelho'] = 0;
                $data['freguesia'] = 0;
            } else {
                if ( (int)$data['freguesia']            === (int) $this->userPercursosPedestresData->freguesia ){
                    $data['freguesia'] = 0;
                }
            }


        }
        if ( (int)$data['zonamento']            === (int) $this->userPercursosPedestresData->zonamento )   unset($data['zonamento']);
        if ( (int)$data['freguesiaInicio']      === (int) $this->userPercursosPedestresData->freguesiaInicio )   unset($data['freguesiaInicio']);
        if ( (int)$data['freguesiaFim']         === (int) $this->userPercursosPedestresData->freguesiaFim )   unset($data['freguesiaFim']);
        if ( (string)$data['sitioInicio']       === (string) $this->userPercursosPedestresData->sitioInicio )   unset($data['sitioInicio']);
        if ( (string)$data['sitioFim']          === (string) $this->userPercursosPedestresData->sitioFim )   unset($data['sitioFim']);
        if ( (string)$data['localidades']       === (string) $this->userPercursosPedestresData->localidades )   unset($data['localidades']);
        if ( (string)$data['nomecredito']       === (string) $this->userPercursosPedestresData->nomecredito )   unset($data['nomecredito']);
        if ( (string)$data['linkcredito']       === (string) $this->userPercursosPedestresData->linkcredito )   unset($data['linkcredito']);
        if ( (string)$data['DestaqueInput']     === (string) $this->userPercursosPedestresData->DestaqueInput )   unset($data['DestaqueInput']);
        if ( (string)$data['HastagsInput']      === (string) $this->userPercursosPedestresData->HastagsInput )   unset($data['HastagsInput']);
        if ( (int)$data['patrocinador']         === (int) $this->userPercursosPedestresData->patrocinador )   unset($data['patrocinador']);

        if ( (string)$data['vdFileUpChangedCapa']     == '0' )   unset($data['vdFileUpChangedCapa']);
        if ( (string)$data['vdFileUpChangedGaleria']   == '0' )   unset($data['vdFileUpChangedGaleria']);
        if ( (string)$data['vdFileUpChangedGPX']  == '0' )   unset($data['vdFileUpChangedGPX']);
        if ( (string)$data['vdFileUpChangedKML']  == '0' )   unset($data['vdFileUpChangedKML']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('percursospedestres_id',$data)) ) unset($data['percursospedestres_id']);

        return($data);
    }


    public function save4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::update4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;

    }


    public function delete4Manager($data)
    {

        return true;
    }


    public function getPostedTipologiaFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $tipologia_id  = $app->input->getInt('tipologia_id');
        if (!empty($tipologia_id)) $data['tipologia_id'] = $tipologia_id;

        $name_PT    = $app->input->get('name_PT',null,'STRING');
        $name_EN    = $app->input->get('name_EN',null,'STRING');
        $name_FR    = $app->input->get('name_FR',null,'STRING');
        $name_DE    = $app->input->get('name_DE',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['name_PT'] = $name_PT;
        $arCamposGet['name_EN'] = $name_EN;
        $arCamposGet['name_FR'] = $name_FR;
        $arCamposGet['name_DE'] = $name_DE;
        $arCamposGet['estado']  = $estado;

        $data = $arCamposGet;

        if (!empty($tipologia_id)) $data['tipologia_id'] = $tipologia_id;

        return($data);


    }


    public function getOnlyChangedPostedTipologiaFormData4Manager($data)
    {
        if ( (string)$data['name_PT']   === (string) $this->userPercursosPedestresData->name_PT )    unset($data['name_PT']);
        if ( (string)$data['name_EN']   === (string) $this->userPercursosPedestresData->name_EN )    unset($data['name_EN']);
        if ( (string)$data['name_FR']   === (string) $this->userPercursosPedestresData->name_FR )    unset($data['name_FR']);
        if ( (string)$data['name_DE']   === (string) $this->userPercursosPedestresData->name_DE )    unset($data['name_DE']);
        if ( (int)$data['estado']       === (int) $this->userPercursosPedestresData->estado )    unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('tipologia_id',$data)) ) unset($data['tipologia_id']);

        return($data);
    }


    public function validateTipologiaBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['name_PT']))    $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA_NAMEPT');
        if (empty($data['name_EN']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA_NAMEEN');
        if (empty($data['name_FR']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA_NAMEFR');
        if (empty($data['name_DE']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA_NAMEDE');
        if (empty($data['estado']) || $data['estado'] == 0)     $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA_ESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function createTipologia4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::createTipologia4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;
    }


    public function saveTipologia4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::updateTipologia4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;

    }


    public function getPostedDificuldadeFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $dificuldade_id  = $app->input->getInt('dificuldade_id');
        if (!empty($dificuldade_id)) $data['dificuldade_id'] = $dificuldade_id;

        $name    = $app->input->get('name',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['name']    = $name;
        $arCamposGet['estado']  = $estado;

        $data = $arCamposGet;

        if (!empty($dificuldade_id)) $data['dificuldade_id'] = $dificuldade_id;

        return($data);
    }


    public function validateDificuldadeBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['estado']) || $data['estado'] == 0)     $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DIFICULDADE_ESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedDificuldadeFormData4Manager($data)
    {
        if ( (string)$data['name']  === (string) $this->userPercursosPedestresData->name )    unset($data['name']);
        if ( (int)$data['estado']   === (int) $this->userPercursosPedestresData->estado )    unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('dificuldade_id',$data)) ) unset($data['dificuldade_id']);

        return($data);
    }


    public function saveDificuldade4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::updateDificuldade4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;

    }


    public function createDificuldade4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::createDificuldade4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;
    }


    public function getPostedTerrenoFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $terreno_id  = $app->input->getInt('terreno_id');
        if (!empty($terreno_id)) $data['terreno_id'] = $terreno_id;

        $name_PT    = $app->input->get('name_PT',null,'STRING');
        $name_EN    = $app->input->get('name_EN',null,'STRING');
        $name_FR    = $app->input->get('name_FR',null,'STRING');
        $name_DE    = $app->input->get('name_DE',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        $arCamposGet = array ();
        $arCamposGet['name_PT'] = $name_PT;
        $arCamposGet['name_EN'] = $name_EN;
        $arCamposGet['name_FR'] = $name_FR;
        $arCamposGet['name_DE'] = $name_DE;
        $arCamposGet['estado']  = $estado;

        $data = $arCamposGet;

        if (!empty($terreno_id)) $data['terreno_id'] = $terreno_id;

        return($data);

    }


    public function getOnlyChangedPostedTerrenoFormData4Manager($data)
    {
        if ( (string)$data['name_PT']   === (string) $this->userPercursosPedestresData->name_PT )    unset($data['name_PT']);
        if ( (string)$data['name_EN']   === (string) $this->userPercursosPedestresData->name_EN )    unset($data['name_EN']);
        if ( (string)$data['name_FR']   === (string) $this->userPercursosPedestresData->name_FR )    unset($data['name_FR']);
        if ( (string)$data['name_DE']   === (string) $this->userPercursosPedestresData->name_DE )    unset($data['name_DE']);
        if ( (int)$data['estado']       === (int) $this->userPercursosPedestresData->estado )    unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('terreno_id',$data)) ) unset($data['terreno_id']);

        return($data);
    }


    public function validateTerrenoBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['name_PT']))    $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO_NAMEPT');
        if (empty($data['name_EN']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO_NAMEEN');
        if (empty($data['name_FR']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO_NAMEFR');
        if (empty($data['name_DE']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO_NAMEDE');
        if (empty($data['estado']) || $data['estado'] == 0)     $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO_ESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function saveTerreno4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::updateTerreno4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;

    }


    public function createTerreno4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::createTerreno4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;
    }


    public function getPostedDuracaoFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $duracao_id  = $app->input->getInt('duracao_id');
        if (!empty($duracao_id)) $data['duracao_id'] = $duracao_id;

        $duracao_min    = $app->input->get('duracao_min',null,'STRING');
        $duracao_max    = $app->input->get('duracao_max',null,'STRING');
        $estado         = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['duracao_min']    = $duracao_min;
        $arCamposGet['duracao_max']    = $duracao_max;
        $arCamposGet['estado']  = $estado;

        $data = $arCamposGet;

        if (!empty($duracao_id)) $data['duracao_id'] = $duracao_id;

        return($data);
    }


    public function validateDuracaoBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['duracao_min'])){
            $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_MIN_ERR');
        }   else if (!is_numeric ($data['duracao_min'])){
            $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_MIN_ERR_INT');
        }

        if (empty($data['duracao_max'])) {
            $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_MAX_ERR');
        } else if (!is_numeric ($data['duracao_max'])){
            $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_MAX_ERR_INT');
        }

        if (empty($data['estado']) || $data['estado'] == 0)     $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_ESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedDuracaoFormData4Manager($data)
    {
        if ( (int)$data['duracao_min']  === (int) $this->userPercursosPedestresData->duracao_min )  unset($data['duracao_min']);
        if ( (int)$data['duracao_max']  === (int) $this->userPercursosPedestresData->duracao_max )  unset($data['duracao_max']);
        if ( (int)$data['estado']       === (int) $this->userPercursosPedestresData->estado )       unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('duracao_id',$data)) ) unset($data['duracao_id']);

        return($data);
    }


    public function saveDuracao4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::updateDuracao4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;

    }


    public function createDuracao4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::createDuracao4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;
    }


    public function getPostedVertigensFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $vertigens_id  = $app->input->getInt('vertigens_id');
        if (!empty($vertigens_id)) $data['vertigens_id'] = $vertigens_id;

        $name_PT    = $app->input->get('name_PT',null,'STRING');
        $name_EN    = $app->input->get('name_EN',null,'STRING');
        $name_FR    = $app->input->get('name_FR',null,'STRING');
        $name_DE    = $app->input->get('name_DE',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['name_PT'] = $name_PT;
        $arCamposGet['name_EN'] = $name_EN;
        $arCamposGet['name_FR'] = $name_FR;
        $arCamposGet['name_DE'] = $name_DE;
        $arCamposGet['estado']  = $estado;

        $data = $arCamposGet;

        if (!empty($vertigens_id)) $data['vertigens_id'] = $vertigens_id;

        return($data);
    }


    public function getOnlyChangedPostedVertigensFormData4Manager($data)
    {
        if ( (string)$data['name_PT']   === (string) $this->userPercursosPedestresData->name_PT )    unset($data['name_PT']);
        if ( (string)$data['name_EN']   === (string) $this->userPercursosPedestresData->name_EN )    unset($data['name_EN']);
        if ( (string)$data['name_FR']   === (string) $this->userPercursosPedestresData->name_FR )    unset($data['name_FR']);
        if ( (string)$data['name_DE']   === (string) $this->userPercursosPedestresData->name_DE )    unset($data['name_DE']);
        if ( (int)$data['estado']       === (int) $this->userPercursosPedestresData->estado )    unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('vertigens_id',$data)) ) unset($data['vertigens_id']);

        return($data);
    }


    public function saveVertigens4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::updateVertigens4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;

    }


    public function validateVertigensBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['name_PT']))    $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS_NAMEPT');
        if (empty($data['name_EN']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS_NAMEEN');
        if (empty($data['name_FR']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS_NAMEFR');
        if (empty($data['name_DE']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS_NAMEDE');
        if (empty($data['estado']) || $data['estado'] == 0)     $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS_ESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function createVertigens4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::createVertigens4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;
    }


    public function getPostedZonamentoFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $zonamento_id  = $app->input->getInt('zonamento_id');
        if (!empty($zonamento_id)) $data['zonamento_id'] = $zonamento_id;

        $name_PT    = $app->input->get('name_PT',null,'STRING');
        $name_EN    = $app->input->get('name_EN',null,'STRING');
        $name_FR    = $app->input->get('name_FR',null,'STRING');
        $name_DE    = $app->input->get('name_DE',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['name_PT'] = $name_PT;
        $arCamposGet['name_EN'] = $name_EN;
        $arCamposGet['name_FR'] = $name_FR;
        $arCamposGet['name_DE'] = $name_DE;
        $arCamposGet['estado']  = $estado;

        $data = $arCamposGet;

        if (!empty($zonamento_id)) $data['zonamento_id'] = $zonamento_id;

        return($data);
    }


    public function validateZonamentoBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['name_PT']))    $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO_NAMEPT');
        if (empty($data['name_EN']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO_NAMEEN');
        if (empty($data['name_FR']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO_NAMEFR');
        if (empty($data['name_DE']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO_NAMEDE');
        if (empty($data['estado']) || $data['estado'] == 0)     $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO_ESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedZonamentoFormData4Manager($data)
    {
        if ( (string)$data['name_PT']   === (string) $this->userPercursosPedestresData->name_PT )    unset($data['name_PT']);
        if ( (string)$data['name_EN']   === (string) $this->userPercursosPedestresData->name_EN )    unset($data['name_EN']);
        if ( (string)$data['name_FR']   === (string) $this->userPercursosPedestresData->name_FR )    unset($data['name_FR']);
        if ( (string)$data['name_DE']   === (string) $this->userPercursosPedestresData->name_DE )    unset($data['name_DE']);
        if ( (int)$data['estado']       === (int) $this->userPercursosPedestresData->estado )    unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('zonamento_id',$data)) ) unset($data['zonamento_id']);

        return($data);
    }


    public function saveZonamento4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::updateZonamento4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;

    }


    public function createZonamento4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::createZonamento4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;
    }


    public function getPostedSentidoFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $sentido_id  = $app->input->getInt('sentido_id');
        if (!empty($sentido_id)) $data['sentido_id'] = $sentido_id;

        $name_PT    = $app->input->get('name_PT',null,'STRING');
        $name_EN    = $app->input->get('name_EN',null,'STRING');
        $name_FR    = $app->input->get('name_FR',null,'STRING');
        $name_DE    = $app->input->get('name_DE',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['name_PT'] = $name_PT;
        $arCamposGet['name_EN'] = $name_EN;
        $arCamposGet['name_FR'] = $name_FR;
        $arCamposGet['name_DE'] = $name_DE;
        $arCamposGet['estado']  = $estado;

        $data = $arCamposGet;

        if (!empty($sentido_id)) $data['sentido_id'] = $sentido_id;

        return($data);
    }


    public function validateSentidoBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['name_PT']))    $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO_NAMEPT');
        if (empty($data['name_EN']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO_NAMEEN');
        if (empty($data['name_FR']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO_NAMEFR');
        if (empty($data['name_DE']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO_NAMEDE');
        if (empty($data['estado']) || $data['estado'] == 0)     $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO_ESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedSentidoFormData4Manager($data)
    {
        if ( (string)$data['name_PT']   === (string) $this->userPercursosPedestresData->name_PT )    unset($data['name_PT']);
        if ( (string)$data['name_EN']   === (string) $this->userPercursosPedestresData->name_EN )    unset($data['name_EN']);
        if ( (string)$data['name_FR']   === (string) $this->userPercursosPedestresData->name_FR )    unset($data['name_FR']);
        if ( (string)$data['name_DE']   === (string) $this->userPercursosPedestresData->name_DE )    unset($data['name_DE']);
        if ( (int)$data['estado']       === (int) $this->userPercursosPedestresData->estado )    unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('sentido_id',$data)) ) unset($data['sentido_id']);

        return($data);
    }


    public function saveSentido4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::updateSentido4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;

    }


    public function createSentido4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::createSentido4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;
    }


    public function getPostedPaisagemFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $paisagem_id  = $app->input->getInt('paisagem_id');
        if (!empty($paisagem_id)) $data['paisagem_id'] = $paisagem_id;

        $name_PT    = $app->input->get('name_PT',null,'STRING');
        $name_EN    = $app->input->get('name_EN',null,'STRING');
        $name_FR    = $app->input->get('name_FR',null,'STRING');
        $name_DE    = $app->input->get('name_DE',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['name_PT'] = $name_PT;
        $arCamposGet['name_EN'] = $name_EN;
        $arCamposGet['name_FR'] = $name_FR;
        $arCamposGet['name_DE'] = $name_DE;
        $arCamposGet['estado']  = $estado;

        $data = $arCamposGet;

        if (!empty($paisagem_id)) $data['paisagem_id'] = $paisagem_id;

        return($data);
    }


    public function validatePaisagemBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['name_PT']))    $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM_NAMEPT');
        if (empty($data['name_EN']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM_NAMEEN');
        if (empty($data['name_FR']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM_NAMEFR');
        if (empty($data['name_DE']))    $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM_NAMEDE');
        if (empty($data['estado']) || $data['estado'] == 0)     $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM_ESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedPaisagemFormData4Manager($data)
    {
        if ( (string)$data['name_PT']   === (string) $this->userPercursosPedestresData->name_PT )    unset($data['name_PT']);
        if ( (string)$data['name_EN']   === (string) $this->userPercursosPedestresData->name_EN )    unset($data['name_EN']);
        if ( (string)$data['name_FR']   === (string) $this->userPercursosPedestresData->name_FR )    unset($data['name_FR']);
        if ( (string)$data['name_DE']   === (string) $this->userPercursosPedestresData->name_DE )    unset($data['name_DE']);
        if ( (int)$data['estado']       === (int) $this->userPercursosPedestresData->estado )    unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('paisagem_id',$data)) ) unset($data['paisagem_id']);

        return($data);
    }


    public function savePaisagem4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::updatePaisagem4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;

    }


    public function createPaisagem4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::createPaisagem4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;
    }


    public function getPostedDistanciaFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $distancia_id  = $app->input->getInt('distancia_id');
        if (!empty($distancia_id)) $data['distancia_id'] = $distancia_id;

        $distancia_min  = $app->input->get('distancia_min',null,'STRING');
        $distancia_max  = $app->input->get('distancia_max',null,'STRING');
        $estado         = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['distancia_min']    = $distancia_min;
        $arCamposGet['distancia_max']    = $distancia_max;
        $arCamposGet['estado']  = $estado;

        $data = $arCamposGet;

        if (!empty($distancia_id)) $data['distancia_id'] = $distancia_id;

        return($data);
    }


    public function validateDistanciaBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['distancia_min'])){
            $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA_MIN_ERR');
        }   else if (!is_numeric ($data['distancia_min'])){
            $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA_MIN_ERR_INT');
        }

        if (empty($data['distancia_max'])) {
            $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA_MAX_ERR');
        } else if (!is_numeric ($data['distancia_max'])){
            $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA_MAX_ERR_INT');
        }

        if (empty($data['estado']) || $data['estado'] == 0)     $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA_ESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedDistanciaFormData4Manager($data)
    {
        if ( (int)$data['distancia_min']   === (int) $this->userPercursosPedestresData->distancia_min )    unset($data['distancia_min']);
        if ( (int)$data['distancia_max']   === (int) $this->userPercursosPedestresData->distancia_max )    unset($data['distancia_max']);
        if ( (int)$data['estado']       === (int) $this->userPercursosPedestresData->estado )    unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('distancia_id',$data)) ) unset($data['distancia_id']);

        return($data);
    }


    public function saveDistancia4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::updateDistancia4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;

    }


    public function createDistancia4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::createDistancia4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;
    }


    public function getPostedAltitudeFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $altitude_id  = $app->input->getInt('altitude_id');
        if (!empty($altitude_id)) $data['altitude_id'] = $altitude_id;

        $altitude_min    = $app->input->get('altitude_min',null,'STRING');
        $altitude_max    = $app->input->get('altitude_max',null,'STRING');
        $estado         = $app->input->get('estado',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['altitude_min']    = $altitude_min;
        $arCamposGet['altitude_max']    = $altitude_max;
        $arCamposGet['estado']  = $estado;

        $data = $arCamposGet;

        if (!empty($altitude_id)) $data['altitude_id'] = $altitude_id;

        return($data);
    }


    public function validateAltitudeBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['altitude_min'])){
            $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDE_MIN_ERR');
        }   else if (!is_numeric ($data['altitude_min'])){
            $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDE_MIN_ERR_INT');
        }

        if (empty($data['altitude_max'])) {
            $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDE_MAX_ERR');
        } else if (!is_numeric ($data['altitude_max'])){
            $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDE_MAX_ERR_INT');
        }

        if (empty($data['estado']) || $data['estado'] == 0)     $msg .= "<br>" . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA_ESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedAltitudeFormData4Manager($data)
    {
        if ( (int)$data['altitude_min'] === (int) $this->userPercursosPedestresData->altitude_min )    unset($data['altitude_min']);
        if ( (int)$data['altitude_max'] === (int) $this->userPercursosPedestresData->altitude_max )    unset($data['altitude_max']);
        if ( (int)$data['estado']       === (int) $this->userPercursosPedestresData->estado )    unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('altitude_id',$data)) ) unset($data['altitude_id']);

        return($data);
    }


    public function saveAltitude4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::updateAltitude4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;

    }


    public function createAltitude4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePercursosPedestresHelper::createAltitude4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
        return true;
    }

}
