<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteciecdiretorioHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ciecdiretorio.php');


use Joomla\Registry\Registry;

/**
 * ciecdiretorio model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelciecdiretorio extends JModelForm
{
    /**
     * @var		object	The user ciecdiretorio data.
     * @since   1.6
     */
    protected $data;
    protected $userSessionID;
    protected $userciecdiretorioData;
    public    $forcedLogout;

    //protected $ObjciecdiretorioFields;


    public function __construct($config = array())
    {
        $config = array_merge(
            array(
                'events_map' => array('validate' => 'user')
            ), $config
        );

        $this->forcedLogout = false;

        parent::__construct($config);

    }

    /*  ------- REMOVE ... ??? */

    public function checkin($userId = null)
    {

        return true;
    }

    public function checkout($userId = null)
    {

        return true;
    }

    public function getData()
    {
        return false;
    }

    public function getDataNew()
    {
        return false;

    }

    public function getForm($data = array(), $loadData = true)
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }

    /* --------------------- */


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    public function setUserIdAndData4User($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteciecdiretorioHelper::getciecdiretorioView4UserDetail($data['ciecdiretorio_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userciecdiretorioData = $loadedData;

        return true;
    }


    public function setUserIdAndData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteciecdiretorioHelper::getciecdiretorioView4ManagerDetail($data['ciecdiretorio_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // CARREGA DADOS DA AREA ACTUAÇÃO - multiple choice
        // comparar dados serializados com explode/implode com dados carregados têm de ser iguais
        $ComparaSerializeTipologia = VirtualDeskSiteciecdiretorioHelper::getTipologiaListSelected ( $this->data->id);
        asort($ComparaSerializeTipologia);
        $ComparaSerializeTipologia = implode('|',$ComparaSerializeTipologia);
        if($ComparaSerializeTipologia!=  $this->data->tipologia )  return false;
        $FieldSerializaTipologia   = explode('|',$this->data->tipologia);
        if (!empty($this->data->tipologia)) $this->data->areaact = $FieldSerializaTipologia;


        $this->userSessionID = $userSessionID;
        $this->userciecdiretorioData = $loadedData;
        return true;
    }


    protected function loadFormData()
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }


    protected function populateState()
    {
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
    }


    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }


    public function getPostedFormData4User()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $ciecdiretorio_id  = $app->input->getInt('ciecdiretorio_id');
        if (!empty($ciecdiretorio_id)) $data['ciecdiretorio_id'] = $ciecdiretorio_id;

        $assunto         = $app->input->get('assunto',null,'STRING');
        $descricao       = $app->input->get('descricao',null,'RAW');
        $departamento    = $app->input->get('departamento',null,'STRING');
        $observacoes     = $app->input->get('observacoes',null,'RAW');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChanged      = $app->input->get('vdFileUpChanged',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();

        $arCamposGet['assunto']         = $assunto;
        $arCamposGet['descricao']       = $descricao;
        $arCamposGet['departamento']    = $departamento;
        $arCamposGet['observacoes']     = $observacoes;

        $arCamposGet['vdFileUpChanged'] = $vdFileUpChanged;

        //$data = $this->ObjciecdiretorioFields->checkCamposEmpty($arCamposGet, $data);
        $data = $arCamposGet;
        if (!empty($ciecdiretorio_id)) $data['ciecdiretorio_id'] = $ciecdiretorio_id;
        return($data);
    }


    public function getPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $ciecdiretorio_id  = $app->input->getInt('ciecdiretorio_id');
        if (!empty($ciecdiretorio_id)) $data['ciecdiretorio_id'] = $ciecdiretorio_id;

        $nomeProjeto = $app->input->get('nomeProjeto',null,'STRING');
        $descricao = $app->input->get('descricao',null,'RAW');
        $programa = $app->input->get('programa',null,'STRING');
        $linhaFinanciamento = $app->input->get('linhaFinanciamento',null,'STRING');
        $escala = $app->input->get('escala',null,'STRING');
        $entidade = $app->input->get('entidade',null,'STRING');
        $ano = $app->input->get('ano',null,'STRING');
        $link = $app->input->get('link',null,'STRING');
        $call = $app->input->get('call',null,'STRING');
        $dataInicio = $app->input->get('dataInicio',null,'RAW');
        $dataFim = $app->input->get('dataFim',null,'RAW');
        $valor = $app->input->get('valor',null,'STRING');
        $layout = $app->input->get('layoutDetalhe',null,'STRING');
        $lider = $app->input->get('lider',null,'STRING');
        $pais = $app->input->get('pais',null,'STRING');
        $tipologia = $app->input->get('tipologia',null,'ARRAY');
        $ProgramaParceiro = $app->input->get('ParceiroInput',null,'RAW');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChanged      = $app->input->get('vdFileUpChanged',null,'STRING');

        /// Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();

        $arCamposGet['nomeProjeto'] = $nomeProjeto;
        $arCamposGet['descricao'] = $descricao;
        $arCamposGet['programa'] = $programa;
        $arCamposGet['linhaFinanciamento'] = $linhaFinanciamento;
        $arCamposGet['escala'] = $escala;
        $arCamposGet['entidade'] = $entidade;
        $arCamposGet['ano'] = $ano;
        $arCamposGet['link'] = $link;
        $arCamposGet['call'] = $call;
        $arCamposGet['dataInicio'] = $dataInicio;
        $arCamposGet['dataFim'] = $dataFim;
        $arCamposGet['valor'] = $valor;
        $arCamposGet['layoutDetalhe'] = $layout;
        $arCamposGet['lider'] = $lider;
        $arCamposGet['pais'] = $pais;
        $arCamposGet['tipologia'] = $tipologia;
        $arCamposGet['ProgramaParceiro'] = $ProgramaParceiro;

        $arCamposGet['vdFileUpChanged'] = $vdFileUpChanged;

        //$data = $this->ObjciecdiretorioFields->checkCamposEmpty($arCamposGet, $data);
        $data = $arCamposGet;
        if (!empty($ciecdiretorio_id)) $data['ciecdiretorio_id'] = $ciecdiretorio_id;
        return($data);
    }


    public function getOnlyChangedPostedFormData4Manager($data)
    {

        if ( (string)$data['nomeProjeto']         === (string) $this->userciecdiretorioData->projeto )     unset($data['nomeProjeto']);
        if ( (string)$data['descricao']         === (string) $this->userciecdiretorioData->descricao )      unset($data['descricao']);
        if ( (int)$data['programa']       === (int) $this->userciecdiretorioData->programa )     unset($data['programa']);
        if ( (int)$data['linhaFinanciamento']       === (int) $this->userciecdiretorioData->linhaFinanciamento )   unset($data['linhaFinanciamento']);
        if ( (string)$data['escala']     === (string) $this->userciecdiretorioData->escala )   unset($data['escala']);
        if ( (string)$data['entidade']     === (string) $this->userciecdiretorioData->entidade )   unset($data['entidade']);
        if ( (int)$data['ano']     === (int) $this->userciecdiretorioData->ano )   unset($data['ano']);
        if ( (string)$data['link']     === (string) $this->userciecdiretorioData->link )   unset($data['link']);
        if ( (string)$data['call']     === (string) $this->userciecdiretorioData->call )   unset($data['call']);
        if ( (string)$data['dataInicio']     === (string) $this->userciecdiretorioData->dataInicio )   unset($data['dataInicio']);
        if ( (string)$data['dataFim']     === (string) $this->userciecdiretorioData->dataFim )   unset($data['dataFim']);
        if ( (string)$data['valor']     === (string) $this->userciecdiretorioData->valor )   unset($data['valor']);
        if ( (int)$data['layoutDetalhe']     === (int) $this->userciecdiretorioData->layoutDetalhe )   unset($data['layoutDetalhe']);
        if ( (string)$data['lider']     === (string) $this->userciecdiretorioData->lider )   unset($data['lider']);
        if ( (int)$data['pais']     === (int) $this->userciecdiretorioData->pais )   unset($data['pais']);


        //if ( (string)$data['tipologia']     === (string) $this->userciecdiretorioData->tipologia )   unset($data['tipologia']);
        $arDadosTipologia = $data['tipologia'];
        if(!is_array($arDadosTipologia)) $arDadosTipologia = array();
        asort($arDadosTipologia);
        if ( implode('|',$arDadosTipologia) === (string) $this->userciecdiretorioData->tipologia ) unset($data['tipologia']);


        if ( (string)$data['vdFileUpChanged'] == '0' )   unset($data['vdFileUpChanged']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('ciecdiretorio_id',$data)) ) unset($data['ciecdiretorio_id']);

        return($data);
    }

    public function validateBeforeSave4Manager($data)
    {
        $msg = '';

        if (empty($data['nomeProjeto']))       $msg .= "<br>" . JText::_('COM_VIRTUALDESK_CIECDIRETORIO_FIELDREQUIRED_NOME') ;
        if (empty($data['programa']))     $msg .= "<br>" . JText::_('COM_VIRTUALDESK_CIECDIRETORIO_FIELDREQUIRED_PROGRAMA') ;

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function save4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteciecdiretorioHelper::update4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteciecdiretorioHelper::cleanAllTmpUserState();
        return true;

    }


    public function create4User($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteciecdiretorioHelper::create4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteciecdiretorioHelper::cleanAllTmpUserState();
        return true;

    }


    public function create4Manager($data)
    {

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteciecdiretorioHelper::create4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteciecdiretorioHelper::cleanAllTmpUserState();
        return true;
    }


    public function delete4User($data)
    {

        return true;
    }


    public function delete4Manager($data)
    {

        return true;
    }


}
