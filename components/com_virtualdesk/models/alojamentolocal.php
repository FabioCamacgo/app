<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteAlojamentoLocalHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alojamentolocal.php');


use Joomla\Registry\Registry;

/**
 * AlojamentoLocal model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelAlojamentoLocal extends JModelForm
{
	/**
	 * @var		object	The user alojamentolocal data.
	 * @since   1.6
	 */
	protected $data;
    protected $userSessionID;
    protected $userAlojamentoLocalData;
    public    $forcedLogout;

    //protected $ObjAlojamentoLocalFields;


	public function __construct($config = array())
	{
		$config = array_merge(
			array(
				'events_map' => array('validate' => 'user')
			), $config
		);

        $this->forcedLogout = false;

		parent::__construct($config);

	}

	/*  ------- REMOVE ... ??? */

	public function checkin($userId = null)
	{

		return true;
	}

	public function checkout($userId = null)
	{

		return true;
	}

	public function getData()
	{
        return false;
	}

    public function getDataNew()
    {
        return false;

    }

	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	/* --------------------- */


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    public function setUserIdAndData4User($data)
    {

        return true;
    }


    public function setUserIdAndData4Manager($data)
    {

        return true;
    }


	protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}


	protected function populateState()
	{
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
	}


    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }


    public function getPostedFormData4User()
    {   $app         = JFactory::getApplication();
        $data        = array();



        return($data);

    }


    public function getPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();



        return($data);

    }


    public function getOnlyChangedPostedFormData4User($data)
     {

         return($data);
     }


    public function getOnlyChangedPostedFormData4Manager($data)
    {

        return($data);
    }


    public function validateBeforeSave4User($data)
    {

        return(true);

    }


    public function validateBeforeSave4Manager($data)
    {

        return(true);

    }


	public function save4User($data)
	{


        return true;
	}


    public function save4Manager($data)
    {


        return true;
    }


    public function create4User($data)
    {

        return true;
    }


    public function create4Manager($data)
    {

        return true;
    }


    public function delete4User($data)
    {

        return true;
    }


    public function delete4Manager($data)
    {

        return true;
    }


}
