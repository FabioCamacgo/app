<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
use Joomla\Registry\Registry;

/**
 * Profile model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelPermAdmin extends JModelForm
{
	/**
	 * @var		object	The user profile data.
	 * @since   1.6
	 */
	protected $data;

    protected $userSessionID;

    protected $userProfileData;

    public    $forcedLogout;

    protected $ObjUserFields;

    protected $userPermAdminModuloData;
    protected $userPermAdminGrupoData;
    protected $userPermAdminTipoData;
    protected $userPermAdminActionData;
    protected $userPermAdminUserActionData;
    protected $userPermAdminGrupoActionData;
    protected $userPermAdminGrupoUsersData;
    protected $userPermAdminUserGruposData;



	/**
	 * Constructor
	 *
	 * @param   array  $config  An array of configuration options (name, state, dbo, table_path, ignore_request).
	 *
	 * @since   3.2
	 *
	 * @throws  Exception
	 */
	public function __construct($config = array())
	{
		$config = array_merge(
			array(
				'events_map' => array('validate' => 'user')
			), $config
		);

        $this->forcedLogout = false;

		parent::__construct($config);

	}


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    public function setUserIdAndModuloData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSitePermAdminHelper::getPermModuloDetail ($data['permadmin_modulo_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID     = $userSessionID;
        $this->userPermAdminModuloData = $loadedData;
        return true;
    }


    public function setUserIdAndGrupoData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSitePermAdminHelper::getPermGrupoDetail ($data['permadmin_grupo_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID     = $userSessionID;
        $this->userPermAdminGrupoData = $loadedData;
        return true;
    }


    public function setUserIdAndTipoData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSitePermAdminHelper::getPermTipoDetail ($data['permadmin_tipo_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID     = $userSessionID;
        $this->userPermAdminTipoData = $loadedData;
        return true;
    }


    public function setUserIdAndActionData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSitePermAdminHelper::getPermActionDetail ($data['permadmin_action_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID     = $userSessionID;
        $this->userPermAdminActionData = $loadedData;
        return true;
    }


    public function setUserIdAndUserActionData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSitePermAdminHelper::getPermUserActionsDetail($data['permadmin_user_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
            return(false);
        }

        $this->userPermAdminUserActionData = array();
        foreach($loadedData as $row) {
            if ((int)$row->action_id > 0) {

            $modulo_id   = (string)$row->modulo_id;
            $tipo_id     = (string)$row->tipo_id;
            $action_id   = (string)$row->action_id;
            $uactions_id = (string)$row->uactions_id;
            $rowPermBits = array();
            $rowPermBits['c'] = $row->pcreate;
            $rowPermBits['r'] = $row->pread;
            $rowPermBits['u'] = $row->pupdate;
            $rowPermBits['d'] = $row->pdelete;
            $rowPermBits['rall'] = $row->preadall;

            $chave = $modulo_id . '_' . $tipo_id . '_' . $action_id.'_'.$uactions_id;
            $this->userPermAdminUserActionData[$chave] = $rowPermBits;
           }
        }

        $this->userSessionID     = $userSessionID;
        return true;
    }


    public function setUserIdAndGrupoActionData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSitePermAdminHelper::getPermGrupoActionsDetail($data['permadmin_grupo_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
            return(false);
        }

        $this->userPermAdminGrupoActionData = array();
        foreach($loadedData as $row) {
            if ((int)$row->action_id > 0) {

                $modulo_id   = (string)$row->modulo_id;
                $tipo_id     = (string)$row->tipo_id;
                $action_id   = (string)$row->action_id;
                $gactions_id = (string)$row->gactions_id;
                $rowPermBits = array();
                $rowPermBits['c'] = $row->pcreate;
                $rowPermBits['r'] = $row->pread;
                $rowPermBits['u'] = $row->pupdate;
                $rowPermBits['d'] = $row->pdelete;
                $rowPermBits['rall'] = $row->preadall;

                $chave = $modulo_id . '_' . $tipo_id . '_' . $action_id.'_'.$gactions_id;
                $this->userPermAdminGrupoActionData[$chave] = $rowPermBits;
            }
        }

        $this->userSessionID     = $userSessionID;
        return true;
    }


    public function setUserIdAndGrupoUsersData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSitePermAdminHelper::getPermGroupUsersList(false,-1,$data['permadmin_grupo_id']);
        /*if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
            return(false);
        }*/

        if(empty($loadedData)) $loadedData = array();
        $this->userPermAdminGrupoUsersData = array();
        $this->userPermAdminGrupoUsersData  = $loadedData;

        $this->userSessionID     = $userSessionID;
        return true;
    }





	  /**
	 * Method to getform data.
	 *
	 * @return  mixed  	Data object on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function getData()
	{

		//return $this->data;
        return false;
	}


	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}


	protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}


	/**
	 * Method to auto-populate the model state.
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
	}


    public function getPostedFormData()
    {  $app         = JFactory::getApplication();

       $data        = array();
       return($data);
    }


    public function getPostedFormData2Modulo()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $modulo_id  = $app->input->getInt('permadmin_modulo_id');
        $nome       = $app->input->get('nome',null, 'STRING');
        $tagchave   = $app->input->get('tagchave',null, 'STRING');
        $enabled    = $app->input->get('enabled',null, 'STRING');
        $enabled2dropbox  = $app->input->get('enabled2dropbox',null, 'STRING');

        if (!empty($modulo_id)) $data['permadmin_modulo_id'] = $modulo_id;
        if (!empty($nome))  $data['nome']                    = $nome;
        if (!empty($tagchave)) $data['tagchave']             = $tagchave;

        if ((string)$enabled!='') {
            $data['enabled']             = $enabled;
        }
        else {
            $data['enabled']             = '0';
        }

        if ((string)$enabled2dropbox!='') {
            $data['enabled2dropbox']             = $enabled2dropbox;
        }
        else {
            $data['enabled2dropbox']             = '0';
        }

        return($data);
    }


    public function getPostedFormData2Grupo()
    {   $app         = JFactory::getApplication();
        $data        = array();
        $grupo_id    = $app->input->getInt('permadmin_grupo_id');
        $nome        = $app->input->get('nome',null, 'STRING');
        $descricao   = $app->input->get('descricao',null, 'STRING');
        $email       = $app->input->get('email',null, 'STRING');
        $isManager   = $app->input->get('isManager',null, 'STRING');
        if (!empty($grupo_id)) $data['permadmin_grupo_id'] = $grupo_id;
        if (!empty($nome))  $data['nome']                  = $nome;
        if (!empty($descricao))  $data['descricao']        = $descricao;
        if (!empty($email))  $data['email']                = $email;

        if ((string)$isManager!='') {
            $data['isManager']             = $isManager;
        }
        else {
            $data['isManager']             = '0';
        }

        return($data);
    }


    public function getPostedFormData2Tipo()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $modulo_id  = $app->input->getInt('permadmin_tipo_id');
        $nome       = $app->input->get('nome',null, 'STRING');
        $tagchave   = $app->input->get('tagchave',null, 'STRING');

        if (!empty($modulo_id)) $data['permadmin_tipo_id'] = $modulo_id;
        if (!empty($nome))  $data['nome']                    = $nome;
        if (!empty($tagchave)) $data['tagchave']             = $tagchave;

        return($data);
    }


    public function getPostedFormData2Action()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $action_id  = $app->input->getInt('permadmin_action_id');
        $nome       = $app->input->get('nome',null, 'STRING');
        $tagchave   = $app->input->get('tagchave',null, 'STRING');
        $descricao  = $app->input->get('descricao',null, 'STRING');
        $modulo     = $app->input->get('modulo',null, 'STRING');
        $tipo       = $app->input->get('tipo',null, 'STRING');

        if (!empty($action_id)) $data['permadmin_action_id'] = $action_id;
        if (!empty($nome))  $data['nome']                    = $nome;
        if (!empty($tagchave)) $data['tagchave']             = $tagchave;
        if (!empty($descricao)) $data['descricao']           = $descricao;
        if (!empty($modulo)) $data['modulo']                 = $modulo;
        if (!empty($tipo)) $data['tipo']                     = $tipo;

        return($data);
    }


    public function getPostedFormData2UserAction()
    {   $app         = JFactory::getApplication();
        $data        = array();


        //Vai ler a estrutura de chaves e colocar no array de dados...
        //foreach []

        $user_id  = $app->input->getInt('permadmin_user_id');
        $permArray = $app->input->get('uaid',null, 'ARRAY');
        //$permArray2 = $app->input->get('uaid',null, null);


        if (!empty($user_id)) $data['permadmin_user_id'] = $user_id;
        if (!empty($permArray)) $data['uaid']        = $permArray;


        return($data);
    }


    public function getPostedFormData2GrupoAction()
    {   $app         = JFactory::getApplication();
        $data        = array();

        //Vai ler a estrutura de chaves e colocar no array de dados...
        $grupo_id  = $app->input->getInt('permadmin_grupo_id');
        $permArray = $app->input->get('uaid',null, 'ARRAY');

        if (!empty($grupo_id)) $data['permadmin_grupo_id'] = $grupo_id;
        if (!empty($permArray)) $data['uaid']              = $permArray;

        return($data);
    }


    public function getPostedFormData2GrupoUsers()
    {   $app         = JFactory::getApplication();
        $data        = array();

        //Vai ler a estrutura de chaves e colocar no array de dados...
        $grupo_id  = $app->input->getInt('permadmin_grupo_id');
        $permArray = $app->input->get('gusrid',null, 'ARRAY');

        if (!empty($grupo_id))  $data['permadmin_grupo_id'] = $grupo_id;
        if (!empty($permArray)) $data['gusrid']              = $permArray;

        return($data);
    }


    public function getPostedFormData2UserGrupos()
    {   $app         = JFactory::getApplication();
        $data        = array();

        //Vai ler a estrutura de chaves e colocar no array de dados...
        $grupo_id  = $app->input->getInt('permadmin_user_id');
        $permArray = $app->input->get('ugrpsid',null, 'ARRAY');

        if (!empty($grupo_id))  $data['permadmin_user_id'] = $grupo_id;
        if (!empty($permArray)) $data['ugrpsid']              = $permArray;

        return($data);
    }


    public function getOnlyChangedPostedFormData($data)
    {

        // no caso do useravatar só se o array vier com um ficheiro é que vai ser considerado... verifica antes no getPostedFormData()
        return($data);
    }


    public function getOnlyChangedPostedFormData2Modulo($data)
    {
        if ( (string)$data['nome'] === (string) $this->userPermAdminModuloData->nome ) unset($data['nome']);
        if ( (string)$data['tagchave'] === (string) $this->userPermAdminModuloData->tagchave ) unset($data['tagchave']);
        if ( (string)$data['enabled'] === (string) $this->userPermAdminModuloData->enabled ) unset($data['enabled']);
        if ( (string)$data['enabled2dropbox'] === (string) $this->userPermAdminModuloData->enabled2dropbox ) unset($data['enabled2dropbox']);

        // Se no final só tivermos definido o contactus_id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('permadmin_modulo_id',$data)) ) unset($data['permadmin_modulo_id']);

        return($data);
    }


    public function getOnlyChangedPostedFormData2Grupo($data)
    {
        if ( (string)$data['nome'] === (string) $this->userPermAdminGrupoData->nome )           unset($data['nome']);
        if ( (string)$data['descricao'] === (string) $this->userPermAdminGrupoData->descricao ) unset($data['descricao']);
        if ( (string)$data['email'] === (string) $this->userPermAdminGrupoData->email )         unset($data['email']);
        if ( (string)$data['isManager'] === (string) $this->userPermAdminGrupoData->isManager ) unset($data['isManager']);
        // Se no final só tivermos definido o contactus_id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('permadmin_grupo_id',$data)) ) unset($data['permadmin_grupo_id']);
        return($data);
    }


    public function getOnlyChangedPostedFormData2Tipo($data)
    {
        if ( (string)$data['nome'] === (string) $this->userPermAdminTipoData->nome ) unset($data['nome']);
        if ( (string)$data['tagchave'] === (string) $this->userPermAdminTipoData->tagchave ) unset($data['tagchave']);

        // Se no final só tivermos definido o contactus_id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('permadmin_tipo_id',$data)) ) unset($data['permadmin_tipo_id']);

        return($data);
    }


    public function getOnlyChangedPostedFormData2Action($data)
    {
        if ( (string)$data['nome'] === (string) $this->userPermAdminActionData->nome ) unset($data['nome']);
        if ( (string)$data['tagchave'] === (string) $this->userPermAdminActionData->tagchave ) unset($data['tagchave']);
        if ( (string)$data['descricao'] === (string) $this->userPermAdminActionData->descricao ) unset($data['descricao']);

        // Se no final só tivermos definido o contactus_id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('permadmin_action_id',$data)) ) unset($data['permadmin_action_id']);

        return($data);
    }


    public function getOnlyChangedPostedFormData2UserAction($data)
    {

        // Percorre o array de dados carregado e o array enviado, comparando e só gravando o que foi alterado.
        // foreach($data['uaid'] as $key => $row)
       foreach($this->userPermAdminUserActionData as $key => $row)
        {
            $valToApply = 0;

            if(!empty($data['uaid'][$key]['c'])) {
                if ( (string)$data['uaid'][$key]['c'] === (string) $this->userPermAdminUserActionData[$key]['c'] ) unset($data['uaid'][$key]['c']);
            }
            else { //vazio... então foi retirado ou ainda não está criada a permissão .... acrescentamos com o
                if(!empty($this->userPermAdminUserActionData[$key]['c'])) $data['uaid'][$key]['c'] = $valToApply;
            }

            if(!empty($data['uaid'][$key]['r'])) {
                if ( (string)$data['uaid'][$key]['r'] === (string) $this->userPermAdminUserActionData[$key]['r'] ) unset($data['uaid'][$key]['r']);
            }
            else { //vazio... então foi retirado .... acrescentamos com o
                if(!empty($this->userPermAdminUserActionData[$key]['r'])) $data['uaid'][$key]['r'] = $valToApply;
            }

            if(!empty($data['uaid'][$key]['u'])) {
                if ( (string)$data['uaid'][$key]['u'] === (string) $this->userPermAdminUserActionData[$key]['u'] ) unset($data['uaid'][$key]['u']);
            }
            else { //vazio... então foi retirado .... acrescentamos com o
                if(!empty($this->userPermAdminUserActionData[$key]['u'])) $data['uaid'][$key]['u'] = $valToApply;
            }

            if(!empty($data['uaid'][$key]['d'])) {
                if ( (string)$data['uaid'][$key]['d'] === (string) $this->userPermAdminUserActionData[$key]['d'] ) unset($data['uaid'][$key]['d']);
            }
            else { //vazio... então foi retirado .... acrescentamos com o
                if(!empty($this->userPermAdminUserActionData[$key]['d'])) $data['uaid'][$key]['d'] = $valToApply;
            }

            if(!empty($data['uaid'][$key]['rall'])) {
                if ( (string)$data['uaid'][$key]['rall'] === (string) $this->userPermAdminUserActionData[$key]['rall'] ) unset($data['uaid'][$key]['rall']);
            }
            else { //vazio... então foi retirado .... acrescentamos com o
                if(!empty($this->userPermAdminUserActionData[$key]['rall'])) $data['uaid'][$key]['rall'] = $valToApply;
            }

           /* if ( (string)$data['uaid'][$key]['r'] === (string) $this->userPermAdminUserActionData[$key]['r'] ) unset($data['uaid'][$key]['r']);
            if ( (string)$data['uaid'][$key]['u'] === (string) $this->userPermAdminUserActionData[$key]['u'] ) unset($data['uaid'][$key]['u']);
            if ( (string)$data['uaid'][$key]['d'] === (string) $this->userPermAdminUserActionData[$key]['d'] ) unset($data['uaid'][$key]['d']);
            if ( (string)$data['uaid'][$key]['rall'] === (string) $this->userPermAdminUserActionData[$key]['rall'] ) unset($data['uaid'][$key]['rall']);
           */
        }

       // if ( (string)$data['nome'] === (string) $this->userPermAdminActionData->nome ) unset($data['nome']);

        $totalPerms = array_sum(array_map("count", $data['uaid']));

        // Se no final só tivermos definido o contactus_id, então não devemos atualizar nada
        if( (sizeof($totalPerms)==0) && (array_key_exists('permadmin_user_id',$data)) ) {
            $data = array();
        }

        return($data);
    }


    public function getOnlyChangedPostedFormData2GrupoAction($data)
    {
        // Percorre o array de dados carregado e o array enviado, comparando e só gravando o que foi alterado.
        foreach($this->userPermAdminGrupoActionData as $key => $row)
        {
            $valToApply = 0;

            if(!empty($data['uaid'][$key]['c'])) {
                if ( (string)$data['uaid'][$key]['c'] === (string) $this->userPermAdminGrupoActionData[$key]['c'] ) unset($data['uaid'][$key]['c']);
            }
            else { //vazio... então foi retirado ou ainda não está criada a permissão .... acrescentamos com o
                if(!empty($this->userPermAdminGrupoActionData[$key]['c'])) $data['uaid'][$key]['c'] = $valToApply;
            }

            if(!empty($data['uaid'][$key]['r'])) {
                if ( (string)$data['uaid'][$key]['r'] === (string) $this->userPermAdminGrupoActionData[$key]['r'] ) unset($data['uaid'][$key]['r']);
            }
            else { //vazio... então foi retirado .... acrescentamos com o
                if(!empty($this->userPermAdminGrupoActionData[$key]['r'])) $data['uaid'][$key]['r'] = $valToApply;
            }

            if(!empty($data['uaid'][$key]['u'])) {
                if ( (string)$data['uaid'][$key]['u'] === (string) $this->userPermAdminGrupoActionData[$key]['u'] ) unset($data['uaid'][$key]['u']);
            }
            else { //vazio... então foi retirado .... acrescentamos com o
                if(!empty($this->userPermAdminGrupoActionData[$key]['u'])) $data['uaid'][$key]['u'] = $valToApply;
            }

            if(!empty($data['uaid'][$key]['d'])) {
                if ( (string)$data['uaid'][$key]['d'] === (string) $this->userPermAdminGrupoActionData[$key]['d'] ) unset($data['uaid'][$key]['d']);
            }
            else { //vazio... então foi retirado .... acrescentamos com o
                if(!empty($this->userPermAdminGrupoActionData[$key]['d'])) $data['uaid'][$key]['d'] = $valToApply;
            }

            if(!empty($data['uaid'][$key]['rall'])) {
                if ( (string)$data['uaid'][$key]['rall'] === (string) $this->userPermAdminGrupoActionData[$key]['rall'] ) unset($data['uaid'][$key]['rall']);
            }
            else { //vazio... então foi retirado .... acrescentamos com o
                if(!empty($this->userPermAdminGrupoActionData[$key]['rall'])) $data['uaid'][$key]['rall'] = $valToApply;
            }

        }

        // if ( (string)$data['nome'] === (string) $this->userPermAdminActionData->nome ) unset($data['nome']);

        $totalPerms = array_sum(array_map("count", $data['uaid']));

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($totalPerms)==0) && (array_key_exists('permadmin_grupo_id',$data)) ) {
            $data = array();
        }

        return($data);
    }


    public function getOnlyChangedPostedFormData2GrupoUsers($data)
    {
        // Percorre os dados carregado da BD, compara com mos enviados, se não existir são para ELIMINAR
        $arTempLoadedUserIds = array();
        foreach($this->userPermAdminGrupoUsersData as $key1 => $row1)
        {
            if((int)$row1->iduser<=0) continue;
            if(in_array($row1->iduser,$data['gusrid'])==false) $data['gusrid_delete'][$row1->iduser] = $row1->iduser;
            $arTempLoadedUserIds[]=$row1->iduser;
        }

        // Percorre os dados enviados do FORM, compara com mos enviados, se não existirem são para INSERIR
        foreach($data['gusrid'] as $key2 => $row2)
        {
            if(in_array($row2,$arTempLoadedUserIds)==false) $data['gusrid_insert'][$row2] = $row2;
        }

        $totalPermsDelete = array_sum(array_map("count", $data['gusrid_delete']));
        $totalPermsInsert = array_sum(array_map("count", $data['gusrid_insert']));

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($totalPermsDelete)==0) && (sizeof($totalPermsInsert)==0) && (array_key_exists('permadmin_grupo_id',$data)) ) {
            $data = array();
        }

        return($data);
    }


    public function getOnlyChangedPostedFormData2UserGrupos($data)
    {
        // Percorre os dados carregado da BD, compara com mos enviados, se não existir são para ELIMINAR
        $arTempLoadedGroupsIds = array();
        foreach($this->userPermAdminUserGruposData as $key1 => $row1)
        {
            if((int)$row1->id<=0) continue;
            if(in_array($row1->id,$data['ugrpsid'])==false) $data['ugrpsid_delete'][$row1->id] = $row1->id;
            $arTempLoadedGroupsIds[]=$row1->id;
        }

        // Percorre os dados enviados do FORM, compara com mos enviados, se não existirem são para INSERIR
        foreach($data['ugrpsid'] as $key2 => $row2)
        {
            if(in_array($row2,$arTempLoadedGroupsIds)==false) $data['ugrpsid_insert'][$row2] = $row2;
        }

        $totalPermsDelete = array_sum(array_map("count", $data['ugrpsid_delete']));
        $totalPermsInsert = array_sum(array_map("count", $data['ugrpsid_insert']));

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($totalPermsDelete)==0) && (sizeof($totalPermsInsert)==0) && (array_key_exists('permadmin_user_id',$data)) ) {
            $data = array();
        }

        return($data);
    }


    /**
     * Verifica se ows dados a gravar estão coorecto: email, username (compliant and exist),
     *
     * @param   array  $data  The form data.
     *
     * @return  mixed
     *
     * @since   1.6
     */
    public function validateBeforeSave2Modulo($data)
    {
        $msg = '';
        if (empty($data['nome']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['tagchave'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if ((string)$data['enabled']=='') $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ENABLED');
        if ((string)$data['enabled2dropbox']=='') $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ENABLED2DROPBOX');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);
    }


    public function validateBeforeSave2Grupo($data)
    {
        $msg = '';
        if (empty($data['nome']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);
    }


    public function validateBeforeSave2Tipo($data)
    {
        $msg = '';
        if (empty($data['nome']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['tagchave'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);
    }


    public function validateBeforeSave2Action($data)
    {
        $msg = '';
        if (empty($data['nome']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['tagchave'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if (empty($data['modulo']))   $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_MODULO');
        if (empty($data['tipo']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TUPO');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);
    }


    public function validateBeforeSave2UserAction($data)
    {
        $msg = '';
        #if (empty($data['uaid']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED');


        /*if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }*/
        return(true);
    }


    public function validateBeforeSave2GrupoAction($data)
    {
        $msg = '';
        #if (empty($data['uaid']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED');


        /*if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }*/
        return(true);
    }


    public function validateBeforeSave2GrupoUsers($data)
    {
        $msg = '';
        #if (empty($data['uaid']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED');


        /*if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }*/
        return(true);
    }


    public function validateBeforeSave2UserGrupos($data)
    {
        $msg = '';
        #if (empty($data['uaid']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED');


        /*if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }*/
        return(true);
    }


    /**
     * Verifica se ows dados a gravar estão coorecto: email, username (compliant and exist),
     *
     * @param   array  $data  The form data.
     *
     * @return  mixed
     *
     * @since   1.6
    */
    public function validateNewRegistrationBeforeSave($data)
    {

        return(true);
    }


	/**
	 * Method to save the form data.
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  mixed  The user id on success, false on failure.
	 *
	 * @since   1.6
	 */
    public function save2Modulo($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePermAdminHelper::updateModulo($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
        return true;
    }


    public function save2Grupo($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePermAdminHelper::updateGrupo($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
        return true;
    }


    public function save2Tipo($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePermAdminHelper::updateTipo($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
        return true;
    }


    public function save2Action($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePermAdminHelper::updateAction($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
        return true;
    }


    public function save2UserAction($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePermAdminHelper::updateUserAction($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
        return true;
    }


    public function save2GrupoAction($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePermAdminHelper::updateGrupoAction($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
        return true;
    }


    public function save2GrupoUsers($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePermAdminHelper::updateGrupoUsers($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
        return true;
    }


    public function save2UserGrupos($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePermAdminHelper::updateUserGrupos($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
        return true;
    }


    public function create2Modulo($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePermAdminHelper::createModulo($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
        return true;
    }


    public function create2Grupo($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePermAdminHelper::createGrupo($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
        return true;
    }


    public function create2Tipo($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePermAdminHelper::createTipo($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
        return true;
    }


    public function create2Action($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePermAdminHelper::createAction($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
        return true;
    }


}