<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteCulturaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura.php');


use Joomla\Registry\Registry;

/**
 * Cultura model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelCultura extends JModelForm
{
	/**
	 * @var		object	The user cultura data.
	 * @since   1.6
	 */
	protected $data;
    protected $userSessionID;
    protected $userCulturaData;
    public    $forcedLogout;

    //protected $ObjCulturaFields;


	public function __construct($config = array())
	{
		$config = array_merge(
			array(
				'events_map' => array('validate' => 'user')
			), $config
		);

        $this->forcedLogout = false;

		parent::__construct($config);

	}

	/*  ------- REMOVE ... ??? */

	public function checkin($userId = null)
	{

		return true;
	}

	public function checkout($userId = null)
	{

		return true;
	}

	public function getData()
	{
        return false;
	}

    public function getDataNew()
    {
        return false;

    }

	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	/* --------------------- */


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    public function setUserIdAndData4User($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteCulturaHelper::getCulturaView4UserDetail($data['cultura_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userCulturaData = $loadedData;

        return true;
    }


    public function setUserIdAndData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteCulturaHelper::getCulturaView4ManagerDetail($data['cultura_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userCulturaData = $loadedData;
        return true;
    }


	protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}


	protected function populateState()
	{
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
	}


    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }


    public function getPostedFormData4User()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $cultura_id  = $app->input->getInt('cultura_id');
        if (!empty($cultura_id)) $data['cultura_id'] = $cultura_id;


        $coOrganizacao  = $app->input->get('coOrganizacao',null,'STRING');
        $nome_evento    = $app->input->get('nome_evento',null,'STRING');
        $desc_evento    = $app->input->get('desc_evento',null,'RAW');
        $cat_evento     = $app->input->get('cat_evento',null,'STRING');
        $freguesia_evento = $app->input->get('freguesia_evento',null,'STRING');
        $local_evento   = $app->input->get('local_evento',null,'STRING');
        $data_inicio    = $app->input->get('data_inicio',null,'STRING');
        $hora_inicio    = $app->input->get('hora_inicio',null,'STRING');
        $data_fim       = $app->input->get('data_fim',null,'STRING');
        $hora_fim       = $app->input->get('hora_fim',null,'STRING');

        $website        = $app->input->get('website',null,'STRING');
        $facebook       = $app->input->get('facebook',null,'STRING');
        $instagram      = $app->input->get('instagram',null,'STRING');
        $layout_evento  = $app->input->get('layout_evento',null,'STRING');
        $coordenadas    = $app->input->get('coordenadas',null,'STRING');
        $observacoes    = $app->input->get('observacoes',null,'RAW');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChanged      = $app->input->get('vdFileUpChanged',null,'STRING');


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();


        $arCamposGet['coOrganizacao']      = $coOrganizacao;
        $arCamposGet['nome_evento']      = $nome_evento;
        $arCamposGet['desc_evento']      = $desc_evento;
        $arCamposGet['cat_evento']       = $cat_evento;
        $arCamposGet['freguesia_evento'] = $freguesia_evento;
        $arCamposGet['local_evento']     = $local_evento;
        $arCamposGet['data_inicio']      = $data_inicio;
        $arCamposGet['hora_inicio']      = $hora_inicio;
        $arCamposGet['data_fim']         = $data_fim;
        $arCamposGet['hora_fim']         = $hora_fim;
        $arCamposGet['website']          = $website;
        $arCamposGet['facebook']         = $facebook;
        $arCamposGet['instagram']        = $instagram;
        $arCamposGet['layout_evento']    = $layout_evento;
        $arCamposGet['coordenadas']      = $coordenadas;
        $arCamposGet['observacoes']      = $observacoes;

        $arCamposGet['vdFileUpChanged']  = $vdFileUpChanged;

        //$data = $this->ObjCulturaFields->checkCamposEmpty($arCamposGet, $data);
        $data = $arCamposGet;
        if (!empty($cultura_id)) $data['cultura_id'] = $cultura_id;
        return($data);
    }


    public function getPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $cultura_id  = $app->input->getInt('cultura_id');
        if (!empty($cultura_id)) $data['cultura_id'] = $cultura_id;

        $nif_evento     = $app->input->get('nif_evento',null,'STRING');
        $promotor_nome  = $app->input->get('promotor_nome',null,'STRING');
        $promotor_email = $app->input->get('promotor_email',null,'STRING');
        $coOrganizacao = $app->input->get('coOrganizacao',null,'STRING');

        $nome_evento    = $app->input->get('nome_evento',null,'STRING');
        $desc_evento    = $app->input->get('desc_evento',null,'RAW');
        $cat_evento     = $app->input->get('cat_evento',null,'STRING');
        $freguesia_evento = $app->input->get('freguesia_evento',null,'STRING');
        $local_evento   = $app->input->get('local_evento',null,'STRING');
        $data_inicio    = $app->input->get('data_inicio',null,'STRING');
        $hora_inicio    = $app->input->get('hora_inicio',null,'STRING');
        $data_fim       = $app->input->get('data_fim',null,'STRING');
        $hora_fim       = $app->input->get('hora_fim',null,'STRING');

        $website        = $app->input->get('website',null,'STRING');
        $facebook       = $app->input->get('facebook',null,'STRING');
        $instagram      = $app->input->get('instagram',null,'STRING');
        $coordenadas    = $app->input->get('coordenadas',null,'STRING');
        $layout_evento  = $app->input->get('layout_evento',null,'STRING');
        $observacoes    = $app->input->get('observacoes',null,'RAW');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChanged      = $app->input->get('vdFileUpChanged',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['nif_evento']       = $nif_evento;
        $arCamposGet['promotor_nome']    = $promotor_nome;
        $arCamposGet['promotor_email']   = $promotor_email;
        $arCamposGet['coOrganizacao']    = $coOrganizacao;
        $arCamposGet['nome_evento']      = $nome_evento;
        $arCamposGet['desc_evento']      = $desc_evento;
        $arCamposGet['cat_evento']       = $cat_evento;
        $arCamposGet['freguesia_evento'] = $freguesia_evento;
        $arCamposGet['local_evento']     = $local_evento;
        $arCamposGet['data_inicio']      = $data_inicio;
        $arCamposGet['hora_inicio']      = $hora_inicio;
        $arCamposGet['data_fim']         = $data_fim;
        $arCamposGet['hora_fim']         = $hora_fim;
        $arCamposGet['website']          = $website;
        $arCamposGet['facebook']         = $facebook;
        $arCamposGet['instagram']        = $instagram;
        $arCamposGet['layout_evento']    = $layout_evento;
        $arCamposGet['coordenadas']      = $coordenadas;
        $arCamposGet['observacoes']      = $observacoes;

        $arCamposGet['vdFileUpChanged']  = $vdFileUpChanged;

        //$data = $this->ObjCulturaFields->checkCamposEmpty($arCamposGet, $data);

        $data = $arCamposGet;
        if (!empty($cultura_id)) $data['cultura_id'] = $cultura_id;
        return($data);
    }


    public function getOnlyChangedPostedFormData4User($data)
     {
         if ( (string)$data['coOrganizacao']    === (string) $this->userCulturaData->coOrganizacao )   unset($data['coOrganizacao']);
         if ( (string)$data['nome_evento']    === (string) $this->userCulturaData->nome_evento )   unset($data['nome_evento']);
         if ( (string)$data['desc_evento']    === (string) $this->userCulturaData->desc_evento )   unset($data['desc_evento']);
         if ( (int)$data['cat_evento']        === (int) $this->userCulturaData->id_categoria )       unset($data['cat_evento']);
         if ( (int)$data['freguesia_evento']  === (int) $this->userCulturaData->id_freguesia ) unset($data['freguesia_evento']);
         if ( (string)$data['local_evento']   === (string) $this->userCulturaData->local_evento )  unset($data['local_evento']);
         if ( (string)$data['data_inicio']    === (string) $this->userCulturaData->data_inicio ) unset($data['data_inicio']);
         if ( (string)$data['hora_inicio']    === (string) $this->userCulturaData->hora_inicio ) unset($data['hora_inicio']);
         if ( (string)$data['data_fim']       === (string) $this->userCulturaData->data_fim ) unset($data['data_fim']);
         if ( (string)$data['hora_fim']       === (string) $this->userCulturaData->hora_fim ) unset($data['hora_fim']);
         if ( (string)$data['website']       === (string) $this->userCulturaData->website )   unset($data['website']);
         if ( (string)$data['facebook']      === (string) $this->userCulturaData->facebook )   unset($data['facebook']);
         if ( (string)$data['instagram']     === (string) $this->userCulturaData->instagram )   unset($data['instagram']);
         if ( (string)$data['coordenadas']=== ((string)$this->userCulturaData->latitude.','.(string)$this->userCulturaData->longitude) )   unset($data['coordenadas']);
         if ( (int)$data['layout_evento'] === (int) $this->userCulturaData->id_layout_evento )   unset($data['layout_evento']);
         if ( (string)$data['observacoes']     === (string) $this->userCulturaData->observacoes )   unset($data['observacoes']);

         if ( (string)$data['vdFileUpChanged']     == '0' )   unset($data['vdFileUpChanged']);

         // Se no final só tivermos definido o id, então não devemos atualizar nada
         if( (sizeof($data)==1) && (array_key_exists('cultura_id',$data)) ) unset($data['cultura_id']);

         return($data);
     }


    public function getOnlyChangedPostedFormData4Manager($data)
    {
        if ( (string)$data['nif_evento']      === (string) $this->userCulturaData->nif_evento )     unset($data['nif_evento']);
        if ( (string)$data['promotor_nome']   === (string) $this->userCulturaData->promotor_nome )  unset($data['promotor_nome']);
        if ( (string)$data['promotor_email']  === (string) $this->userCulturaData->promotor_email ) unset($data['promotor_email']);
        if ( (string)$data['coOrganizacao']    === (string) $this->userCulturaData->coOrganizacao )   unset($data['coOrganizacao']);

        if ( (string)$data['nome_evento']    === (string) $this->userCulturaData->nome_evento )   unset($data['nome_evento']);
        if ( (string)$data['desc_evento']    === (string) $this->userCulturaData->desc_evento )   unset($data['desc_evento']);
        if ( (int)$data['cat_evento']        === (int) $this->userCulturaData->id_categoria )       unset($data['cat_evento']);
        if ( (int)$data['freguesia_evento']  === (int) $this->userCulturaData->id_freguesia ) unset($data['freguesia_evento']);
        if ( (string)$data['local_evento']   === (string) $this->userCulturaData->local_evento )  unset($data['local_evento']);
        if ( (string)$data['data_inicio']    === (string) $this->userCulturaData->data_inicio ) unset($data['data_inicio']);
        if ( (string)$data['hora_inicio']    === (string) $this->userCulturaData->hora_inicio ) unset($data['hora_inicio']);
        if ( (string)$data['data_fim']       === (string) $this->userCulturaData->data_fim ) unset($data['data_fim']);
        if ( (string)$data['hora_fim']       === (string) $this->userCulturaData->hora_fim ) unset($data['hora_fim']);
        if ( (string)$data['website']       === (string) $this->userCulturaData->website )   unset($data['website']);
        if ( (string)$data['facebook']      === (string) $this->userCulturaData->facebook )   unset($data['facebook']);
        if ( (string)$data['instagram']     === (string) $this->userCulturaData->instagram )   unset($data['instagram']);
        if ( (string)$data['coordenadas']   === ((string)$this->userCulturaData->latitude.','.(string)$this->userCulturaData->longitude) )   unset($data['coordenadas']);
        if ( (int)$data['layout_evento']    === (int) $this->userCulturaData->id_layout_evento )   unset($data['layout_evento']);
        if ( (string)$data['observacoes']     === (string) $this->userCulturaData->observacoes )   unset($data['observacoes']);

        if ( (string)$data['vdFileUpChanged']     == '0' )   unset($data['vdFileUpChanged']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('cultura_id',$data)) ) unset($data['cultura_id']);

        return($data);
    }


    public function validateBeforeSave4User($data)
    {
        $msg = '';
        if (empty($data['nome_evento']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['desc_evento']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }


    public function validateBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['nome_evento']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['desc_evento']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


	public function save4User($data)
	{
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteCulturaHelper::update4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteCulturaHelper::cleanAllTmpUserState();
        return true;

        return true;
	}


    public function save4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteCulturaHelper::update4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteCulturaHelper::cleanAllTmpUserState();
        return true;

    }


    public function create4User($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteCulturaHelper::create4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteCulturaHelper::cleanAllTmpUserState();
        return true;

    }


    public function create4Manager($data)
    {

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteCulturaHelper::create4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteCulturaHelper::cleanAllTmpUserState();
        return true;
    }


    public function delete4User($data)
    {

        return true;
    }


    public function delete4Manager($data)
    {

        return true;
    }


}
