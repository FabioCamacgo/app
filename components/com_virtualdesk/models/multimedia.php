<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_virtualdesk
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
    JLoader::register('VirtualDeskSiteMultimediaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/multimedia/virtualdesksite_multimedia.php');


    use Joomla\Registry\Registry;

    /**
     * Diretorio Servicos model class for Users.
     *
     * @since  1.6
     */
    class VirtualDeskModelMultimedia extends JModelForm
    {
        /**
         * @var		object	The user diretorio servicos data.
         * @since   1.6
         */
        protected $data;
        protected $userSessionID;
        protected $userMultimediaData;
        public    $forcedLogout;



        public function __construct($config = array())
        {
            $config = array_merge(
                array(
                    'events_map' => array('validate' => 'user')
                ), $config
            );

            $this->forcedLogout = false;

            parent::__construct($config);

        }

        /*  ------- REMOVE ... ??? */

        public function checkin($userId = null)
        {

            return true;
        }

        public function checkout($userId = null)
        {

            return true;
        }

        public function getData()
        {
            return false;
        }

        public function getDataNew()
        {
            return false;

        }

        public function getForm($data = array(), $loadData = true)
        {
            // optamos por apenas carregar os dados, o form é feito na template
            return false;
        }

        /* --------------------- */


        public function setUserIdData()
        {
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
            if ($userSessionID <= 0) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
            }
            $this->userSessionID   = $userSessionID;
            return true;
        }


        public function setUserIdAndData4User($data)
        {

            return true;
        }


        public function setUserIdAndData4Manager($data)
        {
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
            if ($userSessionID <= 0) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
            }

            // Inicializa o array que compara depois se houve alteração de valores
            $loadedData = VirtualDeskSiteMultimediaHelper::getMultimediaDetail4Manager($data['multimedia_id']);
            if (empty($loadedData)) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }

            $this->userSessionID = $userSessionID;
            $this->userMultimediaData = $loadedData;
            return true;
        }


        protected function loadFormData()
        {
            // optamos por apenas carregar os dados, o form é feito na template
            return false;
        }


        protected function populateState()
        {
            // Get the application object.
            //$params = JFactory::getApplication()->getParams('com_virtualdesk');
            $params = JFactory::getApplication()->getParams();
            $userId = (int) JFactory::getUser()->get('id');
            // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
            // Set the user id.
            $this->setState('user.id', $userId);
            // Load the parameters.
            $this->setState('params', $params);
        }


        public function getPostedFormData()
        {
            $app         = JFactory::getApplication();
            $data        = array();
            return($data);
        }


        public function getPostedFormData4User()
        {   $app         = JFactory::getApplication();
            $data        = array();



            return($data);

        }


        public function getPostedFormData4Manager()
        {
            $app         = JFactory::getApplication();
            $data        = array();

            $multimedia_id  = $app->input->getInt('multimedia_id');
            if (!empty($multimedia_id)) $data['multimedia_id'] = $multimedia_id;


            $nomeVideo          = $app->input->get('nomeVideo',null,'STRING');
            $linkVideo          = $app->input->get('linkVideo',null,'STRING');
            $creditos           = $app->input->get('creditos',null,'STRING');
            $categoria          = $app->input->get('categoria',null,'STRING');
            $publish_FP         = $app->input->get('publish_FP',null,'STRING');
            $estado             = $app->input->get('estado',null,'STRING');
            $data_publicacao    = $app->input->get('data_publicacao',null,'STRING');

            $vdFileUpChangedCapa      = $app->input->get('vdFileUpChangedCapa',null,'STRING');

            // Campos EXTRA
            // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
            // se os campos estiverem hidden não devem passar
            $arCamposGet = array ();
            $arCamposGet['nomeVideo']       = $nomeVideo;
            $arCamposGet['linkVideo']       = $linkVideo;
            $arCamposGet['creditos']        = $creditos;
            $arCamposGet['categoria']       = $categoria;
            $arCamposGet['data_publicacao'] = $data_publicacao;
            $arCamposGet['publish_FP']      = $publish_FP;
            $arCamposGet['estado']          = $estado;

            $arCamposGet['vdFileUpChangedCapa']     = $vdFileUpChangedCapa;

            $data = $arCamposGet;

            if (!empty($multimedia_id)) $data['multimedia_id'] = $multimedia_id;

            return($data);

        }


        public function getOnlyChangedPostedFormData4User($data)
        {

            return($data);
        }


        public function getOnlyChangedPostedFormData4Manager($data)
        {
            if ( (string)$data['nomeVideo']         === (string) $this->userMultimediaData->nomeVideo )         unset($data['nomeVideo']);
            if ( (string)$data['linkVideo']         === (string) $this->userMultimediaData->linkVideo )         unset($data['linkVideo']);
            if ( (string)$data['creditos']          === (string) $this->userMultimediaData->creditos )          unset($data['creditos']);
            if ( (int)$data['categoria']            === (int) $this->userMultimediaData->categoria )            unset($data['categoria']);
            if ( (string)$data['data_publicacao']   === (string) $this->userMultimediaData->data_publicacao )   unset($data['data_publicacao']);
            if ( (int)$data['estado']               === (int) $this->userMultimediaData->estado )               unset($data['estado']);

            if ( (string)$data['vdFileUpChangedCapa']     == '0' )   unset($data['vdFileUpChangedCapa']);

            // Se no final só tivermos definido o id, então não devemos atualizar nada
            if( (sizeof($data)==1) && (array_key_exists('multimedia_id',$data)) ) unset($data['multimedia_id']);

            return($data);
        }


        public function validateBeforeSave4User($data)
        {

            return(true);

        }


        public function validateBeforeSave4Manager($data)
        {
            $msg = '';
            if (empty($data['nomeVideo']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_MULTIMEDIA_ERRO_NOMEVIDEO');
            if (empty($data['linkVideo']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_MULTIMEDIA_ERRO_LINKVIDEO');

            if($msg != "" ) {
                JFactory::getApplication()->enqueueMessage($msg, 'error' );
                return false;
            }

            return(true);
        }


        public function save4User($data)
        {

            return true;
        }


        public function save4Manager($data)
        {
            $app = JFactory::getApplication();
            // Check for request forgeries.
            JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

            // check session +  joomla user
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
            if ($userSessionID <= 0) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }

            // check se tem VD user associado and loads VD profile table and fields
            $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
            if ($userVDTable === false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }


            // executa faz o update (bind + save ) no VD user e no joomla user
            $resUpdate = VirtualDeskSiteMultimediaHelper::update4Manager($userSessionID, $userVDTable->id, $data);
            if ($resUpdate===false)
            {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                return false;
            }

            // Flush the data from the session.
            VirtualDeskSiteMultimediaHelper::cleanAllTmpUserState();
            return true;
        }


        public function create4User($data)
        {

            return true;
        }


        public function create4Manager($data)
        {
            $app = JFactory::getApplication();
            // Check for request forgeries.
            JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

            // check session +  joomla user
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
            if ($userSessionID <= 0) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }

            // check se tem VD user associado and loads VD profile table and fields
            $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
            if ($userVDTable === false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }


            // executa faz o update (bind + save ) no VD user e no joomla user
            $resUpdate = VirtualDeskSiteMultimediaHelper::create4Manager($userSessionID, $userVDTable->id, $data);
            if ($resUpdate===false)
            {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                return false;
            }


            // Flush the data from the session.
            VirtualDeskSiteMultimediaHelper::cleanAllTmpUserState();
            return true;
        }


        public function delete4User($data)
        {

            return true;
        }


        public function delete4Manager($data)
        {

            return true;
        }


    }

?>