<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSitePatrocinadoresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/patrocinadores/virtualdesksite_patrocinadores.php');


use Joomla\Registry\Registry;

/**
 * Percursos Pedestres model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelPatrocinadores extends JModelForm
{
    /**
     * @var		object	The user Percursos Pedestres data.
     * @since   1.6
     */
    protected $data;
    protected $userSessionID;
    protected $userPatrocinadoresData;
    public    $forcedLogout;


    public function __construct($config = array())
    {
        $config = array_merge(
            array(
                'events_map' => array('validate' => 'user')
            ), $config
        );

        $this->forcedLogout = false;

        parent::__construct($config);

    }


    public function checkin($userId = null)
    {

        return true;
    }


    public function checkout($userId = null)
    {

        return true;
    }


    public function getData()
    {
        return false;
    }


    public function getDataNew()
    {
        return false;

    }


    public function getForm($data = array(), $loadData = true)
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    public function setUserIdAndData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSitePatrocinadoresHelper::getPatrocinadoresDetail4Manager($data['patrocinadores_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userPatrocinadoresData = $loadedData;
        return true;
    }


    protected function loadFormData()
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }


    protected function populateState()
    {
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
    }


    public function getPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $patrocinadores_id  = $app->input->getInt('patrocinadores_id');
        if (!empty($patrocinadores_id)) $data['patrocinadores_id'] = $patrocinadores_id;

        $nomepatrocinador   = $app->input->get('nomepatrocinador',null,'STRING');
        $urlpatrocinador    = $app->input->get('urlpatrocinador',null,'STRING');
        $estado             = $app->input->get('estado',null,'STRING');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChangedLogo      = $app->input->get('vdFileUpChangedLogo',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['nomepatrocinador']    = $nomepatrocinador;
        $arCamposGet['urlpatrocinador']     = $urlpatrocinador;
        $arCamposGet['estado']              = $estado;

        $arCamposGet['vdFileUpChangedLogo']     = $vdFileUpChangedLogo;

        $data = $arCamposGet;

        if (!empty($patrocinadores_id)) $data['patrocinadores_id'] = $patrocinadores_id;

        return($data);

    }


    public function validateBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['nomepatrocinador']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedFormData4Manager($data)
    {
        if ( (string)$data['nomepatrocinador']  === (string) $this->userPatrocinadoresData->nomepatrocinador )    unset($data['nomepatrocinador']);
        if ( (string)$data['urlpatrocinador']   === (string) $this->userPatrocinadoresData->urlpatrocinador )    unset($data['urlpatrocinador']);
        if ( (int)$data['estado']               === (int) $this->userPatrocinadoresData->estado )    unset($data['estado']);

        if ( (string)$data['vdFileUpChangedLogo']     == '0' )   unset($data['vdFileUpChangedLogo']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('patrocinadores_id',$data)) ) unset($data['patrocinadores_id']);

        return($data);
    }


    public function create4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePatrocinadoresHelper::create4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePatrocinadoresHelper::cleanAllTmpUserState();
        return true;
    }


    public function save4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitePatrocinadoresHelper::update4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitePatrocinadoresHelper::cleanAllTmpUserState();
        return true;

    }


}
