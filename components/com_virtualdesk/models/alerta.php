<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta.php');
JLoader::register('VirtualDeskSiteAlertaFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alertafields.php');
JLoader::register('VirtualDeskBackofficeAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdeskBackOffice_alerta.php');

use Joomla\Registry\Registry;

/**
 * Alerta model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelAlerta extends JModelForm
{
	/**
	 * @var		object	The user alerta data.
	 * @since   1.6
	 */
	protected $data;

    protected $userSessionID;

    protected $userAlertaData;

    protected $managerAlertaData;

    protected $managerConfCategoriaData;

    protected $managerConfSubCategoriaData;

    public    $forcedLogout;

    protected $ObjAlertaFields;


	public function __construct($config = array())
	{
		$config = array_merge(
			array(
				'events_map' => array('validate' => 'user')
			), $config
		);

        $this->forcedLogout = false;

		parent::__construct($config);

        $this->ObjAlertaFields =  new VirtualDeskSiteAlertaFieldsHelper();
        $this->ObjAlertaFields->getFields();

	}

	/*  ------- REMOVE ... ??? */

	public function checkin($userId = null)
	{
		// Get the user id.
		/*$userId = (!empty($userId)) ? $userId : (int) $this->getState('user.id');

		if ($userId)
		{
			// Initialise the table with JUser.
			$table = JTable::getInstance('User');

			// Attempt to check the row in.
			if (!$table->checkin($userId))
			{	$this->setError($table->getError());
				return false;
			}
		}
*/
		return true;
	}

	public function checkout($userId = null)
	{
	    /*
		// Get the user id.
		$userId = (!empty($userId)) ? $userId : (int) $this->getState('user.id');

		if ($userId)
		{
			// Initialise the table with JUser.
			$table = JTable::getInstance('User');

			// Get the current user object.
			$user = JFactory::getUser();

			// Attempt to check the row out.
			if (!$table->checkout($user->get('id'), $userId))
			{
				$this->setError($table->getError());

				return false;
			}
		}
*/
		return true;
	}

	public function getData()
	{  /* $app = JFactory::getApplication();
		if ($this->data === null) {

            // check session +  joomla user
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($this->getState('user.id'));
            if ($userSessionID <= 0) {
                $this->setError('ERROR');
                $this->data = false;
                return $this->data;
            }

            // check se jos user está "enabled": not blocked and active and not checkout
            if (!VirtualDeskSiteUserHelper::checkUserJoomlaIsEnabled($userSessionID)) {
                $this->setError('ERROR');
                $this->data = false;
                return $this->data;

            }

            // check : VD user está enabled and active and not blocked
            if (!VirtualDeskSiteUserHelper::checkUserVirtualDeskIsEnabled($userSessionID)) {
                $this->setError('ERROR');
                $this->data = false;
                return $this->data;
            }


            // Se receber o alerta_id então é para carregar o registo, se não receber

            $IdAlerta        = JFactory::getApplication()->input->getInt('alerta_id');

            if( (int) $IdAlerta > 0 ) {
                // Carrega detalhe de um registo (pode ser para visualizar ou para editar (?layout=edit)
                $AlertaData = VirtualDeskBackofficeAlertaHelper::getAlertaDetail($userSessionID, $IdAlerta);
            }
            else {
                // Se não tem o alerta id, então é para carregar a lista
                $AlertaData = VirtualDeskBackofficeAlertaHelper::getAlertaList($userSessionID);
            }

            $this->data = $AlertaData;

                // Override the base data with any data in the session.
                // Mas só se a task for para editar caso contrário não carrega o que está na sessão...!
                if( JFactory::getApplication()->input->get('layout') == 'edit')
                {
                    if(!is_array($this->data)) {
                        $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.edit.alerta.data', array());
                        foreach ($temp as $k => $v) {
                            //Dados em array (FILE uploaded) é um file input
                            if ( (string)$k !== 'attachment' && (string)$k !== 'attachmentDELETE' && (string)$k !== 'attachmentUPDATE' ) $this->data->{$k} = $v;
                        }
                    }
                }
                else {
                    // Limpa valores guardados anteriormente
                    $app->setUserState('com_virtualdesk.edit.alerta.data', null);
                    $app->setUserState('com_virtualdesk.addnew.alerta.data', null);
                }



        }

		return $this->data;
*/
        return false;
	}

    public function getDataNew()
    {

        return false;

    }

	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	/* --------------------- */


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    public function setUserIdAndData4User($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteAlertaHelper::getAlertaView4UserDetail($data['alerta_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userAlertaData = $loadedData;
        return true;
    }


    public function setUserIdAndData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteAlertaHelper::getAlertaView4ManagerDetail($data['alerta_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->managerAlertaData = $loadedData;
        return true;
    }


    public function setUserIdAndDataConfCategoria4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteAlertaHelper::getAlertaViewConfCategoria4ManagerDetail($data['categoria_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID            = $userSessionID;
        $this->managerConfCategoriaData = $loadedData;
        return true;
    }


    public function setUserIdAndDataConfSubCategoria4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteAlertaHelper::getAlertaViewConfSubCategoria4ManagerDetail($data['subcategoria_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID               = $userSessionID;
        $this->managerConfSubCategoriaData = $loadedData;
        return true;
    }


    protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}


	protected function populateState()
	{
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
	}


    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }


    public function getPostedFormData4User()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $alerta_id  = $app->input->getInt('alerta_id');
        if (!empty($alerta_id)) $data['alerta_id'] = $alerta_id;

        $descricao      = $app->input->get('descricao',null,'STRING');
        $sitio          = $app->input->get('sitio',null,'STRING');
        $ptosrefs       = $app->input->get('ptosrefs',null,'STRING');
        $freguesias     = $app->input->get('freguesias',null,'STRING');
        $morada         = $app->input->get('morada',null,'STRING');
        $categoria      = $app->input->get('categoria',null,'STRING');
        $subcategoria   = $app->input->get('vdmenumain',null,'STRING');
        $coordenadas    = $app->input->get('coordenadas',null,'STRING');
        $veracid2       = $app->input->get('veracid2',null,'STRING');
        $politica2      = $app->input->get('politica2',null,'STRING');

        $nome           = $app->input->get('nome',null,'STRING');
        $fiscalid       = $app->input->get('fiscalid',null,'STRING');
        $email          = $app->input->get('email',null,'STRING');
        $telefone       = $app->input->get('telefone',null,'STRING');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChanged      = $app->input->get('vdFileUpChanged',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['descricao']     = $descricao;
        $arCamposGet['sitio']         = $sitio;
        $arCamposGet['ptosrefs']      = $ptosrefs;
        $arCamposGet['freguesias']    = $freguesias;
        $arCamposGet['morada']        = $morada;
        $arCamposGet['categoria']     = $categoria;
        $arCamposGet['subcategoria']  = $subcategoria;
        $arCamposGet['coordenadas']   = $coordenadas;
        $arCamposGet['veracid2']      = $veracid2;
        $arCamposGet['politica2']     = $politica2;

        $arCamposGet['nome']          = $nome;
        $arCamposGet['fiscalid']      = $fiscalid;
        $arCamposGet['email']         = $email;
        $arCamposGet['telefone']      = $telefone;

        $arCamposGet['vdFileUpChanged']      = $vdFileUpChanged;


        //$data = $this->ObjAlertaFields->checkCamposEmpty($arCamposGet, $data);

        $data = $arCamposGet;

        if (!empty($alerta_id)) $data['alerta_id'] = $alerta_id;

        return($data);
    }


    public function getPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $alerta_id  = $app->input->getInt('alerta_id');
        if (!empty($alerta_id)) $data['alerta_id'] = $alerta_id;

        $descricao      = $app->input->get('descricao',null,'STRING');
        $sitio          = $app->input->get('sitio',null,'STRING');
        $ptosrefs       = $app->input->get('ptosrefs',null,'STRING');
        $freguesias     = $app->input->get('freguesias',null,'STRING');
        $morada         = $app->input->get('morada',null,'STRING');
        $categoria      = $app->input->get('categoria',null,'STRING');
        $subcategoria   = $app->input->get('vdmenumain',null,'STRING');
        $coordenadas    = $app->input->get('coordenadas',null,'STRING');
        //$veracid2       = $app->input->get('veracid2',null,'STRING');
        //$politica2      = $app->input->get('politica2',null,'STRING');

        $nome           = $app->input->get('nome',null,'STRING');
        $fiscalid       = $app->input->get('fiscalid',null,'STRING');
        $email          = $app->input->get('email',null,'STRING');
        $telefone       = $app->input->get('telefone',null,'STRING');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChanged      = $app->input->get('vdFileUpChanged',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['descricao']     = $descricao;
        $arCamposGet['sitio']         = $sitio;
        $arCamposGet['ptosrefs']      = $ptosrefs;
        $arCamposGet['freguesias']    = $freguesias;
        $arCamposGet['morada']        = $morada;
        $arCamposGet['categoria']     = $categoria;
        $arCamposGet['subcategoria']  = $subcategoria;
        $arCamposGet['coordenadas']   = $coordenadas;
        //$arCamposGet['veracid2']      = $veracid2;
        //$arCamposGet['politica2']     = $politica2;

        $arCamposGet['nome']          = $nome;
        $arCamposGet['fiscalid']      = $fiscalid;
        $arCamposGet['email']         = $email;
        $arCamposGet['telefone']      = $telefone;

        $arCamposGet['vdFileUpChanged']      = $vdFileUpChanged;


        //$data = $this->ObjAlertaFields->checkCamposEmpty($arCamposGet, $data);

        $data = $arCamposGet;

        if (!empty($alerta_id)) $data['alerta_id'] = $alerta_id;

        return($data);
    }


    public function getPostedFormDataConfCategoria4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $categoria_id  = $app->input->getInt('categoria_id');
        if (!empty($categoria_id)) $data['categoria_id'] = $categoria_id;

        $name_PT     = $app->input->get('name_PT',null,'STRING');
        $name_EN     = $app->input->get('name_EN',null,'STRING');
        $Ref         = $app->input->get('Ref',null,'STRING');
        $id_concelho = $app->input->get('concelho',null,'STRING');
        $conf_email  = $app->input->get('conf_email',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['name_PT']     = $name_PT;
        $arCamposGet['name_EN']     = $name_EN;
        $arCamposGet['Ref']         = $Ref;
        $arCamposGet['id_concelho'] = $id_concelho;
        $arCamposGet['conf_email']  = $conf_email;

        $data = $arCamposGet;

        if (!empty($categoria_id)) $data['categoria_id'] = $categoria_id;

        return($data);
    }


    public function getPostedFormDataConfSubCategoria4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $subcategoria_id  = $app->input->getInt('subcategoria_id');
        if (!empty($categoria_id)) $data['subcategoria_id'] = $subcategoria_id;

        $name_PT      = $app->input->get('name_PT',null,'STRING');
        $name_EN      = $app->input->get('name_EN',null,'STRING');
        $id_categoria = $app->input->get('categoria',null,'STRING');
        $id_concelho  = $app->input->get('concelho',null,'STRING');
        $conf_email   = $app->input->get('conf_email',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['name_PT']     = $name_PT;
        $arCamposGet['name_EN']     = $name_EN;
        $arCamposGet['Id_categoria'] = $id_categoria;
        $arCamposGet['id_concelho'] = $id_concelho;
        $arCamposGet['conf_email']  = $conf_email;

        $data = $arCamposGet;

        if (!empty($subcategoria_id)) $data['subcategoria_id'] = $subcategoria_id;

        return($data);
    }


    public function getOnlyChangedPostedFormData4User($data)
     {
         if ( (int)$data['categoria']     === (int) $this->userAlertaData->id_categoria )  unset($data['categoria']);
         if ( (int)$data['subcategoria']  === (int) $this->userAlertaData->id_subcategoria )   unset($data['subcategoria']);
         if ( (string)$data['descricao']  === (string) $this->userAlertaData->descricao )   unset($data['descricao']);
         if ( (string)$data['sitio']      === (string) $this->userAlertaData->sitio )      unset($data['sitio']);
         if ( (string)$data['ptosrefs']   === (string) $this->userAlertaData->pontos_referencia )   unset($data['ptosrefs']);
         if ( (int)$data['freguesias']    === (int) $this->userAlertaData->id_freguesia ) unset($data['freguesias']);
         if ( (string)$data['morada']     === (string) $this->userAlertaData->local )     unset($data['morada']);
         if ( (string)$data['coordenadas']=== ((string)$this->userAlertaData->latitude.','.(string)$this->userAlertaData->longitude) )   unset($data['coordenadas']);
         if ( (string)$data['nome']       === (string) $this->userAlertaData->utilizador )   unset($data['nome']);
         if ( (string)$data['email']      === (string) $this->userAlertaData->email )   unset($data['email']);
         if ( (int)$data['fiscalid']      === (int) $this->userAlertaData->nif )   unset($data['fiscalid']);
         if ( (int)$data['telefone']      === (int) $this->userAlertaData->telefone )   unset($data['telefone']);

         //if ( (int)$data['veracid2']      === 1 )   unset($data['veracid2']);
         //if ( (int)$data['politica2']     === 1 )   unset($data['politica2']);

         if ( (string)$data['vdFileUpChanged']      == '0' )   unset($data['vdFileUpChanged']);

         // Se no final só tivermos definido o id, então não devemos atualizar nada
         if( (sizeof($data)==1) && (array_key_exists('alerta_id',$data)) ) unset($data['alerta_id']);

         return($data);
     }


    public function getOnlyChangedPostedFormData4Manager($data)
    {
        if ( (int)$data['categoria']     === (int) $this->managerAlertaData->id_categoria )  unset($data['categoria']);
        if ( (int)$data['subcategoria']  === (int) $this->managerAlertaData->id_subcategoria )   unset($data['subcategoria']);
        if ( (string)$data['descricao']  === (string) $this->managerAlertaData->descricao )   unset($data['descricao']);
        if ( (string)$data['sitio']      === (string) $this->managerAlertaData->sitio )      unset($data['sitio']);
        if ( (string)$data['ptosrefs']   === (string) $this->managerAlertaData->pontos_referencia )   unset($data['ptosrefs']);
        if ( (int)$data['freguesias']    === (int) $this->managerAlertaData->id_freguesia ) unset($data['freguesias']);
        if ( (string)$data['morada']     === (string) $this->managerAlertaData->local )     unset($data['morada']);
        if ( (string)$data['coordenadas']=== ((string)$this->managerAlertaData->latitude.','.(string)$this->managerAlertaData->longitude) )   unset($data['coordenadas']);
        if ( (string)$data['nome']       === (string) $this->managerAlertaData->utilizador )   unset($data['nome']);
        if ( (string)$data['email']      === (string) $this->managerAlertaData->email )   unset($data['email']);
        if ( (int)$data['fiscalid']      === (int) $this->managerAlertaData->nif )   unset($data['fiscalid']);
        if ( (int)$data['telefone']      === (int) $this->managerAlertaData->telefone )   unset($data['telefone']);

        //if ( (int)$data['veracid2']      === 1 )   unset($data['veracid2']);
        //if ( (int)$data['politica2']     === 1 )   unset($data['politica2']);

        if ( (string)$data['vdFileUpChanged']      == '0' )   unset($data['vdFileUpChanged']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('alerta_id',$data)) ) unset($data['alerta_id']);

        return($data);
    }


    public function getOnlyChangedPostedFormDataConfCategoria4Manager($data)
    {
        if ( (string)$data['name_PT']     === (string) $this->managerConfCategoriaData->name_PT )    unset($data['name_PT']);
        if ( (string)$data['name_EN']     === (string) $this->managerConfCategoriaData->name_EN )    unset($data['name_EN']);
        if ( (string)$data['Ref']         === (string) $this->managerConfCategoriaData->Ref )        unset($data['Ref']);
        if ( (int)$data['id_concelho']    === (int) $this->managerConfCategoriaData->id_concelho )   unset($data['id_concelho']);
        if ( (string)$data['conf_email']  === (string) $this->managerConfCategoriaData->conf_email ) unset($data['conf_email']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('categoria_id',$data)) ) unset($data['categoria_id']);

        return($data);
    }


    public function getOnlyChangedPostedFormDataConfSubCategoria4Manager($data)
    {
        if ( (string)$data['name_PT']     === (string) $this->managerConfSubCategoriaData->name_PT )    unset($data['name_PT']);
        if ( (string)$data['name_EN']     === (string) $this->managerConfSubCategoriaData->name_EN )    unset($data['name_EN']);
        if ( (int)$data['Id_categoria']   === (int) $this->managerConfSubCategoriaData->Id_categoria )  unset($data['Id_categoria']);
        if ( (int)$data['id_concelho']    === (int) $this->managerConfSubCategoriaData->id_concelho )   unset($data['id_concelho']);
        if ( (string)$data['conf_email']  === (string) $this->managerConfSubCategoriaData->conf_email ) unset($data['conf_email']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('subcategoria_id',$data)) ) unset($data['subcategoria_id']);

        return($data);
    }


    public function validateBeforeSave4User($data)
    {
        $msg = '';
        if (empty($data['sitio']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['ptosrefs']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['freguesias']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['morada']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['categoria'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if (empty($data['subcategoria'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        //if (empty($data['coordenadas'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        //if (empty($data['veracid2'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        //if (empty($data['politica2'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');

        if (empty($data['nome'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if (empty($data['fiscalid'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if (empty($data['email'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if (empty($data['telefone'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }


    public function validateBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['sitio']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['ptosrefs']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['freguesias']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['morada']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['categoria'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if (empty($data['subcategoria'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        //if (empty($data['coordenadas'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        //if (empty($data['veracid2'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        //if (empty($data['politica2'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');

        if (empty($data['nome'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if (empty($data['fiscalid'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if (empty($data['email'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if (empty($data['telefone'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }


    public function validateBeforeSaveConfCategoria4Manager($data)
    {
        $msg = '';
        if (empty($data['name_PT']))      $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_NOMEPT');
        if (empty($data['name_EN']))      $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_NOMEEN');
        if (empty($data['Ref']))          $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ALERTA_LISTA_REFERENCIA');
        ##if (empty($data['id_concelho']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_CONCELHO');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }


    public function validateBeforeSaveConfSubCategoria4Manager($data)
    {
        $msg = '';
        if (empty($data['name_PT']))      $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_NOMEPT');
        if (empty($data['name_EN']))      $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_NOMEEN');
        if (empty($data['Id_categoria'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ALERTA_LISTA_CATEGORIA');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }


	public function save4User($data)
	{

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAlertaHelper::update4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
        return true;
	}


    public function save4Manager($data)
    {

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAlertaHelper::update4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
        return true;
    }


    public function saveConfCategoria4Manager($data)
    {

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAlertaHelper::updateConfCategoria4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
        return true;
    }


    public function saveConfSubCategoria4Manager($data)
    {

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAlertaHelper::updateConfSubCategoria4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
        return true;
    }


    public function create4User($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAlertaHelper::create4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
        return true;
    }


    public function create4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAlertaHelper::create4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
        return true;
    }


    public function createConfCategoria4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAlertaHelper::createConfCategoria4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
        return true;
    }


    public function createConfSubCategoria4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAlertaHelper::createConfSubCategoria4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
        return true;
    }


    public function delete4User($data)
    {

        return true;
    }


    public function delete4Manager($data)
    {

        return true;
    }


}
