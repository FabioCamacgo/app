<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

use Joomla\Registry\Registry;

/**
 * Profile model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelProfile extends JModelForm
{
	/**
	 * @var		object	The user profile data.
	 * @since   1.6
	 */
	protected $data;

    protected $userSessionID;

    protected $userProfileData;

    public    $forcedLogout;

    protected $ObjUserFields;

	/**
	 * Constructor
	 *
	 * @param   array  $config  An array of configuration options (name, state, dbo, table_path, ignore_request).
	 *
	 * @since   3.2
	 *
	 * @throws  Exception
	 */
	public function __construct($config = array())
	{
		$config = array_merge(
			array(
				'events_map' => array('validate' => 'user')
			), $config
		);

        $this->forcedLogout = false;

		parent::__construct($config);

        $this->ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
        $this->ObjUserFields->getUserFields();
        $this->ObjUserFields->getUserFieldLogin();

		// Load the helper and model used for two factor authentication
//		JLoader::register('UsersModelUser', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/models/user.php');
//		JLoader::register('UsersHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/users.php');
	}

	/**
	 * Method to check in a user.
	 *
	 * @param   integer  $userId  The id of the row to check out.
	 *
	 * @return  boolean  True on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function checkin($userId = null)
	{
		// Get the user id.
		$userId = (!empty($userId)) ? $userId : (int) $this->getState('user.id');

		if ($userId)
		{
			// Initialise the table with JUser.
			$table = JTable::getInstance('User');

			// Attempt to check the row in.
			if (!$table->checkin($userId))
			{	$this->setError($table->getError());
				return false;
			}
		}

		return true;
	}

	/**
	 * Method to check out a user for editing.
	 *
	 * @param   integer  $userId  The id of the row to check out.
	 *
	 * @return  boolean  True on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function checkout($userId = null)
	{
		// Get the user id.
		$userId = (!empty($userId)) ? $userId : (int) $this->getState('user.id');

		if ($userId)
		{
			// Initialise the table with JUser.
			$table = JTable::getInstance('User');

			// Get the current user object.
			$user = JFactory::getUser();

			// Attempt to check the row out.
			if (!$table->checkout($user->get('id'), $userId))
			{
				$this->setError($table->getError());

				return false;
			}
		}

		return true;
	}

	/**
	 * Method to get the profile form data.
	 *
	 * The base form data is loaded and then an event is fired
	 * for users plugins to extend the data.
	 *
	 * @return  mixed  	Data object on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function getData()
	{
		if ($this->data === null) {

            // check session +  joomla user
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($this->getState('user.id'));
            if ($userSessionID <= 0) {
                $this->setError('ERROR');
                $this->data = false;
            }

            // check se jos user está "enabled": not blocked and active and not checkout
            if (!VirtualDeskSiteUserHelper::checkUserJoomlaIsEnabled($userSessionID)) {
                $this->setError('ERROR');
                $this->data = false;
            }


            // check : VD user está enabled and active and not blocked
            if (!VirtualDeskSiteUserHelper::checkUserVirtualDeskIsEnabled($userSessionID)) {
                $this->setError('ERROR');
                $this->data = false;
            }


            // check se tem VD user associado and loads VD profile table and fields
            $userProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($userSessionID);
            if ($userProfileData === false) {
                $this->setError('ERROR');
                $this->data = false;
            }


            // TODO check se jos user não está:  checkout antes de gravar

            $this->data = $userProfileData;

            // Override the base user data with any data in the session.
            // Mas só se a task for para editar caso contrário não carrega o que está na sessão...!
            if( JFactory::getApplication()->input->get('layout') == 'edit')
            {

            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.edit.profile.data', array());
            foreach ($temp as $k => $v) {
                // Além disso não faz sentido carregar os dados em array (FILE uploaded) para o profile do xcepto se for o useravatar pois é um file input
                if( (string)$k !== 'useravatar' ) $this->data->$k = $v;
            }
            }


            // CARREGA DADOS DA AREA ACTUAÇÃO - multiple choice
            // comparar dados serializados com explode/implode com dados carregados têm de ser iguais
            $ComparaSerializeAreaAct = VirtualDeskSiteUserFieldsHelper::getAreaActListSelected ($userSessionID, $userProfileData->id);
            asort($ComparaSerializeAreaAct);
            $ComparaSerializeAreaAct = implode('|',$ComparaSerializeAreaAct);
            if($ComparaSerializeAreaAct!=  $userProfileData->areaact )  return false;
            $FieldSerializaAreaAct   = explode('|',$userProfileData->areaact);
            if (!empty($this->data->areaact)) $this->data->areaact = $FieldSerializaAreaAct;


            // Unset the passwords... para não ficarem visiveis...
            unset($this->data->password);
            unset($this->data->password1);
            unset($this->data->password2);


        }
		return $this->data;
	}


	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}


	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		// Get the application object.
		$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();

		// Get the user id.
		//$userId = JFactory::getApplication()->getUserState('com_virtualdesk.edit.profile.id');
//		$userId = !empty($userId) ? $userId : (int) JFactory::getUser()->get('id');
        $userId = (int) JFactory::getUser()->get('id');

        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
		// Set the user id.
		$this->setState('user.id', $userId);

		// Load the parameters.
		$this->setState('params', $params);
	}



    public function getPostedFormData($bitFromPermAdmin=false)
    {  $app         = JFactory::getApplication();

       $data        = array();

       if($bitFromPermAdmin==true) $permadmin_user_id = $app->input->get('permadmin_user_id',null, 'STRING');

       $login       = $app->input->get('login',null, 'STRING');
       $password1   = $app->input->get('password1',null,'STRING');
       $password2   = $app->input->get('password2',null,'STRING');
       $email1      = $app->input->get('email1',null, 'STRING');
       $email2      = $app->input->get('email2',null, 'STRING');
       $name        = $app->input->get('name',null,'STRING');
       $address     = $app->input->get('address',null,'STRING');
       $postalcode  = $app->input->get('postalcode',null,'STRING');
       $fiscalid    = $app->input->get('fiscalid',null,'STRING');
       $civilid     = $app->input->get('civilid',null,'STRING');

       $firma       = $app->input->get('firma',null,'STRING');
       $designacaocomercial = $app->input->get('designacaocomercial',null,'STRING');
       $sectoratividade = $app->input->get('sectoratividade',null,'STRING');
       $caeprincipal = $app->input->get('caeprincipal',null,'STRING');
       $phone1 = $app->input->get('phone1',null,'STRING');
       $phone2 = $app->input->get('phone2',null,'STRING');
       $emailsec = $app->input->get('emailsec',null,'STRING');
       $facebook = $app->input->get('facebook',null,'STRING');
       $website = $app->input->get('website',null,'STRING');
       $concelho = $app->input->get('concelho',null,'STRING');
       $freguesia = $app->input->get('freguesia','','STRING');
       $localidade = $app->input->get('localidade',null,'STRING');
       $areaact = $app->input->get('areaact',null,'ARRAY');
       $managername = $app->input->get('managername',null,'STRING');
       $managerposition = $app->input->get('managerposition',null,'STRING');
       $managercontact = $app->input->get('managercontact',null,'STRING');
       $manageremail = $app->input->get('manageremail',null,'STRING');
       $maplatlong = $app->input->get('maplatlong',null,'STRING');

       $cargo      = $app->input->get('cargo',null,'STRING');
       $funcao     = $app->input->get('funcao',null,'STRING');

       $veracidadedados      = $app->input->get('veracidadedados',null,'STRING');
       $politicaprivacidade     = $app->input->get('politicaprivacidade',null,'STRING');


       $useravatar  = $app->input->files->get('useravatar');

       // Campos BASE
       if($bitFromPermAdmin==true) {
         if (!empty($permadmin_user_id)) $data['permadmin_user_id'] = $permadmin_user_id;
       }
       if (!empty($login)) $data['login']           = $login;
       if (!empty($password1)) $data['password1']   = $password1;
       if (!empty($password2)) $data['password2']   = $password2;
       if (!empty($email1)) $data['email1']         = $email1;
       if (!empty($email2)) $data['email2']         = $email2;

       // Campos EXTRA
       // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
       // se os campos estiverem hidden não devem passar
       $arCamposGet = array ();
       $arCamposGet['name']    = $name;
       $arCamposGet['address'] = $address;
       $arCamposGet['postalcode'] = $postalcode;
       $arCamposGet['fiscalid'] = $fiscalid;
       $arCamposGet['civilid'] = $civilid;

       $arCamposGet['firma'] = $firma;
       $arCamposGet['designacaocomercial'] = $designacaocomercial;
       $arCamposGet['sectoratividade'] = $sectoratividade;
       $arCamposGet['caeprincipal'] = $caeprincipal;
       $arCamposGet['phone1'] = $phone1;
       $arCamposGet['phone2'] = $phone2;
       $arCamposGet['emailsec'] = $emailsec;
       $arCamposGet['facebook'] = $facebook;
       $arCamposGet['website'] = $website;
       $arCamposGet['concelho'] = $concelho;
       $arCamposGet['freguesia'] = $freguesia;
       $arCamposGet['localidade'] = $localidade;
       $arCamposGet['areaact'] = $areaact;
       $arCamposGet['managername'] = $managername;
       $arCamposGet['managerposition'] = $managerposition;
       $arCamposGet['managercontact'] = $managercontact;
       $arCamposGet['manageremail'] = $manageremail;
       $arCamposGet['maplatlong'] = $maplatlong;

       $arCamposGet['cargo']  = $cargo;
       $arCamposGet['funcao'] = $funcao;

       $arCamposGet['veracidadedados']  = $veracidadedados;
       $arCamposGet['politicaprivacidade'] = $politicaprivacidade;


       $data = $this->ObjUserFields->checkCamposEmptyProfile($arCamposGet, $data);

       // No user avatar só vai considerar um upload da imagem se :
       if (!empty($useravatar)) {
          if( !empty($useravatar['name']) ) {
             $data['useravatar'] = $useravatar;
          }
       }

       return($data);
    }



    public function getNewRegistrationPostedFormData()
    {   $app         = JFactory::getApplication();

        $data        = array();

        $login       = $app->input->get('login',null, 'STRING');
        $password1   = $app->input->get('password1',null,'STRING');
        $password2   = $app->input->get('password2',null,'STRING');
        $email1      = $app->input->get('email1',null, 'STRING');
        $email2      = $app->input->get('email2',null, 'STRING');
        $name        = $app->input->get('name',null,'STRING');
        $address     = $app->input->get('address',null,'STRING');
        $postalcode  = $app->input->get('postalcode',null,'STRING');
        $fiscalid    = $app->input->get('fiscalid',null,'STRING');
        $civilid     = $app->input->get('civilid',null,'STRING');

        $firma       = $app->input->get('firma',null,'STRING');
        $designacaocomercial = $app->input->get('designacaocomercial',null,'STRING');
        $sectoratividade = $app->input->get('sectoratividade',null,'STRING');
        $caeprincipal = $app->input->get('caeprincipal',null,'STRING');
        $phone1 = $app->input->get('phone1',null,'STRING');
        $phone2 = $app->input->get('phone2',null,'STRING');
        $emailsec = $app->input->get('emailsec',null,'STRING');
        $facebook = $app->input->get('facebook',null,'STRING');
        $website = $app->input->get('website',null,'STRING');
        $concelho = $app->input->get('concelho',null,'STRING');
        $freguesia = $app->input->get('freguesia','','STRING');
        $localidade = $app->input->get('localidade',null,'STRING');
        $areaact = $app->input->get('areaact',null,'ARRAY');
        $managername = $app->input->get('managername',null,'STRING');
        $managerposition = $app->input->get('managerposition',null,'STRING');
        $managercontact = $app->input->get('managercontact',null,'STRING');
        $manageremail = $app->input->get('manageremail',null,'STRING');
        $maplatlong = $app->input->get('maplatlong',null,'STRING');

        $cargo  = $app->input->get('cargo',null,'STRING');
        $funcao = $app->input->get('funcao',null,'STRING');

        $veracidadedados  = $app->input->get('veracidadedados',null,'STRING');
        $politicaprivacidade = $app->input->get('politicaprivacidade',null,'STRING');


        // Campos BASE
        if (!empty($login)) $data['login']           = $login;
        if (!empty($login)) $data['username']        = $login;
        if (!empty($password1)) $data['password1']   = $password1;
        if (!empty($password2)) $data['password2']   = $password2;
        if (!empty($email1)) $data['email']         = $email1;
        if (!empty($email1)) $data['email1']         = $email1;
        if (!empty($email2)) $data['email2']         = $email2;
        //if (!empty($name)) $data['name']             = $name;


        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['name']    = $name;
        $arCamposGet['address'] = $address;
        $arCamposGet['postalcode'] = $postalcode;
        $arCamposGet['fiscalid'] = $fiscalid;
        $arCamposGet['civilid'] = $civilid;

        $arCamposGet['firma'] = $firma;
        $arCamposGet['designacaocomercial'] = $designacaocomercial;
        $arCamposGet['sectoratividade'] = $sectoratividade;
        $arCamposGet['caeprincipal'] = $caeprincipal;
        $arCamposGet['phone1'] = $phone1;
        $arCamposGet['phone2'] = $phone2;
        $arCamposGet['emailsec'] = $emailsec;
        $arCamposGet['facebook'] = $facebook;
        $arCamposGet['website'] = $website;
        $arCamposGet['concelho'] = $concelho;
        $arCamposGet['freguesia'] = $freguesia;
        $arCamposGet['localidade'] = $localidade;
        $arCamposGet['areaact'] = $areaact;
        $arCamposGet['managername'] = $managername;
        $arCamposGet['managerposition'] = $managerposition;
        $arCamposGet['managercontact'] = $managercontact;
        $arCamposGet['manageremail'] = $manageremail;
        $arCamposGet['maplatlong'] = $maplatlong;

        $arCamposGet['cargo']  = $cargo;
        $arCamposGet['funcao'] = $funcao;

        $arCamposGet['veracidadedados']  = $veracidadedados;
        $arCamposGet['politicaprivacidade'] = $politicaprivacidade;

        $data = $this->ObjUserFields->checkCamposEmptyRegistration($arCamposGet, $data);

        return($data);
    }



    public function getOnlyChangedPostedFormData($data, $bitFromPermAdmin=false)
    {
        if ( (string)$data['login'] === (string) $this->userProfileData->login ) unset($data['login']);

        // Carrega o parâmetro que indica se o login pode ou não ser editado:
        $params           = JComponentHelper::getParams('com_virtualdesk');
        $login_allowedit  = $params->get('userfield_login_allowedit');
        if ( $login_allowedit === 0 ) unset($data['login']);

        if ( (string)$data['email1'] === (string) $this->userProfileData->email ) {
            unset($data['email1']);
            unset($data['email2']);
        }

        if ( (string)$data['name'] === (string) $this->userProfileData->name ) unset($data['name']);
        if ( (string)$data['address'] === (string) $this->userProfileData->address ) unset($data['address']);
        if ( (string)$data['postalcode'] === (string) $this->userProfileData->postalcode ) unset($data['postalcode']);
        if ( (string)$data['fiscalid'] === (string) $this->userProfileData->fiscalid ) unset($data['fiscalid']);
        if ( (string)$data['civilid'] === (string) $this->userProfileData->civilid ) unset($data['civilid']);

        if ( (string)$data['firma'] === (string) $this->userProfileData->firma ) unset($data['firma']);
        if ( (string)$data['designacaocomercial'] === (string) $this->userProfileData->designacaocomercial ) unset($data['designacaocomercial']);
        if ( (string)$data['sectoratividade'] === (string) $this->userProfileData->sectoratividade ) unset($data['sectoratividade']);
        if ( (string)$data['caeprincipal'] === (string) $this->userProfileData->caeprincipal ) unset($data['caeprincipal']);
        if ( (string)$data['phone1'] === (string) $this->userProfileData->phone1 ) unset($data['phone1']);
        if ( (string)$data['phone2'] === (string) $this->userProfileData->phone2 ) unset($data['phone2']);
        if ( (string)$data['website'] === (string) $this->userProfileData->website ) unset($data['website']);
        if ( (string)$data['facebook'] === (string) $this->userProfileData->facebook ) unset($data['facebook']);
        if ( (string)$data['emailsec'] === (string) $this->userProfileData->emailsec ) unset($data['emailsec']);
        if ( (string)$data['concelho'] === (string) $this->userProfileData->concelho ) unset($data['concelho']);
        if ( (string)$data['freguesia'] === (string) $this->userProfileData->freguesia ) unset($data['freguesia']);
        if ( (string)$data['localidade'] === (string) $this->userProfileData->localidade ) unset($data['localidade']);
        if ( (string)$data['managername'] === (string) $this->userProfileData->managername ) unset($data['managername']);
        if ( (string)$data['managerposition'] === (string) $this->userProfileData->managerposition ) unset($data['managerposition']);
        if ( (string)$data['managercontact'] === (string) $this->userProfileData->managercontact ) unset($data['managercontact']);
        if ( (string)$data['manageremail'] === (string) $this->userProfileData->manageremail ) unset($data['manageremail']);
        if ( (string)$data['maplatlong'] === (string) $this->userProfileData->maplatlong ) unset($data['maplatlong']);

        if ( (string)$data['cargo'] === (string) $this->userProfileData->cargo ) unset($data['cargo']);
        if ( (string)$data['funcao'] === (string) $this->userProfileData->funcao ) unset($data['funcao']);

        if ( (string)$data['veracidadedados'] === (string) $this->userProfileData->veracidadedados ) unset($data['veracidadedados']);
        if ( (string)$data['politicaprivacidade'] === (string) $this->userProfileData->politicaprivacidade ) unset($data['politicaprivacidade']);


        $arDadosAreaAct = $data['areaact'];
        if(!is_array($arDadosAreaAct)) $arDadosAreaAct = array();
        asort($arDadosAreaAct);
        if ( implode('|',$arDadosAreaAct) === (string) $this->userProfileData->areaact ) unset($data['areaact']);


        // Se no final só tivermos definido o contactus_id, então não devemos atualizar nada
        if( $bitFromPermAdmin==true) {
            if ((sizeof($data) == 1) && (array_key_exists('permadmin_user_id', $data))) unset($data['permadmin_user_id']);
        }

        // no caso do useravatar só se o array vier com um ficheiro é que vai ser considerado... verifica antes no getPostedFormData()
        return($data);
    }


    public function setUserIdAndProfileData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Carrega os dados existentes na base de dados
        $userProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($userSessionID);
        if ( $userProfileData === false ) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_PASSCHANGE_USERNOTFOUND'), 'error');
            return(false);
        }

       $this->userSessionID   = $userSessionID;
       $this->userProfileData = $userProfileData;
       return true;
    }


    public function setUserIdFullData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $getInputPermUser_id = JFactory::getApplication()->input->getInt('permadmin_user_id');
        $LoadedUserJoomlaID  = VirtualDeskSiteUserHelper::getJoomlaIDFromVDUserId($getInputPermUser_id);
        $loadedData          = VirtualDeskSiteUserHelper::getUserFromJoomlaID($LoadedUserJoomlaID);

        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID     = $userSessionID;
        $this->userProfileData = $loadedData;
        return true;
    }


    /**
     * Verifica se ows dados a gravar estão coorecto: email, username (compliant and exist),
     *
     * @param   array  $data  The form data.
     *
     * @return  mixed
     *
     * @since   1.6
     */
    public function validateBeforeSave($data)
    {

        $obParam      = new VirtualDeskSiteParamsHelper();
        $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
        $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');

        // check username IS compliant ...
        if(!VirtualDeskUserHelper::checkIfUserNameIsCompliant($data['login'])) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_NEWREGISTRATION_ERROR_USERNAMENOTCOMPLIANT'), 'error');
            return(false);
        }

        // Verifica se o username no caso de estar a ser alterado se já existe noutro utilizador
        if(VirtualDeskSiteUserHelper::checkIfUserNameisDuplicate($this->userSessionID, $data['login'])) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_NEWREGISTRATION_ERROR_USERNAMEEXIST',$copyrightAPP, $emailCopyrightGeral), 'error');
            return(false);
        }

        // Verifica se o usernameestá configurado para receber NIF. Se sim aplica a validação do conteúdo
        if(!VirtualDeskSiteUserHelper::checkUserNameIsNIF($data['login'])) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_USER_INVALID_NIF'), 'error');
            return(false);
        }

        // Verifica se o EMAIL no caso de estar a ser alterado se já existe noutro utilizador
        if(VirtualDeskSiteUserHelper::checkIfEmailisDuplicate($this->userSessionID, $data['email1'])) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_NEWREGISTRATION_ERROR_EMAILEXIST'), 'error');
            return(false);
        }

        // email1 + email2 devem estar iguais. Se não tiver valor nem altera
        // Verifica se o EMAIL no caso de estar a ser alterado se já existe noutro utilizador
        if( (string)$data['email1']!='' &&  (string)$data['email1'] != (string)$data['email2'] ) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_NEWREGISTRATION_ERROR_EMAILNOTMATCH'), 'error');
            return(false);
        }

        //Se a password1 estiver preenchida deve estar igual à password 2... por defeito vêm ambas VAZIAS e não são alteradas
        if( (string)$data['password1']!='' &&  (string)$data['password1'] != (string)$data['password2'] ) {
            unset ($data['password1']);
            unset ($data['password2']);
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_NEWREGISTRATION_ERROR_PASSNOTMATCH'), 'error');
            return(false);
        }

        // Se a password é para ser alterada então temos de validar a password (tamanho, caracteres, ver como o joomla faz a validação...
        if( (string)$data['password1']!='')
        {
           $passCheck =  new VirtualDeskPasswordCheckHelper('com_virtualdesk');
           if(! $passCheck->executeChecks($this->userProfileData , $data , false) ) {
               JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_WARNING', $passCheck->output_warning), 'warning' );
               return false;
           }
        }

        if(!VirtualDeskSiteFileUploadHelper::checkUserAvatar($this->userSessionID, $data['useravatar']))
        {
            // JFactory::getApplication()->enqueueMessage('ERROR O ficheiro não ,,, ...', 'warning');
            return(false);
        }

        // Verifica se o campos obrigatórios estão preenchidos
        $msgRequiredEmptyFieldsProfile =  $this->ObjUserFields->checkRequiredEmptyFieldsProfile();
        if($msgRequiredEmptyFieldsProfile != "" ) {
            JFactory::getApplication()->enqueueMessage($msgRequiredEmptyFieldsProfile, 'error' );
            return false;
        }



        return(true);
    }



    /**
     * Verifica se ows dados a gravar estão coorecto: email, username (compliant and exist),
     *
     * @param   array  $data  The form data.
     *
     * @return  mixed
     *
     * @since   1.6
    */
    public function validateNewRegistrationBeforeSave($data)
    {

        $obParam      = new VirtualDeskSiteParamsHelper();
        $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
        $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');

        // Verifica campos BASE , os campos EXTRA são validados antes e assinalados no final deste método
        // Se faltar preencher alguns dos campos devolve um erro de preenchimento
        //
        $arFieldsRequired = array('login','password1','password2','email1','email2');
        foreach($arFieldsRequired as $chvRequired => $valRequired)
        {
            if (empty($data[$valRequired]))
            { JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED').$valRequired, 'error');
              return(false);
              break;
            }
        }

        // check username IS compliant ...
        if(!VirtualDeskUserHelper::checkIfUserNameIsCompliant($data['login'])) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_NEWREGISTRATION_ERROR_USERNAMENOTCOMPLIANT'), 'error');
            return(false);
        }

        // Verifica se o username no caso de estar a ser alterado se já existe noutro utilizador
        if(VirtualDeskSiteUserHelper::checkIfUserNameisDuplicate(-1, $data['login'])) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_NEWREGISTRATION_ERROR_USERNAMEEXIST', $copyrightAPP, $emailCopyrightGeral), 'error');
            return(false);
        }

        // Verifica se o usernameestá configurado para receber NIF. Se sim aplica a validação do conteúdo
        if(!VirtualDeskSiteUserHelper::checkUserNameIsNIF($data['login'])) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_USER_INVALID_NIF'), 'error');
            return(false);
        }

        // Verifica se o EMAIL no caso de estar a ser alterado se já existe noutro utilizador
        if(VirtualDeskSiteUserHelper::checkIfEmailisDuplicate(-1, $data['email1'])) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_NEWREGISTRATION_ERROR_EMAILEXIST'), 'error');
            return(false);
        }

        // email1 + email2 devem estar iguais. Se não tiver valor nem altera
        // Verifica se o EMAIL no caso de estar a ser alterado se já existe noutro utilizador
        if( (string)$data['email1']!='' &&  (string)$data['email1'] != (string)$data['email2'] ) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_NEWREGISTRATION_ERROR_EMAILNOTMATCH'), 'error');
            return(false);
        }

        //Se a password1 estiver preenchida deve estar igual à password 2... por defeito vêm ambas VAZIAS e não são alteradas
        if( (string)$data['password1']!='' &&  (string)$data['password1'] != (string)$data['password2'] ) {
            unset ($data['password1']);
            unset ($data['password2']);
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_NEWREGISTRATION_ERROR_PASSNOTMATCH'), 'error');
            return(false);
        }


        // Se a password é para ser alterada então temos de validar a password (tamanho, caracteres, ver como o joomla faz a validação...
        if( (string)$data['password1']!='')
        {   $passCheck =  new VirtualDeskPasswordCheckHelper('com_virtualdesk');
            if(! $passCheck->executeChecks(null , $data , false) ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_WARNING', $passCheck->output_warning), 'error' );
                return false;
            }
        }

        // Verifica se o campos obrigatórios estão preenchidos
        $msgRequiredEmptyFieldsProfile =  $this->ObjUserFields->checkRequiredEmptyFieldsRegistration();
        if($msgRequiredEmptyFieldsProfile != "" ) {
            JFactory::getApplication()->enqueueMessage($msgRequiredEmptyFieldsProfile, 'error' );
            return false;
        }

        return(true);
    }






	/**
	 * Method to save the form data.
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  mixed  The user id on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function save($data, $bitFromPermAdmin=false)
	{



        // check session +  joomla user
//        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($this->getState('user.id'));
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PROFILE_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se jos user está "enabled": not blocked and active and not checkout
        if (!VirtualDeskSiteUserHelper::checkUserJoomlaIsEnabled($userSessionID)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PROFILE_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check : VD user está enabled and active and not blocked
        if (!VirtualDeskSiteUserHelper::checkUserVirtualDeskIsEnabled($userSessionID)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PROFILE_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PROFILE_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // VOLTEI A FAZER ESTa VERIFICAÇÂO AQUI. Já tinha sido feito getOnlyChangedPostedFormData()
        //Carrega o parâmetro que indica se o login pode ou não ser editado:
        $params           = JComponentHelper::getParams('com_virtualdesk');
        $login_allowedit  = $params->get('userfield_login_allowedit');
        if ( $login_allowedit === 0 ) unset($data['login']);


        if( ($bitFromPermAdmin==true) && (empty($data['permadmin_user_id'])==true) ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PROFILE_SAVE_FAILED'), 'error');
            $this->data = false;
            return false;
        }

        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdateUser = VirtualDeskSiteUserHelper::updateUserProfile($userSessionID, $data, $bitFromPermAdmin);
        if ($resUpdateUser===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PROFILE_SAVE_FAILED'), 'error');
            return false;
        }
        elseif ($resUpdateUser==='forcelogout')
        { $this->forcedLogout = true;
        }

		return $userTable->id;
	}



    public function getForgotPasswordPostedFormData()
    {  $app         = JFactory::getApplication();
       $data        = array();
       $email1      = $app->input->get('email1',null, 'STRING');
       if (!empty($email1)) $data['email1'] = $email1;
       return($data);
    }


    /**
    * @param   array  $data  The form data.
    * @return  mixed  false if occurs an erro, true if ok.
    * @since   1.6
    */
    public function validateForgotPasswordBeforeSave($data)
    {
        // Se faltar preencher alguns dos campos devolve um erro de preenchimento
        $arFieldsRequired = array('email1');
        foreach($arFieldsRequired as $chvRequired => $valRequired)
        {   if (empty($data[$valRequired]))
            { JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED').$valRequired, 'error');
                return(false);
                break;
            }
        }

        // Verifica se o EMAIL existe num user da VD e se o mesmo está no JOsuser
        if( !VirtualDeskSiteUserHelper::checkForgotPasswordEmailExists($data['email1']) ) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_EMAILNOTREGISTERED'), 'error');
            return(false);
        }

        // O Utilizador tem de estar no grupo dos "VD User" se não estiver não poder resgatar a senha
        if( !VirtualDeskSiteUserHelper::checkForgotPasswordEmailInVDUserGroup($data['email1']) ) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_USERNOTINGROUP'), 'error');
            return(false);
        }

        if( !VirtualDeskSiteUserHelper::checkForgotPasswordEmailUserBlocked($data['email1']) ) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_USERISBLOCKED'), 'error');
            return(false);
        }

        return(true);
    }



}
