<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');

use Joomla\Registry\Registry;

/**
 * Agenda model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelFormmain extends JModelForm
{
	/**
	 * @var		object	The user agenda data.
	 * @since   1.6
	 */
	protected $data;
    protected $userSessionID;
    protected $userFormMainData;
    public    $forcedLogout;

    //protected $ObjAgendaFields;


	public function __construct($config = array())
	{
		$config = array_merge(
			array(
				'events_map' => array('validate' => 'user')
			), $config
		);

        $this->forcedLogout = false;

		parent::__construct($config);

	}

	/*  ------- REMOVE ... ??? */

	public function checkin($userId = null)
	{

		return true;
	}

	public function checkout($userId = null)
	{

		return true;
	}

	public function getData()
	{
        return false;
	}

    public function getDataNew()
    {
        return false;

    }

	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	/* --------------------- */


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


	protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}


	protected function populateState()
	{
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
	}


    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }


    public function getPostedFormData4Admin()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $form_id  = $app->input->getInt('form_id');
        if (!empty($form_id)) $data['form_id'] = $form_id;

        $nomeFormulario = $app->input->get('nomeFormulario',null,'STRING');
        $refInterna     = $app->input->get('refInterna',null,'STRING');
        $descricao      = $app->input->get('descricao',null,'RAW');
        $modulo         = $app->input->get('modulo',null,'STRING');



        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();
        $arCamposGet['nomeFormulario']  = $nomeFormulario;
        $arCamposGet['refInterna']      = $refInterna;
        $arCamposGet['descricao']       = $descricao;
        $arCamposGet['modulo']          = $modulo;


        $data = $arCamposGet;

        if (!empty($form_id)) $data['form_id'] = $form_id;

        return($data);

    }


    public function validateBeforeSave4Admin($data)
    {
        $msg = '';
        if (empty($data['nomeFormulario']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }

    public function create4Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteFormmainHelper::create4Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteFormmainHelper::cleanAllTmpUserState();
        return true;
    }


    public function getPostedFieldsFormData4Admin()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $fields_id  = $app->input->getInt('fields_id');
        if (!empty($fields_id)) $data['fields_id'] = $fields_id;

        $tagCampo   = $app->input->get('tagCampo',null,'STRING');
        $nomeCampo  = $app->input->get('nomeCampo',null,'STRING');
        $descricao  = $app->input->get('descricao',null,'RAW');
        $tipoCampo  = $app->input->get('tipoCampo',null,'STRING');


        $arCamposGet = array ();
        $arCamposGet['tagCampo']    = $tagCampo;
        $arCamposGet['nomeCampo']   = $nomeCampo;
        $arCamposGet['descricao']   = $descricao;
        $arCamposGet['tipoCampo']   = $tipoCampo;

        $data = $arCamposGet;

        if (!empty($fields_id)) $data['fields_id'] = $fields_id;

        return($data);
    }


    public function validateFieldsBeforeSave4Admin($data)
    {
        $msg = '';
        if (empty($data['tagCampo']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if (empty($data['nomeCampo']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);
    }

    public function createFields4Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteFormmainHelper::createFields4Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteFormmainHelper::cleanAllTmpUserState();
        return true;
    }


}
