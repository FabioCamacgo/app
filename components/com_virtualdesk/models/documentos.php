<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

    defined('_JEXEC') or die;
    JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
    JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
    JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
    JLoader::register('VirtualDeskSiteDocumentosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/virtualdesksite_documentos.php');


use Joomla\Registry\Registry;

/**
 * Percursos Pedestres model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelDocumentos extends JModelForm
{
    /**
     * @var		object	The user Documentos data.
     * @since   1.6
     */
    protected $data;
    protected $userSessionID;
    protected $userDocumentosData;
    public    $forcedLogout;


    public function __construct($config = array())
    {
        $config = array_merge(
            array(
                'events_map' => array('validate' => 'user')
            ), $config
        );

        $this->forcedLogout = false;

        parent::__construct($config);

    }


    public function checkin($userId = null)
    {

        return true;
    }


    public function checkout($userId = null)
    {

        return true;
    }


    public function getData()
    {
        return false;
    }


    public function getDataNew()
    {
        return false;

    }


    public function getForm($data = array(), $loadData = true)
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    protected function loadFormData()
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }


    protected function populateState()
    {
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
    }


    public function getPostedCatNivel1FormData4Admin()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $categoria_id  = $app->input->getInt('categoria_id');
        if (!empty($categoria_id)) $data['categoria_id'] = $categoria_id;

        $categoria  = $app->input->get('categoria',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        $arCamposGet = array ();
        $arCamposGet['categoria']   = $categoria;
        $arCamposGet['estado']      = $estado;

        $data = $arCamposGet;

        if (!empty($categoria_id)) $data['categoria_id'] = $categoria_id;

        return($data);

    }


    public function setUserIdAndDataCatNivel14Admin($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteDocumentosHelper::getCategoriaNivel14AdminDetail($data['categoria_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userDocumentosData = $loadedData;
        return true;
    }


    public function validateCatNivel1BeforeSave4Admin($data)
    {
        $msg = '';
        if (empty($data['categoria']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_DOCUMENTOS_CAT_ERRO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedCatNivel1FormData4Admin($data)
    {
        if ((string)$data['categoria']   === (string) $this->userDocumentosData->categoria )    unset($data['categoria']);
        if ((int)$data['estado']  === (int) $this->userDocumentosData->estado )   unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('categoria_id',$data)) ) unset($data['categoria_id']);

        return($data);
    }


    public function createCatNivel14Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteDocumentosHelper::createCatNivel14Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
        return true;
    }


    public function saveCatNivel14Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteDocumentosHelper::updateCatNivel14Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
        return true;

    }


    public function getPostedCatNivel2FormData4Admin()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $categoria_id  = $app->input->getInt('categoria_id');
        if (!empty($categoria_id)) $data['categoria_id'] = $categoria_id;

        $categoria  = $app->input->get('categoria',null,'STRING');
        $catLevel1  = $app->input->get('catLevel1',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        $arCamposGet = array ();
        $arCamposGet['categoria']   = $categoria;
        $arCamposGet['catLevel1']   = $catLevel1;
        $arCamposGet['estado']      = $estado;

        $data = $arCamposGet;

        if (!empty($categoria_id)) $data['categoria_id'] = $categoria_id;

        return($data);

    }


    public function setUserIdAndDataCatNivel24Admin($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteDocumentosHelper::getCategoriaNivel24AdminDetail($data['categoria_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userDocumentosData = $loadedData;
        return true;
    }


    public function validateCatNivel2BeforeSave4Admin($data)
    {
        $msg = '';
        if (empty($data['categoria']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_DOCUMENTOS_CAT_ERRO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedCatNivel2FormData4Admin($data)
    {
        if ((string)$data['categoria']   === (string) $this->userDocumentosData->categoria )    unset($data['categoria']);
        if ((int)$data['catLevel1']  === (int) $this->userDocumentosData->catLevel1 )   unset($data['catLevel1']);
        if ((int)$data['estado']  === (int) $this->userDocumentosData->estado )   unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('categoria_id',$data)) ) unset($data['categoria_id']);

        return($data);
    }


    public function createCatNivel24Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteDocumentosHelper::createCatNivel24Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
        return true;
    }


    public function saveCatNivel24Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteDocumentosHelper::updateCatNivel24Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
        return true;

    }


    public function getPostedCatNivel3FormData4Admin()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $categoria_id  = $app->input->getInt('categoria_id');
        if (!empty($categoria_id)) $data['categoria_id'] = $categoria_id;

        $categoria  = $app->input->get('categoria',null,'STRING');
        $catLevel1  = $app->input->get('catLevel1',null,'STRING');
        $catLevel2  = $app->input->get('catLevel2',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        $arCamposGet = array ();
        $arCamposGet['categoria']   = $categoria;
        $arCamposGet['catLevel1']   = $catLevel1;
        $arCamposGet['catLevel2']   = $catLevel2;
        $arCamposGet['estado']      = $estado;

        $data = $arCamposGet;

        if (!empty($categoria_id)) $data['categoria_id'] = $categoria_id;

        return($data);

    }


    public function setUserIdAndDataCatNivel34Admin($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteDocumentosHelper::getCategoriaNivel34AdminDetail($data['categoria_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userDocumentosData = $loadedData;
        return true;
    }


    public function validateCatNivel3BeforeSave4Admin($data)
    {
        $msg = '';
        if (empty($data['categoria']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_DOCUMENTOS_CAT_ERRO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedCatNivel3FormData4Admin($data)
    {
        if ((string)$data['categoria']   === (string) $this->userDocumentosData->categoria )    unset($data['categoria']);
        if ((int)$data['catLevel1']  === (int) $this->userDocumentosData->catLevel1 )   unset($data['catLevel1']);
        if ((int)$data['catLevel2']  === (int) $this->userDocumentosData->catLevel2 )   unset($data['catLevel2']);
        if ((int)$data['estado']  === (int) $this->userDocumentosData->estado )   unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('categoria_id',$data)) ) unset($data['categoria_id']);

        return($data);
    }


    public function createCatNivel34Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteDocumentosHelper::createCatNivel34Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
        return true;
    }


    public function saveCatNivel34Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteDocumentosHelper::updateCatNivel34Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
        return true;

    }


    public function getPostedCatNivel4FormData4Admin()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $categoria_id  = $app->input->getInt('categoria_id');
        if (!empty($categoria_id)) $data['categoria_id'] = $categoria_id;

        $categoria  = $app->input->get('categoria',null,'STRING');
        $catLevel1  = $app->input->get('catLevel1',null,'STRING');
        $catLevel2  = $app->input->get('catLevel2',null,'STRING');
        $catLevel3  = $app->input->get('catLevel3',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        $arCamposGet = array ();
        $arCamposGet['categoria']   = $categoria;
        $arCamposGet['catLevel1']   = $catLevel1;
        $arCamposGet['catLevel2']   = $catLevel2;
        $arCamposGet['catLevel3']   = $catLevel3;
        $arCamposGet['estado']      = $estado;

        $data = $arCamposGet;

        if (!empty($categoria_id)) $data['categoria_id'] = $categoria_id;

        return($data);

    }


    public function setUserIdAndDataCatNivel44Admin($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteDocumentosHelper::getCategoriaNivel44AdminDetail($data['categoria_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userDocumentosData = $loadedData;
        return true;
    }


    public function validateCatNivel4BeforeSave4Admin($data)
    {
        $msg = '';
        if (empty($data['categoria']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_DOCUMENTOS_CAT_ERRO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedCatNivel4FormData4Admin($data)
    {
        if ((string)$data['categoria']   === (string) $this->userDocumentosData->categoria )    unset($data['categoria']);
        if ((int)$data['catLevel1']  === (int) $this->userDocumentosData->catLevel1 )   unset($data['catLevel1']);
        if ((int)$data['catLevel2']  === (int) $this->userDocumentosData->catLevel2 )   unset($data['catLevel2']);
        if ((int)$data['catLevel3']  === (int) $this->userDocumentosData->catLevel3 )   unset($data['catLevel3']);
        if ((int)$data['estado']  === (int) $this->userDocumentosData->estado )   unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('categoria_id',$data)) ) unset($data['categoria_id']);

        return($data);
    }


    public function createCatNivel44Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteDocumentosHelper::createCatNivel44Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
        return true;
    }


    public function saveCatNivel44Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteDocumentosHelper::updateCatNivel44Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
        return true;

    }


    public function getPostedFormData4Admin()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $documento_id  = $app->input->getInt('documento_id');
        if (!empty($documento_id)) $data['documento_id'] = $documento_id;

        $nome       = $app->input->get('nome',null,'STRING');
        $catLevel1  = $app->input->get('catLevel1',null,'STRING');
        $catLevel2  = $app->input->get('catLevel2',null,'STRING');
        $catLevel3  = $app->input->get('catLevel3',null,'STRING');
        $catLevel4  = $app->input->get('catLevel4',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');
        $TagInput   = $app->input->get('TagInput',null,'RAW');

        $vdFileUpChangedDoc      = $app->input->get('vdFileUpChangedDoc',null,'STRING');

        $arCamposGet = array ();
        $arCamposGet['nome']        = $nome;
        $arCamposGet['catLevel1']   = $catLevel1;
        $arCamposGet['catLevel2']   = $catLevel2;
        $arCamposGet['catLevel3']   = $catLevel3;
        $arCamposGet['catLevel4']   = $catLevel4;
        $arCamposGet['estado']      = $estado;
        $arCamposGet['TagInput']    = $TagInput;

        $arCamposGet['vdFileUpChangedDoc']     = $vdFileUpChangedDoc;

        $data = $arCamposGet;

        if (!empty($documento_id)) $data['documento_id'] = $documento_id;

        return($data);
    }


    public function setUserIdAndData4Admin($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteDocumentosHelper::getDocumentos4AdminDetail($data['documento_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userDocumentosData = $loadedData;
        return true;
    }


    public function validateBeforeSave4Admin($data)
    {
        $msg = '';
        if (empty($data['nome']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_DOCUMENTOS_NOME_ERRO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedFormData4Admin($data)
    {
        if ((string)$data['nome']       === (string) $this->userDocumentosData->nome )      unset($data['nome']);
        if ((int)$data['catLevel1']     === (int) $this->userDocumentosData->catLevel1 )    unset($data['catLevel1']);
        if ((int)$data['catLevel2']     === (int) $this->userDocumentosData->catLevel2 )    unset($data['catLevel2']);
        if ((int)$data['catLevel3']     === (int) $this->userDocumentosData->catLevel3 )    unset($data['catLevel3']);
        if ((int)$data['catLevel4']     === (int) $this->userDocumentosData->catLevel4 )    unset($data['catLevel4']);
        if ((int)$data['estado']        === (int) $this->userDocumentosData->estado )       unset($data['estado']);
        if ( (string)$data['TagInput']  === (string) $this->userDocumentosData->TagInput )  unset($data['TagInput']);

        if ( (string)$data['vdFileUpChangedDoc']     == '0' )   unset($data['vdFileUpChangedDoc']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('documento_id',$data)) ) unset($data['documento_id']);

        return($data);
    }


    public function create4Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteDocumentosHelper::create4Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
        return true;
    }


    public function save4Admin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteDocumentosHelper::update4Admin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
        return true;

    }


    public function getPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $documento_id  = $app->input->getInt('documento_id');
        if (!empty($documento_id)) $data['documento_id'] = $documento_id;

        $nome           = $app->input->get('nome',null,'STRING');
        $catLevel1      = $app->input->get('catLevel1',null,'STRING');
        $catLevel2      = $app->input->get('catLevel2',null,'STRING');
        $catLevel3      = $app->input->get('catLevel3',null,'STRING');
        $catLevel4      = $app->input->get('catLevel4',null,'STRING');
        $estado         = $app->input->get('estado',null,'STRING');
        $TagInput   = $app->input->get('TagInput',null,'RAW');

        $vdFileUpChangedDoc      = $app->input->get('vdFileUpChangedDoc',null,'STRING');

        $arCamposGet = array ();
        $arCamposGet['nome']        = $nome;
        $arCamposGet['catLevel1']   = $catLevel1;
        $arCamposGet['catLevel2']   = $catLevel2;
        $arCamposGet['catLevel3']   = $catLevel3;
        $arCamposGet['catLevel4']   = $catLevel4;
        $arCamposGet['estado']      = $estado;
        $arCamposGet['TagInput']      = $TagInput;

        $arCamposGet['vdFileUpChangedDoc']     = $vdFileUpChangedDoc;

        $data = $arCamposGet;

        if (!empty($documento_id)) $data['documento_id'] = $documento_id;

        return($data);
    }


    public function validateBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['nome']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_DOCUMENTOS_NOME_ERRO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function create4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteDocumentosHelper::create4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
        return true;
    }

}
