<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteAcaosocialHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_acaosocial.php');
JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');

use Joomla\Registry\Registry;

/**
 * Agenda model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelAcaosocial extends JModelForm
{
	/**
	 * @var		object	The user agenda data.
	 * @since   1.6
	 */
	protected $data;
    protected $userSessionID;
    protected $userAcaosocialData;
    public    $forcedLogout;

    //protected $ObjAgendaFields;


	public function __construct($config = array())
	{
		$config = array_merge(
			array(
				'events_map' => array('validate' => 'user')
			), $config
		);

        $this->forcedLogout = false;

		parent::__construct($config);

	}

	/*  ------- REMOVE ... ??? */

	public function checkin($userId = null)
	{

		return true;
	}

	public function checkout($userId = null)
	{

		return true;
	}

	public function getData()
	{
        return false;
	}

    public function getDataNew()
    {
        return false;

    }

	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	/* --------------------- */

    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }

	protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	protected function populateState()
	{
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
	}

    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }

    public function getPostedFormData4User(){
        $app         = JFactory::getApplication();
        $data        = array();

        $pedido_id  = $app->input->getInt('pedido_id');
        $formId  = $app->input->getInt('formId');
        $userID  = $app->input->getInt('userID');
        if (!empty($pedido_id)) $data['pedido_id'] = $pedido_id;


        $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);
        $arCamposGet = array ();

        foreach($dataValues as $row){

            switch($row['tipo']){
                /*Tratamento repeater*/
                case('Raw'):
                    $arCamposGet[$row['fieldtag']] = $app->input->get($row['fieldtag'],null,'RAW');
                    break;

                default:
                    $arCamposGet[$row['fieldtag']] = $app->input->get($row['fieldtag'],null,'STRING');
                    break;
            }

        }

        $data = $arCamposGet;

        if (!empty($pedido_id)) $data['pedido_id'] = $pedido_id;
        if (!empty($formId)) $data['formId'] = $formId;
        if (!empty($userID)) $data['userID'] = $userID;

        return($data);
    }

    public function validateReqApoioSocialBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createReqApoioSocial4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAcaosocialHelper::createReqApoioSocial4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAcaosocialHelper::cleanAllTmpUserState();
        return true;
    }

    public function setUserIdAndReqApoioSocialFormData4User($data)
    {
        $app         = JFactory::getApplication();

        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        $pedido_id  = $app->input->getInt('pedido_id');

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteAcaosocialHelper::getDadosFormView4UserDetail($pedido_id);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userAcaosocialData = $loadedData;

        return true;
    }

    public function getOnlyChangedPostedReqApoioSocialFormData4User($data)
    {
        $app         = JFactory::getApplication();

        $formId  = $app->input->getInt('formId');

        $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

        foreach($dataValues as $row){
            switch ($row['tipo']) {
                case 'Varchar':
                    if ( (string)$data[$row['fieldtag']]    === (string) $this->userAcaosocialData[$row['fieldtag']]->val_varchar )    unset($data[$row['fieldtag']]);
                    break;
                case 'Número':
                    if ( (string)$data[$row['fieldtag']]    === (string) $this->userAcaosocialData[$row['fieldtag']]->val_number )    unset($data[$row['fieldtag']]);
                    break;
                case 'Data':
                    if ( (string)$data[$row['fieldtag']]    === (string) $this->userAcaosocialData[$row['fieldtag']]->val_date )    unset($data[$row['fieldtag']]);
                    break;
                case 'Texto':
                    if ( (string)$data[$row['fieldtag']]    === (string) $this->userAcaosocialData[$row['fieldtag']]->val_text )    unset($data[$row['fieldtag']]);
                    break;
                default :
                    if ( (string)$data[$row['fieldtag']]    === (string) $this->userAcaosocialData[$row['fieldtag']]->val_varchar )    unset($data[$row['fieldtag']]);
                    break;
            }
        }

        //if ( (string)$data['vdFileUpChanged']      == '0' )   unset($data['vdFileUpChanged']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==2) && (array_key_exists('pedido_id',$data)) ) unset($data['pedido_id']);
        if( (sizeof($data)==1) && (array_key_exists('form_id',$data)) ) unset($data['form_id']);
        return($data);
    }

    public function saveReqApoioSocial4User($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAcaosocialHelper::updateReqApoioSocial4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAcaosocialHelper::cleanAllTmpUserState();

        return true;
    }

    public function validateBolsaEstudoEnsinoSuperiorBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createBolsaEstudoEnsinoSuperior4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAcaosocialHelper::createBolsaEstudoEnsinoSuperior4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAcaosocialHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateCandApoioSocialBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createCandApoioSocial4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAcaosocialHelper::createCandApoioSocial4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAcaosocialHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateMarcacaoAtendimentoSocialBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createMarcacaoAtendimentoSocial4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAcaosocialHelper::createMarcacaoAtendimentoSocial4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAcaosocialHelper::cleanAllTmpUserState();
        return true;
    }

}
