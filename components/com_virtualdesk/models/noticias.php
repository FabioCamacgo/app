<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_virtualdesk
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
    JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
    JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
    JLoader::register('VirtualDeskSiteNoticiasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/noticias/virtualdesksite_noticias.php');


use Joomla\Registry\Registry;

/**
 * Percursos Pedestres model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelNoticias extends JModelForm
{
    /**
     * @var		object	The user Percursos Pedestres data.
     * @since   1.6
     */
    protected $data;
    protected $userSessionID;
    protected $userNoticiasData;
    public    $forcedLogout;


    public function __construct($config = array())
    {
        $config = array_merge(
            array(
                'events_map' => array('validate' => 'user')
            ), $config
        );

        $this->forcedLogout = false;

        parent::__construct($config);

    }


    public function checkin($userId = null)
    {

        return true;
    }


    public function checkout($userId = null)
    {

        return true;
    }


    public function getData()
    {
        return false;
    }


    public function getDataNew()
    {
        return false;

    }


    public function getForm($data = array(), $loadData = true)
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    protected function loadFormData()
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }


    protected function populateState()
    {
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
    }

    public function setUserIdAndDataCat4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteNoticiasHelper::getCategoria4ManagerDetail($data['categoria_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userAgendaData = $loadedData;
        return true;
    }


    public function getPostedCatFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $categoria_id  = $app->input->getInt('categoria_id');
        if (!empty($categoria_id)) $data['categoria_id'] = $categoria_id;

        $categoria  = $app->input->get('categoria',null,'STRING');
        $estado     = $app->input->get('estado',null,'STRING');


        $arCamposGet = array ();
        $arCamposGet['categoria']   = $categoria;
        $arCamposGet['estado']      = $estado;

        $data = $arCamposGet;

        if (!empty($categoria_id)) $data['categoria_id'] = $categoria_id;

        return($data);

    }


    public function getOnlyChangedPostedCatFormData4Manager($data)
    {
        if ((string)$data['categoria']   === (string) $this->userNoticiasData->categoria )    unset($data['categoria']);
        if ((int)$data['estado']  === (int) $this->userNoticiasData->estado )   unset($data['estado']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('categoria_id',$data)) ) unset($data['categoria_id']);

        return($data);
    }


    public function validateCatBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['categoria']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_NOTICIAS_CAT');
        if (empty($data['estado']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_NOTICIAS_ESTADO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function createCat4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteNoticiasHelper::createCat4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteNoticiasHelper::cleanAllTmpUserState();
        return true;
    }


    public function saveCat4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteNoticiasHelper::updateCat4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteNoticiasHelper::cleanAllTmpUserState();
        return true;

    }


    public function setUserIdAndData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteNoticiasHelper::getNoticiasDetail4Manager($data['noticia_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->userNoticiasData = $loadedData;
        return true;
    }


    public function getPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $noticia_id  = $app->input->getInt('noticia_id');
        if (!empty($noticia_id)) $data['noticia_id'] = $noticia_id;

        $titulo      = $app->input->get('titulo',null,'STRING');
        $descricao  = $app->input->get('descricao',null,'RAW');
        $categoria   = $app->input->get('categoria',null,'STRING');
        $data_publicacao   = $app->input->get('data_publicacao',null,'STRING');
        $facebook   = $app->input->get('facebook',null,'STRING');
        $video   = $app->input->get('video',null,'STRING');
        $estado   = $app->input->get('estado',null,'STRING');

        $vdFileUpChangedCapa      = $app->input->get('vdFileUpChangedCapa',null,'STRING');
        $vdFileUpChangedGaleria      = $app->input->get('vdFileUpChangedGaleria',null,'STRING');

        $arCamposGet = array ();
        $arCamposGet['titulo']              = $titulo;
        $arCamposGet['descricao']           = $descricao;
        $arCamposGet['categoria']           = $categoria;
        $arCamposGet['data_publicacao']     = $data_publicacao;
        $arCamposGet['facebook']            = $facebook;
        $arCamposGet['video']               = $video;
        $arCamposGet['estado']              = $estado;

        $arCamposGet['vdFileUpChangedCapa']    = $vdFileUpChangedCapa;
        $arCamposGet['vdFileUpChangedCapa']    = $vdFileUpChangedGaleria;

        $data = $arCamposGet;

        if (!empty($noticia_id)) $data['noticia_id'] = $noticia_id;

        return($data);

    }


    public function validateBeforeSave4Manager($data)
    {
        $msg = '';
        if (empty($data['titulo']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_NOTICIAS_ERRO_TITULO');
        if (empty($data['categoria']) || $data['categoria'] == 0)  $msg .= "<br>" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_NOTICIAS_ERRO_CATEGORIA');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function getOnlyChangedPostedFormData4Manager($data)
    {
        if ( (string)$data['titulo']            === (string) $this->userNoticiasData->titulo )    unset($data['titulo']);
        if ( (string)$data['descricao']         === (string) $this->userNoticiasData->descricao )    unset($data['descricao']);
        if ( (int)$data['categoria']            === (int) $this->userNoticiasData->categoria )    unset($data['categoria']);
        if ( (string)$data['facebook']          === (string) $this->userNoticiasData->facebook )    unset($data['facebook']);
        if ( (string)$data['video']             === (string) $this->userNoticiasData->video )    unset($data['video']);
        if ( (string)$data['data_publicacao']   === (string) $this->userNoticiasData->data_publicacao )    unset($data['data_publicacao']);
        if ( (int)$data['estado']               === (int) $this->userNoticiasData->estado )    unset($data['estado']);

        if ( (string)$data['vdFileUpChangedCapa']     == '0' )   unset($data['vdFileUpChangedCapa']);
        if ( (string)$data['vdFileUpChangedGaleria']     == '0' )   unset($data['vdFileUpChangedGaleria']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('noticia_id',$data)) ) unset($data['noticia_id']);

        return($data);
    }


    public function create4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteNoticiasHelper::create4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteNoticiasHelper::cleanAllTmpUserState();
        return true;
    }


    public function save4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteNoticiasHelper::update4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteNoticiasHelper::cleanAllTmpUserState();
        return true;

    }


    public function getPostedFormData4Gestor()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $noticia_id  = $app->input->getInt('noticia_id');
        if (!empty($noticia_id)) $data['noticia_id'] = $noticia_id;

        $titulo      = $app->input->get('titulo',null,'STRING');
        $descricao  = $app->input->get('descricao',null,'RAW');
        $categoria   = $app->input->get('categoria',null,'STRING');
        $facebook   = $app->input->get('facebook',null,'STRING');
        $video   = $app->input->get('video',null,'STRING');
        $estado   = $app->input->get('estado',null,'STRING');

        $vdFileUpChangedCapa      = $app->input->get('vdFileUpChangedCapa',null,'STRING');
        $vdFileUpChangedGaleria      = $app->input->get('vdFileUpChangedGaleria',null,'STRING');

        $arCamposGet = array ();
        $arCamposGet['titulo']              = $titulo;
        $arCamposGet['descricao']           = $descricao;
        $arCamposGet['categoria']           = $categoria;
        $arCamposGet['facebook']            = $facebook;
        $arCamposGet['video']               = $video;
        $arCamposGet['estado']              = $estado;

        $arCamposGet['vdFileUpChangedCapa']    = $vdFileUpChangedCapa;
        $arCamposGet['vdFileUpChangedCapa']    = $vdFileUpChangedGaleria;

        $data = $arCamposGet;

        if (!empty($noticia_id)) $data['noticia_id'] = $noticia_id;

        return($data);

    }


    public function validateBeforeSave4Gestor($data)
    {
        $msg = '';
        if (empty($data['titulo']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_NOTICIAS_ERRO_TITULO');
        if (empty($data['categoria']) || $data['categoria'] == 0)  $msg .= "<br>" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_NOTICIAS_ERRO_CATEGORIA');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function create4Gestor($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteNoticiasHelper::create4Gestor($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteNoticiasHelper::cleanAllTmpUserState();
        return true;
    }

}
