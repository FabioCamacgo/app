<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSitealertarptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alertarpt.php');


use Joomla\Registry\Registry;

/**
 * alertarpt model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelalertarpt extends JModelForm
{
    /**
     * @var		object	The user alertarpt data.
     * @since   1.6
     */
    protected $data;
    protected $userSessionID;
    protected $useralertarptData;
    public    $forcedLogout;

    //protected $ObjalertarptFields;


    public function __construct($config = array())
    {
        $config = array_merge(
            array(
                'events_map' => array('validate' => 'user')
            ), $config
        );

        $this->forcedLogout = false;

        parent::__construct($config);

    }

    /*  ------- REMOVE ... ??? */

    public function checkin($userId = null)
    {

        return true;
    }

    public function checkout($userId = null)
    {

        return true;
    }

    public function getData()
    {
        return false;
    }

    public function getDataNew()
    {
        return false;

    }

    public function getForm($data = array(), $loadData = true)
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }

    /* --------------------- */


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    public function setUserIdAndData4User($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSitealertarptHelper::getalertarptView4UserDetail($data['alertarpt_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->useralertarptData = $loadedData;

        return true;
    }


    public function setUserIdAndData4Manager($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSitealertarptHelper::getalertarptView4ManagerDetail($data['alertarpt_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID = $userSessionID;
        $this->useralertarptData = $loadedData;
        return true;
    }


    protected function loadFormData()
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }


    protected function populateState()
    {
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
    }


    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }


    public function getPostedFormData4User()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $alertarpt_id  = $app->input->getInt('alertarpt_id');
        if (!empty($alertarpt_id)) $data['alertarpt_id'] = $alertarpt_id;

        $assunto         = $app->input->get('assunto',null,'STRING');
        $descricao       = $app->input->get('descricao',null,'RAW');
        $departamento    = $app->input->get('departamento',null,'STRING');
        $observacoes     = $app->input->get('observacoes',null,'RAW');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChanged      = $app->input->get('vdFileUpChanged',null,'STRING');

        // Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();

        $arCamposGet['assunto']         = $assunto;
        $arCamposGet['descricao']       = $descricao;
        $arCamposGet['departamento']    = $departamento;
        $arCamposGet['observacoes']     = $observacoes;

        $arCamposGet['vdFileUpChanged'] = $vdFileUpChanged;

        //$data = $this->ObjalertarptFields->checkCamposEmpty($arCamposGet, $data);
        $data = $arCamposGet;
        if (!empty($alertarpt_id)) $data['alertarpt_id'] = $alertarpt_id;
        return($data);
    }


    public function getPostedFormData4Manager()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $alertarpt_id  = $app->input->getInt('alertarpt_id');
        if (!empty($alertarpt_id)) $data['alertarpt_id'] = $alertarpt_id;

        $nomeProjeto = $app->input->get('nomeProjeto',null,'STRING');
        $descricao = $app->input->get('descricao',null,'RAW');
        $programa = $app->input->get('programa',null,'STRING');
        $linhaFinanciamento = $app->input->get('linhaFinanciamento',null,'STRING');
        $escala = $app->input->get('escala',null,'STRING');
        $entidade = $app->input->get('entidade',null,'STRING');
        $ano = $app->input->get('ano',null,'STRING');
        $link = $app->input->get('link',null,'STRING');
        $call = $app->input->get('call',null,'STRING');
        $dataInicio = $app->input->get('dataInicio',null,'RAW');
        $dataFim = $app->input->get('dataFim',null,'RAW');
        $valor = $app->input->get('valor',null,'STRING');
        $layout = $app->input->get('layout',null,'STRING');
        $lider = $app->input->get('lider',null,'STRING');
        $pais = $app->input->get('pais',null,'STRING');
        $tipologia = $app->input->get('tipologia',null,'STRING');
        $ProgramaParceiro = $app->input->get('ParceiroInput',null,'RAW');

        // Verifica se foi alterados algum ficheiro
        $vdFileUpChanged      = $app->input->get('vdFileUpChanged',null,'STRING');

        /// Campos EXTRA
        // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
        // se os campos estiverem hidden não devem passar
        $arCamposGet = array ();

        $arCamposGet['nomeProjeto'] = $nomeProjeto;
        $arCamposGet['descricao'] = $descricao;
        $arCamposGet['programa'] = $programa;
        $arCamposGet['linhaFinanciamento'] = $linhaFinanciamento;
        $arCamposGet['escala'] = $escala;
        $arCamposGet['entidade'] = $entidade;
        $arCamposGet['ano'] = $ano;
        $arCamposGet['link'] = $link;
        $arCamposGet['call'] = $call;
        $arCamposGet['dataInicio'] = $dataInicio;
        $arCamposGet['dataFim'] = $dataFim;
        $arCamposGet['valor'] = $valor;
        $arCamposGet['layout'] = $layout;
        $arCamposGet['lider'] = $lider;
        $arCamposGet['pais'] = $pais;
        $arCamposGet['tipologia'] = $tipologia;
        $arCamposGet['ProgramaParceiro'] = $ProgramaParceiro;

        $arCamposGet['vdFileUpChanged'] = $vdFileUpChanged;

        //$data = $this->ObjalertarptFields->checkCamposEmpty($arCamposGet, $data);
        $data = $arCamposGet;
        if (!empty($alertarpt_id)) $data['alertarpt_id'] = $alertarpt_id;
        return($data);
    }


    public function getOnlyChangedPostedFormData4Manager($data)
    {

        if ( (string)$data['nomeProjeto']         === (string) $this->useralertarptData->projeto )     unset($data['nomeProjeto']);
        if ( (string)$data['descricao']         === (string) $this->useralertarptData->descricao )      unset($data['descricao']);
        if ( (int)$data['programa']       === (int) $this->useralertarptData->programa )     unset($data['programa']);
        if ( (int)$data['linhaFinanciamento']       === (int) $this->useralertarptData->linhaFinanciamento )   unset($data['linhaFinanciamento']);
        if ( (string)$data['escala']     === (string) $this->useralertarptData->escala )   unset($data['escala']);
        if ( (string)$data['entidade']     === (string) $this->useralertarptData->entidade )   unset($data['entidade']);
        if ( (int)$data['ano']     === (int) $this->useralertarptData->ano )   unset($data['ano']);
        if ( (string)$data['link']     === (string) $this->useralertarptData->link )   unset($data['link']);
        if ( (string)$data['call']     === (string) $this->useralertarptData->call )   unset($data['call']);
        if ( (string)$data['dataInicio']     === (string) $this->useralertarptData->dataInicio )   unset($data['dataInicio']);
        if ( (string)$data['dataFim']     === (string) $this->useralertarptData->dataFim )   unset($data['dataFim']);
        if ( (string)$data['valor']     === (string) $this->useralertarptData->valor )   unset($data['valor']);
        if ( (int)$data['layout']     === (int) $this->useralertarptData->layout )   unset($data['layout']);
        if ( (string)$data['lider']     === (string) $this->useralertarptData->lider )   unset($data['lider']);
        if ( (int)$data['pais']     === (int) $this->useralertarptData->pais )   unset($data['pais']);
        if ( (string)$data['tipologia']     === (string) $this->useralertarptData->tipologia )   unset($data['tipologia']);

        if ( (string)$data['vdFileUpChanged'] == '0' )   unset($data['vdFileUpChanged']);

        // Se no final só tivermos definido o id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('alertarpt_id',$data)) ) unset($data['alertarpt_id']);

        return($data);
    }

    public function validateBeforeSave4Manager($data)
    {
        $msg = '';

        if (empty($data['nomeProjeto']))       $msg .= "<br>" . JText::_('COM_VIRTUALDESK_ALERTARPT_FIELDREQUIRED_NOME') ;
        if (empty($data['programa']))     $msg .= "<br>" . JText::_('COM_VIRTUALDESK_ALERTARPT_FIELDREQUIRED_PROGRAMA') ;

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }

        return(true);

    }


    public function save4Manager($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitealertarptHelper::update4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitealertarptHelper::cleanAllTmpUserState();
        return true;

    }


    public function create4User($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitealertarptHelper::create4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitealertarptHelper::cleanAllTmpUserState();
        return true;

    }


    public function create4Manager($data)
    {

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSitealertarptHelper::create4Manager($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSitealertarptHelper::cleanAllTmpUserState();
        return true;
    }


    public function delete4User($data)
    {

        return true;
    }


    public function delete4Manager($data)
    {

        return true;
    }


}
