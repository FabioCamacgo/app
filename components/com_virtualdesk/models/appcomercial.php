<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskSiteAppComercialHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_appcomercial.php');
JLoader::register('VirtualDeskPasswordCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

use Joomla\Registry\Registry;

/**
 * Profile model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelAppComercial extends JModelForm
{
    /**
     * @var		object	The user profile data.
     * @since   1.6
     */
    protected $data;

    protected $userSessionID;

    protected $userProfileData;

    public    $forcedLogout;

    protected $ObjUserFields;

    public function __construct($config = array())
    {
        $config = array_merge(
            array(
                'events_map' => array('validate' => 'user')
            ), $config
        );

        $this->forcedLogout = false;

        parent::__construct($config);

        $this->ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
        $this->ObjUserFields->getUserFields();
        $this->ObjUserFields->getUserFieldLogin();
    }

    public function getForm($data = array(), $loadData = true)
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }

    protected function loadFormData()
    {
        // optamos por apenas carregar os dados, o form é feito na template
        return false;
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @return  void
     *
     * @since   1.6
     */
    protected function populateState()
    {
        // Get the application object.
        $params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();

        // Get the user id.
        //$userId = JFactory::getApplication()->getUserState('com_virtualdesk.edit.profile.id');
//		$userId = !empty($userId) ? $userId : (int) JFactory::getUser()->get('id');
        $userId = (int) JFactory::getUser()->get('id');

        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);

        // Load the parameters.
        $this->setState('params', $params);
    }

    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();

        $data        = array();

        $nome                   = $app->input->get('nome',null, 'STRING');
        $email                  = $app->input->get('email',null,'STRING');
        $donoFoto               = $app->input->get('donoFoto',null,'STRING');
        $nomeFoto               = $app->input->get('nomeFoto',null, 'STRING');
        $veracidadedados        = $app->input->get('veracidadedados',null,'STRING');
        $politicaprivacidade    = $app->input->get('politicaprivacidade',null,'STRING');



        // Campos BASE
        if (!empty($nome)) $data['nome']                    = $nome;
        if (!empty($email)) $data['email']                  = $email;
        if (!empty($donoFoto)) $data['donoFoto']            = $donoFoto;
        if (!empty($nomeFoto)) $data['nomeFoto']            = $nomeFoto;
        if (!empty($nomeFoto)) $data['veracidadedados']     = $veracidadedados;
        if (!empty($nomeFoto)) $data['politicaprivacidade'] = $politicaprivacidade;


        return($data);
    }

}
