<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_virtualdesk
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteAmbienteHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ambiente.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');


use Joomla\Registry\Registry;

/**
 * Ambiente model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelAmbiente extends JModelForm
{
	/**
	 * @var		object	The user ambiente data.
	 * @since   1.6
	 */
	protected $data;
    protected $userSessionID;
    protected $userAmbienteData;
    public    $forcedLogout;

    //protected $ObjAmbienteFields;


	public function __construct($config = array())
	{
		$config = array_merge(
			array(
				'events_map' => array('validate' => 'user')
			), $config
		);

        $this->forcedLogout = false;

		parent::__construct($config);

	}

	/*  ------- REMOVE ... ??? */

	public function checkin($userId = null)
	{

		return true;
	}

	public function checkout($userId = null)
	{

		return true;
	}

	public function getData()
	{
        return false;
	}

    public function getDataNew()
    {
        return false;

    }

	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	/* --------------------- */


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    public function setUserIdAndData4User($data)
    {

        return true;
    }


    public function setUserIdAndData4Manager($data)
    {

        return true;
    }


	protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	protected function populateState()
	{
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
	}

    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }


    public function getPostedFormData4User(){
        $app         = JFactory::getApplication();
        $data        = array();

        $pedido_id  = $app->input->getInt('pedido_id');
        $formId  = $app->input->getInt('formId');
        $userID  = $app->input->getInt('userID');
        if (!empty($pedido_id)) $data['pedido_id'] = $pedido_id;


        $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);
        $arCamposGet = array ();

        foreach($dataValues as $row){
            $arCamposGet[$row['fieldtag']] = $app->input->get($row['fieldtag'],null,'STRING');
        }

        $data = $arCamposGet;

        if (!empty($pedido_id)) $data['pedido_id'] = $pedido_id;
        if (!empty($formId)) $data['formId'] = $formId;
        if (!empty($userID)) $data['userID'] = $userID;

        return($data);
    }

    public function validateRecolhaAnimaisBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createRecolhaAnimais4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAmbienteHelper::createRecolhaAnimais4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAmbienteHelper::cleanAllTmpUserState();
        return true;
    }

}
