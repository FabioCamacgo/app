<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
use Joomla\Registry\Registry;

/**
 * Profile model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelConfigAdmin extends JModelForm
{
	/**
	 * @var		object	The user profile data.
	 * @since   1.6
	 */
	protected $data;

    protected $userSessionID;

    protected $userProfileData;

    public    $forcedLogout;

    protected $ObjUserFields;

    protected $userConfigAdminPluginData;
    protected $userConfigAdminPluginLayoutData;
    protected $userConfigAdminParamData;
    protected $userConfigAdminParamTipoData;
    protected $userConfigAdminParamGrupoData;


	/**
	 * Constructor
	 *
	 * @param   array  $config  An array of configuration options (name, state, dbo, table_path, ignore_request).
	 *
	 * @since   3.2
	 *
	 * @throws  Exception
	 */
	public function __construct($config = array())
	{
		$config = array_merge(
			array(
				'events_map' => array('validate' => 'user')
			), $config
		);
        $this->forcedLogout = false;
		parent::__construct($config);
	}


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }


    public function setUserIdAndPluginData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteconfigAdminHelper::getPluginDetail ($data['configadmin_plugin_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID             = $userSessionID;
        $this->userConfigAdminPluginData = $loadedData;
        return true;
    }


    public function setUserIdAndPluginLayoutData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteconfigAdminHelper::getPluginLayoutDetail ($data['configadmin_plugin_id'],$data['configadmin_pluginlayout_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID             = $userSessionID;
        $this->userConfigAdminPluginLayoutData = $loadedData;
        return true;
    }


    public function setUserIdAndParamData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteconfigAdminHelper::getParamDetail ($data['configadmin_param_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID             = $userSessionID;
        $this->userConfigAdminParamData = $loadedData;
        return true;
    }


    public function setUserIdAndParamTipoData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteconfigAdminHelper::getParamTipoDetail ($data['configadmin_paramtipo_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID             = $userSessionID;
        $this->userConfigAdminParamTipoData = $loadedData;
        return true;
    }


    public function setUserIdAndParamGrupoData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $loadedData = VirtualDeskSiteconfigAdminHelper::getParamGrupoDetail ($data['configadmin_paramgrupo_id']);
        if (empty($loadedData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        $this->userSessionID             = $userSessionID;
        $this->userConfigAdminParamGrupoData = $loadedData;
        return true;
    }


	/**
	 * Method to getform data.
	 *
	 * @return  mixed  	Data object on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function getData()
	{
		//return $this->data;
        return false;
	}


	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}


	protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}


	/**
	 * Method to auto-populate the model state.
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
	}


    public function getPostedFormData()
    {  $app         = JFactory::getApplication();

       $data        = array();
       return($data);
    }


    public function getPostedFormData2Plugin()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $plugin_id  = $app->input->getInt('configadmin_plugin_id');
        $nome       = $app->input->get('nome',null, 'STRING');
        $tagchave   = $app->input->get('tagchave',null, 'STRING');
        $enabled    = $app->input->get('enabled',null, 'STRING');

        if (!empty($plugin_id)) $data['configadmin_plugin_id'] = $plugin_id;
        if (!empty($nome))  $data['nome']                      = $nome;
        if (!empty($tagchave)) $data['tagchave']               = $tagchave;

        if ((string)$enabled!='') {
            $data['enabled']             = $enabled;
        }
        else {
            $data['enabled']             = '0';
        }

        return($data);
    }


    public function getPostedFormData2PluginLayout()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $pluginlayout_id  = $app->input->getInt('configadmin_pluginlayout_id');
        $plugin_id        = $app->input->getInt('configadmin_plugin_id');
        $nome             = $app->input->get('nome',null, 'STRING');
        $tagchave         = $app->input->get('tagchave',null, 'STRING');
        $path             = $app->input->get('path',null, 'STRING');
        $descricao        = $app->input->get('descricao',null, 'STRING');
        $idtipolayout     = $app->input->get('idtipolayout',null, 'STRING');

        if (!empty($pluginlayout_id)) $data['configadmin_pluginlayout_id'] = $pluginlayout_id;
        if (!empty($plugin_id)) $data['configadmin_plugin_id'] = $plugin_id;
        if (!empty($nome))  $data['nome']                      = $nome;
        if (!empty($tagchave)) $data['tagchave']               = $tagchave;
        if (!empty($path)) $data['path']                       = $path;
        if (!empty($descricao)) $data['descricao']             = $descricao;
        if (!empty($idtipolayout)) $data['idtipolayout']       = $idtipolayout;

        return($data);
    }


    public function getPostedFormData2Param()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $param_id   = $app->input->getInt('configadmin_param_id');
        $nome       = $app->input->get('nome',null, 'STRING');
        $tagchave   = $app->input->get('tagchave',null, 'STRING');
        $valor      = $app->input->get('valor',null, 'STRING');
        $descricao  = $app->input->get('descricao',null, 'STRING');
        $idtipoparam  = $app->input->get('idtipoparam',null, 'STRING');
        $idgrupoparam = $app->input->get('idgrupoparam',null, 'STRING');
        $idpermmodulo = $app->input->get('idpermmodulo',null, 'STRING');
        $enabled    = $app->input->get('enabled',null, 'STRING');

        if (!empty($param_id))      $data['configadmin_param_id'] = $param_id;
        if (!empty($nome))          $data['nome']                 = $nome;
        if (!empty($tagchave))      $data['tagchave']             = $tagchave;
        if ( !empty($valor) || $valor=='0' )   $data['valor']     = $valor;
        if (!empty($descricao))     $data['descricao']            = $descricao;
        if (!is_null($idtipoparam)) $data['idtipoparam']          = $idtipoparam;
        if (!is_null($idgrupoparam))  $data['idgrupoparam']       = $idgrupoparam;
        if (!is_null($idpermmodulo))  $data['idpermmodulo']       = $idpermmodulo;

        if ((string)$enabled!='') {
            $data['enabled']             = $enabled;
        }
        else {
            $data['enabled']             = '0';
        }

        return($data);
    }


    public function getPostedFormData2ParamTipo()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $paramtipo_id   = $app->input->getInt('configadmin_paramtipo_id');
        $nome       = $app->input->get('nome',null, 'STRING');
        $tagchave   = $app->input->get('tagchave',null, 'STRING');


        if (!empty($paramtipo_id))     $data['configadmin_paramtipo_id'] = $paramtipo_id;
        if (!empty($nome))          $data['nome']                 = $nome;
        if (!empty($tagchave))      $data['tagchave']             = $tagchave;


        return($data);
    }


    public function getPostedFormData2ParamGrupo()
    {   $app         = JFactory::getApplication();
        $data        = array();

        $paramgrupo_id   = $app->input->getInt('configadmin_paramgrupo_id');
        $nome       = $app->input->get('nome',null, 'STRING');
        $tagchave   = $app->input->get('tagchave',null, 'STRING');

        if (!empty($paramgrupo_id))     $data['configadmin_paramgrupo_id'] = $paramgrupo_id;
        if (!empty($nome))          $data['nome']                 = $nome;
        if (!empty($tagchave))      $data['tagchave']             = $tagchave;

        return($data);
    }


    public function getOnlyChangedPostedFormData2Plugin($data)
    {
        if ( (string)$data['nome'] === (string) $this->userConfigAdminPluginData->nome ) unset($data['nome']);
        if ( (string)$data['tagchave'] === (string) $this->userConfigAdminPluginData->tagchave ) unset($data['tagchave']);
        if ( (string)$data['enabled'] === (string) $this->userConfigAdminPluginData->enabled ) unset($data['enabled']);

        // Se no final só tivermos definido o contactus_id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('configadmin_plugin_id',$data)) ) unset($data['configadmin_plugin_id']);

        return($data);
    }


    public function getOnlyChangedPostedFormData2PluginLayout($data)
    {
        if ( (string)$data['nome'] === (string) $this->userConfigAdminPluginLayoutData->nome ) unset($data['nome']);
        if ( (string)$data['tagchave'] === (string) $this->userConfigAdminPluginLayoutData->tagchave ) unset($data['tagchave']);
        if ( (string)$data['path'] === (string) $this->userConfigAdminPluginLayoutData->path ) unset($data['path']);
        if ( (string)$data['descricao'] === (string) $this->userConfigAdminPluginLayoutData->descricao ) unset($data['descricao']);
        if ( (string)$data['idtipolayout'] === (string) $this->userConfigAdminPluginLayoutData->idtipolayout ) unset($data['idtipolayout']);

        // Se no final só tivermos definido os ids, então não devemos atualizar nada
        if( (sizeof($data)==2) && (array_key_exists('configadmin_pluginlayout_id',$data)) && (array_key_exists('configadmin_pluginlayout_id',$data)) ) {
            unset($data['configadmin_pluginlayout_id']);
            unset($data['configadmin_plugin_id']);
        }

        return($data);
    }


    public function getOnlyChangedPostedFormData2Param($data)
    {
        if ( (string)$data['nome'] === (string) $this->userConfigAdminParamData->nome )                 unset($data['nome']);
        if ( (string)$data['tagchave'] === (string) $this->userConfigAdminParamData->tagchave )         unset($data['tagchave']);
        if ( (string)$data['valor'] === (string) $this->userConfigAdminParamData->valor )               unset($data['valor']);
        if ( (string)$data['descricao'] === (string) $this->userConfigAdminParamData->descricao )       unset($data['descricao']);
        if ( (string)$data['idtipoparam'] === (string) $this->userConfigAdminParamData->idtipoparam )   unset($data['idtipoparam']);
        if ( (string)$data['idgrupoparam'] === (string) $this->userConfigAdminParamData->idgrupoparam ) unset($data['idgrupoparam']);
        if ( (string)$data['idpermmodulo'] === (string) $this->userConfigAdminParamData->idpermmodulo ) unset($data['idpermmodulo']);
        if ( (string)$data['enabled'] === (string) $this->userConfigAdminParamData->enabled )           unset($data['enabled']);

        // Se no final só tivermos definido o contactus_id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('configadmin_param_id',$data)) ) unset($data['configadmin_param_id']);

        return($data);
    }


    public function getOnlyChangedPostedFormData2ParamTipo($data)
    {
        if ( (string)$data['nome'] === (string) $this->userConfigAdminParamTipoData->nome )                 unset($data['nome']);
        if ( (string)$data['tagchave'] === (string) $this->userConfigAdminParamTipoData->tagchave )         unset($data['tagchave']);

        // Se no final só tivermos definido o contactus_id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('configadmin_paramtipo_id',$data)) ) unset($data['configadmin_paramtipo_id']);

        return($data);
    }


    public function getOnlyChangedPostedFormData2ParamGrupo($data)
    {
        if ( (string)$data['nome'] === (string) $this->userConfigAdminParamGrupoData->nome )                 unset($data['nome']);
        if ( (string)$data['tagchave'] === (string) $this->userConfigAdminParamGrupoData->tagchave )         unset($data['tagchave']);

        // Se no final só tivermos definido o contactus_id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('configadmin_paramgrupo_id',$data)) ) unset($data['configadmin_paramgrupo_id']);

        return($data);
    }


    public function validateBeforeSave2Plugin($data)
    {
        $msg = '';
        if (empty($data['nome']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['tagchave'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if ((string)$data['enabled']=='') $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ENABLED');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);
    }


    public function validateBeforeSave2PluginLayout($data)
    {
        $msg = '';
        if (empty($data['nome']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['tagchave'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if (empty($data['path'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_CONFIGADMIN_CMP_PATH');
        if (empty($data['idtipolayout'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TIPO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);
    }


    public function validateBeforeSave2Param($data)
    {
        $msg = '';
        if (empty($data['nome']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['tagchave'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');
        if (empty($data['valor']) && $data['valor']!='0' ) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_CONFIGADMIN_CMP_VALOR');

        if ((string)$data['enabled']=='') $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ENABLED');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);
    }


    public function validateBeforeSave2ParamTipo($data)
    {
        $msg = '';
        if (empty($data['nome']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['tagchave'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);
    }


    public function validateBeforeSave2ParamGrupo($data)
    {
        $msg = '';
        if (empty($data['nome']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME');
        if (empty($data['tagchave'])) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG');

        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);
    }


    public function save2Plugin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteConfigAdminHelper::updatePlugin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteConfigAdminHelper::cleanAllTmpUserState();
        return true;
    }


    public function save2PluginLayout($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteConfigAdminHelper::updatePluginLayout($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteConfigAdminHelper::cleanAllTmpUserState();
        return true;
    }


    public function save2Param($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteConfigAdminHelper::updateParam($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteConfigAdminHelper::cleanAllTmpUserState();
        return true;
    }


    public function save2ParamTipo($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteConfigAdminHelper::updateParamTipo($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteConfigAdminHelper::cleanAllTmpUserState();
        return true;
    }


    public function save2ParamGrupo($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteConfigAdminHelper::updateParamGrupo($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteConfigAdminHelper::cleanAllTmpUserState();
        return true;
    }


    public function create2Plugin($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteConfigAdminHelper::createPlugin($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteConfigAdminHelper::cleanAllTmpUserState();
        return true;
    }


    public function create2PluginLayout($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteConfigAdminHelper::createPluginLayout($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteConfigAdminHelper::cleanAllTmpUserState();
        return true;
    }


    public function create2Param($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteConfigAdminHelper::createParam($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteConfigAdminHelper::cleanAllTmpUserState();
        return true;
    }


    public function create2ParamTipo($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteConfigAdminHelper::createParamTipo($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteConfigAdminHelper::cleanAllTmpUserState();
        return true;
    }


    public function create2ParamGrupo($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteConfigAdminHelper::createParamGrupo($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteConfigAdminHelper::cleanAllTmpUserState();
        return true;
    }

}