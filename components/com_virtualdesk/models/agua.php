<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_virtualdesk
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteAguaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agua.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');
    JLoader::register('VirtualDeskSiteAguaBackofficeHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/Agua/virtualdesksite_agua.php');


use Joomla\Registry\Registry;

/**
 * AGUa model class for Users.
 *
 * @since  1.6
 */
class VirtualDeskModelAgua extends JModelForm
{
	/**
	 * @var		object	The user agua data.
	 * @since   1.6
	 */
	protected $data;
    protected $userSessionID;
    protected $userAguaData;
    public    $forcedLogout;

    //protected $ObjAguaFields;


	public function __construct($config = array())
	{
		$config = array_merge(
			array(
				'events_map' => array('validate' => 'user')
			), $config
		);

        $this->forcedLogout = false;

		parent::__construct($config);

	}

	/*  ------- REMOVE ... ??? */

	public function checkin($userId = null)
	{

		return true;
	}

	public function checkout($userId = null)
	{

		return true;
	}

	public function getData()
	{
        return false;
	}

    public function getDataNew()
    {
        return false;

    }

	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	/* --------------------- */

    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }

    public function setUserIdAndData4User($data)
    {

        return true;
    }

    public function setUserIdAndData4Manager($data)
    {

        return true;
    }

	protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	protected function populateState()
	{
        // Get the application object.
        //$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
        // Set the user id.
        $this->setState('user.id', $userId);
        // Load the parameters.
        $this->setState('params', $params);
	}

    public function getPostedFormData()
    {
        $app         = JFactory::getApplication();
        $data        = array();
        return($data);
    }

    public function getPostedFormData4User(){
        $app         = JFactory::getApplication();
        $data        = array();

        $pedido_id  = $app->input->getInt('pedido_id');
        $formId  = $app->input->getInt('formId');
        $userID  = $app->input->getInt('userID');
        if (!empty($pedido_id)) $data['pedido_id'] = $pedido_id;


        $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);
        $arCamposGet = array ();

        foreach($dataValues as $row){
            $arCamposGet[$row['fieldtag']] = $app->input->get($row['fieldtag'],null,'STRING');
        }

        $data = $arCamposGet;

        if (!empty($pedido_id)) $data['pedido_id'] = $pedido_id;
        if (!empty($formId)) $data['formId'] = $formId;
        if (!empty($userID)) $data['userID'] = $userID;

        return($data);
    }

    public function validateVerificacaoContadorBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createVerificacaoContador4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAguaBackofficeHelper::createVerificacaoContador4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateAlteracaoLocalContadorBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createAlteracaoLocalContador4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAguaBackofficeHelper::createAlteracaoLocalContador4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateSubstituicaoContadorBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createSubstituicaoContador4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAguaBackofficeHelper::createSubstituicaoContador4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateAlteracaoTitularContadorBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createAlteracaoTitularContador4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAguaBackofficeHelper::createAlteracaoTitularContador4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateCancelamentoContratoBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createCancelamentoContrato4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAguaBackofficeHelper::createCancelamentoContrato4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateContratoPrestacaoServicosBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createContratoPrestacaoServicos4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAguaBackofficeHelper::createContratoPrestacaoServicos4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateRestabelecimentoAguaBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createRestabelecimentoAgua4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAguaBackofficeHelper::createRestabelecimentoAgua4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateFaturaEletronicaBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createFaturaEletronica4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAguaBackofficeHelper::createFaturaEletronica4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateDebitoDiretoBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createDebitoDireto4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }



        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAguaBackofficeHelper::createDebitoDireto4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateTarifarioEspecialBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createTarifarioEspecial4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAguaBackofficeHelper::createTarifarioEspecial4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateRamalAbastecimentoAguaBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createRamalAbastecimentoAgua4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAguaBackofficeHelper::createRamalAbastecimentoAgua4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
        return true;
    }

    public function validateRamalAguasResiduaisBeforeSave4User($data){
        $msg = '';
        //if (empty($data['fieldtipopedido']))     $msg .= "\n" . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ACAOSOCIAL_TIPOPEDIDO');


        if($msg != "" ) {
            JFactory::getApplication()->enqueueMessage($msg, 'error' );
            return false;
        }
        return(true);

    }

    public function createRamalAguasResiduais4User($data){

        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdate = VirtualDeskSiteAguaBackofficeHelper::createRamalAguasResiduais4User($userSessionID, $userVDTable->id, $data);
        if ($resUpdate===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            return false;
        }

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
        return true;
    }

}
