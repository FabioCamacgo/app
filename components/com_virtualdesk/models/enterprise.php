<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskNIFCheckHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesknifcheck.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteEnterpriseHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_enterprise.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteEnterpriseFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_enterprisefields.php');
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldesk.php');

use Joomla\Registry\Registry;

/**
 * Profile model class for Entreprise. 
 * @since  1.6
 */
class VirtualDeskModelEnterprise extends JModelForm
{
	/**
	 * @var		object	The user profile data.
	 * @since   1.6
	 */
	protected $data;
    protected $userSessionID;
    protected $userEnterpriseData;
    public    $forcedLogout;
    protected $ObjEnterpriseFields;

	/**
	 * Constructor	    
	 * @param   array  $config  An array of configuration options (name, state, dbo, table_path, ignore_request).
	 * @since   3.2	    
	 * @throws  Exception
	 */
	public function __construct($config = array())
	{
//		$config = array_merge(
//			array(
//				'events_map' => array('validate' => 'user')
//			), $config
//		);
        $this->forcedLogout = false;

		parent::__construct($config);

        $this->ObjEnterpriseFields      =  new VirtualDeskSiteEnterpriseFieldsHelper();
        $this->ObjEnterpriseFields->getFields();
	}


	/**
	 * Method to get the Enterprise form data.
	 * The base form data is loaded and then an event is fired for users plugins to extend the data.
	 * @return  mixed  	Data object on success, false on failure.
	 * @since   1.6
	 */
	public function getData()
	{   $app = JFactory::getApplication();
		if ($this->data === null) {

            // check session +  joomla user
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($this->getState('user.id'));
            if ($userSessionID <= 0) {
                $this->setError('ERROR');
                $this->data = false;
            }

            $IdEnterprise = JFactory::getApplication()->input->getInt('enterprise_id');

            if( (int) $IdEnterprise > 0 )
            {
               $EnterpriseData =  VirtualDeskSiteEnterpriseHelper::getEnterpriseDetail($userSessionID, $IdEnterprise);

            }
            else
            {  $EnterpriseData      =  VirtualDeskSiteEnterpriseHelper::getEnterpriseList($userSessionID);
               if( sizeof($EnterpriseData) == 1)  $EnterpriseData = $EnterpriseData[0];
            }

            if ($EnterpriseData === false) {
                $this->setError('ERROR');
                $this->data = false;
            }

            $this->data = $EnterpriseData;

            // Override the base data with any data in the session.
            // Mas só se a task for para editar caso contrário não carrega o que está na sessão...!
            if( JFactory::getApplication()->input->get('layout') == 'edit')
            {
             if(!is_array($this->data)) {
                 $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.edit.enterprise.data', array());
                 foreach ($temp as $k => $v) {
                     // Além disso não faz sentido carregar os dados em array (FILE uploaded) para o profile do xcepto se for o useravatar pois é um file input
                     if (     (string)$k !== 'enterpriseavatar'
                           && (string)$k !== 'attachment'
                           && (string)$k !== 'attachmentDELETE'
                           && (string)$k !== 'attachmentUPDATE'
                     ) $this->data->{$k} = $v;
                 }
             }
            }
            else {
                // Limpa valores guardados anteriormente
                $app->setUserState('com_virtualdesk.edit.enterprise.data', null);
                $app->setUserState('com_virtualdesk.addnew.enterprise.data', null);
            }

        }
		return $this->data;
	}

    /**
     * Method to get the Enterprise form data.
     * The base form data is loaded and then an event is fired for users plugins to extend the data.
     * @return  mixed  	Data object on success, false on failure.
     * @since   1.6
     */
    public function getDataNew()
    {
        if ($this->data === null) {

            // Estava a dar um erro que não acontecia com o getData... forcei o populate state
            $this->populateState();

            // check session +  joomla user
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($this->getState('user.id'));
            if ($userSessionID <= 0) {
                $this->setError('ERROR');
                $this->data = false;
            }

            $this->data = new stdClass();
            $this->data->idusersession = $userSessionID;

            // Override the base data with any data in the session.
            // Mas só se a task for para editar caso contrário não carrega o que está na sessão...!
            if( JFactory::getApplication()->input->get('layout') == 'addnew')
            {
                $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew.enterprise.data', array());
                foreach ($temp as $k => $v) {
                    // Além disso não faz sentido carregar os dados em array (FILE uploaded) para o profile do xcepto se for o useravatar pois é um file input
                    if( (string)$k !== 'enterpriseavatar'
                         && (string)$k !== 'attachment'
                         && (string)$k !== 'attachmentDELETE'
                         && (string)$k !== 'attachmentUPDATE'
                    ) $this->data->$k = $v;
                }
            }

        }
        return $this->data;
    }


	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}


	/**
	 * Method to auto-populate the model state.
	 * Note. Calling getState in this method will result in recursion.
	 * @return  void
	 * @since   1.6
	 */
	protected function populateState()
	{
		// Get the application object.
		//$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
		// Set the user id.
		$this->setState('user.id', $userId);
		// Load the parameters.
		$this->setState('params', $params);
	}



    public function getPostedFormData()
    {  $app         = JFactory::getApplication();
       $data        = array();

       // Campos BASE
       $enterprise_id = $app->input->getInt('enterprise_id');
       $nipc       = $app->input->get('nipc',null, 'STRING');
       $name       = $app->input->get('name',null,'STRING');
       $email      = $app->input->get('email',null, 'STRING');
       $enterpriseavatar  = $app->input->files->get('enterpriseavatar');
       $firmname   = $app->input->get('firmname',null, 'STRING');

       if (!empty($enterprise_id)) $data['enterprise_id'] = $enterprise_id;
       if (!empty($nipc))  $data['nipc']         = $nipc;
       if (!empty($email)) $data['email']        = $email;
       if (!empty($name))  $data['name']         = $name;
       if (!empty($firmname))  $data['firmname'] = $firmname;


       // Campos EXTRA
       $address     = $app->input->get('address',null,'STRING');
       $postalcode  = $app->input->get('postalcode',null,'STRING');
       $phone1      = $app->input->get('phone1',null,'STRING');
       $phone2      = $app->input->get('phone2',null,'STRING');
       $website     = $app->input->get('website',null,'STRING');
       $facebook    = $app->input->get('facebook',null,'STRING');
       $freguesia   = $app->input->get('freguesia',null,'STRING');
       $localidade  = $app->input->get('localidade',null,'STRING');
       $areaact     = $app->input->get('areaact',null,'ARRAY');

       $attachment       = $app->input->files->get('attachment');  // lista de files posted
       $attachmentDELETE = $app->input->get('attachmentDELETE');   // lista de files a eliminar
       $attachmentUPDATE = array();
       if (array_key_exists('attachmentUPDATE', $_POST)) $attachmentUPDATE = $_POST['attachmentUPDATE'];


       // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo. Se os campos estiverem hidden não devem passar.
       $arCamposGet = array ();
       $arCamposGet['address'] = $address;
       $arCamposGet['postalcode'] = $postalcode;
       $arCamposGet['phone1'] = $phone1;
       $arCamposGet['phone2'] = $phone2;
       $arCamposGet['website'] = $website;
       $arCamposGet['facebook'] = $facebook;
       $arCamposGet['freguesia'] = $freguesia;
       $arCamposGet['localidade'] = $localidade;
       $arCamposGet['areaact'] = $areaact;


       $data = $this->ObjEnterpriseFields->checkCamposEmpty($arCamposGet, $data);

//        // No enterprise avatar (LOGO da Empresa) só vai considerar um upload da imagem se :
//        if (!empty($enterpriseavatar)) {
//            if( !empty($enterpriseavatar['name']) ) {
//                $data['enterpriseavatar'] = $enterpriseavatar;
//            }
//        }

        // Apesar de no método anterior se verificar o campo, esta função é mais específica para o o upload de ficheiros....
        $data = $this->checkFilesPostedData($data, $attachment, $attachmentDELETE, $attachmentUPDATE, $enterpriseavatar);

        return($data);
    }


    /*
    *Verifica se foram enviados ficheiros para criar ou se foram indicados ficheiros a eliminar
    */
    private function checkFilesPostedData($data, $attachment, $attachmentDELETE, $attachmentUPDATE, $enterpriseavatar)
    {
        // Apesar de no método anterior se verificar o campo, esta função é mais específica para o o upload de ficheiros...
        // Por exemplo para o caso da possibilidade de ficheiros múltiplos, vem um array e o método anterior não detecta como o array estando vazio
        // TODO no this->ObjXXXXXXXXXXFields->checkCamposEmpty  deveria ser verificado se o campo é o attachmente ou se eé um array... se este está vazio.. ???
        // TODO por enquanto fica aqui essa verificação.
        unset($data['attachment']);

        if (!empty($enterpriseavatar)) {
            if( !empty($enterpriseavatar['name']) ) {
                $data['enterpriseavatar'] = $enterpriseavatar;
            }
        }

        // só vai considerar um upload de ficheiros se :
        if (!empty($attachment)) {
            if( !empty($attachment['name']) ) {
                if((string)$attachment['name']!='')  $data['attachment'] = $attachment;
            }
            else if( !empty($attachment[0]['attachment']['name']) ) {
                if((string)$attachment[0]['attachment']['name'] != '') $data['attachment'] = $attachment;
            }
        }
        // Verifica array com ficheiros a eliminar
        if (!empty($attachmentDELETE)) {
            if( !empty($attachmentDELETE[0]) ) $data['attachmentDELETE'] = $attachmentDELETE;
        }

        // Verifica array com ficheiros a atualizar o nome/descrição
        if (!empty($attachmentUPDATE) && (is_array($attachmentUPDATE)) ) {
            if( sizeof($attachmentUPDATE)>0 ) $data['attachmentUPDATE'] = $attachmentUPDATE;
        }

        return($data);

    }



    public function getOnlyChangedPostedFormData($data)
    {
        if ( (string)$data['nipc'] === (string) $this->userEnterpriseData->nipc ) unset($data['nipc']);
        if ( (string)$data['email'] === (string) $this->userEnterpriseData->email ) unset($data['email']);
        if ( (string)$data['name'] === (string) $this->userEnterpriseData->name ) unset($data['name']);
        if ( (string)$data['firmname'] === (string) $this->userEnterpriseData->firmname ) unset($data['firmname']);

        if ( (string)$data['address'] === (string) $this->userEnterpriseData->address ) unset($data['address']);
        if ( (string)$data['postalcode'] === (string) $this->userEnterpriseData->postalcode ) unset($data['postalcode']);
        if ( (string)$data['phone1'] === (string) $this->userEnterpriseData->phone1 ) unset($data['phone1']);
        if ( (string)$data['phone2'] === (string) $this->userEnterpriseData->phone2 ) unset($data['phone2']);
        if ( (string)$data['website'] === (string) $this->userEnterpriseData->website ) unset($data['website']);
        if ( (string)$data['facebook'] === (string) $this->userEnterpriseData->facebook ) unset($data['facebook']);
        if ( (string)$data['freguesia'] === (string) $this->userEnterpriseData->freguesia ) unset($data['freguesia']);
        if ( (string)$data['localidade'] === (string) $this->userEnterpriseData->localidade ) unset($data['localidade']);

        $arDadosAreaAct = $data['areaact'];
        if(!is_array($arDadosAreaAct)) $arDadosAreaAct = array();
        asort($arDadosAreaAct);
        if ( implode('|',$arDadosAreaAct) === (string) $this->userEnterpriseData->areaact ) unset($data['areaact']);

        // no caso do avatar + attachment só se o array vier com um ficheiro é que vai ser considerado... verifica antes no getPostedFormData()
        // Se no final só tivermos definido o enterprise_id, então não devemos atualizar nada

        if( (sizeof($data)==1) && (array_key_exists('enterprise_id',$data)) ) unset($data['enterprise_id']);

        return($data);
    }



    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }



    public function setUserIdAndEnterpriseData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $userEnterpriseData = VirtualDeskSiteEnterpriseHelper::getEnterpriseDetailRawData ($userSessionID, $data['enterprise_id']);
        if (empty($userEnterpriseData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED'), 'error');
            $this->data = false;
        }

       $this->userSessionID   = $userSessionID;
       $this->userEnterpriseData = $userEnterpriseData;
       return true;
    }



    /**
     * Verifica se os dados a gravar estão correctos
     *
     * @param   array  $data  The form data.
     *
     * @return  mixed
     *
     * @since   1.6
     */
    public function validateBeforeSave($data)
    {

        // Verifica se o EMAIL no caso de estar a ser alterado se já existe noutro utilizador
        if(VirtualDeskSiteEnterpriseHelper::checkIfNIPCisDuplicate($this->userSessionID, $data['nipc'])) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_ENTERPRISE_ERROR_NIPCEXIST'), 'error');
            return(false);
        }

        if(!VirtualDeskSiteFileUploadHelper::checkUserAvatar($this->userSessionID, $data['enterpriseavatar']))
        {
            return(false);
        }

        if(!VirtualDeskSiteFileUploadHelper::checkUserAvatar($this->userSessionID, $data['attachment']))
        {
            return(false);
        }

        // Verifica se os campos obrigatórios estão preenchidos
        $msgRequiredEmptyFields =  $this->ObjEnterpriseFields->checkRequiredEmptyFields();
        if($msgRequiredEmptyFields != "" ) {
            JFactory::getApplication()->enqueueMessage($msgRequiredEmptyFields, 'error' );
            return false;
        }

        return(true);
    }




	/**
	 * Method to save the form data.
	 * @param   array  $data  The form data.
	 * @return  mixed  The user id on success, false on failure.
	 * @since   1.6
	 */
	public function save($data)
	{
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED'), 'error');
            $this->data = false;
        }



        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdateEnterprise = VirtualDeskSiteEnterpriseHelper::updateEnterprise($userSessionID, $userVDTable->id, $data);
        if ($resUpdateEnterprise===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED'), 'error');
            return false;
        }

        $app->setUserState('com_virtualdesk.edit.enterprise.data', null);
        $app->setUserState('com_virtualdesk.addnew.enterprise.data', null);
        return true;
	}



    /**
     * Method to save the form data.
     * @param   array  $data  The form data.
     * @return  mixed  The user id on success, false on failure.
     * @since   1.6
     */
    public function create($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdateEnterprise = VirtualDeskSiteEnterpriseHelper::createEnterprise($userSessionID, $userVDTable->id, $data);
        if ($resUpdateEnterprise===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED'), 'error');
            return false;
        }

        $app->setUserState('com_virtualdesk.edit.enterprise.data', null);
        $app->setUserState('com_virtualdesk.addnew.enterprise.data', null);
        return true;
    }

}