<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_virtualdesk
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
    JLoader::register('VirtualDeskSiteDiretorioServicosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos.php');


    use Joomla\Registry\Registry;

    /**
     * Diretorio Servicos model class for Users.
     *
     * @since  1.6
     */
    class VirtualDeskModelDiretorioServicos extends JModelForm
    {
        /**
         * @var		object	The user diretorio servicos data.
         * @since   1.6
         */
        protected $data;
        protected $userSessionID;
        protected $userDiretorioServicosData;
        public    $forcedLogout;



        public function __construct($config = array())
        {
            $config = array_merge(
                array(
                    'events_map' => array('validate' => 'user')
                ), $config
            );

            $this->forcedLogout = false;

            parent::__construct($config);

        }

        /*  ------- REMOVE ... ??? */

        public function checkin($userId = null)
        {

            return true;
        }

        public function checkout($userId = null)
        {

            return true;
        }

        public function getData()
        {
            return false;
        }

        public function getDataNew()
        {
            return false;

        }

        public function getForm($data = array(), $loadData = true)
        {
            // optamos por apenas carregar os dados, o form é feito na template
            return false;
        }

        /* --------------------- */


        public function setUserIdData()
        {
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
            if ($userSessionID <= 0) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
            }
            $this->userSessionID   = $userSessionID;
            return true;
        }


        public function setUserIdAndData4User($data)
        {

            return true;
        }


        public function setUserIdAndData4Manager($data)
        {
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
            if ($userSessionID <= 0) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
            }

            // Inicializa o array que compara depois se houve alteração de valores
            $loadedData = VirtualDeskSiteDiretorioServicosHelper::getDiretorioServicosDetail4Manager($data['empresa_id']);
            if (empty($loadedData)) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }

            $this->userSessionID = $userSessionID;
            $this->userDiretorioServicosData = $loadedData;
            return true;
        }


        protected function loadFormData()
        {
            // optamos por apenas carregar os dados, o form é feito na template
            return false;
        }


        protected function populateState()
        {
            // Get the application object.
            //$params = JFactory::getApplication()->getParams('com_virtualdesk');
            $params = JFactory::getApplication()->getParams();
            $userId = (int) JFactory::getUser()->get('id');
            // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
            // Set the user id.
            $this->setState('user.id', $userId);
            // Load the parameters.
            $this->setState('params', $params);
        }


        public function getPostedFormData()
        {
            $app         = JFactory::getApplication();
            $data        = array();
            return($data);
        }


        public function getPostedFormData4User()
        {   $app         = JFactory::getApplication();
            $data        = array();



            return($data);

        }


        public function getPostedFormData4Manager()
        {   $app         = JFactory::getApplication();
            $data        = array();

            $empresa_id  = $app->input->getInt('empresa_id');
            if (!empty($empresa_id)) $data['empresa_id'] = $empresa_id;



            $nomeEmpresa                = $app->input->get('nomeEmpresa',null,'STRING');
            $designacaoComercial        = $app->input->get('designacaoComercial',null,'STRING');
            $nipcEmpresa                = $app->input->get('nipcEmpresa',null,'STRING');
            $emailContacto              = $app->input->get('emailContacto',null,'STRING');
            $tipofirma                  = $app->input->get('tipofirma',null,'STRING');
            $descricao                  = $app->input->get('descricao',null,'RAW');
            $categoria                  = $app->input->get('categoria',null,'STRING');
            $subcategoria               = $app->input->get('subcategoria',null,'ARRAY');
            $concelho                   = $app->input->get('concelho',null,'STRING');
            $freguesia                  = $app->input->get('freguesia',null,'STRING');
            $morada                     = $app->input->get('morada',null,'STRING');
            $coordenadas                = $app->input->get('coordenadas',null,'STRING');
            $telefone                   = $app->input->get('telefone',null,'STRING');
            $website                    = $app->input->get('website',null,'STRING');
            $emailComercial             = $app->input->get('emailComercial',null,'STRING');
            $emailAdministrativo        = $app->input->get('emailAdministrativo',null,'STRING');
            $horaAbertura               = $app->input->get('horaAbertura',null,'STRING');
            $horaEncerramento           = $app->input->get('horaEncerramento',null,'STRING');
            $abertoAlmoco               = $app->input->get('abertoAlmoco',null,'STRING');
            $inicioAlmoco               = $app->input->get('inicioAlmoco',null,'STRING');
            $fimAlmoco                  = $app->input->get('fimAlmoco',null,'STRING');
            $abertoFimSemana            = $app->input->get('abertoFimSemana',null,'STRING');
            $horaAberturaSabado         = $app->input->get('horaAberturaSabado',null,'STRING');
            $horaEncerramentoSabado     = $app->input->get('horaEncerramentoSabado',null,'STRING');
            $horaAberturaDomingo        = $app->input->get('horaAberturaDomingo',null,'STRING');
            $horaEncerramentoDomingo    = $app->input->get('horaEncerramentoDomingo',null,'STRING');
            $facebook                   = $app->input->get('facebook',null,'STRING');
            $instagram                  = $app->input->get('instagram',null,'STRING');
            $youtube                    = $app->input->get('youtube',null,'STRING');
            $twitter                    = $app->input->get('twitter',null,'STRING');
            $checkboxHide1              = $app->input->get('checkboxHide1',null,'STRING');
            $checkboxHide2              = $app->input->get('checkboxHide2',null,'STRING');
            $checkboxHide3              = $app->input->get('checkboxHide3',null,'STRING');
            $checkboxHide4              = $app->input->get('checkboxHide4',null,'STRING');
            $checkboxHide5              = $app->input->get('checkboxHide5',null,'STRING');
            $checkboxHide6              = $app->input->get('checkboxHide6',null,'STRING');
            $checkboxHide7              = $app->input->get('checkboxHide7',null,'STRING');
            $checkboxHide8              = $app->input->get('checkboxHide8',null,'STRING');
            $checkboxHide9              = $app->input->get('checkboxHide9',null,'STRING');
            $checkboxHide10             = $app->input->get('checkboxHide10',null,'STRING');
            $checkboxHide11             = $app->input->get('checkboxHide11',null,'STRING');
            $checkboxHide12             = $app->input->get('checkboxHide12',null,'STRING');
            $checkboxoutro              = $app->input->get('checkboxoutro',null,'STRING');
            $IdiomaInput                = $app->input->get('IdiomaInput',null,'RAW');
            $premioHide1                = $app->input->get('premioHide1',null,'STRING');
            $premioHide2                = $app->input->get('premioHide2',null,'STRING');
            $informacoes                = $app->input->get('informacoes',null,'RAW');
            $VideoInput                 = $app->input->get('VideoInput',null,'RAW');




            // Verifica se foi alterados algum ficheiro
            $vdFileUpChangedCapa        = $app->input->get('vdFileUpChangedCapa',null,'STRING');
            $vdFileUpChangedLogo        = $app->input->get('vdFileUpChangedLogo',null,'STRING');
            $vdFileUpChangedGaleria     = $app->input->get('vdFileUpChangedGaleria',null,'STRING');

            // Campos EXTRA
            // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo
            // se os campos estiverem hidden não devem passar
            $arCamposGet = array ();
            $arCamposGet['nomeEmpresa']             = $nomeEmpresa;
            $arCamposGet['designacaoComercial']     = $designacaoComercial;
            $arCamposGet['nipcEmpresa']             = $nipcEmpresa;
            $arCamposGet['emailContacto']           = $emailContacto;
            $arCamposGet['tipofirma']               = $tipofirma;
            $arCamposGet['descricao']               = $descricao;
            $arCamposGet['categoria']               = $categoria;
            $arCamposGet['subcategoria']            = $subcategoria;
            $arCamposGet['concelho']                = $concelho;
            $arCamposGet['freguesia']               = $freguesia;
            $arCamposGet['morada']                  = $morada;
            $arCamposGet['coordenadas']             = $coordenadas;
            $arCamposGet['telefone']                = $telefone;
            $arCamposGet['website']                 = $website;
            $arCamposGet['emailComercial']          = $emailComercial;
            $arCamposGet['emailAdministrativo']     = $emailAdministrativo;
            $arCamposGet['horaAbertura']            = $horaAbertura;
            $arCamposGet['horaEncerramento']        = $horaEncerramento;
            $arCamposGet['abertoAlmoco']            = $abertoAlmoco;
            $arCamposGet['inicioAlmoco']            = $inicioAlmoco;
            $arCamposGet['fimAlmoco']               = $fimAlmoco;
            $arCamposGet['abertoFimSemana']         = $abertoFimSemana;
            $arCamposGet['horaAberturaSabado']      = $horaAberturaSabado;
            $arCamposGet['horaEncerramentoSabado']  = $horaEncerramentoSabado;
            $arCamposGet['horaAberturaDomingo']     = $horaAberturaDomingo;
            $arCamposGet['horaEncerramentoDomingo'] = $horaEncerramentoDomingo;
            $arCamposGet['facebook']                = $facebook;
            $arCamposGet['instagram']               = $instagram;
            $arCamposGet['youtube']                 = $youtube;
            $arCamposGet['twitter']                 = $twitter;
            $arCamposGet['checkboxHide1']           = $checkboxHide1;
            $arCamposGet['checkboxHide2']           = $checkboxHide2;
            $arCamposGet['checkboxHide3']           = $checkboxHide3;
            $arCamposGet['checkboxHide4']           = $checkboxHide4;
            $arCamposGet['checkboxHide5']           = $checkboxHide5;
            $arCamposGet['checkboxHide6']           = $checkboxHide6;
            $arCamposGet['checkboxHide7']           = $checkboxHide7;
            $arCamposGet['checkboxHide8']           = $checkboxHide8;
            $arCamposGet['checkboxHide9']           = $checkboxHide9;
            $arCamposGet['checkboxHide10']          = $checkboxHide10;
            $arCamposGet['checkboxHide11']          = $checkboxHide11;
            $arCamposGet['checkboxHide12']          = $checkboxHide12;
            $arCamposGet['checkboxoutro']           = $checkboxoutro;
            $arCamposGet['IdiomaInput']             = $IdiomaInput;
            $arCamposGet['premioHide1']             = $premioHide1;
            $arCamposGet['premioHide2']             = $premioHide2;
            $arCamposGet['informacoes']             = $informacoes;
            $arCamposGet['VideoInput']             = $VideoInput;

            $arCamposGet['vdFileUpChangedCapa']     = $vdFileUpChangedCapa;
            $arCamposGet['vdFileUpChangedLogo']     = $vdFileUpChangedLogo;
            $arCamposGet['vdFileUpChangedGaleria']  = $vdFileUpChangedGaleria;



            $data = $arCamposGet;

            if (!empty($empresa_id)) $data['empresa_id'] = $empresa_id;

            return($data);

        }


        public function getOnlyChangedPostedFormData4User($data)
         {

             return($data);
         }


        public function getOnlyChangedPostedFormData4Manager($data)
        {
            if ( (string)$data['nomeEmpresa']               === (string) $this->userDiretorioServicosData->nomeEmpresa )                unset($data['nomeEmpresa']);
            if ( (string)$data['designacaoComercial']       === (string) $this->userDiretorioServicosData->designacaoComercial )        unset($data['designacaoComercial']);
            if ( (string)$data['nipcEmpresa']               === (string) $this->userDiretorioServicosData->nipcEmpresa )                unset($data['nipcEmpresa']);
            if ( (string)$data['emailContacto']             === (string) $this->userDiretorioServicosData->emailContacto )              unset($data['emailContacto']);
            if ( (int)$data['tipofirma']                    === (int) $this->userDiretorioServicosData->tipofirma )                     unset($data['tipofirma']);
            if ( (string)$data['descricao']                 === (string) $this->userDiretorioServicosData->descricao )                  unset($data['descricao']);
            if ( (int)$data['categoria']                    === (int) $this->userDiretorioServicosData->categoria )                     unset($data['categoria']);
            if ( (string)$data['subcategoria']              === (string) $this->userDiretorioServicosData->subcategoria )               unset($data['subcategoria']);
            if ( (int)$data['concelho']                     === (int) $this->userDiretorioServicosData->concelho )                      unset($data['concelho']);
            if ( (int)$data['freguesia']                    === (int) $this->userDiretorioServicosData->freguesia )                     unset($data['freguesia']);
            if ( (string)$data['morada']                    === (string) $this->userDiretorioServicosData->morada )                     unset($data['morada']);
            if ( (string)$data['coordenadas']               === ((string)$this->userDiretorioServicosData->latitude.','.(string)$this->userDiretorioServicosData->longitude) )   unset($data['coordenadas']);
            if ( (string)$data['telefone']                  === (string) $this->userDiretorioServicosData->telefone )                   unset($data['telefone']);
            if ( (string)$data['website']                   === (string) $this->userDiretorioServicosData->website )                    unset($data['website']);
            if ( (string)$data['emailComercial']            === (string) $this->userDiretorioServicosData->emailComercial )             unset($data['emailComercial']);
            if ( (string)$data['emailAdministrativo']       === (string) $this->userDiretorioServicosData->emailAdministrativo )        unset($data['emailAdministrativo']);
            if ( (string)$data['horaAbertura']              === (string) $this->userDiretorioServicosData->horaAbertura )               unset($data['horaAbertura']);
            if ( (string)$data['horaEncerramento']          === (string) $this->userDiretorioServicosData->horaEncerramento )           unset($data['horaEncerramento']);
            if ( (int)$data['abertoAlmoco']                 === (int) $this->userDiretorioServicosData->abertoAlmoco )                  unset($data['abertoAlmoco']);
            if ( (string)$data['inicioAlmoco']              === (string) $this->userDiretorioServicosData->inicioAlmoco )               unset($data['inicioAlmoco']);
            if ( (string)$data['fimAlmoco']                 === (int) $this->userDiretorioServicosData->fimAlmoco )                     unset($data['fimAlmoco']);
            if ( (int)$data['abertoFimSemana']              === (int) $this->userDiretorioServicosData->abertoFimSemana )               unset($data['abertoFimSemana']);
            if ( (string)$data['horaAberturaSabado']        === (string) $this->userDiretorioServicosData->horaAberturaSabado )         unset($data['horaAberturaSabado']);
            if ( (string)$data['horaEncerramentoSabado']    === (string) $this->userDiretorioServicosData->horaEncerramentoSabado )     unset($data['horaEncerramentoSabado']);
            if ( (string)$data['horaAberturaDomingo']       === (string) $this->userDiretorioServicosData->horaAberturaDomingo )        unset($data['horaAberturaDomingo']);
            if ( (string)$data['horaEncerramentoDomingo']   === (string) $this->userDiretorioServicosData->horaEncerramentoDomingo )    unset($data['horaEncerramentoDomingo']);
            if ( (string)$data['facebook']                  === (string) $this->userDiretorioServicosData->facebook )                   unset($data['facebook']);
            if ( (string)$data['instagram']                 === (string) $this->userDiretorioServicosData->instagram )                  unset($data['instagram']);
            if ( (string)$data['youtube']                   === (string) $this->userDiretorioServicosData->youtube )                    unset($data['youtube']);
            if ( (string)$data['twitter']                   === (string) $this->userDiretorioServicosData->twitter )                    unset($data['twitter']);
            if ( (int)$data['checkboxHide1']                === (int) $this->userDiretorioServicosData->checkboxHide1 )                 unset($data['checkboxHide1']);
            if ( (int)$data['checkboxHide2']                === (int) $this->userDiretorioServicosData->checkboxHide2 )                 unset($data['checkboxHide2']);
            if ( (int)$data['checkboxHide3']                === (int) $this->userDiretorioServicosData->checkboxHide3 )                 unset($data['checkboxHide3']);
            if ( (int)$data['checkboxHide4']                === (int) $this->userDiretorioServicosData->checkboxHide4 )                 unset($data['checkboxHide4']);
            if ( (int)$data['checkboxHide5']                === (int) $this->userDiretorioServicosData->checkboxHide5 )                 unset($data['checkboxHide5']);
            if ( (int)$data['checkboxHide6']                === (int) $this->userDiretorioServicosData->checkboxHide6 )                 unset($data['checkboxHide6']);
            if ( (int)$data['checkboxHide7']                === (int) $this->userDiretorioServicosData->checkboxHide7 )                 unset($data['checkboxHide7']);
            if ( (int)$data['checkboxHide8']                === (int) $this->userDiretorioServicosData->checkboxHide8 )                 unset($data['checkboxHide8']);
            if ( (int)$data['checkboxHide9']                === (int) $this->userDiretorioServicosData->checkboxHide9 )                 unset($data['checkboxHide9']);
            if ( (int)$data['checkboxHide10']               === (int) $this->userDiretorioServicosData->checkboxHide10 )                unset($data['checkboxHide10']);
            if ( (int)$data['checkboxHide11']               === (int) $this->userDiretorioServicosData->checkboxHide11 )                unset($data['checkboxHide11']);
            if ( (int)$data['checkboxHide12']               === (int) $this->userDiretorioServicosData->checkboxHide12 )                unset($data['checkboxHide12']);
            if ( (int)$data['checkboxoutro']                === (int) $this->userDiretorioServicosData->checkboxoutro )                 unset($data['checkboxoutro']);
            if ( (string)$data['IdiomaInput']               === (string) $this->userDiretorioServicosData->IdiomaInput )                unset($data['IdiomaInput']);
            if ( (int)$data['premioHide1']                  === (int) $this->userDiretorioServicosData->premioHide1 )                   unset($data['premioHide1']);
            if ( (int)$data['premioHide2']                  === (int) $this->userDiretorioServicosData->premioHide2 )                   unset($data['premioHide2']);
            if ( (string)$data['informacoes']               === (string) $this->userDiretorioServicosData->informacoes )                unset($data['informacoes']);
            if ( (string)$data['VideoInput']                === (string) $this->userDiretorioServicosData->VideoInput )                 unset($data['VideoInput']);

            if ( (string)$data['vdFileUpChangedCapa']     == '0' )   unset($data['vdFileUpChangedCapa']);
            if ( (string)$data['vdFileUpChangedLogo']   == '0' )   unset($data['vdFileUpChangedLogo']);
            if ( (string)$data['vdFileUpChangedGaleria']  == '0' )   unset($data['vdFileUpChangedGaleria']);

            // Se no final só tivermos definido o id, então não devemos atualizar nada
            if( (sizeof($data)==1) && (array_key_exists('empresa_id',$data)) ) unset($data['empresa_id']);

            return($data);
        }


        public function validateBeforeSave4User($data)
        {

            return(true);

        }


        public function validateBeforeSave4Manager($data)
        {
            $msg = '';
            if (empty($data['nomeEmpresa']))  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_ERRO_NOMEEMPRESA');

            if($msg != "" ) {
                JFactory::getApplication()->enqueueMessage($msg, 'error' );
                return false;
            }

            return(true);

        }


        public function save4User($data)
        {


            return true;
        }


        public function save4Manager($data)
        {
            $app = JFactory::getApplication();
            // Check for request forgeries.
            JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

            // check session +  joomla user
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
            if ($userSessionID <= 0) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }

            // check se tem VD user associado and loads VD profile table and fields
            $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
            if ($userVDTable === false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }


            // executa faz o update (bind + save ) no VD user e no joomla user
            $resUpdate = VirtualDeskSiteDiretorioServicosHelper::update4Manager($userSessionID, $userVDTable->id, $data);
            if ($resUpdate===false)
            {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                return false;
            }

            // Flush the data from the session.
            VirtualDeskSiteDiretorioServicosHelper::cleanAllTmpUserState();
            return true;
        }


        public function create4User($data)
        {

            return true;
        }


        public function create4Manager($data)
        {
            $app = JFactory::getApplication();
            // Check for request forgeries.
            JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

            // check session +  joomla user
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
            if ($userSessionID <= 0) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }

            // check se tem VD user associado and loads VD profile table and fields
            $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
            if ($userVDTable === false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->data = false;
            }


            // executa faz o update (bind + save ) no VD user e no joomla user
            $resUpdate = VirtualDeskSiteDiretorioServicosHelper::create4Manager($userSessionID, $userVDTable->id, $data);
            if ($resUpdate===false)
            {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                return false;
            }

            // Flush the data from the session.
            VirtualDeskSiteDiretorioServicosHelper::cleanAllTmpUserState();
            return true;
        }


        public function delete4User($data)
        {

            return true;
        }


        public function delete4Manager($data)
        {

            return true;
        }


    }

?>