<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldeskuser.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteContactUsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactus.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteContactUsFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactusfields.php');
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virutaldesk.php');

use Joomla\Registry\Registry;

/**
 * Profile model class for Entreprise. 
 * @since  1.6
 */
class VirtualDeskModelContactUs extends JModelForm
{
	/**
	 * @var		object	The user profile data.
	 * @since   1.6
	 */
	protected $data;
    protected $userSessionID;
    protected $userContactUsData;
    public    $forcedLogout;
    protected $ObjContactUsFields;

	/**
	 * Constructor	    
	 * @param   array  $config  An array of configuration options (name, state, dbo, table_path, ignore_request).
	 * @since   3.2	    
	 * @throws  Exception
	 */
	public function __construct($config = array())
	{

        $this->forcedLogout = false;

		parent::__construct($config);

        $this->ObjContactUsFields      =  new VirtualDeskSiteContactUsFieldsHelper();
        $this->ObjContactUsFields->getFields();
	}


	/**
	 * Method to get the ContactUs form data.
	 * The base form data is loaded and then an event is fired for users plugins to extend the data.
	 * @return  mixed  	Data object on success, false on failure.
	 * @since   1.6
	 */
	public function getData()
	{   $app = JFactory::getApplication();
		if ($this->data === null) {

            // check session +  joomla user
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($this->getState('user.id'));
            if ($userSessionID <= 0) {
                $this->setError('ERROR');
                $this->data = false;
            }

            $IdContactUs         = JFactory::getApplication()->input->getInt('contactus_id');
            $IdCategoryContactUs = JFactory::getApplication()->input->getInt('contactus_idcategory');

            if( (int) $IdContactUs > 0 )
            {
               $ContactUsData =  VirtualDeskSiteContactUsHelper::getContactUsDetail($userSessionID, $IdContactUs);

               // CARREGA DADOS DA AREA ACTUAÇÃO - multiple choice
               // comparar dados serializados com explode/implode com dados carregados têm de ser iguais
               $ComparaSerializeAreaAct = VirtualDeskSiteContactUsFieldsHelper::getAreaActListSelected ($userSessionID, $ContactUsData->id);
               asort($ComparaSerializeAreaAct);
               $ComparaSerializeAreaAct = implode('|',$ComparaSerializeAreaAct);
               if($ComparaSerializeAreaAct!=  $ContactUsData->areaact )  return false;
               $FieldSerializaAreaAct   = explode('|',$ContactUsData->areaact);
               if (!empty($ContactUsData->areaact)) $ContactUsData->areaact = $FieldSerializaAreaAct;

            }
            else
            {  // Se tiver o Id de categoria é para filtar os dados por essa categoria
                if( (int) $IdCategoryContactUs > 0 ) {
                  $ContactUsData      =  VirtualDeskSiteContactUsHelper::getContactUsListByCat($userSessionID,$IdCategoryContactUs);
                }
                else {

                  // Passou a ser por DATATABLES ->
                 //   TODO MEHLORAR o dESEMPENHO
                   $ContactUsData      =  VirtualDeskSiteContactUsHelper::getContactUsList($userSessionID,false,5);
                  //  $ContactUsData = true;


                }
               if( sizeof($ContactUsData) == 1)  $ContactUsData = $ContactUsData[0];
            }

            if ($ContactUsData === false) {
                $this->setError('ERROR');
                $this->data = false;
            }

            $this->data = $ContactUsData;


            // Override the base data with any data in the session.
            // Mas só se a task for para editar caso contrário não carrega o que está na sessão...!
            if( JFactory::getApplication()->input->get('layout') == 'edit')
            {
             if(!is_array($this->data)) {
                 $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.edit.contactus.data', array());
                 foreach ($temp as $k => $v) {
                     // Além disso não faz sentido carregar os dados em array (FILE uploaded) para o profile do xcepto se for o useravatar pois é um file input
                     if (    (string)$k !== 'attachment'
                          && (string)$k !== 'attachmentDELETE'
                          && (string)$k !== 'attachmentUPDATE'
                     ) $this->data->{$k} = $v;
                 }
             }
            }
            else {
                // Limpa valores guardados anteriormente
               $app->setUserState('com_virtualdesk.edit.contactus.data', null);
               $app->setUserState('com_virtualdesk.addnew.contactus.data', null);
            }

        }
		return $this->data;
	}

    /**
     * Method to get the ContactUs form data.
     * The base form data is loaded and then an event is fired for users plugins to extend the data.
     * @return  mixed  	Data object on success, false on failure.
     * @since   1.6
     */
    public function getDataNew()
    {
        if ($this->data === null) {

            // Estava a dar um erro que não acontecia com o getData... forcei o populate state
            $this->populateState();

            // check session +  joomla user
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($this->getState('user.id'));
            if ($userSessionID <= 0) {
                $this->setError('ERROR');
                $this->data = false;
            }

            $this->data = new stdClass();
            $this->data->idusersession = $userSessionID;

            // Override the base data with any data in the session.
            // Mas só se a task for para editar caso contrário não carrega o que está na sessão...!
            if( JFactory::getApplication()->input->get('layout') == 'addnew')
            {
                $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew.contactus.data', array());
                foreach ($temp as $k => $v) {
                    // Além disso não faz sentido carregar os dados em array (FILE uploaded) para o profile do xcepto se for o useravatar pois é um file input
                    if(    (string)$k !== 'attachment'
                        && (string)$k !== 'attachmentDELETE'
                        && (string)$k !== 'attachmentUPDATE'
                    ) $this->data->$k = $v;
                }
            }
        }
        return $this->data;
    }


	public function getForm($data = array(), $loadData = true)
	{
		// optamos por apenas carregar os dados, o form é feito na template
		return false;
	}

	protected function loadFormData()
	{
	    // optamos por apenas carregar os dados, o form é feito na template
		return false;
	}


	/**
	 * Method to auto-populate the model state.
	 * Note. Calling getState in this method will result in recursion.
	 * @return  void
	 * @since   1.6
	 */
	protected function populateState()
	{
		// Get the application object.
		//$params = JFactory::getApplication()->getParams('com_virtualdesk');
        $params = JFactory::getApplication()->getParams();
        $userId = (int) JFactory::getUser()->get('id');
        // Não sei se será necessário este valor na sessão.... pois não estou a utilizar depois no save...? Nem consigo
		// Set the user id.
		$this->setState('user.id', $userId);
		// Load the parameters.
		$this->setState('params', $params);
	}



    public function getPostedFormData()
    {  $app         = JFactory::getApplication();
       $data        = array();

       // Campos BASE  (existem sempre)
       $contactus_id  = $app->input->getInt('contactus_id');
       $subject       = $app->input->get('subject',null, 'STRING');
       $description   = $app->input->get('description',null, 'STRING');
       $description2  = $app->input->get('description2',null, 'STRING');
       $description3  = $app->input->get('description3',null, 'STRING');
       $description4  = $app->input->get('description4',null, 'STRING');
       $areaact       = $app->input->get('areaact',null,'ARRAY');


       if (!empty($contactus_id)) $data['contactus_id'] = $contactus_id;
       if (!empty($subject))  $data['subject']          = $subject;
       if (!empty($description)) $data['description']   = $description;
       if (!empty($description2)) $data['description2']   = $description2;
       if (!empty($description3)) $data['description3']   = $description3;
       if (!empty($description4)) $data['description4']   = $description4;


       // Campos EXTRA
       $attachment       = $app->input->files->get('attachment');  // lista de files posted
       $attachmentDELETE = $app->input->get('attachmentDELETE');   // lista de files a eliminar
       // $attachmentUPDATE = $app->input->get('attachmentUPDATE');   // lista de files a alterar o nome/descrição
       $attachmentUPDATE = array();
       if (array_key_exists('attachmentUPDATE', $_POST)) $attachmentUPDATE = $_POST['attachmentUPDATE'];
       $type       = $app->input->get('type',null,'STRING');
       $category   = $app->input->get('category',null,'STRING');
       $obs        = $app->input->get('obs',null,'STRING');
       $subcategory = $app->input->get('subcategory',null,'STRING');
       $status      = $app->input->get('status',null,'STRING');
       $ctlanguage  = $app->input->get('ctlanguage',null,'STRING');
       $vdwebsitelist = $app->input->get('vdwebsitelist',null,'STRING');
       $vdmenumain    = $app->input->get('vdmenumain','','STRING');
       $vdmenusec     = $app->input->get('vdmenusec','','STRING');  // fica com '' e não com null para poder eliminar se não tiver nenhuma opção
       $vdmenusec2    = $app->input->get('vdmenusec2','','STRING');  // fica com '' e não com null para poder eliminar se não tiver nenhuma opção


       // EXCEPÇÂO para a CATEGORIA... se não tiver preenchida a categoria, verifica se recebeu no URL
       if(empty($category)) $category = $app->input->get('contactus_idcategory',null,'STRING');
       if(empty($type)) $type = $app->input->get('contactus_idtype',null,'STRING');


       // se os campos extra NÂO forem required deve permitir o nulo para limpar o campo. Se os campos estiverem hidden não devem passar.
       $arCamposGet = array ();
       $arCamposGet['type']        = $type;
       $arCamposGet['category']    = $category;
       $arCamposGet['subcategory'] = $subcategory;
       $arCamposGet['status']      = $status;
       $arCamposGet['ctlanguage']  = $ctlanguage;
       $arCamposGet['attachment']  = $attachment;
       $arCamposGet['obs']         = $obs;
       $arCamposGet['areaact']     = $areaact;
       $arCamposGet['vdwebsitelist'] = $vdwebsitelist;
       $arCamposGet['vdmenumain']    = $vdmenumain;
       $arCamposGet['vdmenusec']     = $vdmenusec;
       $arCamposGet['vdmenusec2']    = $vdmenusec2;

       // Verifica os campos se estão vazios (unset) ou se têm valor (set). Se são obrigatórios e estão vazios marca numa lista de erros
       $data = $this->ObjContactUsFields->checkCamposEmpty($arCamposGet, $data);

       // Apesar de no método anterior se verificar o campo, esta função é mais específica para o o upload de ficheiros....
       $data = $this->checkFilesPostedData($data, $attachment, $attachmentDELETE, $attachmentUPDATE);

       return($data);
    }


    /*
    *Verifica se foram enviados ficheiros para criar ou se foram indicados ficheiros a eliminar
    */
    private function checkFilesPostedData($data, $attachment, $attachmentDELETE, $attachmentUPDATE)
    {
        // Apesar de no método anterior se verificar o campo, esta função é mais específica para o o upload de ficheiros...
        // Por exemplo para o caso da possibilidade de ficheiros múltiplos, vem um array e o método anterior não detecta como o array estando vazio
        // TODO no this->ObjContactUsFields->checkCamposEmpty  deveria ser verificado se o campo é o attachmente ou se eé um array... se este está vazio.. ???
        // TODO por enquanto fica aqui essa verificação.
        unset($data['attachment']);

        // No contactus só vai considerar um upload de ficheiros se :
        if (!empty($attachment)) {
            if( !empty($attachment['name']) ) {
                if((string)$attachment['name']!='')  $data['attachment'] = $attachment;
            }
            elseif ( is_array($attachment)){
                foreach($attachment as $keyAt => $valAt) {
                    if (!empty($attachment[$keyAt]['attachment']['name'])) {
                        if ((string)$attachment[$keyAt]['attachment']['name'] != '') {
                            $data['attachment'] = $attachment;
                            break;
                        }
                    }
                }
            }

        }
        // Verifica array com ficheiros a eliminar
        if (!empty($attachmentDELETE)) {
            if( !empty($attachmentDELETE[0]) ) $data['attachmentDELETE'] = $attachmentDELETE;
        }

        // Verifica array com ficheiros a atualizar o nome/descrição
        if (!empty($attachmentUPDATE) && (is_array($attachmentUPDATE)) ) {
            if( sizeof($attachmentUPDATE)>0 ) $data['attachmentUPDATE'] = $attachmentUPDATE;
        }

        return($data);

    }


    public function getOnlyChangedPostedFormData($data)
    {
        if ( (string)$data['subject'] === (string) $this->userContactUsData->subject ) unset($data['subject']);
        if ( (string)$data['description'] === (string) $this->userContactUsData->description ) unset($data['description']);
        if ( (string)$data['description2'] === (string) $this->userContactUsData->description2 ) unset($data['description2']);
        if ( (string)$data['description3'] === (string) $this->userContactUsData->description3 ) unset($data['description3']);
        if ( (string)$data['description4'] === (string) $this->userContactUsData->description4 ) unset($data['description4']);
        if ( (string)$data['type'] === (string) $this->userContactUsData->type ) unset($data['type']);
        if ( (string)$data['category'] === (string) $this->userContactUsData->category ) unset($data['category']);
        if ( (string)$data['subcategory'] === (string) $this->userContactUsData->subcategory ) unset($data['subcategory']);
        if ( (string)$data['status'] === (string) $this->userContactUsData->status ) unset($data['status']);
        if ( (string)$data['ctlanguage'] === (string) $this->userContactUsData->ctlanguage ) unset($data['ctlanguage']);
        if ( (string)$data['obs'] === (string) $this->userContactUsData->obs ) unset($data['obs']);

        if ( (string)$data['vdwebsitelist'] === (string) $this->userContactUsData->vdwebsitelist ) unset($data['vdwebsitelist']);
        if ( (string)$data['vdmenumain'] === (string) $this->userContactUsData->vdmenumain ) unset($data['vdmenumain']);
        if ( (string)$data['vdmenusec'] === (string) $this->userContactUsData->vdmenumain ) unset($data['vdmenusec']);
        if ( (string)$data['vdmenusec2'] === (string) $this->userContactUsData->vdmenusec ) unset($data['vdmenusec2']);

        $arDadosAreaAct = $data['areaact'];
        if(!is_array($arDadosAreaAct)) $arDadosAreaAct = array();
        asort($arDadosAreaAct);
        if ( implode('|',$arDadosAreaAct) === (string) $this->userContactUsData->areaact ) unset($data['areaact']);

        // no caso do attachment(s) só se o array vier com um ficheiro é que vai ser considerado... verifica antes no getPostedFormData()
        // Se no final só tivermos definido o contactus_id, então não devemos atualizar nada
        if( (sizeof($data)==1) && (array_key_exists('contactus_id',$data)) ) unset($data['contactus_id']);

        return($data);
    }


    public function setUserIdData()
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }
        $this->userSessionID   = $userSessionID;
        return true;
    }



    public function setUserIdAndContactUsData($data)
    {
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_USERSESSIONNOT'), 'error');
        }

        // Inicializa o array que compara depois se houve alteração de valores
        $userContactUsData = VirtualDeskSiteContactUsHelper::getContactUsDetail ($userSessionID, $data['contactus_id']);
        if (empty($userContactUsData)) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED'), 'error');
            $this->data = false;
        }

       $this->userSessionID     = $userSessionID;
       $this->userContactUsData = $userContactUsData;
       return true;
    }



    /**
     * Verifica se os dados a gravar estão correctos
     *
     * @param   array  $data  The form data.
     *
     * @return  mixed
     *
     * @since   1.6
     */
    public function validateBeforeSave($data)
    {

        if(!VirtualDeskSiteFileUploadHelper::checkUserAvatar($this->userSessionID, $data['attachment']))
        {
            return(false);
        }

        // Verifica se os campos obrigatórios estão preenchidos
        $msgRequiredEmptyFields =  $this->ObjContactUsFields->checkRequiredEmptyFields();
        if($msgRequiredEmptyFields != "" ) {
            JFactory::getApplication()->enqueueMessage($msgRequiredEmptyFields, 'error' );
            return false;
        }



        return(true);
    }




	/**
	 * Method to save the form data.
	 * @param   array  $data  The form data.
	 * @return  mixed  The user id on success, false on failure.
	 * @since   1.6
	 */
	public function save($data)
	{
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED'), 'error');
            $this->data = false;
        }


        // Verifica se foi atingido o limite de ficheiros que podem ser anexados
        if(VirtualDeskSiteContactUsHelper::CheckFileLimitOfSaveRequest ($userSessionID,$data)===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_FILELIMITREACHED'), 'error' );
            return false;
        }


        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdateContactUs = VirtualDeskSiteContactUsHelper::updateContactUs($userSessionID, $userVDTable->id, $data);
        if ($resUpdateContactUs===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED'), 'error');
            return false;
        }

        $app->setUserState('com_virtualdesk.edit.contactus.data', null);
        $app->setUserState('com_virtualdesk.addnew.contactus.data', null);
        return true;
	}



    /**
     * Method to save the form data.
     * @param   array  $data  The form data.
     * @return  mixed  The user id on success, false on failure.
     * @since   1.6
     */
    public function create($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // Verifica se foi atingido o limite de ficheiros que podem ser anexados
        if(VirtualDeskSiteContactUsHelper::CheckFileLimitOfSaveRequest ($userSessionID,$data)===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_FILELIMITREACHED'), 'error' );
            return false;
        }

        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdateContactUs = VirtualDeskSiteContactUsHelper::createContactUs($userSessionID, $userVDTable->id, $data);
        if ($resUpdateContactUs===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED'), 'error');
            return false;
        }

        $app->setUserState('com_virtualdesk.edit.contactus.data', null);
        $app->setUserState('com_virtualdesk.addnew.contactus.data', null);
        return true;
    }


    /**
     * Method to delete the form data.
     * @param   array  $data  The form data.
     * @return  mixed  The user id on success, false on failure.
     * @since   1.6
     */
    public function delete($data)
    {
        $app = JFactory::getApplication();
        // Check for request forgeries.
        //JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // check session +  joomla user
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
        if ($userSessionID <= 0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // check se tem VD user associado and loads VD profile table and fields
        $userVDTable = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($userSessionID);
        if ($userVDTable === false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED'), 'error');
            $this->data = false;
        }

        // executa faz o update (bind + save ) no VD user e no joomla user
        $resUpdateContactUs = VirtualDeskSiteContactUsHelper::deleteContactUs($userSessionID, $userVDTable->id, $data);
        if ($resUpdateContactUs===false)
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED'), 'error');
            return false;
        }

        $app->setUserState('com_virtualdesk.edit.contactus.data', null);
        $app->setUserState('com_virtualdesk.addnew.contactus.data', null);
        return true;
    }


}